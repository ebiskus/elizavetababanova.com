<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Онлайн-семинар "7 шагов к Изобилию" - Запись</title>
		<meta property="og:title" content="Бесплатный онлайн-семинар. 7 шагов к изобилию" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://elizavetababanova.com/7izobilie/seminar/" />
		<meta name="description" content="От психологии процветания к конкретной практике достижения финансовых целей." />
		<meta property="og:image" content="http://elizavetababanova.com/7izobilie/seminar/image/ss2.jpg" />
		<link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="stylesheet.css?1" />
		<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
		<script type="text/javascript" async="" src="http://mc.yandex.ru/metrika/watch.js"></script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?110"></script>
		<script type="text/javascript">
			ua=navigator.userAgent;
			l='<LINK rel="stylesheet" type="text/css" href="';
			c='.css"> ';
			if (ua.indexOf('Opera')!=-1) document.write(l+'style'+c);
			if (ua.indexOf('Netscape6')!=-1) document.write(l+'style'+c);
			if (ua.indexOf('Firefox')!=-1) document.write(l+'style'+c);
		</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "e0e2b60f-9113-4824-ae05-b6d4bd6ecc3f", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	</head>
	<body>
		<article id="wrapper">
			<header>
				<div class="layout">
					<div class="wrap">
						<div id="logo"><img src="image/logo.png"></div>
						<div id="social_links">
							<ul>
								<li>
									<a href="http://vk.com/elizaveta.babanova" class="social-vkontakte"></a>
								</li>
								<li>
									<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995" class="social-facebook"></a>
								</li>
								<li>
									<a href="http://twitter.com/liza_babanova" class="social-twitter"></a>
								</li>
								<li>
									<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide" class="social-youtube"></a>
								</li>
								<!-- <li>
									<a href="#" class="social-google"></a>
								</li> -->
							</ul>
						</div>
					</div>
				</div>	
			</header>
			<div id="content">
				<section id="main_banner">
					<div class="layout">
						<div class="wrap">
							<nav id="menu">
								<ul>
									<li><a href="http://elizavetababanova.com/7izobilie">7 ШАГОВ К ИЗОБИЛИЮ</a></li>
									<li class="active"><a>ЗАПИСЬ СЕМИНАРА</a></li>
									<li><a href="http://elizavetababanova.com/7izobilie/trening_dengi/">ТРЕНИНГ "ДЕНЬГИ"</a></li>
								</ul>
							</nav>
							<div class="heading">
								<div class="text-1">БЕСПЛАТНЫЙ ОНЛАЙН-СЕМИНАР</div>
								<div class="text-2">7 ШАГОВ К ИЗОБИЛИЮ</div>
								<div class="text-3">От психологии процветания к конкретной практике<br/>
									достижения финансовых целей.</div>
							</div>
							<div id="header_computer">
								<!--<div class="button-play"></div>-->
								<div class="video">
									<iframe width="664" height="400" src="//www.youtube.com/embed/j2flCKWP_Os" style="border:0;outline:0" frameborder="0" scrolling="no"></iframe>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="block_registration">
					<div class="layout">
						<div class="wrap">
							<div class="block">
								<div class="description">Продолжить<br/><span class="small">движение к Изобилию</span></div>
								<a class="button" target="_blank" href="http://elizavetababanova.com/7izobilie/trening_dengi/">
									<span class="text">ЗАРЕГИСТРИРОВАТЬСЯ НА ТРЕНИНГ</span>
								</a>
							</div>
						</div>
					</div>
				</section>
				<section id="block_social_likes">
					<div class="layout">
						<div class="wrap">
							<div class="social-buttons">
								<span class='st_facebook_hcount' displayText='Facebook'></span>
								<span class='st_twitter_hcount' displayText='Tweet'></span>
								<span class='st_vkontakte_hcount' displayText='Vkontakte'></span>
							</div>
							<div class="social-arrow"></div>
						</div>
					</div>
				</section>
				<section id="block_social_comments">
					<div class="layout">
						<div class="wrap">
							<div class="column left">
								<!-- Put this script tag to the <head> of your page -->
								<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>

								<script type="text/javascript">
								  VK.init({apiId: 4540063, onlyWidgets: true});
								</script>

								<!-- Put this div tag to the place, where the Comments block will be -->
								<div id="vk_comments"></div>
								<script type="text/javascript">
								VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
								</script>
							</div>
							<div class="column right">
							<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1500181473555503&version=v2.0";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>
								<div class="fb-comments" data-href="http://elizavetababanova.com/7izobilie/seminar/" data-width="400" data-numposts="2" data-colorscheme="light"></div>
							</div>
						</div>
					</div>
				</section>
			</div>
			
		</article>
		<footer>
			<div class="layout">
				<div class="wrap">
					<div id="copyright">ИП Бабанова Елизавета Дмитриевна 2014 Все права защищены</div>
					<div id="footer_menu">
						<a href="">Политика конфиденциальности</a> | 
						<a href="">Правила пользования</a> | 
						<a href="">Отказ от ответственности</a>
					</div>
					<div id="website"><a href="">www.elizavetababanova.com</a></div>
				</div>
			</div>
		</footer>

 
		<!--LiveInternet counter-->
		<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
		//--></script><a href="http://www.liveinternet.ru/click" target="_blank"><img src="//counter.yadro.ru/hit?t44.10;r;s1440*900*24;uhttp%3A//elizavetababanova.com/7izobilie/;0.8865126178134233" alt="" title="LiveInternet" border="0" width="0" height="0"></a><style type="text/css">.fancybox-margin{margin-right:15px;}</style>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
		(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
		try {
		w.yaCounter18188044 = new Ya.Metrika({id:18188044,
		webvisor:true,
		clickmap:true,
		trackLinks:true,
		accurateTrackBounce:true});
		} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript>&lt;div&gt;&lt;img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /&gt;&lt;/div&gt;</noscript>
		<!-- /Yandex.Metrika counter -->

		<!--Google-->
		<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
		})();
		</script>
		<!--/google-->
	</body>
</html>