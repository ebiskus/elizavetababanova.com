<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Тренинг "Деньги: взаимная любовь и неминуемый успех"</title>
		<meta property="og:title" content="Тренинг 'Деньги: взаимная любовь и неминуемый успех'" />
		<meta name="viewport" content="width=940">
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://elizavetababanova.com/7izobilie/trening_dengi/" />
		<meta name="description" content="Регистрация на Тренинг и Апгрейд Финансов" />
		<meta property="og:image" content="http://elizavetababanova.com/7izobilie/trening_dengi/image/social-img.png" />
		<link href='http://fonts.googleapis.com/css?family=Muli:300,400,300italic,400italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Handlee' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="stylesheet.css?1" />
		<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
		<script type="text/javascript" async="" src="http://mc.yandex.ru/metrika/watch.js"></script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?110"></script>
		<script type="text/javascript">
			ua=navigator.userAgent;
			l='<LINK rel="stylesheet" type="text/css" href="';
			c='.css"> ';
			if (ua.indexOf('Opera')!=-1) document.write(l+'style'+c);
			if (ua.indexOf('Netscape6')!=-1) document.write(l+'style'+c);
			if (ua.indexOf('Firefox')!=-1) document.write(l+'style'+c);
		</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "e0e2b60f-9113-4824-ae05-b6d4bd6ecc3f", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	</head>
	<body>
		<article id="wrapper">
			<header>
				<div class="layout">
					<div class="wrap">
						<div id="logo"><a href="http://elizavetababanova.com"><img src="image/logo.png" alt="logo" /></a></div>
						<div id="social_links">
							<ul>
								<li>
									<a href="http://vk.com/elizaveta.babanova" class="social-vkontakte"></a>
								</li>
								<li>
									<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995" class="social-facebook"></a>
								</li>
								<li>
									<a href="http://twitter.com/liza_babanova" class="social-twitter"></a>
								</li>
								<li>
									<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide" class="social-youtube"></a>
								</li>
								<!-- <li>
									<a href="#" class="social-google"></a>
								</li> -->
							</ul>
						</div>
					</div>
				</div>	
			</header>
			<div id="content">
				<section id="main_banner">
					<div class="layout">
						<div class="wrap">
							<div class="block-traning">
								<div class="block-traning-top">
									<span class="text-name-trening">Тренинг</span>
								</div>
								<div class="block-traning-center">
									<span class="money">ДЕНЬГИ</span>
									<span class="mutual-love">ВЗАИМНАЯ ЛЮБОВЬ И НЕМИНУЕМЫЙ УСПЕХ</span>
								</div>
								<div class="block-traning-bottom">
									<div class="start">
										Старт 20 сентября<br />20:00 Мск
									</div>
									<a href="#three_options" class="need-this-training">
										<span>Я уже точно знаю,<br />что мне нужен этот тренинг! </span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="block_these_problems">
					<div class="layout">
						<div class="wrap">
							<div class="one-of-those-problems">
								<span>Есть ли у вас хотя бы одна из этих проблем?</span>
								<ul>
									<li><img src="image/block-these-problems-image1.png" alt="block-these-problems-image1" /><span>Нехватка денег</span></li>
									<li><img src="image/block-these-problems-image2.png" alt="block-these-problems-image2" /><span>Долги</span></li>
									<li><img src="image/block-these-problems-image3.png" alt="block-these-problems-image3" /><span>Отсутствие ощущения стабильности</span></li>
									<li><img src="image/block-these-problems-image4.png" alt="block-these-problems-image4" /><span>Незнание того, как выгодно монетизировать свои способности и навыки</span></li>
									<li><img src="image/block-these-problems-image5.png" alt="block-these-problems-image5" /><span>Неумение распределять свой бюджет</span></li>
									<li><img src="image/block-these-problems-image6.png" alt="block-these-problems-image6" /><span>Неспособность умножать свой доход</span></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section id="capable-of-more-image">
					<div class="layout">
						<div class="wrap">
							<div class="capable-of-more">
								<span class="text-capable-of-more">Но при этом вы понимаете, что<br />способны на большее?</span>
								<span class="text-lets-deal">Давайте разбираться, в чем дело.</span>
							</div>
							<div class="nonphysical-physical">
								Мы живем одновременно в двух мирах –<br />нефизическом и физическом.
							</div>
						</div>
					</div>
				</section>
				<section id="capable-of-more-image2">
					<div class="layout">
						<div class="wrap">
							<div class="nonphysical-world">
								<span>Нефизический мир</span>
								<p>– место наших фантазий, устремлений, представлений о себе и о мире. Если вы можете что-либо себе представить, к примеру, прекрасный ужин, новое платье, то вы уже знаете, что такое нефизический мир.</p>
							</div>
							<div class="physical-world">
								<span>Физический мир</span>
								<p>– то, что мы видим, ощущаем, осязаем. Это та объективная реальность, в которой мы живем.</p>
							</div>
						</div>
					</div>
				</section>		
				<section style="margin-top: 67px;">
					<div class="layout">
						<div class="wrap">
							<span class="text-gap-between-nonphysically">Чем больше разрыв между нефизическим и физическим миром, тем больше мы страдаем.</span>
						</div>
					</div>
				</section>
				<section>
					<div class="gap-between-nonphysically-image">
						<div class="layout">
							<div class="wrap">
								<div class="gap-between-nonphysically-information">
									<p>К примеру, если мы видим себя успешными, реализованными, богатыми и признанными, но при этом мы экономим на всем и не можем заработать столько, сколько нам надо для счастья, мы всегда будем испытывать дискомфорт и недоумевать, почему так?</p>
									<span>«Я же достойна или достоен одного, а получаю совершенно другое».</span>
								</div>
							</div>
						</div>
					</div>	
				</section>
				<section>
					<div class="gap-between-nonphysically-image2">
						<div class="layout">
							<div class="wrap">
								<div class="gap-between-nonphysically-information2">
									<p>Чтобы мощно улучшить нашу физическую реальность – улучшить условия жизни, заработать больше денег – нам необходимо совершить главную работу:</p>
									<span>установление верных концепций нефизического мира.</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section>
					<div id="impossible-build-house">
						<div class="layout">
							<div class="wrap">
								<div id="impossible-head-thinking"></div>
								<div class="text-impossible-build-house">
									<p>Невозможно построить дом, сначала его не спроектировав.</p>
									<p>Невозможно сшить платье без создания модели и понимания кроя.</p>
									<p>Невозможно выиграть футбольный матч «по наитию». Лучшие пасы всегда заранее спланированы, а потом отточены на многочасовых тренировках.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="finance">
						<div class="layout">
							<div class="wrap">
								<div class="top">
									<div class="heading">А КАК ВЫ СТРОИТЕ СВОЮ <br/>ФИНАНСОВУЮ СФЕРУ?</div>
									<div class="text">По наитию? <br/>
										Или по грамотному плану, реализованному <br/>
										вследствие понимания законов денег?</div>
								</div>
								<div class="bottom">
									<div class="img"></div>
									<div class="description">
										<p class="small">Следующий важный вопрос: </p>
										<p>Насколько сильно вы улучшили <br/>
											свою сферу финансов за последние 5 лет?</p>
										<p>Стали ли вы больше зарабатывать <br/>
											и качественнее жить за последний год?</p>
									</div>
								</div>
							</div>
						</div>
				</section>
				<section id="results">
					<div class="layout">
						<div class="wrap">
							<div class="description">
								<p>Как ни крути, <br/>
									если вы не наблюдаете <br/>
									улучшений в том темпе, <br/>
									в котором вы их себе представляете, <br/>
									значит,  что-то не так <br/>
									с одной из составляющих <br/>
									(а может быть, сразу со всеми):
								</p>
								<ul>
									<li>С вашими мечтами</li>
									<li>C принятием финансовых законов</li>
									<li>С вашими постоянными действиями</li>
								</ul>
								<div class="img">
									<div class="results-circle"></div>
								</div>
							</div>
							
							<div class="text-bottom">Если одна из этих составляющих <br/>отсутствует или искажена, <br/>результата не будет. </div>
						</div>
					</div>
				</section>
				<section id="target">
					<div class="layout">
						<div class="wrap">
							<div class="top-border"></div>
							<div class="description">
								<span class="heading">Цель этого тренинга - </span><br/>
								<span class="text">помочь вам выстроить всю цепочку так, чтобы ваши мечты <br/>неизбежно приводили вас к желаемой реальности. </span>
							</div>
						</div>
					</div>
				</section>
				<section id="training_tasks">
					<div class="layout">
						<div class="wrap">
							<div class="heading">Какие еще задачи у этого тренинга:</div>
							<div class="row">
								<div class="column">
									<div class="img"><img src="image/training_tasks_img_1.png" /></div>
									<div class="text">Замена искаженных убеждений на правильные, основанные на знании природных законов денег</div>
								</div>
								<div class="column">
									<div class="img"><img src="image/training_tasks_img_2.png" /></div>
									<div class="text">Тестирование работающих убеждений и внедрение их в свою жизнь</div>
								</div>
								<div class="column">
									<div class="img"><img src="image/training_tasks_img_3.png" /></div>
									<div class="text">Развитие способностей генерировать доходы</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="false_thoughts">
					<div class="layout">
						<div class="wrap">
							<div class="description">
								<p>Когда у вас уйдут ложные убеждения относительно законов, по которым генерируются деньги, в вас высвободится огромная энергия.</p>
								<p>Когда у вас появится четкий вектор развития, который будет отражать ваши намерения и поступки, вы начнете двигаться 
с гигантской скоростью.</p>
							</div>

						</div>
					</div>
				</section>
				<section id="this_is_important">
					<div class="layout">
						<div class="wrap">
							<div class="block-top">
								<div class="heading">И еще, это тоже важно:</div>
								<div class="text"><span class="small">Для закрепления новых убеждений нужно окружение.</span><br/> 
									Что делает маугли – маугли, а человека – человеком? <br/> 
									Eго общество. <br/> 
									Его окружение. <br/> 
									Его среда «обитания». </div>
							</div>
							<div class="block-bottom row">
								<div class="column">Если вы пытаетесь пробиться через стеклянный потолок доходов, и вас окружают люди с низкими доходами и ложными установками относительно денег, вам будет очень сложно перейти на новый уровень в одиночку.</div>
								<div class="column">Но если вы попадаете в среду, где изобилие – норма, где люди получают кайф от работы и от плодов этой работы (денег), то вы, вольно или невольно, рано или поздно, станете подобным этой среде.</div>
							</div>
							<!-- <div class="gaurantee">
								<div class="pad"><div class="icon"></div></div><br/>
								<div class="text"></div>
							</div> -->
						</div>
					</div>
				</section>
				<section id="ask_yourself">
					<div class="layout">
						<div class="wrap">
							<div class="row pencil">
								<div class="img"></div>
								<div class="description">
									<div class="heading">Еще раз спросите себя:</div>
									<p class="text-1">Уверены ли вы, что знаете законы денег?</p>
									<p class="text-2">Уверены ли вы, что ваши сегодняшние действия <br/>
										приведут вас к желаемым результатам?</p>
									<p class="text-3">Уверены ли вы, что ваше окружение будет <br/>
										поддерживать вас в ваших здоровых амбициях?</p>
									<p class="text-4">
										<strong>Сможете ли вы все это сделать в одиночку?</strong><br/>
											Если да, то вперед! Мы вам не нужны.
									</p>
								</div>
							</div>
							<div class="row hands">
								
								<div class="description">
									<p>Ну а если вы чувствуете, что вам нужна помощь хотя бы на одном из этих этапов, то </p>
									<div class="heading">этот тренинг – для вас.</div>
								</div>
								<div class="img"></div>
							</div>
							<div class="row man">
								<div class="img"></div>
								<div class="description">
									<div class="heading">А еще он для вас, если:</div>
									<ul>
										<li>Вам просто необходимо выйти на качественно новый уровень жизни в сфере финансов </li>
										<li>Вы открыты к изменению ложных убеждений касательно денег</li>
										<li>Вы осознали, что текущая система ценностей не приведет вас к изобилию</li>
										<li>Вы поняли, что за изменениями убеждений обязательно пойдут правильные действия, и вы готовы их предпринимать</li>
									<ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="who_does_not">
					<div class="layout">
						<div class="wrap">
							<div class="description">
								<div class="heading">Кому этот тренинг не будет полезен:</div>
								<ul>
									<li>Бесплодным мечтателям и фантазерам</li>
									<li>Искателям халявы </li>
									<li>Тем, кто думает, что деньги – зло</li>
									<li>Людям с ложным убеждением, что большие деньги можно заработать только нечестным путем</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section id="training_program">
					<div class="layout">
						<div class="wrap">
							<div class="block-title">ПРОГРАММА ТРЕНИНГА</div>
							<div class="block-woman">
								<div class="description">
									<div class="top">
										<div class="heading">Тренинг</div>
										<div class="hr"></div>
										<div class="info">Деньги: взаимная любовь и неминуемый успех</div>
										<div class="date">
											<div class="icon-calendar"></div>
											<div class="text">20 сентября, в 12:00 MCК</div>
											<div class="sub-text">В прямом эфире</div>
										</div>
									</div>
									<div class="bottom">
										<p>Вы получаете 3-x часовой вебинар с:</p>
										<ul>
											<li>Главными установками, которые сформируют новую парадигму мышления (помните, что все изначально формируется в нефизическом мире).</li>
											<li>Практическими заданиями, которые вы сделаете прямо на занятии под руководством вашего тренера.</li>
											<li>Инструментами для дальнейшей самостоятельной практики.</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="block-plus">
								<div class="icon-plus">+</div>
							</div>
							<div class="block-man">
								<div class="description">
									<div class="top">
										<div class="heading">Апгрейд Финансов</div>
										<div class="hr"></div>
										<div class="info-1">21 день для развития ваших способностей генерировать доходы</div>
										<div class="info-2">21 день интенсивной работы по закреплению результатов тренинга и мощного прорыва в сфере финансов</div>
										<div class="date">
											<div class="icon-slider"></div>
											<div class="icon-calendar"></div>
											<div class="text">24 сентября – <br/>
												14 октября</div>
	
										</div>
									</div>
									<div class="bottom">
										<p>Правильное мышление без воплощения в физической реальности – ничто. Для внедрения всех инструментов нам нужна дисциплина.</p>
										<p>Апгрейд Финансов содержит заключительную составляющую формулы генерирования доходов:  он даст вам платформу для развития самодисциплины. </p>									
									</div>
								</div>
							</div>
							<div class="bottom-title">3 варианта участия</div>
						</div>
					</div>
				</section>
				<section id="three_options">
					<div class="layout">
						<div class="wrap">
							
							<!-- <div class="row options">
								<div class="option-1">
									<span class="number">1</span>
									<span class="text">вариант</span>
								</div>
								<div class="option-2">
									<span class="number">2</span>
									<span class="text">вариант</span>
								</div>
								<div class="option-3">
									<div class="best-choice">Очень выгодное предложение!</div>
									<span class="number">3</span>
									<span class="text">вариант = Тренинг + Апгрейд</span>
								</div>
							</div> -->
							<div class="row block-payment">
								<div class="row row-1">
									<div class="option-1">
										<div class="options_name">
											<span class="number">1</span>
											<span class="text">вариант</span>
										</div>
										<div class="variants_bg_1">
											<div class="heading">Тренинг</div>
											<div class="text">«Деньги: <br/>взаимная любовь <br/>и неминуемый успех»</div>
											<div class="price">$150</div>
											<div class="buttons">
												<a class="registrate" href="http://elizavetababanova.com/registratsiya-na-trening-dengi">Зарегистрироваться</a>
												<a class="pay-now" href="http://elizavetababanova.com/instruktsii-po-oplate">оплатить сейчас</a>
											</div>
										</div>
									</div>
									<div class="option-2">
										<div class="options_name">
											<span class="number">2</span>
											<span class="text">вариант</span>
										</div>
										<div class="variants_bg_2">
											<div class="heading">Апгрейд Финансов</div>
											<div class="text">«21 день <br/>для развития ваших способностей <br/>генерировать доходы»</div>
											<div class="price">$150</div>
											<div class="buttons">
												<a class="registrate" href="http://elizavetababanova.com/registratsiya-na-trening-dengi">Зарегистрироваться</a>
												<a class="pay-now" href="http://elizavetababanova.com/instruktsii-po-oplate">оплатить сейчас</a>
											</div>
										</div>
									</div>
									<div class="option-3">
										<div class="options_name">
											<div class="best-choice">Очень выгодное предложение!</div>
											<span class="number">3</span>
											<span class="text">вариант = Тренинг + Апгрейд</span>
										</div>
										<div class="variants_bg_3">
											<div class="heading-1">Тренинг
												<span class="smanll-icon-plus"></span>
											</div>
											<div class="heading-2">Апгрейд Финансов
												<span class="smanll-icon-plus"></span>
											</div>
											<div class="heading-3">Бонусы</div>
											<div class="price">
												<div class="icon-discount"></div>
												<div class="old-price">$300
													<span class="icon-line-through"></span>
												</div>
												<div class="new-price"></div>
											</div>
											<div class="buttons">
												<a class="registrate" href="http://elizavetababanova.com/registratsiya-na-trening-dengi">Зарегистрироваться</a>
												<a class="pay-now" href="http://elizavetababanova.com/instruktsii-po-oplate">оплатить сейчас</a>
											</div>
										</div>
									</div>
								</div>
								<div class="row row-2">
									<div class="left">
										<div>
											<div class="heading">12 вебинаров <br/>«Вопрос-Ответ»</div>
											<div class="sub-text">в прямом эфире <br/>с Елизаветой Бабановой</div>
										</div>
										<div>
											<div class="heading">Как увеличить доход <br/>бизнеса на 30%,<br/> используя 3 стратегии</div>
											<div class="sub-text">модуль из программы Business Class</div>
										</div>
										<div>
											<div class="heading">Как создать жизнь<br/> мирового уровня</div>
											<div class="sub-text">модуль из программы <br/>Феномен Мастерства</div>
										</div>
									</div>
									<div class="right">
										<span class="price">$300<span class="icon-line-through"></span></span>
									</div>
								</div>



							</div>
						</div>
					</div>
				</section>
				<section id="woman_computer">
					<div class="layout">
						<div class="wrap">

						</div>
					</div>
				</section>
				<section id="online_trainers">
					<div class="layout">
						<div class="wrap">
							<div class="heading">Онлайн-тренинг ведет</div>
							<div class="row">
								<div class="column column-1">
									<img class="img" src="image/online_trainers_liza.png" />
								</div>
								<div class="column column-2">
									<div class="row">
										<div class="icon-brackets-down"></div>
									</div>
									<div class="row top">
										<div class="column">
											<div class="title">Елизавета <br/>Бабанова</div>
											<div class="text">автор продвигающих тренингов, основатель Системы Сферического Развития</div>
										</div>
										<div class="column">
											<div class="text">Технологии, которые вы получите на тренинге, помогут вам планировать ваши финансы, управлять ими, приумножать их. Как тысячи моих клиентов, вы ощутите ни с чем не сравнимое счастье достижения поставленных целей.</div>
										</div>
										<div class="clear"></div>
									</div>
									<div class="row">
										<div class="icon-brackets-up"></div>
									</div>
									<div class="row bottom">
										<div class="column">
											<p>Елизавета родилась в маленьком городе в 300 км от Москвы, в семье инженера и преподавателя. Благодаря своему интересу к обучению и энтузиазму, в 15 лет она выиграла грант и уехала на учебу в США. Окончила два вуза, в России и Америке. </p>
											<p>Сделала  успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США. Инвестиционный капитал фирмы, в которой она работала, вырос за это время с 140 миллионов до 4 миллиардов долларов.</p>
											<p>В какой-то момент пришло разочарование в выборе карьеры, и Елизавета ушла с "теплого места".</p>
										</div>
										<div class="column">
											<p>Грамотное отношение к личным финансам позволило ей несколько лет заниматься поиском своего профессионального призвания, без необходимости трудоустройства.</p>
 											<p>Следующий этап  – выход в онлайн-пространство и создание двух успешных бизнес-проектов в области образования и саморазвития. Один – с партнером, второй – самостоятельно.</p>
											<p>Сегодня материалы Елизаветы читает более 100 тысяч подписчиков в 134 странах мира. Параллельно со своим основным профессиональным проектом  Елизавета развивает новый Интернет-ресурс, преподает онлайн и оффлайн, пишет кандидатскую диссертацию по психологии. Она ведет здоровый образ жизни и очень много путешествует.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="faces">
					<div class="layout">
						<div class="wrap">

						</div>
					</div>
				</section>
				<section id="reviews">
					<div class="layout">
						<div class="wrap">
							<div class="block-1">
								<div class="heading">Отзывы</div>
								<div class="sub-heading">выпускников</div>

								<div class="row">
									<div class="column review-1">
										<div class="img"><img src="image/reviews_1_diana.png" /></div>
										<div class="name">ДИАНА СУПРУН</div>
										<div class="hr"></div>
										<div class="description">Увеличилась мотивация и снова возник интерес к ежедневному выполнению задач, которые раньше откладывались. Хочется двигаться дальше, не останавливаясь.</div>
									</div>
									<div class="column review-2">
										<div class="img"><img src="image/reviews_2_oksana.png" /></div>
										<div class="name">ОКСАНА КАЗАКОВА</div>
										<div class="hr"></div>
										<div class="description">Буквально за несколько недель я многое изменила в себе и в жизни своей семьи: вынырнула из депрессии, кардинально поменяла режим, по-другому увидела и стала чувствовать жизнь вокруг.</div>
									</div>
									<div class="column review-3">
										<div class="img"><img src="image/reviews_3_ivan.png" /></div>
										<div class="name">ИВАН ПЕТРЕНКО</div>
										<div class="hr"></div>
										<div class="description">Стало больше энергии, появилось в жизни больше позитива, я стал спокойнее, увереннее в себе, в своих силах. Теперь я знаю, что в жизни можно добиться всего, чего пожелаешь.</div>
									</div>
								</div>
								<div class="row">
									<div class="column review-4">
										<div class="img"><img src="image/reviews_4_irina.png" /></div>
										<div class="name">ИРИНА КОЗЕЕВА</div>
										<div class="hr"></div>
										<div class="description">Ваш курс завёл механизм: запустил не только намерение, он запустил конкретные действия! От теории – к практике – к Успеху!</div>
									</div>
									<div class="column review-5">
										<div class="img"><img src="image/reviews_5_alexsander.png" /></div>
										<div class="name">АЛЕКСАНДР ЛИЧИК</div>
										<div class="hr"></div>
										<div class="description">Искренне радуюсь и благодарю вас, Елизавета, за то, что делитесь своими конструктивными мыслями и идеями. В ваших материалах есть душа. Спасибо за то, что даете людям возможность расти и развиваться, учиться и постигать.</div>
									</div>
									<div class="column review-6">
										<div class="img"><img src="image/reviews_6_irina_kasuk.png" /></div>
										<div class="name">ИРИНА КАСЮК</div>
										<div class="hr"></div>
										<div class="description">С вашей поддержкой через несколько дней открываю совершенно новый магазин. Я сама до конца не могу поверить, что я это сделала!</div>
									</div>
								</div>
							</div>
							<div class="block-2">
								<div class="heading">Отзывы</div>
								<div class="sub-heading">коллег и единомышленников</div>
								<div class="row">
									<div class="column review-7">
										<div class="img"><img src="image/reviews_7_evgeniy.png" /></div>
										<div class="name">Евгений Сжёнов</div>
										<div class="position">Президент Международной Ассоциации <br/>Непрерывного Образования</div>
										<div class="description">Проекты Елизаветы соответствуют мировым стандартам качества.</div>
									</div>
									<div class="column review-8">
										<div class="img"><img src="image/reviews_8_criss.png" /></div>
										<div class="name">КРИС КЕЙ</div>
										<div class="position">Американский предприниматель, <br/>владелец трех успешных бизнесов <br/>с оборотом в десятки миллионов долларов </div>
										<div class="description">Елизавета является одним из ведущих мировых экспертов в сфере личностного развития. Я нахожу ее инструменты, мудрость и проницательность бесценным источником для моего личностного и профессионального роста. Я очень рекомендую ее программы всем, кто хочет вывести свою жизнь на следующий уровень. Ее система работает.</div>
									</div>
									<div class="column review-9">
										<div class="img"><img src="image/reviews_9_valentin.png" /></div>
										<div class="name">ВАЛЕНТИН ПРЕОБРАЖЕНСКИЙ</div>
										<div class="position">Управляющий партнер, <br/>Avega Capital</div>
										<div class="description">Елизавета пробуждает любовь к жизни и дремлющие мечты, заряжает энергией и отвагой кардинально изменить свою жизнь. Советы Елизаветы конкретны и полны мудрости, во многом, благодаря ее опыту работы в финансовом центре мира. Елизавета знакомит с замечательными людьми с разных континентов, с легкостью превращая их в друзей и партнеров.</div>
									</div>
								</div>
								<div class="row">
									<div class="column review-10">
										<div class="img"><img src="image/reviews_10_liliya.png" /></div>
										<div class="name">Лилия Осия</div>
										<div class="position">Чемпионка мира <br/>по модельному фитнесу</div>
										<div class="description">Вы мечтаете прийти к успеху без перегорания и без надрыва? Вы мечтаете реализоваться в жизни, придя к гармонии внутреннего и внешнего мира? Как вы считаете, возможно ли сделать взлет за месяц? Да!  </div>
									</div>
									<div class="column review-11">
										<div class="img"><img src="image/reviews_11_dmitriy.png" /></div>
										<div class="name">Дмитрий Шаменков</div>
										<div class="position">Ученый, <br/>автор Системы Управления Здоровья</div>
										<div class="description">Это реально здорово! Елизавета открывает женщинам мир, показывает своим результатом, своей жизнью возможности, вдохновляя их.</div>
									</div>
									<div class="column review-12">
										<div class="img"><img src="image/reviews_12_irina_hlimonenko.png" /></div>
										<div class="name">Ирина Хлимоненко</div>
										<div class="position">Соучредитель Mindvalley Russian</div>
										<div class="description">После консультаций с Елизаветой мы стали работать гораздо четче, появилась ясность, куда мы хотим прийти, и мы начали продвигаться к намеченным целям гораздо быстрее. Очень благодарны Лизе за проделанную работу и надеемся на продуктивное сотрудничество в будущем!</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="vk_widget">
					<div class="layout">
						<div class="wrap">
							<!-- VK Widget -->
							<div style="height: 260px;
							width: 940px;
							background: none;
							" id="vk_groups"></div>
							<script type="text/javascript">
							VK.Widgets.Group("vk_groups", {mode: 0, width: "940", height: "260", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 44023512);
							</script>
						</div>
					</div>
				</section>
			</div>
		</article>
		<footer>
			<div class="layout">
				<div class="wrap">
					<div id="copyright">ИП Бабанова Елизавета Дмитриевна 2014 Все права защищены</div>
					<div id="footer_menu">
						<a href="">Политика конфиденциальности</a> | 
						<a href="">Правила пользования</a> | 
						<a href="">Отказ от ответственности</a>
					</div>
					<div id="website"><a href="">www.elizavetababanova.com</a></div>
				</div>
			</div>
		</footer>



 
		<!--LiveInternet counter-->
		<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
		//--></script><a href="http://www.liveinternet.ru/click" target="_blank"><img src="//counter.yadro.ru/hit?t44.10;r;s1440*900*24;uhttp%3A//elizavetababanova.com/7izobilie/;0.8865126178134233" alt="" title="LiveInternet" border="0" width="0" height="0"></a><style type="text/css">.fancybox-margin{margin-right:15px;}</style>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
		(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
		try {
		w.yaCounter18188044 = new Ya.Metrika({id:18188044,
		webvisor:true,
		clickmap:true,
		trackLinks:true,
		accurateTrackBounce:true});
		} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript>&lt;div&gt;&lt;img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /&gt;&lt;/div&gt;</noscript>
		<!-- /Yandex.Metrika counter -->

		<!--Google-->
		<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
		})();
		</script>
		<!--/google-->
	</body>
</html>