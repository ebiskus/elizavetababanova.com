<?php
/*
Plugin Name: MaxSite Ushki
Version: 2.2 (23-09-2006)
Plugin URI: http://maxsite.org/
Description: Плагин позволяющий вешать ушки (любой html-код). См. закладку Настройки 
Author: MAX
Author URI: http://maxsite.org/
*/

/*
Пример использования ушек:

<?php if (function_exists('show_ushka')){show_ushka('reklama', '<br>');} ?>
 - будут отображены все ушки с именем "reklama" и разделителем между ними "<br>".

или 
 
<?php 
if (function_exists('show_ushka'))
	show_ushka('reklama', '<br>', true, 'Добавьте ушку reklama');
?>

Если ушка начинается с "php_", то это исполняемый PHP-код.

*/


### заголовок плагина на его странице 
$max_ushki_PLUGIN_PAGE_TITLE = 'Настройка ушек';

### пункт меню плагина
$max_ushki_PLUGIN_MENU_TITLE = 'Ушки';

### имя таблицы БД
$ushki_db_table = 'max_ushki';

##############################################3
### stop edit!!!
##############################################

global $table_prefix;
$ushki_db_table = $table_prefix . $ushki_db_table;


function max_ushki_add_admin_menu(){
	global $max_ushki_PLUGIN_PAGE_TITLE, $max_ushki_PLUGIN_MENU_TITLE;
	// страница в меню ОПЦИИ число = уровень доступа участников
	add_options_page($max_ushki_PLUGIN_PAGE_TITLE, $max_ushki_PLUGIN_MENU_TITLE , 8, __FILE__, 'max_ushki_admin_menu');
}

function max_ushki_admin_install(){
	global $wpdb, $ushki_db_table;
	
	if ( function_exists('maxsite_collate') ) $collate = maxsite_collate();
		else $collate = '';
			
	$query = "SHOW TABLES LIKE '" . $ushki_db_table . "'";
	if(!$result = $wpdb->get_var($query)) { 
		$createT = "CREATE TABLE $ushki_db_table (
		ushki_id bigint(20) NOT NULL auto_increment,
		ushki_name varchar(255) NOT NULL default '',
		ushki_code text,
		PRIMARY KEY (ushki_id)) {$collate}";
		$wpdb->query($createT);
	}
}


function max_ushki_admin_menu(){
	max_ushki_start_html();
	
	switch (max_ushki_checkPostData())  { 
		case 1: //   my_update
		max_ushki_update();
		break; // case 1
	
		case 2: //  my_add добавить новую ушку
		max_ushki_add_new();
		break; // case 2
	
		case 3: //  my_delete
		max_ushki_delete();
		break; // case 3

	}; // switch
	
	max_ushki_show_form(); 
	max_ushki_end_html(); 
}

function max_ushki_start_html() {
	global $max_ushki_PLUGIN_PAGE_TITLE;

echo <<<EOF
<div class="wrap">
<h2>{$max_ushki_PLUGIN_PAGE_TITLE}</h2>
<p>Ушки - это любой html-блок, который вы можете определить для своего блога. Вначале вам нужно определить 
название ушки и его код. После этого вызов любой ушки можно осуществить с помощью функции:
<br><b>show_ushka('name', 'разделитель', echo, 'не найдено')</b>, где:</p>
<ul>
<li>name - имя ушки;</li> 
<li>разделитель - html-код между ушками;</li>
<li>echo: true - выводить текст, false - только вернуть результат (return);</li>
<li>не найдено - значение, если ушка не найдена.</li>
</ul>
<p>Если ушка начинается с "php_", то это исполняемый PHP-код. Код необходимо оформлять по всем правилам PHP.</p>
EOF;
}

function max_ushki_end_html() {
	echo '<p style="text-align: right;">&copy; <a href="http://maxsite.org/">MaxSite.org</a>, 2006-2008</p>';
	echo '</div>';
}

function max_ushki_checkPostData() {
	global $_POST;
	switch ($_POST['my_action']) {
		case 'my_update':
		return 1;
		break;

		case 'my_add':
		return 2;
		break;

		case 'my_delete':
		return 3;
		break;
		
	default: return 0;
	}
}

function max_ushki_show_form(){
	global $_SERVER, $wpdb, $ushki_db_table;

echo <<<EOF
<form action="http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}" method="post">	
EOF;

	$result = $wpdb->get_results("SELECT * FROM " . $ushki_db_table);
	
	if($result) {
		foreach($result as $row) {
		$ushki_id   = $row->ushki_id;
		$ushki_name = $row->ushki_name;
		$ushki_code = htmlspecialchars($row->ushki_code, ENT_QUOTES);

		echo "<p><input type=\"radio\" name=\"id\" value=\"{$ushki_id}\" />
		<input type=\"text\" size=\"80\" value=\"{$ushki_name}\" name=\"name{$ushki_id}\" />
		<br /><textarea style=\"font: 11pt 'Courier New'; margin-left: 20px; height: 200px; width: 97%;\" name=\"code{$ushki_id}\">{$ushki_code}</textarea></p><hr />";		
		}
	} else  {
		echo "<p><b>У вас не создано ни одной ушки.</b></p>";
		}
	
echo <<<EOF
<p>Действие: <select name="my_action">
<option selected value="my_update">Сохранить изменения выбранной ушки
<option value="my_add">Создать новую ушку
<option value="my_delete">Удалить выбранную ушку
</select>
<input type="submit" name="submit" value="Пошел!" />
</form>
EOF;
}

function max_ushki_add_new(){
	global $wpdb, $ushki_db_table;
	$result = $wpdb->get_results("INSERT INTO $ushki_db_table(ushki_name, ushki_code) VALUES ( \"Ushka\", \"Code\")");
}

function max_ushki_delete(){
	global $wpdb, $ushki_db_table, $_POST;
	
	if (($_POST['my_action'] == 'my_delete')&&
	   ($_POST['id'] != '')) 
	{
	$result = $wpdb->get_results("DELETE FROM $ushki_db_table WHERE ushki_id=\"{$_POST['id']}\" LIMIT 1");
	}
}

function max_ushki_update(){
	global $wpdb, $ushki_db_table, $_POST;
	if (($_POST['my_action'] == 'my_update')&&
	   ($_POST['id'] != '')) 
	{
	$ushki_name = $_POST["name{$_POST["id"]}"];
	$ushki_code = $_POST["code{$_POST["id"]}"];
	$result = $wpdb->get_results("UPDATE $ushki_db_table SET ushki_name=\"{$ushki_name}\", ushki_code=\"{$ushki_code}\" WHERE ushki_id=\"{$_POST['id']}\"");
	}
}

function show_ushka($name = '', $razd = '', $echo = true, $t_f_not = false) {
	global $wpdb, $ushki_db_table;
	
	if ($name != '') {
		$result = $wpdb->get_results("SELECT ushki_code FROM $ushki_db_table WHERE ushki_name=\"{$name}\"");
		$ushki_out = '';
		if ($result) {
		  foreach($result as $row) {
			if ($ushki_out != '') {
				$ushki_out = $ushki_out . $razd. $row->ushki_code;
				}
				else {
				$ushki_out = $row->ushki_code;
				}
		  }
		}
		else  // нет такой ушки
			if ( $t_f_not ) $ushki_out = $t_f_not;

		$ushki_out = str_replace('&lt;','<', $ushki_out);
		$ushki_out = str_replace('&gt;','>', $ushki_out);
		$ushki_out = str_replace('&quot;','\"', $ushki_out);
		$ushki_out = str_replace('&#039','\'', $ushki_out);
		$ushki_out = str_replace('&amp;','&', $ushki_out);
		
		// если ушка начинается с php_ то это выполняемый php-код
		if (stristr($name, 'php_') == false) {
			if ( $echo ) echo $ushki_out;
				else return $ushki_out;
		}
		else return eval( '?>' . stripslashes( $ushki_out ) . '<?php ');
	}
}



if (isset($_GET['activate']) && $_GET['activate'] == 'true') {
	add_action('init', 'max_ushki_admin_install'); 
	} 
add_action('admin_menu', 'max_ushki_add_admin_menu');
?>