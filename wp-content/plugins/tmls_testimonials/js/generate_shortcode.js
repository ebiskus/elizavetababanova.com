(function($){
$(window).load(function(){
	
	// inputs
	
	tmls_category = $('#tmls_category');
	tmls_layout = $('#tmls_layout');
	tmls_style = $('#tmls_style');
	tmls_image_size = $('#tmls_image_size');
	tmls_image_radius = $('#tmls_image_radius');
	tmls_font_style = $('#tmls_font_style');
	tmls_text_font_family = $('#tmls_text_font_family');
	tmls_text_font_color = $('#tmls_text_font_color');
	tmls_text_font_size = $('#tmls_text_font_size');
	tmls_name_font_family = $('#tmls_name_font_family');
	tmls_name_font_color = $('#tmls_name_font_color');
	tmls_neme_font_size = $('#tmls_neme_font_size');
	tmls_neme_font_weight = $('#tmls_neme_font_weight');
	tmls_position_font_family = $('#tmls_position_font_family');
	tmls_position_font_color = $('#tmls_position_font_color');
	tmls_position_font_size = $('#tmls_position_font_size');
	tmls_orderByList = $('#tmls_orderByList');
	tmls_orderList = $('#tmls_orderList');
	tmls_numberInput = $('#tmls_numberInput');
	tmls_auto_play = $('#tmls_auto_play');
	tmls_pause_on_hover = $('#tmls_pause_on_hover');
	tmls_next_prev_visibility = $('#tmls_next_prev_visibility');
	tmls_next_prev_radius = $('#tmls_next_prev_radius');
	tmls_next_prev_position = $('#tmls_next_prev_position');
	tmls_scroll_duration = $('#tmls_scroll_duration');
	tmls_pause_duration = $('#tmls_pause_duration');
	tmls_border_style = $('#tmls_border_style');
	tmls_border_color = $('#tmls_border_color');
	tmls_columns_number = $('#tmls_columns_number');
	
	
	tmls_controls = $('input,select');
	tmls_buttons = $('.button-primary');
	
	// containers
	
	tmls_div_shortcode = $('#tmls_div_shortcode');
	
	tmls_gene_short_preview = $('#tmls_gene_short_preview');
	
	// options rows
	slider_options = $('.slider_options');
	border_options = $('.border_options');
	grid_options = $('.grid_options');
	image_options = $('.image_options');
	font_options = $('.font_options');
	border_color = $('.border_color');

	
	generate_shortcode();
	
	
	tmls_controls.change(function(){
		generate_shortcode();
	});
	
	tmls_buttons.click(function(){
		generate_shortcode();
	});
	
	
	// Layout
	tmls_layout.change(function(){
		
		if( tmls_layout.val() == 'tmls_slider' ) {
			slider_options.slideDown('slow');
			border_options.slideUp('slow');
			grid_options.slideUp('slow');
			
		}
		else if( tmls_layout.val() == 'tmls_grid' ) {
			slider_options.slideUp('slow');
			border_options.slideDown('slow');
			grid_options.slideDown('slow');
			
			if( tmls_border_style.val() == 'no_border' ) {
				border_color.slideUp('slow');
			}
			else {
				border_color.slideDown('slow');
			}
		}
		else if( tmls_layout.val() == 'tmls_list' ) {
			slider_options.slideUp('slow');
			border_options.slideDown('slow');
			grid_options.slideUp('slow');
			
			if( tmls_border_style.val() == 'no_border' ) {
				border_color.slideUp('slow');
			}
			else {
				border_color.slideDown('slow');
			}
		}
		
	});
	
	// Image Size
	tmls_image_size.change(function(){
	
		if( tmls_image_size.val() == 'no_image' ) {
			image_options.slideUp('slow');
		}
		else {
			image_options.slideDown('slow');
		}
		
	});
	
	// Font Style
	tmls_font_style.change(function(){
	
		if( tmls_font_style.val() == 'default' ) {
			font_options.slideUp('slow');
		}
		else {
			font_options.slideDown('slow');
		}
		
	});
	
	// Border Style
	tmls_border_style.change(function(){
	
		if( tmls_border_style.val() == 'no_border' ) {
			border_color.slideUp('slow');
		}
		else {
			border_color.slideDown('slow');
		}
		
	});
	
	

});

function generate_shortcode() {
	
	var postarray = {};
	var shortcode='[tmls ';
	
	postarray['category'] = tmls_category.val();
	shortcode+='category="'+tmls_category.val()+'" ';
	
	postarray['layout'] = tmls_layout.val();
	shortcode+='layout="'+tmls_layout.val()+'" ';
	
	postarray['style'] = tmls_style.val();
	shortcode+='style="'+tmls_style.val()+'" ';
	
	postarray['image_size'] = tmls_image_size.val();
	shortcode+='image_size="'+tmls_image_size.val()+'" ';
	
	if( tmls_image_size.val()!='no_image' ) {
		postarray['image_radius'] = tmls_image_radius.val();
		shortcode+='image_radius="'+tmls_image_radius.val()+'" ';
	}
	
	if( tmls_font_style.val()!='default' ) {
		postarray['text_font_family'] = tmls_text_font_family.val();
		shortcode+='text_font_family="'+tmls_text_font_family.val()+'" ';
		
		postarray['text_font_color'] = tmls_text_font_color.val();
		shortcode+='text_font_color="'+tmls_text_font_color.val()+'" ';
		
		postarray['text_font_size'] = tmls_text_font_size.val();
		shortcode+='text_font_size="'+tmls_text_font_size.val()+'" ';
		
		postarray['name_font_family'] = tmls_name_font_family.val();
		shortcode+='name_font_family="'+tmls_name_font_family.val()+'" ';
		
		postarray['name_font_color'] = tmls_name_font_color.val();
		shortcode+='name_font_color="'+tmls_name_font_color.val()+'" ';
		
		postarray['neme_font_size'] = tmls_neme_font_size.val();
		shortcode+='neme_font_size="'+tmls_neme_font_size.val()+'" ';
		
		postarray['neme_font_weight'] = tmls_neme_font_weight.val();
		shortcode+='neme_font_weight="'+tmls_neme_font_weight.val()+'" ';
		
		postarray['position_font_family'] = tmls_position_font_family.val();
		shortcode+='position_font_family="'+tmls_position_font_family.val()+'" ';
		
		postarray['position_font_color'] = tmls_position_font_color.val();
		shortcode+='position_font_color="'+tmls_position_font_color.val()+'" ';
		
		postarray['position_font_size'] = tmls_position_font_size.val();
		shortcode+='position_font_size="'+tmls_position_font_size.val()+'" ';
	}
	
	postarray['order_by'] = tmls_orderByList.val();
	shortcode+='order_by="'+tmls_orderByList.val()+'" ';
	
	postarray['order'] = tmls_orderList.val();
	shortcode+='order="'+tmls_orderList.val()+'" ';
	
	if( tmls_numberInput.val()!='' ) {
		postarray['number'] = tmls_numberInput.val();
		shortcode+='number="'+tmls_numberInput.val()+'" ';
	}
	
	if( tmls_layout.val()=='tmls_slider' ) {
		postarray['auto_play'] = tmls_auto_play.val();
		shortcode+='auto_play="'+tmls_auto_play.val()+'" ';
		
		postarray['pause_on_hover'] = tmls_pause_on_hover.val();
		shortcode+='pause_on_hover="'+tmls_pause_on_hover.val()+'" ';
		
		postarray['next_prev_visibility'] = tmls_next_prev_visibility.val();
		shortcode+='next_prev_visibility="'+tmls_next_prev_visibility.val()+'" ';
		
		postarray['next_prev_radius'] = tmls_next_prev_radius.val();
		shortcode+='next_prev_radius="'+tmls_next_prev_radius.val()+'" ';
		
		postarray['next_prev_position'] = tmls_next_prev_position.val();
		shortcode+='next_prev_position="'+tmls_next_prev_position.val()+'" ';
		
		postarray['scroll_duration'] = tmls_scroll_duration.val();
		shortcode+='scroll_duration="'+tmls_scroll_duration.val()+'" ';
		
		postarray['pause_duration'] = tmls_pause_duration.val();
		shortcode+='pause_duration="'+tmls_pause_duration.val()+'" ';
	}
	
	if( tmls_layout.val()!='tmls_slider' ) {
		postarray['border_style'] = tmls_border_style.val();
		shortcode+='border_style="'+tmls_border_style.val()+'" ';
		
		postarray['border_color'] = tmls_border_color.val();
		shortcode+='border_color="'+tmls_border_color.val()+'" ';
	}
	
	if( tmls_layout.val()=='tmls_grid' ) {
		postarray['columns_number'] = tmls_columns_number.val();
		shortcode+='columns_number="'+tmls_columns_number.val()+'" ';
	}
	
	shortcode+=']';
	
	tmls_div_shortcode.html(shortcode);
	
	tmls_gene_short_preview.html('<p>Loading ...</p>');
	
	tmls_gene_short_preview.load('../wp-content/plugins/tmls_testimonials/inc/generate_shortcode/do_shortcode.php', postarray , function(){
		
		tmls_sliders = $('.tmls_slider');
		tmls_style3_names = $('.style3 .tmls_name');
		tmls_visible_slider_buttons = $('.tmls_next_prev.tmls_visible');
		
		if (tmls_sliders.length )
		{
			tmls_sliders.each(function(){
				
				tmls_slider_play($(this));
				
				$(this).parent().parent().mouseenter(function(){
					$(this).children('.tmls_show_on_hover').slideToggle();
				});
				
				$(this).parent().parent().mouseleave(function(){
					$(this).children('.tmls_show_on_hover').slideToggle();
				});
			});
			
			tmls_visible_slider_buttons.fadeIn();
			
			$(window).resize(function() {
				tmls_sliders.each(function(){
					tmls_slider_play($(this));
				});
			});
			
		}
		
		
		
		if(tmls_style3_names.length) {
			tmls_style3_names.each(function(){
				$(this).css('padding-top', ($(this).parent().height()/2) - (($(this).height()+ 2.5 + $(this).parent().children('.tmls_position').height())/2));
			});
		}
		
		if( typeof jQuery.wp === 'object' && typeof jQuery.wp.wpColorPicker === 'function' ){

			jQuery( '#tmls_text_font_color' ).wpColorPicker();
			jQuery( '#tmls_name_font_color' ).wpColorPicker();
			jQuery( '#tmls_position_font_color' ).wpColorPicker();
			jQuery( '#tmls_border_color' ).wpColorPicker();

		}
		else {
			//We use farbtastic if the WordPress color picker widget doesn't exist
			jQuery('#tmls_text_font_colorpicker').farbtastic('#tmls_text_font_color');
			jQuery('#tmls_name_font_colorpicker').farbtastic('#tmls_name_font_color');
			jQuery('#tmls_position_font_colorpicker').farbtastic('#tmls_position_font_color');
			jQuery('#tmls_border_colorpicker').farbtastic('#tmls_border_color');
			
			tmls_farbtastic_inputs = $('#tmls_text_font_color,#tmls_name_font_color,#tmls_position_font_color,#tmls_border_color');
			
			tmls_farbtastic_inputs.focus(function(){
				$(this).parent().children('.tmls_farbtastic').slideDown();
			});
			
			tmls_farbtastic_inputs.focusout(function(){
				$(this).parent().children('.tmls_farbtastic').slideUp();
			});
		}		
				
	
	});
	
	
}


function tmls_slider_play(tmls_slider) {
	
	
	tmls_slider.carouFredSel({
		responsive: true,
		width:'variable',
		height:'variable',
		prev: {
			button: function() {
				return $(this).parents().children(".tmls_next_prev").children(".tmls_prev");
			}
		},
		next: {
			button: function() {
				return $(this).parents().children(".tmls_next_prev").children(".tmls_next");
			}
		},
		scroll: {
			items:1,          
			duration: tmls_slider.data('scrollduration'),
			fx:'crossfade'
		},
		auto: {
			play: tmls_slider.data('autoplay'),
			pauseDuration:tmls_slider.data('pauseduration'),
			pauseOnHover:tmls_slider.data('pauseonhover')
		},
		items: {
			width:700
		}
				
	});
			
}


})(jQuery);