
<?php
	require('../../../../../wp-load.php');
	
	$shortcode="[tmls ";
	
	if($_POST['category']!=null) {
		$shortcode.='category="'.$_POST['category'].'" ';
	}
	
	if($_POST['layout']!=null) {
		$shortcode.='layout="'.$_POST['layout'].'" ';
	}
	
	if($_POST['style']!=null) {
		$shortcode.='style="'.$_POST['style'].'" ';
	}
	
	if($_POST['image_size']!=null) {
		$shortcode.='image_size="'.$_POST['image_size'].'" ';
	}
	
	if($_POST['image_radius']!=null) {
		$shortcode.='image_radius="'.$_POST['image_radius'].'" ';
	}
	
	if($_POST['text_font_family']!=null) {
		$shortcode.='text_font_family="'.$_POST['text_font_family'].'" ';
	}
	
	if($_POST['text_font_color']!=null) {
		$shortcode.='text_font_color="'.$_POST['text_font_color'].'" ';
	}
	
	if($_POST['text_font_size']!=null) {
		$shortcode.='text_font_size="'.$_POST['text_font_size'].'" ';
	}
	
	if($_POST['name_font_family']!=null) {
		$shortcode.='name_font_family="'.$_POST['name_font_family'].'" ';
	}
	
	if($_POST['name_font_color']!=null) {
		$shortcode.='name_font_color="'.$_POST['name_font_color'].'" ';
	}
	
	if($_POST['neme_font_size']!=null) {
		$shortcode.='neme_font_size="'.$_POST['neme_font_size'].'" ';
	}
	
	if($_POST['neme_font_weight']!=null) {
		$shortcode.='neme_font_weight="'.$_POST['neme_font_weight'].'" ';
	}
	
	if($_POST['position_font_family']!=null) {
		$shortcode.='position_font_family="'.$_POST['position_font_family'].'" ';
	}
	
	if($_POST['position_font_color']!=null) {
		$shortcode.='position_font_color="'.$_POST['position_font_color'].'" ';
	}
	
	if($_POST['position_font_size']!=null) {
		$shortcode.='position_font_size="'.$_POST['position_font_size'].'" ';
	}
	
	if($_POST['order_by']!=null) {
		$shortcode.='order_by="'.$_POST['order_by'].'" ';
	}
	
	if($_POST['order']!=null) {
		$shortcode.='order="'.$_POST['order'].'" ';
	}
	
	if($_POST['number']!=null) {
		$shortcode.='number="'.$_POST['number'].'" ';
	}
	
	if($_POST['auto_play']!=null) {
		$shortcode.='auto_play="'.$_POST['auto_play'].'" ';
	}
	
	if($_POST['pause_on_hover']!=null) {
		$shortcode.='pause_on_hover="'.$_POST['pause_on_hover'].'" ';
	}
	
	if($_POST['next_prev_visibility']!=null) {
		$shortcode.='next_prev_visibility="'.$_POST['next_prev_visibility'].'" ';
	}
	
	if($_POST['next_prev_radius']!=null) {
		$shortcode.='next_prev_radius="'.$_POST['next_prev_radius'].'" ';
	}
	
	if($_POST['next_prev_position']!=null) {
		$shortcode.='next_prev_position="'.$_POST['next_prev_position'].'" ';
	}
	
	if($_POST['scroll_duration']!=null) {
		$shortcode.='scroll_duration="'.$_POST['scroll_duration'].'" ';
	}
	
	if($_POST['pause_duration']!=null) {
		$shortcode.='pause_duration="'.$_POST['pause_duration'].'" ';
	}
	
	if($_POST['border_style']!=null) {
		$shortcode.='border_style="'.$_POST['border_style'].'" ';
	}
	
	if($_POST['border_color']!=null) {
		$shortcode.='border_color="'.$_POST['border_color'].'" ';
	}
	
	if($_POST['columns_number']!=null) {
		$shortcode.='columns_number="'.$_POST['columns_number'].'" ';
	}
	
	
	$shortcode.="]";
	
	echo do_shortcode( $shortcode );
?>