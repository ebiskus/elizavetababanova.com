=== Secure WordPress ===
Contributors: Bueltge
Donate link: http://bueltge.de/wunschliste/
Tags: secure, notice, hack, hacked, protection, version, security
Requires at least: 2.6
Tested up to: 2.9-rare
Stable tag: 0.1

Secure your WordPress Installation with small functions.

== Description ==
Little help to secure your WordPress installation: Remove Error information on login page; adds index.html to plugin directory; removes the wp-version, except in admin area.

1. removes error-information on login-page
1. adds index.html to plugin-directory (virtual)
1. removes the wp-version, except in admin-area
1. removes Really Simple Discovery
1. removes Windows Live Writer
1. remove core update information for non-admins
1. remove plugin-update information for non-admins
1. remove theme-update informationfor non-admins (only WP 2.8 and higher)
1. Add string for use [WP Scanner](http://blogsecurity.net/wpscan "WP Scanner")

Please visit [the official website](http://bueltge.de/wordpress-login-sicherheit-plugin/652/#historie "Secure WordPress") for further details and the latest information on this plugin.

= Localizations =
Italien translation by [Gianni Diurno](http://gidibao.net/ "Gianni Diurno"), polish translation by Michal Maciejewski, belorussion file by [Fat Cow](http://www.fatcow.com/ "Fat Cow"), ukrainian translation by [WordPress Plugins UA](http://wpp.pp.ua/ "WordPress Plugins UA") and hungarian language files by [K&ouml;rmendi P&eacute;ter](http://www.seo-hungary.com/ "K&ouml;rmendi P&eacute;ter") Thanks a lot.

== Installation ==
1. Unpack the download-package
1. Upload fthe file to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Configure the options
1. Ready

See on [the official website](http://bueltge.de/wordpress-login-sicherheit-plugin/652/ "Secure WordPress").


== Screenshots ==
1. options-area (WordPress 2.9-rare)


== Other Notes ==
= Acknowledgements =
Italien translation by [Gianni Diurno](http://gidibao.net/ "Gianni Diurno"), polish translation by Michal Maciejewski, belorussion file by [Fat Cow](http://www.fatcow.com/ "Fat Cow"), ukrainian translation by [WordPress Plugins UA](http://wpp.pp.ua/ "WordPress Plugins UA") and hungarian language files by [K&ouml;rmendi P&eacute;ter](http://www.seo-hungary.com/ "K&ouml;rmendi P&eacute;ter") Thanks a lot.

= Licence =
Good news, this plugin is free for everyone! Since it's released under the GPL, you can use it free of charge on your personal or commercial blog. But if you enjoy this plugin, you can thank me and leave a [small donation](http://bueltge.de/wunschliste/ "Wishliste and Donate") for the time I've spent writing and supporting this plugin. And I really don't want to know how many hours of my life this plugin has already eaten ;)

= Translations =
The plugin comes with various translations, please refer to the [WordPress Codex](http://codex.wordpress.org/Installing_WordPress_in_Your_Language "Installing WordPress in Your Language") for more information about activating the translation. If you want to help to translate the plugin to your language, please have a look at the .pot file which contains all defintions and may be used with a [gettext](http://www.gnu.org/software/gettext/) editor like [Poedit](http://www.poedit.net/) (Windows).


== Changelog ==
= v0.3.9 (07/09/2009) =
* change index.html in index.php for better works

= v0.3.8 (22/06/2009) =
* add function to remove theme-update information for non-admins
* rescan language file; edit de_DE

Please see the older changes on version on the [the official website](http://bueltge.de/wordpress-login-sicherheit-plugin/652/ "Secure WordPress")!


== Frequently Asked Questions ==
= Where can I get more information? =
Please visit [the official website](http://bueltge.de/wordpress-login-sicherheit-plugin/652/ "Secure WordPress") for the latest information on this plugin.

= I love this plugin! How can I show the developer how much I appreciate his work? =
Please visit [the official website](http://bueltge.de/wordpress-login-sicherheit-plugin/652/ "Secure WordPress") and let him know your care or see the [wishlist](http://bueltge.de/wunschliste/ "Wishlist") of the author.