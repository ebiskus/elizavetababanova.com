<?php
/*
Plugin Name: Darkwing Duck Popup
Description: Скрипт лайт-бокс попапа для эффективного интернет-маркетинга.
Version: 1.1
Revision Date: Febrary 20, 2011
Tested up to: WP 3.0.5
Author: Свят Маслов
Author URI: http://svjatmaslov.ru
*/

define('DARKWING_MENU_PAGE', 'darkwing-config');
$iDarkWing_url = '';

function iDarkWing_init() {
	global $iDarkWing_url;
	$iDarkWing_url = trim(get_option('iDarkWing_url'));
	add_action('admin_menu', 'iDarkWing_config_page');
}
add_action('init', 'iDarkWing_init');

function iDarkWing_config_page() {
	$admin_page = 'iDarkWing_conf';
	if ( function_exists('add_submenu_page') )
		add_submenu_page('plugins.php', __('DD Popup'), __('DD Popup')
			, 'manage_options', DARKWING_MENU_PAGE, $admin_page);
}

function iDarkWing_conf() {
	global $iDarkWing_url;

	if ( isset($_POST['submit']) ) {

		if ( function_exists('current_user_can') && !current_user_can('manage_options') )
			die(__('Cheatin&#8217; uh?'));

		if (isset($_POST['DarkWing_url']) && trim($_POST['DarkWing_url'])) {
			update_option('iDarkWing_url', $iDarkWing_url = trim($_POST['DarkWing_url']));
		} else {
			delete_option('iDarkWing_url');
		}

		if (isset($_POST['DarkWing_mainpage_show']) && trim($_POST['DarkWing_mainpage_show'])) {
			update_option('iDarkWing_box_mainpage_show', trim($_POST['DarkWing_mainpage_show']));
		} else {
			delete_option('iDarkWing_box_mainpage_show');
		}

		if (isset($_POST['DarkWing_pages_show']) && trim($_POST['DarkWing_pages_show'])) {
			update_option('iDarkWing_box_pages_show', trim($_POST['DarkWing_pages_show']));
		} else {
			delete_option('iDarkWing_box_pages_show');
		}

		if (isset($_POST['DarkWing_posts_show']) && trim($_POST['DarkWing_posts_show'])) {
			update_option('iDarkWing_box_posts_show', trim($_POST['DarkWing_posts_show']));
		} else {
			delete_option('iDarkWing_box_posts_show');
		}

		if (isset($_POST['DarkWing_width']) && intval($_POST['DarkWing_width']) > 0) {
			update_option('iDarkWing_box_width', intval($_POST['DarkWing_width']));
		} else {
			delete_option('iDarkWing_box_width');
		}

		if (isset($_POST['DarkWing_height']) && intval($_POST['DarkWing_height']) > 0) {
			update_option('iDarkWing_box_height', intval($_POST['DarkWing_height']));
		} else {
			delete_option('iDarkWing_box_height');
		}

		if (isset($_POST['DarkWing_lightbox']) && trim($_POST['DarkWing_lightbox'])) {
			update_option('iDarkWing_box_lightbox', trim($_POST['DarkWing_lightbox']));
		} else {
			delete_option('iDarkWing_box_lightbox');
		}

		if (isset($_POST['DarkWing_show']) && trim($_POST['DarkWing_show'])) {
			update_option('iDarkWing_box_show', trim($_POST['DarkWing_show']));
		} else {
			delete_option('iDarkWing_box_show');
		}

		if (isset($_POST['DarkWing_delay']) && intval($_POST['DarkWing_delay']) > 0
		&& isset($_POST['DarkWing_show']) && 'exit' != trim($_POST['DarkWing_show'])) {
			update_option('iDarkWing_box_delay', intval($_POST['DarkWing_delay']));
		} else {
			delete_option('iDarkWing_box_delay');
		}

		if (isset($_POST['DarkWing_time']) && intval($_POST['DarkWing_time']) > 0
		&& isset($_POST['DarkWing_time_days']) && intval($_POST['DarkWing_time_days']) > 0) {
			update_option('iDarkWing_box_time', intval($_POST['DarkWing_time_days']));
		} else {
			delete_option('iDarkWing_box_time');
		}

		if (isset($_POST['DarkWing_show_from']) && trim($_POST['DarkWing_show_from'])) {
			update_option('iDarkWing_box_show_from', trim($_POST['DarkWing_show_from']));
		} else {
			delete_option('iDarkWing_box_show_from');
		}
	} // if ( isset($_POST['submit']) )

	if ( !empty($_POST ) ) {
		echo '<div id="message" class="updated fade"><p><strong>'
		. __('Options saved.') . '</strong></p></div>';
	}

	echo '<div class="wrap"><h2>' . __('Darkwing Duck Popup') . '</h2>';
	echo html_DarkWingConf_Styles();
	echo html_DarkWingConf_WindowDetails();
	//echo html_DarkWingConf_UpgradeBox();
	print_html_DarkWingConf_BlogList();
	echo html_DarkWingConf_Footer();
	echo '</div>';

} // function iDarkWing_conf()

wp_enqueue_script('jquery');

$suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '': '.pack';
wp_enqueue_script('fancybox', '/wp-content/plugins/darkwingpopup/fancybox/jquery.fancybox-1.3.4'.$suffix.'.js', array('jquery'), '1.3.4');
wp_enqueue_style( 'fancybox', '/wp-content/plugins/darkwingpopup/fancybox/jquery.fancybox-1.3.4.css', false, '1.3.4');

add_action('wp_head', 'iDarkWing_FancyIssueCSS');
add_action('wp_head', 'iDarkWing_box');
function iDarkWing_box() {
	global $iDarkWing_url;
	$SITECOOKIEPATH = '/';

	$box_showing  = 86400 * intval(get_option('iDarkWing_box_time'));
	$box_width    = intval(get_option('iDarkWing_box_width'));
	$box_height   = intval(get_option('iDarkWing_box_height'));
	$box_lightbox = intval(get_option('iDarkWing_box_lightbox')) > 0? 'true': 'false';	
	$is_home = intval(get_option('iDarkWing_box_mainpage_show')) > 0? 'true': 'false';
	$is_post = intval(get_option('iDarkWing_box_posts_show')) > 0? 'true': 'false';
	$is_page = intval(get_option('iDarkWing_box_pages_show')) > 0? 'true': 'false';
	$box_id       = 'iDarkWingLink'.time();
	$box_time     = time();
	$cookie_expires = date('D, d M Y H:i:s'
		, time() + max($box_showing, 31536000) /* keep cookies at least 1 year */
		) . ' GMT';

	if(is_home() && !$is_home)
		return false;
	elseif(is_page() && !$is_page)
		return false;
	elseif(is_single() && !$is_post)
		return false;
		
	if ('all' != get_option('iDarkWing_box_show_from')) {
		if(preg_match("|{$_SERVER['HTTP_HOST']}|", $_SERVER['HTTP_REFERER']))
			return false;
	}
	
	if ('exit' == get_option('iDarkWing_box_show')) {
		$SHOW =<<<SHOW_ON_EXIT
		var iDarkWingBoxStarted = 0;
		var iDarkWingBoxDelay   = 0;
		jQuery(document).mousemove(function(e) {
			tempY = document.all? event.clientY: e.pageY - document.body.scrollTop;
			if (tempY < 0) tempY = 0;
			if (20 > tempY && 0 > --iDarkWingBoxDelay) {
				if (0 == iDarkWingBoxStarted) {
					iDarkWingBoxStarted = 1;
					iDarkWingBox_show();
			}	}
		});
SHOW_ON_EXIT;
	}
	else {
		$SHOW = 'setTimeout("iDarkWingBox_show();", '.(1000 * intval(get_option('iDarkWing_box_delay'))).');';
	}

	$wpurl = get_bloginfo('wpurl');
	if (trim($iDarkWing_url)
	&& (!isset($_COOKIE['iDarkWingBox'])
	|| time() - intval($_COOKIE['iDarkWingBox']) > $box_showing)) {
echo <<<FANCY_BOX
<script type="text/javascript">
<!--//
function iDarkWingBox_show() {
	if ('function' == typeof jQuery.fancybox) iDarkWingBox_showbox();
	else {
		jQuery('#fancybox-wrap,#fancybox-tmp,#fancybox-loading,#fancybox-overlay,#fancybox-outer').remove();
		jQuery.getScript('$wpurl/wp-content/plugins/darkwingpopup/fancybox/jquery.fancybox-1.3.4.js', iDarkWingBox_showbox);
	}
}
function iDarkWingBox_showbox() {
	document.cookie="iDarkWingBox=$box_time; path=$SITECOOKIEPATH; expires=$cookie_expires;";
	jQuery.fancybox('$iDarkWing_url', {
		'width'     : $box_width,
		'height'    : $box_height,
		'type'      : 'iframe',
		'scrolling' : 'no',
		'autoScale' : false,
		'showCloseButton'    : true,
		'enableEscapeButton' : false,
		'overlayOpacity'     : 0.5,
		'overlayShow'        : $box_lightbox,
		'transitionIn'       : 'none',
		'transitionOut'      : 'none',
		'centerOnScroll'     : true,
		'hideOnOverlayClick' : false
	});
}
jQuery(document).ready(function(){
$SHOW
});
//-->
</script>
FANCY_BOX;
	} // if (trim($iDarkWing_url)... && SHOW
} // function iDarkWing_box()

function iDarkWing_FancyIssueCSS() {
$dir = plugin_dir_url( __FILE__ );
echo <<<FancyIssueCSS
<style type="text/css">
#fancybox-loading.fancybox-ie div { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_loading.png', sizingMethod='scale'); }
.fancybox-ie #fancybox-close { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_close.png', sizingMethod='scale'); }
.fancybox-ie #fancybox-title-over { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_title_over.png', sizingMethod='scale'); zoom: 1; }
.fancybox-ie #fancybox-title-left { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_title_left.png', sizingMethod='scale'); }
.fancybox-ie #fancybox-title-main { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_title_main.png', sizingMethod='scale'); }
.fancybox-ie #fancybox-title-right { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_title_right.png', sizingMethod='scale'); }
.fancybox-ie #fancybox-left-ico { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_nav_left.png', sizingMethod='scale'); }
.fancybox-ie #fancybox-right-ico { background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_nav_right.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-n { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_n.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-ne { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_ne.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-e { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_e.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-se { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_se.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-s { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_s.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-sw { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_sw.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-w { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_w.png', sizingMethod='scale'); }
.fancybox-ie #fancy-bg-nw { filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$dir}fancybox/fancy_shadow_nw.png', sizingMethod='scale'); }
</style>
FancyIssueCSS;
}

function html_DarkWingConf_Styles() {
return <<<STYLES
<style type="text/css">
h3.dbx-handle {
	height: 19px;
	padding: 4px 1em 1px 0;
	font-size: 14px;
	color: navy;
}
div.dbx-title {
	font-family: Arial, sans serif;
	font-size: 13px;
	color: black;
}
div.dbx-head {
	font-family: Arial, sans serif;
	font-weight: bold;
	padding-bottom: 1ex;
	font-size: 17px;
	color: gray;
}

.submit {
	border:none;
	border-bottom: 1px solid silver;
	margin:0;
	padding:1.5em 0 1em 0;
}
.form-table td {
	padding: 8px;
	font-size: 11px;
	border-bottom: none;
}
#window-details td {
	color: #2583ad;
	font-size: 13px;
	font-weight: bold;
}
td.w50 {
	width: 50px;
}
.form-table th {
	text-align: right;
	width: 200px;
	color: #2583ad;
	border-bottom: none;
}
.form-table td label, .form-table th label {
	font-weight: normal;
}
</style>
STYLES;
} // function html_DarkWingConf_Styles()

function html_DarkWingConf_WindowDetails() {
	$iDarkWing_url        = get_option('iDarkWing_url');
	$iDarkWing_box_width  = get_option('iDarkWing_box_width');  if (empty($iDarkWing_box_width))  $iDarkWing_box_width  = 600;
	$iDarkWing_box_height = get_option('iDarkWing_box_height'); if (empty($iDarkWing_box_height)) $iDarkWing_box_height = 500;
	$iDarkWing_box_delay  = get_option('iDarkWing_box_delay');  if (empty($iDarkWing_box_delay))  $iDarkWing_box_delay  = 0;
	$iDarkWing_box_time   = get_option('iDarkWing_box_time');   if (empty($iDarkWing_box_time))   $iDarkWing_box_time   = 1;
	$iDarkWing_box_show_exit  = 'exit' == get_option('iDarkWing_box_show')? 'checked': '';
	$iDarkWing_box_show_load  = 'exit' != get_option('iDarkWing_box_show')? 'checked': '';	
	$iDarkWing_box_mainpage_show_0 = intval(get_option('iDarkWing_box_mainpage_show')) > 0? '': 'checked';
	$iDarkWing_box_mainpage_show_1 = intval(get_option('iDarkWing_box_mainpage_show')) > 0? 'checked': '';
	$iDarkWing_box_pages_show_0 = intval(get_option('iDarkWing_box_pages_show')) > 0? '': 'checked';
	$iDarkWing_box_pages_show_1 = intval(get_option('iDarkWing_box_pages_show')) > 0? 'checked': '';
	$iDarkWing_box_posts_show_0 = intval(get_option('iDarkWing_box_posts_show')) > 0? '': 'checked';
	$iDarkWing_box_posts_show_1 = intval(get_option('iDarkWing_box_posts_show')) > 0? 'checked': '';	
	$iDarkWing_box_show_from_all  = 'all' == get_option('iDarkWing_box_show_from')? 'checked': '';
	$iDarkWing_box_show_from_other  = 'all' != get_option('iDarkWing_box_show_from')? 'checked': '';	
	$iDarkWing_box_lightbox_0 = intval(get_option('iDarkWing_box_lightbox')) > 0? '': 'checked';
	$iDarkWing_box_lightbox_1 = intval(get_option('iDarkWing_box_lightbox')) > 0? 'checked': '';
	$iDarkWing_box_time_0     = 0 == intval(get_option('iDarkWing_box_time'))? 'checked': '';
	$iDarkWing_box_time_1     = intval(get_option('iDarkWing_box_time')) > 0 ? 'checked': '';

$dir = plugin_dir_url( __FILE__ );
	
return <<<WINDOW_DETAILS
<style type='text/css'>
div.dashboard-widget-content {
    padding: 10px 15px;
}
div.dashboard-widget-content ul {
    margin: 0;
    text-indent: 0;
    padding-left: 15px;
}
div.dashboard-widget-content li {
    margin: .5em 0 1em;
}
div.dashboard-widget-content ul {
    list-style: none;
    padding: 0;
}
div.dashboard-widget-content ul li {
    display: block;
    width: 19.95%;
    padding-bottom: 10px;
    margin: 0;
    float: left;
    font-size: 12px;
}
div.dashboard-widget-content {
    margin: 10px 5px;
    padding: 0;
}

div.dashboard-widget-content ul li .post {
    display:block;
    font-family:Georgia,'Times New Roman',Times,serif;
    font-size:17px;
    line-height: 1.2em;
    height:90px;
    overflow:hidden;
}

div.dashboard-widget-content ul li a {
    display: block;
    height:100%;
    overflow:hidden;
    margin: 5px 10px;
    text-decoration: none;
    padding: .5em;
    border-right: 1px solid #dadada;
    border-bottom: 1px solid #dadada;
	background-color: #f9f9f9;
}
div.dashboard-widget-content ul li a cite {
    display: block;
    font-family: 'Lucida Sans', 'Lucida Grande', 'Lucida Sans Unicode', Tahoma, Verdana, sans-serif;
    height:28px;
}
</style>
<br />
<img src="{$dir}darkwing.gif" style="position: absolute; right: 0;">
<form action="" method="post" id="iDarkWing-conf"><table id="window-details" class="form-table" style="width:750px;"><tbody>
<tr><td><div class='dbx-title dbx-head'>1. Что показывать?</div></td></tr>
<tr class="form-field"><td><label for="DarkWing_url">URL отображаемой страницы:</label></td>
	<td colspan="2" align="right"><input id="DarkWing_url" name="DarkWing_url" value="$iDarkWing_url"
		type="text" size="50" style="width:270px; float: left;" /></td></tr>
<tr class="form-field"><td><label for="DarkWing_width">Ширина попапа:</label></td>
	<td class="w50"><input id="DarkWing_width" type="text" size="3"
		name="DarkWing_width" value="$iDarkWing_box_width" /></td></tr>
<tr class="form-field"><td><label for="DarkWing_height">Высота попапа:</label></td>
	<td class="w50"><input id="DarkWing_height" type="text" size="3"
		name="DarkWing_height" value="$iDarkWing_box_height" /></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><div class='dbx-title dbx-head'>2. Где показывать?</div></td></tr>
<tr class="form-field"><td><label for="DarkWing_url">На главной странице:</label></td>
	<td class="w50"><input type="radio" id="DarkWing_mainpage_show_on" style="width:auto;"
		name="DarkWing_mainpage_show" value="1" $iDarkWing_box_mainpage_show_1 />
		<label for="DarkWing_mainpage_show_on">Да</label></td>
	<td><input type="radio" id="DarkWing_mainpage_show_off" style="width:auto;"
		name="DarkWing_mainpage_show" value="0" $iDarkWing_box_mainpage_show_0 />
		<label for="DarkWing_mainpage_show_off">Нет</label></td></tr>
<tr class="form-field"><td><label for="DarkWing_posts_show">На страницах постов:</label></td>
	<td class="w50"><input type="radio" id="DarkWing_posts_show_on" style="width:auto;"
		name="DarkWing_posts_show" value="1" $iDarkWing_box_posts_show_1 />
		<label for="DarkWing_posts_show_on">Да</label></td>
	<td><input type="radio" id="DarkWing_posts_show_off" style="width:auto;"
		name="DarkWing_posts_show" value="0" $iDarkWing_box_posts_show_0 />
		<label for="DarkWing_posts_show_off">Нет</label></td></tr>
		
<tr class="form-field"><td><label for="DarkWing_pages_show">На статичных страницах:</label></td>
	<td class="w50"><input type="radio" id="DarkWing_pages_show_on" style="width:auto;"
		name="DarkWing_pages_show" value="1" $iDarkWing_box_pages_show_1 />
		<label for="DarkWing_pages_show_on">Да</label></td>
	<td><input type="radio" id="DarkWing_pages_show_off" style="width:auto;"
		name="DarkWing_pages_show" value="0" $iDarkWing_box_pages_show_0 />
		<label for="DarkWing_pages_show_off">Нет</label></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><div class='dbx-title dbx-head'>3. Как показывать?</div></td></tr>
<tr class="form-field"><td><label for="DarkWing_show">Тип попапа:</label></td>
	<td><input type="radio" id="DarkWing_show_exit" style="width:auto;"
		name="DarkWing_show" value="exit"
		onclick="document.getElementById('DarkWing_delay_block').style.visibility = 'hidden';" 
		$iDarkWing_box_show_exit />
		<label for="DarkWing_show_exit">Exit-попап</label></td><td><input type="radio" 
		id="DarkWing_show_load" style="width:auto;" name="DarkWing_show" value="load" 
		onclick="document.getElementById('DarkWing_delay_block').style.visibility = 'visible';" 
		$iDarkWing_box_show_load />
		<label for="DarkWing_show_load" style="padding-right:2em;">Классический</label>
		<span id="DarkWing_delay_block"><input id="DarkWing_delay" type="text" size="3" style="width:3em;"
		name="DarkWing_delay" value="$iDarkWing_box_delay" />
		<label for="DarkWing_delay">Задержка (сек.)</label></span></td></tr>
		<tr class="form-field"><td>Lightbox Effect</td>
	<td><input type="radio" id="DarkWing_lightbox_on" style="width:auto;"
		name="DarkWing_lightbox" value="1" $iDarkWing_box_lightbox_1 />
		<label for="DarkWing_lightbox_on">Вкл.</label></td>
	<td><input type="radio" id="DarkWing_lightbox_off" style="width:auto;"
		name="DarkWing_lightbox" value="0" $iDarkWing_box_lightbox_0 />
		<label for="DarkWing_lightbox_off">Выкл.</label></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><div class='dbx-title dbx-head'>4. Когда показывать?</div></td></tr>
<tr class="form-field"><td><label for="DarkWing_time">Таймаут после показа попапа:</label></td>
	<td><input type="radio" id="DarkWing_time_always" style="width:auto;"
		name="DarkWing_time" value="0" $iDarkWing_box_time_0 />
		<label for="DarkWing_time_always">Нет</label></td>
	<td>
		<input type="radio" id="DarkWing_time_once" style="width:auto;"
		name="DarkWing_time" value="1" $iDarkWing_box_time_1 />
		<input type="text" id="DarkWing_time_days" size="3" style="width:3em;"
		name="DarkWing_time_days" value="$iDarkWing_box_time" />
		<label for="DarkWing_time_days">дней</label></td><td></td></tr>
<tr class="form-field"><td><label for="DarkWing_show_from">Отображать попап при переходе:</label></td>
	<td><input type="radio" id="DarkWing_show_from_all" style="width:auto;"
		name="DarkWing_show_from" value="all" $iDarkWing_box_show_from_all />
		<label for="DarkWing_show_from_all">Любом</label></td>
	<td>
		<input type="radio" id="DarkWing_show_from_other" style="width:auto;"
		name="DarkWing_show_from" value="other" $iDarkWing_box_show_from_other />
		<label for="DarkWing_show_from_other">Со стороннего сайта</label></td><td></td></tr>
<tr><td colspan="3" class="submit" align="center" style="padding: 20px 0 10px 0;">
		<input type="submit" name="submit" class="button-primary" value="Обновить настройки &raquo;" /></td></tr>
</tbody></table>
</form>
WINDOW_DETAILS;
} // function html_DarkWingConf_WindowDetails($header)

function print_html_DarkWingConf_BlogList() {
	if($advert = file_get_contents(dirname(__FILE__).'/advert.txt'))
		echo $advert;
}

function html_DarkWingConf_Footer() {
return <<<DARKWINGCONF_FOOTER
<div style="width: 100%; text-align: center; margin: 15px 0;">
	<div style="color:gray; font-size:12px; padding-top:1ex;">DarkWing POPUP Plug-in
	Created by <a href="http://svjatmaslov.ru" target="external">Svjat Maslov</a></div>
</div>
DARKWINGCONF_FOOTER;
}

// Settings Button on Plugins Panel
function DarkWing_plugin_action_links($links, $file) {

	static $this_plugin;
	if ( ! $this_plugin ) $this_plugin = plugin_basename(__FILE__);

	if ( $file == $this_plugin ){
		$settings_link = '<a href="plugins.php?page=darkwing-config">' . __('Settings') . '</a>';
		array_unshift( $links, $settings_link );
	}

	return $links;

}

// Filters
add_filter( 'plugin_action_links', 'DarkWing_plugin_action_links', 10, 2 ); // Settings Button on Plugins Panel

?>