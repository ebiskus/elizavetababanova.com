<?php
	
	if($_POST['action']=="save_options"):
		update_option("wvw_options", $_POST['wvw_options']);
	endif;
	$options = get_option('wvw_options');
?>
<div class="wrap">
	<div id="icon-tools" class="icon32"><br></div>
    <h2>Настройки плагина "Видео-защита"</h2><br>
    <form action method="post" id="wvw_options_form">
    	<input type="hidden" name="action" value="save_options" />
    	<table class="form-table">
        	<tr>
            	<th><label for="blocked_user_text">Текст заблокированному студенту</label></th>
                <td><textarea cols="100" rows="5" name="wvw_options[blocked_user_text]" id="blocked_user_text"><?php echo $options['blocked_user_text'] ?></textarea></td>
            </tr>
            <tr>
            	<th><label for="noacces_user_text">Текст студенту при отсутствии доступа к видео</label></th>
                <td><textarea cols="100" rows="5" name="wvw_options[noacces_user_text]" id="noacces_user_text"><?php echo $options['noacces_user_text'] ?></textarea></td>
            </tr>
            <tr>
            	<th><label for="noacces_nouser_text">Текст незалогиненному посетителю при отсутствии доступа к видео</label></th>
                <td><textarea cols="100" rows="5" name="wvw_options[noacces_nouser_text]" id="noacces_nouser_text"><?php echo $options['noacces_nouser_text'] ?></textarea></td>
            </tr>
            <tr>
            	<th><label for="max_ip_count">Количество IP превышение которого блокирует доступ для пользователя</label></th>
                <td><input type="text" name="wvw_options[max_ip_count]" id="max_ip_count" value="<?php echo $options['max_ip_count'] ?>" /></td>
            </tr>
            <tr>
            	<th><label for="sound_on">Включить звуковую защиту</label></th>
                <td><input <?php echo ($options['sound_on']==1) ? 'checked="checked"' : '' ?> type="checkbox" name="wvw_options[sound_on]" id="sound_on" value="1" /></td>
            </tr>
            <tr>
            	<td colspan="2"><input type="submit" name="submit" id="submit" class="button button-primary" value="Сохранить изменения"></td>
            </tr>
        </table>
    </form>
    </div>
