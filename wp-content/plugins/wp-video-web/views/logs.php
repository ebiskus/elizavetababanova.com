<?php 
	global $wpdb;
	$start = 0;
		
	$logs = $wpdb->get_results("SELECT * FROM $wpdb->wvw_access_ip_log LIMIT $start, 30"); 
	
	
?>
<div class="wrap">
	<div id="icon-users" class="icon32"><br></div>
    <h2>Студенты</h2><br>
 <br><br>
    <table class="wp-list-table widefat fixed posts" cellspacing="0">
		<thead>
			<tr>
            	<th>Дата</th>
                <th>Ip адрес</th>
             </tr>
		</thead>
        <tfoot>
            <tr>
            	<th>Дата</th>
                <th>Ip адрес</th>
			</tr>
        </tfoot>
		<tbody id="the-list">
        	<?php foreach($logs as $log): ?>
					
            <tr>
            	<th><?php echo $log->date ?></th>
                <td><?php echo $log->ip ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
</table>
    
</div>
