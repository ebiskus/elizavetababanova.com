<?php
	$video_autoplay = get_post_meta($post->ID, 'video-autoplay', true);
?>
<table id="video-box">
	<tr>
    	<td>ID Видео:</td><td><strong><?php echo $post->ID ?></strong></td>
    </tr>
	<tr>
    	<td><label for="">Url видеозаписи:</label></td>
        <td><input id="video-url" type="text" name="video_meta[video-url]" size="100" value="<?php echo get_post_meta($post->ID, 'video-url', true) ?>" /></td>
    </tr>
    <tr>
    	<td><label for="video-number">Номер:</label></td>
        <td><input id="video-number" type="text" name="video_meta[video-number]" size="10" value="<?php echo get_post_meta($post->ID, 'video-number', true) ?>" /></td>
    </tr>
    <tr>
    	<td><label for="video-part">Часть:</label></td>
        <td><input id="video-part" type="text" name="video_meta[video-part]" size="10" value="<?php echo get_post_meta($post->ID, 'video-part', true) ?>" /></td>
    </tr>
    <tr>
    	<td><label for="video-width">Ширина:</label></td>
        <td><input id="video-width" type="text" name="video_meta[video-width]" size="10" value="<?php echo ($width = get_post_meta($post->ID, 'video-width', true)) ? $width : "800"  ?>" /> px</td>
    </tr>
    <tr>
    	<td><label for="video-height">Высота:</label></td>
        <td><input id="video-height" type="text" name="video_meta[video-height]" size="10" value="<?php echo ($width = get_post_meta($post->ID, 'video-height', true)) ? $width : "600" ?>" /> px</td>
    </tr>
    <tr>
    	<td><label for="video-autoplay">Автостарт:</label></td>
        <td>
        	<label><input type="radio" <?php echo ($video_autoplay) ? 'checked="checked"' : '' ?> id="video-autoplay" name="video_meta[video-autoplay]" value="1" /> Да</label>
         	<label><input type="radio" <?php echo (!$video_autoplay) ? 'checked="checked"' : '' ?> id="video-autoplay" name="video_meta[video-autoplay]" value="0" /> Нет</label>
        </td>
    </tr>
</table>
   
    
