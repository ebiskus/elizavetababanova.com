<?php 
	
	$args = array('role' => 'wvw_student');
	
	/* Если введен параметр поиска по емейл добавляем правило */
	if($_POST['user_email']):
		$args['search'] = $_POST['user_email'];		
	endif;
	$users = get_users($args);
?>
<div class="wrap">
	<div id="icon-users" class="icon32"><br></div>
    <h2>Студенты</h2><br>
    <form action method="post"><label>Поиск по e-mail, логину или ID <input type="text" name="user_email" value="<?php echo $_POST['user_email'] ?>" /></label><input type="submit" name="" value="Искать"/></form><br><br>
    <table class="wp-list-table widefat fixed posts" cellspacing="0">
		<thead>
			<tr>
            	<th>ID</th>
                <th>Звуковой код</th>
            	<th>Редактировать</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Логин</th>
                <th>E-mail</th>
                <th>Доступы к видео</th>
                <th>Доступы к категориям</th>
                <th>Сохранить</th>
                <th>История IP</th>
			</tr>
		</thead>
        <tfoot>
            <tr>
            	<th>ID</th>
                <th>Звуковой код</th>
            	<th>Редактировать</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Логин</th>
                <th>E-mail</th>
                <th>Доступы к видео</th>
                <th>Доступы к категориям</th>
                <th>Сохранить</th>
                <th>История IP</th>
			</tr>
        </tfoot>
		<tbody id="the-list">
        	<?php foreach($users as $user): 
					$uts = array();
					$users_terms_selected = get_user_meta($user->ID, "wvw_caps_cat", true);
					foreach($users_terms_selected as $ut) $uts[] = $ut['value'];
					$users_terms_args = array(
											'selected_cats' => $uts,
											'taxonomy' => 'videos'
										);
					
					#Получаем список ip по которым заходил студент
						$ip_array = get_user_meta($user->ID, "ip_log", true);
					# Проверка заблокирован ли пользователь
					if(get_user_meta($user->ID, "wvw_blocked", true)==1)
						$blocked = "<span class=\"unblock_user\" data-id=\"$user->ID\">разблокировать</span>";
					else
						$blocked = "<span class=\"block_user\" data-id=\"$user->ID\">заблокировать</span>";
			?>
            <tr id="wvw-user-form-<?php echo $user->ID ?>">
               	<td><?php echo $user->ID ?></td>
                <td><?php echo $user->user_7n ?></td>
            	<td><?php echo $blocked ?></td>
                <td><?php echo $user->first_name ?></td>
                <td><?php echo $user->last_name ?></td>
                <td><?php echo $user->user_login ?></td>
                <td><?php echo $user->user_email ?></td>
                <td><input type="text" name="wvw-caps-<?php echo $user->ID ?>" value="<?php echo get_user_meta($user->ID, "wvw_caps", true) ?>" /></td>
                <td><div id="wvw-caps-cats-<?php echo $user->ID ?>" class="wvw-caps-cats"><?php wp_terms_checklist( 0, $users_terms_args ); ?></div></td>
                <td><input data-id="<?php echo $user->ID ?>" class="wvw-caps-save" type="submit" value="Сохранить" /></td>
                <td>
                	<div class="ip-log-<?php echo $user->ID ?>">
                	<?php 
					foreach($ip_array as $ip): 
                    	echo $ip."<br />";
					endforeach; ?>
                    </div>
                    <a data-id="<?php echo $user->ID ?>" class="null-ip">очистить IP</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
</table>
    
</div>
