<?php
	$options = get_option('wvw_options');
 ?>
<script type="text/javascript">
        var user_id = '<?php echo $current_user->ID ?>';
        var tone_id = '<?php echo $current_user->user_7n ?>';
        var fio = '<?php echo $current_user->display_name ?>';
        var mail = '<?php echo $current_user->user_email ?>';
		var tone_interval =  60*1; //sec
		var animation_interval = 60 * 1000;
</script>
<script type="text/javascript">
(function($){	
	flowplayer("wvw-video-<?php echo $wvw_video_id ?>", "/wp-content/plugins/wp-video-web/flowplayer/flowplayer-3.2.16.swf", {
		clip:{
			onBegin: function(){<?php if($options['sound_on']==1): ?>bibip = setInterval(function(){$.playNumber(tone_id)}, tone_interval*1000);<?php endif; ?> fly = setInterval(function(){ wvwAnim_<?php echo $wvw_video_id ?>()}, animation_interval+1000);},
			onResume: function(){<?php if($options['sound_on']==1): ?>bibip = setInterval(function(){$.playNumber(tone_id)}, tone_interval*1000);<?php endif; ?> fly = setInterval(function(){ wvwAnim_<?php echo $wvw_video_id ?>()}, animation_interval+1000);},
			onPause: function(){clearInterval(bibip); /*clearInterval(fly);*/},
			onStop: function(){clearInterval(bibip); /*clearInterval(fly);*/},
			onFinish: function(){clearInterval(bibip); /*clearInterval(fly);*/},
			autoBuffering: true,
			autoPlay: <?php echo $wvw_video_autoplay ?>,
			bufferLength: 60,
			provider: 'lighttpd'
		},
		plugins: {
			lighttpd: {
				url: "/wp-content/plugins/wp-video-web/flowplayer/flowplayer.pseudostreaming-3.2.13.swf"
			},
			<?php 
			 $video_aviable = get_post_meta($wvw_video_id, 'video-aviable', true);
			 if(!$video_aviable):
			 ?>
    		// "myContent" is the name of our plugin
   		 	myContent: {
        	// location of the plugin
				url: '/wp-content/plugins/wp-video-web/flowplayer/flowplayer.content-3.2.8.swf',
	 
				// display properties
				top: 0,
				left: 0,
				height: 20,
				width: 130,
				opacity: 0.5,
		 
				// styling properties
				borderRadius: 0,
				border: 0,
	 
				// linked stylesheet
				// stylesheet: 'css/content-plugin.css',
		 
				// "inline" styling (overrides external stylesheet rules),
				style: {
					'.title': {
						fontSize: 10,
						fontFamily: 'verdana,arial,helvetica'
					}
					
				},
        		html: '<p class="title">Защита Ваш ID '+user_id+'</p>'
			},		
			myContent2: {
 
			// location of the plugin
				url: '/wp-content/plugins/wp-video-web/flowplayer/flowplayer.content-3.2.8.swf',
		 
				// display properties
				top: 0,
				right: 0,
				height: 25,
				width: '100%',
				opacity: 0.5,
		 
				// styling properties
				borderRadius: 0,
				border: 0,
		 
				// linked stylesheet
			   // stylesheet: 'css/content-plugin.css',
		 
				// "inline" styling (overrides external stylesheet rules),
				style: {
					'.title': {
						textAlign: 'right',
						fontSize: 12,
						fontFamily: 'verdana,arial,helvetica'
					}
				},
					html: '<p class="title">Курс оплатил '+ fio +', '+ mail +'</p>',
			}	
			<?php endif; ?>
		}
    }).ipad();
	
	var pos = false;
	function wvwAnim_<?php echo $wvw_video_id ?>(){	;
				plugin1 = flowplayer("wvw-video-<?php echo $wvw_video_id ?>").getPlugin("myContent");
				if(pos){
					plugin1.animate({top:0, left:0}, animation_interval);
					pos = false;
				}else{
					plugin1.animate({bottom:30, right:0}, animation_interval);
					pos = true;
				}	
	}
})(jQuery);
</script>
<?php if(current_user_can('administrator')): ?>
	<a target="_blank" href="<?php echo get_bloginfo('url'); ?>/wp-admin/post.php?post=17&action=edit">Редактировать видео</a>
<?php endif; ?>