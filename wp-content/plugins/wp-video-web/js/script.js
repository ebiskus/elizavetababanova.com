(function($){
	plugin_path = "/wp-content/plugins/wp-video-web/";
	$(".wvw-caps-save").live("click", function(){
		var id = $(this).data('id');
		$.post(
			plugin_path + "ajax.php", 
			{
				action: "upd_user_meta",
				user_id: id,
				key: "wvw_caps",
				value: $("input[name='wvw-caps-" + id +"']").val(),
				messege: "Доступы к видио обновлены" 
			},
			function(data){
				$.post(
				plugin_path + "ajax.php", 
				{
					action: "upd_user_meta",
					user_id: id,
					key: "wvw_caps_cat",
					value: $("#wvw-caps-cats-" + id +" input").serializeArray(),
					messege: "Доступы к видио обновлены" 
				}, function(data){
						alert(data);
			});
		});
	});
	
	$(".block_user").live("click", function(){
		var id = $(this).data('id');
		var el = $(this);
		$.post(
			plugin_path + "ajax.php", 
			{
				action: "upd_user_meta",
				user_id: id,
				key: "wvw_blocked",
				value: 1,
				messege: "Пользователь заблокирован" 
			},
			function(data){
				$(el).removeClass('block_user').addClass('unblock_user').text("разблокировать");
				alert(data);
			});
	});
	
	$(".null-ip").live("click", function(){
		var id = $(this).data('id');
		var el = $(this);
		$.post(
			plugin_path + "ajax.php", 
			{
				action: "upd_user_meta",
				user_id: id,
				key: "ip_log",
				value: '',
				messege: "История IP очищена" 
			},
			function(data){
				$(".ip-log-" + id).html('');
				alert(data);
			});
		
	});
	
	$(".unblock_user").live("click", function(){
		var id = $(this).data('id');
		var el = $(this);
		$.post(
			plugin_path + "ajax.php", 
			{
				action: "upd_user_meta",
				user_id: id,
				key: "wvw_blocked",
				value: 0,
				messege: "Пользователь разблокирован" 
			},
			function(data){
				$(el).removeClass('unblock_user').addClass('block_user').text("заблокировать");
				alert(data);
			});
	});
	
})(jQuery);