/**
 * @author Alexander Manzyuk <admsev@gmail.com>
 * Copyright (c) 2012 Alexander Manzyuk - released under MIT License
 * https://github.com/admsev/jquery-play-sound
 * Usage: $.playSound('http://example.org/sound.mp3');
**/

(function($){

  $.extend({
    playSound: function(){
      return $("<embed src='"+arguments[0]+"' hidden='true' autostart='true' loop='false' class='playSound'>").prependTo('body');
    }
  });
  
  $.extend({
    playNumber: function(){
      string = String(arguments[0]);
			$.post("/wp-content/plugins/wp-video-web/mp3.php", {code: string});
			$.playSound("http://compumur.ru/mp3/tmp/" + string +".mp3");
    }
  });
})(jQuery);
 