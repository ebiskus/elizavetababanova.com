<div id="wrap" class="container">
	
			<div id="content" class="eleven columns">   
			
			
	<div class="stat_block pie">	
	   <img class="mir" src="<?php bloginfo('template_directory'); ?>/image/mir.jpg"/><div class="title_stat avant_book">Здоровый допинг</div> 
     <div class="stat_shadow"></div> 
         <div class="stat_text">
     <img src="<?php bloginfo('template_directory'); ?>/image/im_stat.jpg"/>      	     
	   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
          </div> 
  </div> 			
  
  <div class="news4"> 
   

 <?php query_posts('cat=1&showposts=1'); ?>	
<?php 
if (have_posts()) : while (have_posts()) : the_post(); ?>


<div class="news4_block pie one_half"> 
   <div>

<div class="title_news4 avant_book">
<?php
if (!is_single() && !is_page() ) { 
   echo '<a href="'; 
   the_permalink() ;
   echo '">';
   }; 
the_title(); 
if ( !is_single() && !is_page() ) echo '</a>';
?>

<?php 
if (!is_page()) 
edit_post_link(' [edit]');
?>
</div>

<div class="news4_text">
<img src="<?=catch_that_image();?>"/> 
<?php do_excerpt(get_the_excerpt(), 300); ?>  
 <div class="clr"> </div>
 
</div>
 <?php
 
if (!is_single() && !is_page() ) { 
   echo '<a href="'; 
   the_permalink() ;
   echo ' " class="dalee avant_book">Читать дальше >></a>';
   }

?> 
</div>  
 </div>
 
 
<?php endwhile; endif; ?>   
   
  
<?php query_posts('cat=3&showposts=3'); ?>	

<?php   
if (have_posts()) : while (have_posts()) : the_post(); ?>


<div class="news4_block pie one_half"> 
   <div>

<div class="title_news4 avant_book">
<?php
if (!is_single() && !is_page() ) { 
   echo '<a href="'; 
   the_permalink() ;
   echo '">';
   }; 
the_title(); 
if ( !is_single() && !is_page() ) echo '</a>';
?>

<?php 
if (!is_page()) 
edit_post_link(' [edit]');
?>
</div>

<div class="news4_text">

<img src="<?=catch_that_image();?>"/>              
<?php do_excerpt(get_the_excerpt(), 300); ?>  
<div class="clr"> </div>
 
</div>
 <?php
 
if (!is_single() && !is_page() ) { 
   echo '<a href="'; 
   the_permalink() ;
   echo ' " class="dalee avant_book">Читать дальше >></a>';
   }

?> 
</div>  
 </div>
 
 
<?php endwhile; endif; ?>
 <div class="clr"> </div>
</div>

<?php 
if ( !is_single() && !is_page() ) {
   echo '<div class="navpage">';
   next_posts_link('&laquo; Ранее');
   echo '&nbsp;&nbsp;&nbsp;';
   previous_posts_link('Позже &raquo;');
   echo '</div >';
   }
?>



	   
			
     <div class="clr"> </div> 

  
   	<div class="stat_block pie">	
	   <div class="title_stat avant_book">Book Club: «Rework: Бизнес без предрассудков»</div> 
     <div class="stat_shadow"></div> 
         <div class="stat_text">
<p>Хотите быть в курсе самого передового чтива в сфере бизнеса и саморазвития?</p>  
<p>Тогда присоединяйесь к нашему читательскому клубу «Book Club с Елизаветой».</p> 
<p>Каждые две недели я буду предлагать вам книгу для прочтения вместе со мной и нашей растущей группой соратников, увлеченных внутренним и внешним ростом.</p>
<p>Книги я буду выбирать на все мои любимые темы: бизнес, саморазвитие, здоровье, личная эффективность, менеджмент, профессинальный рост, истории успеха и биографии великих людей.</p> 
<p>Нередко мне встречается цитата о том, что у бедных людей большие телевизоры, а у богатых людей — большие библиотеки.</p>                                                                                                                        
<p>Это не совсем так. У богатых людей тоже большие телевизоры, но включают они их крайне редко и только целенаправленно — посмотреть сводку новостей, хорошее кино по DVD, или продвигающий документальный фильм.</p>   
<p>Но что реально отличает богатых людей от бедных — это количество прочитанных ими книг. Поэтому, один из главных секретов успеха — это качественная литература, которую мы с вами должны поглощать в огромном количестве. </p>   

    </div> 
  </div> 
  
</div><!-- #content-->

   
<?php get_sidebar(); ?>

 </div><!-- #wrap -->   

