<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>


<div id="wrapper1">
	<div id="header">

  <a title="<?php bloginfo('name') ?>" href="<?php bloginfo('siteurl') ?>/">
        <div id="logo_top"></div>
  </a>



<div id="menu_top" class="pie avant_book">
  <ul id="nav">
    <li><a href="/">Главная</a> </li>
    <li><a href="/">Блог</a></li>
    <li><a href="/">Обо мне</a></li>
    <li><a href="/">Программы обучения</a></li> 
    <li><a href="/">Консультации</a></li>
    <li><a href="/">Контакты</a></li>         
  </ul>
</div>
<div class="clr"> </div>
 
 
<div class="flexslider">

<script type="text/javascript" charset="utf-8">
$(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({    
    controlNav: false,
    animation: "slide" 
  
  });
});
</script>

  <ul class="slides pie">
    <li>
      <img src="/wp-content/themes/madrom_babanova/image/slide1.jpg" />
    </li>
    <li>
      <img src="/wp-content/themes/madrom_babanova/image/slide2.jpg" />
    </li>
    <li>
      <img src="/wp-content/themes/madrom_babanova/image/slide3.jpg" />
    </li>
  </ul>
</div>
 
  
  </div><!-- #header-->
</div><!-- #wrapper1 -->



<div class="line_shadow"></div>

<div id="wrapper2"> 
 

<?php
  if (is_front_page() ) require('spec.php');  
?>

       
     <div class="clr"> </div>   
</div>
  
  
   
	

			<?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			?>
			
			
 <div class="line_shadow"></div>
	

 <div id="wrapper3">

<div id="footer">
<?php get_footer(); ?>
</div><!-- #footer --> 

</div><!-- #wrapper3 -->

</body>
</html>