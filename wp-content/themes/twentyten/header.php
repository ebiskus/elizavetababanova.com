<?php

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />   

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>



<script src="/wp-content/themes/madrom_babanova/js/jquery.flexslider.js"></script>
<script src="/wp-content/themes/madrom_babanova/js/jquery.jtruncate.pack.js"></script>

<link href='http://fonts.googleapis.com/css?family=Didact+Gothic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>


<script>
  $(document).ready(function(){
    $(".block_spec > div:last-child").addClass("last");
    $(".one_half:nth-child(even)").addClass("last"); 
    $(".news4 .news4_block:first-child .title_news4").addClass("other");    

    
  });
  </script> 
    
<?php require('social_button_hed.php'); ?>     
    
     

<link rel="alternate" type="application/rss+xml" title="RSS 2.0"
   href="<?php bloginfo('rss2_url'); ?>">
<link rel="alternate" type="text/xml" title="RSS .92"
   href="<?php bloginfo('rss_url'); ?>">
<link rel="alternate" type="application/atom+xml" title="Atom 0.3"
   href="<?php bloginfo('atom_url'); ?>">
<?php wp_head(); ?> 

<?php

	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );


	wp_head();
?>
 
</head>


<body>

