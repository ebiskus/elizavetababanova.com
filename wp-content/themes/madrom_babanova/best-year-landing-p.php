<?php
/*
Template Name: Приземляющая страница Лучший год Partner
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta property="fb:admins" content="100003273128680"/>
		<meta property="vk:app_id" content="3398000" />

		<title>Лучший год Вашей жизни</title>

		<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-landing/images/share1.jpg">
		<meta property="og:title" content="Лучший год Вашей жизни"/>
		<meta property="og:type" content="article"/>
		<meta property="og:url" content="http://elizavetababanova.com/best-year-landing-p"/>
		<meta name="description" content="Как сделать следующий год лучшим годом Вашей жизни?">

		<!-- <link rel="stylesheet" type="text/css" href="style/style.css"> -->
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
		<link href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/style/style.css" rel="stylesheet">
		<script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.js"></script>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/plugins.js"></script>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/script.js"></script>

		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

		<!--Google-->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-35488483-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<!--/google-->
	</head>
	<body>
		<header class="header"></header>
		<div class="wrapper">
			<div class="reg">
				<div class="regpodpiska">
					<div class="regtextwidget">
						<form accept-charset="utf-8" action="https://app.getresponse.com/add_contact_webform.html" method="post">
							<input type="hidden" name="webform_id" value="627846" />
							<div class="regiwrapu">
								<input type="text" placeholder="Имя и Фамилия" class="placeholder fs15" name="name" id="oa-firstname">
							</div>
							<div class="regiwrapu">
								<input type="text" placeholder="Электронная почта" class="placeholder fs15" name="email" id="oa-email">
							</div>
							<ul>
								<li class="wf-counter" rel="undefined" style="display:  none !important;">
									<div class="wf-contbox">
										<div>
											<span style="padding: 4px 6px 8px 24px; background-image: url(https://app.getresponse.com/images/core/webforms/countertemplates.png); background-position: 0% 0px; background-repeat: no-repeat no-repeat;" class="wf-counterbox"> <span class="wf-counterboxbg" style="padding: 4px 12px 8px 5px; background-image: url(https://app.getresponse.com/images/core/webforms/countertemplates.png); background-position: 100% -36px; background-repeat: no-repeat no-repeat;"> <span class="wf-counterbox0" style="padding: 5px 0px;"></span> <span style="padding: 5px;" name="https://app.getresponse.com/display_subscribers_count.js?campaign_name=fenomen_seminar&var=0" class="wf-counterbox1 wf-counterq">39466</span> <span style="padding: 5px 0px;" class="wf-counterbox2">subscribers</span> </span> </span>
										</div>
									</div>
								</li>
								<li class="wf-captcha" rel="undefined" style="display:  none !important;">
									<div class="wf-contbox wf-captcha-1" id="wf-captcha-1" wf-captchaword="Enter the words above:"
									wf-captchasound="Enter the numbers you hear:" wf-captchaerror="Incorrect please try again"></div>
								</li>
								<li>
									<button value="1" name="submit" type="submit" class="regbut"></button>
								</li>
								<!-- <li class="wf-privacy" rel="undefined" style="!important;">
								<div class="wf-contbox">
								<div>
								<a target="_blank" class="wf-privacy wf-privacyico" href="http://elizavetababanova.com/privacy-policy.html" style="height: 0px !important; display: inline !important;"><em class="clearfix clearer"></em><img src="img/lock-text.png"></a>
								</div>
								<em class="clearfix clearer"></em>
								</div>
								</li> -->
							</ul>
							<input type="hidden" name="webform_id" value="627846" />
						</form>
						<script type="text/javascript" src="http://app.getresponse.com/view_webform.js?wid=627846&mg_param1=1"></script>
					</div>
				</div>
				<div>
					<a ref="http://elizavetababanova.com/privacy-policy.html" class="privacy fs11"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/conf-bg.png"></a>
				</div>
			</div>
		</div>
		<div class="maininfo">
			<div class="cell0"></div>
			<div class="cell">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/cell1.jpg">
				<p>
					<span class="myriadreg fs15 color3">9 элементов, без которых вы не сможете сделать следующий год Лучшим годом Вашей жизни</span>
				</p>
			</div>
			<div class="cell">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/cell2.jpg">
				<p>
					<span class="myriadreg fs15 color3">3 вещи, которые прямо сейчас препятствуют созданию года вашей
						<br />
						мечты</span>
				</p>
			</div>
			<div class="cell">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/cell3.jpg">
				<p>
					<span class="myriadreg fs15 color3">Почему Новый год – одновременно и лучшее время, и самое трудное время для стремительного старта и как вам наиболее эффективно использовать его энергию, чтобы
						<br />
						набрать скорость?</span>
				</p>
			</div>
			<div class="cell">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/cell4.jpg">
				<p>
					<span class="myriadreg fs15 color3">Как не «сгореть и обессилить» уже к концу января? Где брать силы и как поддерживать эмоциональный запал
						<br />
						для последующих прорывов?</span>
				</p>
			</div>
			<div class="cell">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/cell5.jpg">
				<p>
					<span class="myriadreg fs15 color3">Что такое «цикл скуки» и
						<br />
						как его преодолеть?</span>
				</p>
			</div>
		</div>
		<div class="addinfo">
			<div class="addtxt1">
				<span class="myriadreg fs18 color3"> 5 продвигающих упражнений, благодаря которым вы сделаете свой прорыв уже в январе.
					<br />
					<br />
					С помощью них вы снимите свои ограничивающие убеждения и подготовитесь к «полету». </span>
			</div>
			<div class="addtxt2">
				<span class="myriadreg fs26 color3">Будьте готовы к тому, что после этого семинара у вас уже не останется оправданий, почему вы еще не реализовали свою главную мечту в жизнь.</span>
			</div>
		</div>
		<div class="lizainfo">
			<div class="lizaphoto"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/lisa.jpg" alt="Elizaveta Babanova" />
			</div>
			<div class="lizatxt">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-landing/images/lisa-n.png" alt="." />
				<div class="txt">
					<span class="myriadreg fs16"> <span class="colorlight">автор семинара</span>
						<br />
						<br />
						<span class="color6">
							<p>
								В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.
							</p>
							<p>
								Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.
							</p>
							<p>
								Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.
							</p>
							<p>
								С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.
							</p>
							<p>
								Член Международной Ассоциации Непрерывного Образования.
							</p> </span></span>
				</div>
			</div>
			<div class="widget" id="vk_groups">
				<script type="text/javascript">
					VK.Widgets.Group("vk_groups", {
						mode : 0,
						width : "200",
						height : "350"
					}, 44023512);
				</script>
			</div>
		</div>
		<div class="clear"></div>
		<footer class="footer">
			<span class="fs13 color9">
				<p>
					ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены
				</p>
				<nav class="menu3 ">
					<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> | <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> | <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
				</nav> <a href="http://www.elizavetababanova.com/" class="color9" alt="www.elizavetababanova.com">www.elizavetababanova.com</a>
		</footer>
	</body>
</html>

