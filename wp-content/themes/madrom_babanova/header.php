<!DOCTYPE HTML PUBLIC "- //W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" <?php language_attributes(); ?>>
<head>
<title><?php wp_title(); ?></title>

<!-- 11111 -->

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="WordPress <?php echo $wp_version ?>">
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="shortcut icon"  href="<?php echo get_settings('siteurl'); ?>/favicon.ico" type="image/x-icon">  
<link href="/favicon.ico" rel="icon" type="image/x-icon" />
<script type="text/javascript" src="/jwplayer/jwplayer.js" ></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
<meta name="w1-verification" content="172521810958" />

<?php echo get_post_meta($post->ID, 'Title', true); ?>
<meta content="<?php echo get_post_meta($post->ID, 'social', true); ?>" property="og:image">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script src="<?php bloginfo('template_directory'); ?>/js/jquery.flexslider.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/AnchorScroller.js"></script>

<link href='http://fonts.googleapis.com/css?family=Didact+Gothic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>


<script>
  $(document).ready(function(){
    $(".block_spec > div:last-child").addClass("last");
    $(".one_half:nth-child(even)").addClass("last"); 
    $(".news4 .news4_block:first-child .title_news4").addClass("other"); 
       
    $("#header ul#menu-main li:first-child").addClass("fir pie");
    $("#header ul#menu-main li:last-child").addClass("las pie");   
    
     $("#header ul#menu-main li ul li:first-child").removeClass("fir pie");
    $("#header ul#menu-main li ul  li:last-child").removeClass("las pie"); 
    
  });
  </script> 
    



   
 <?php require('social_button_hed.php'); ?>     
    
     

<link rel="alternate" type="application/rss+xml" title="RSS 2.0"
   href="<?php bloginfo('rss2_url'); ?>">
<link rel="alternate" type="text/xml" title="RSS .92"
   href="<?php bloginfo('rss_url'); ?>">
<link rel="alternate" type="application/atom+xml" title="Atom 0.3"
   href="<?php bloginfo('atom_url'); ?>">
<?php wp_head(); ?>  

<!-- Комментарии VK -->
<script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript" charset="windows-1251"></script>


<script type="text/javascript">
 VK.init({apiId:  2461907, onlyWidgets: true});
</script>
</head>

<body>
 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="wrapper1">

	<div id="header">

  <a title="<?php bloginfo('name') ?>" href="<?php bloginfo('siteurl') ?>/">
        <div id="logo_top"></div>
  </a>



<script type="text/javascript">
jQuery(document).ready(function(){
 
jQuery(".menu li").hover(
 
function(){
jQuery(this).find('ul').show();
},
 
function(){
jQuery(this).find('ul').hide();
}
);
$(".menu-main-container").addClass("pie");  
});
</script>

<nav>
<?php wp_nav_menu('menu=main'); ?>
</nav>

<style type="text/css">.simplemodal-login{background: url(http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/lk3.png) no-repeat center top !important;height:42px;width:168px; display:block;}
.go2{position:absolute;margin-top: 20px;
margin-left: 862px;} .go{font-size:0px !important;background: url(http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/lk4.png) no-repeat center top;height: 28px; width: 80px !important; display:block;}
#go{font-size:0px;background: url(http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/lk3.png) no-repeat center top;height:42px;width:168px; display:block;}

.nav a{display:none}
.simplemodal-forgotpw{display:block!important;}
</style>
 <div class="clr"> </div>

 <?php if ((is_front_page()) and (!is_paged())) { ?>
<?php require('flexslider.php'); ?> 
  <?php } ?>
 
  
  </div><!-- #header-->
</div><!-- #wrapper1 -->



<div class="line_shadow"></div>

<!-- Google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35488483-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<meta name='yandex-verification' content='51ab19d66ab002a5' />
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->



<!-- end header -->