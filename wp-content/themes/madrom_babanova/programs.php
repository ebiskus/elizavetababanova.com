<?php
/*
Template Name: programs
*/
?> 

<?php
require('./wp-blog-header.php');
require_once('header.php');
require_once('functions.php');
?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
 bcn_display();
}
?>
</div>

<div class="singl">

<div id="wrap" class="container singl" style="width: 960px;">
  
<div id="content" style="width:960px;">	

<link rel="stylesheet" type="text/css" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/leto2014/style.css">
     <script src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/leto2014/jquery-1.7.1.min.js"></script>
<script src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/leto2014/hoverIntent.min.js"></script>
<script src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/leto2014/clickTip.js"></script>
<link rel="stylesheet" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/leto2014/simpleTip.css" type="text/css" />
	
					<div class="gpp2">
		<a href="javascript:doPopup('#first')" class="gpp-link"></a>
			<script type="text/javascript">// <![CDATA[
			function doPopup(name) {
					if ($(name).is(':visible')) {
						$(name).hide('slow');
					} else {
						$(name).show('slow');
					}
				}
			// ]]></script>
		
			
	 </div>
			<div id="first">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 162px;" class="numbers">2</span>
					<span style="margin-top: 198px;"class="numbers">3</span>
					<span style="margin-top: 270px;" class="numbers">4</span>
						<p>За 10 занятий мы проработаем все ключевые сферы жизни. Вы поймете, в чем вы можете максимально себя реализовать и проанализируете слабые места, которые требуют дополнительного внимания.
						<br><br>
						Создание целостного и ясного видения жизни своей мечты.
						<br><br>
						Структурированный план работы, с помощью которого вы сможете выстроить согласованные цели по всем категориям и пошагово их реализовывать.
						<br><br>
						Необходимый набор технологий, которые помогут вам получить желаемые результаты.</p>
					</div>
					<div class="money">
					<h1>$295</h1>
					</div>
					<div class="srok">
					<p>рекомендуемый - <br>5 недель</p>
					<span>по 2 занятия в неделю</span>
					<span style="margin-left: 153px;" class="tooltip"><em>Однако, вы можете сами либо<br>
					сократить, либо продлить срок<br>
					прохождения программы в<br>
					зависимости от плотности<br>
					вашего расписания.<i></i></em></span></div>
				</div>
				<div id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>10 онлайн-занятий в записи, видео- и аудиоформате, </h1>
						<p>к которым вы получаете доступ в личном кабинете после оплаты. Вы можете просматривать или прослушивать их в любое удобное для вас время.</p>
					</div>
				</div>	
					
			</div>
			
			<div class="by2">
		<a href="javascript:doPopup('#by')" class="by-link"></a>
	 </div>
	 
	 <div id="by">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 269px;" class="numbers">2</span>
					<span style="margin-top: 324px;"class="numbers">3</span>
					<span style="margin-top: 414px;" class="numbers">4</span>
					<span style="margin-top: 522px;" class="numbers">5</span>
						<p>13 феноменальных модулей в прямом эфире и в записи,<br>
						включающих в себя научно обоснованную теоретическую<br>
						часть и насыщенную практическую - для осуществления<br>
						максимального, ощутимого прорыва. В течение курса вы под<br>
						руководством Елизаветы сформируете план действий на год,<br>
						выработаете иммунитет к препятствиям и страхам,<br>
						определите формулу поддержания себя в тонусе, также<br>
						основательно проработаете такие важные темы как эмоции,<br>
						личная сила и харизма, талант, интуиция, деньги, влияние и<br>
						многие другие.
<br><br>
6 мастер-классов с гостями, каждый из которых -<br>
 выдающаяся Личность, авторитет и Мастер в своей нише.
<br><br>
Благодаря основательной и эффективной проработке всех<br>
 основных сфер жизни под руководством тренера, вы<br>
 получите грандиозные результаты в краткосрочной и<br>
 долгосрочной перспективе.
<br><br>
Стратегии и техники для комфортного перехода к жизни<br>
 своей мечты, повышения качества жизни и создания вокруг<br>
 себя пространства для эффективного развития,<br>
 взаимообогащения в вашем окружении, творчества и<br>
 существенного прогресса в работе.
<br><br>
Круг единомышленников - Команда Елизаветы и все<br>
 участники программы объединены в одном информационном<br>
 пространстве. Вы получите доступ к закрытому чату, где <br>
 сможете делиться продвигающей информацией и получать ее,<br>
 обсуждать проработанный материал, делиться опытом.</p>
					</div>
					<div class="money">
					<p style="font-family: Arial;font-size: 14px;padding-top: 46px;margin-bottom: 7px;">
					В зависимости от выбранного<br> вами пакета</p>
					<h1 style="padding-top: 0px; line-height: 25px;">$150 <br>$950<br>$1750</h1>
				
					</div>
					<div style="margin-top: 101px;" class="srok">
					<p>рекомендуемый - 1 год</p>
					<span>по одному занятию в месяц</span>
					
					<span class="tooltip"><em>Однако, вы можете сами либо<br>
					сократить, либо продлить срок<br>
					прохождения программы в<br>
					зависимости от плотности<br>
					вашего расписания.<i></i></em></span>
					
					</div>
				</div>
				<div style="margin-top: 306px;" id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>Пакет “Ежемесячный”</h1> 
						<p>включает в себя доступ к одному вебинару и онлайн<br>
						встречам “Вопрос-Ответ”</p>
						
						<h1 style="padding-top: 0px;">Пакет “Премиум”</h1> 
						<p>13 модулей + доступы к онлайн-вебинарам “Вопрос-Ответ”.<br>
						+ Приглашения на мастер-классы с гостями + Доступы к 6<br>
						вебинарам: “Как побороть лень?”, “Взрыв Энергии”, “Как<br>
						изменить все в своей жизни к лучшему”, “Как найти свое <br>
						профессиональное призвание”, “Как прийти к здоровому <br>
						образу жизни за 45 дней?”, “Как реализовать свой<br>
						потенциал?”</p>
						
						<h1 style="padding-top: 0px;">Пакет “VIP”</h1> 
						<p>13 модулей + Доступы ко всем существующим программам в<br>
						записи: “Живи по Полной!”, “Феномен Мастерства”, “Апгрейд<br>
						Здоровья”, “Апгрейд Эффективности”, “Бизнес Класс”, “Как<br>
						устроиться на работу мечты?” + Доступы к<br>
						онлайн-вебинарам “Вопрос-Ответ” + Приглашения на<br>
						мастер-классы с гостями + Доступы к 6 вебинарам: “Как <br>
						побороть лень?”, “Взрыв Энергии”, “Как изменить все в своей<br>
						жизни к лучшему”, “Как найти свое профессиональное<br>
						призвание”, “Как прийти к здоровому образу жизни за 45<br>
						дней?”, “Как реализовать свой потенциал?”. </p>
					
					
					</div>
					<div style="left: 0px;" class="left2">
					<h4>$150</h4>
					<p style="width: 226px;">если приобретать помодульно<br>Таким образом, стоимость годовой программы - $1800</p>
					</div>
					<div style="left: 51px;margin-top: -426px;"class="left2">
					<h4 >$950</h4>
					<p>все 13 модулей<br>при единоразовой оплате</p></div>
					<div style="margin-top: -250px;left: 51px;" class="left2">
					<h4 >$1750</h4>
					<p>при единоразовой оплате</p></div>
				</div>	
				<div style="margin-top: -20px;" class="line">	
				<h3>Также у вас есть возможность приобрести каждый модуль<br>
				отдельно для точечной проработки интересующей вас темы.</h3>
				</div>	
	</div>
			
			
	 <div class="fm">
		<a href="javascript:doPopup('#fm')" class="fm-link"></a>
	 </div>
			<div id="fm">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 127px;" class="numbers">2</span>
					<span style="margin-top: 235px;"class="numbers">3</span>
					<span style="margin-top: 306px;" class="numbers">4</span>
					<span style="margin-top: 379px;" class="numbers">5</span>
					<span style="margin-top: 469px;" class="numbers">6</span>
						<p>Квинтэссенцию самых передовых научных исследований,<br> применив которые вы сможете вскрыть чип-код своего Гения.<br><br>

							Платформу для качественной практической работы над<br> дизайном своей жизни. На курсе вы получите большое<br> количество полезных упражнений, которые помогут вам<br> устранить сдерживающие факторы на пути к вашему<br> мастерству.
						<br><br>
							Вдохновляющие, грандиозные истории успеха великих<br> людей, их подробный анализ, как результат - определение<br> возможности проецирования большого успеха в свою жизнь. 
						<br><br>
							Инструменты для внедрения элементов дизайна жизни<br> Мастера в свою деятельность в краткосрочной и<br> долгосрочной перспективе. 
						<br><br>
							Эффективные тактики по увеличению своего дохода,<br> качественному управлению временем, созданию<br> взаимообогащающего круга друзей и знакомых,<br> выстраиванию своего бренда.
						<br><br>
							Бонусные мастер-классы с людьми, которые уже вошли в топ<br> 1% своей индустрии. Среди гостей курса – Ирина Хакамада.</p>
					</div>
					<div class="money">
					<h1>$660</h1>
					</div>
					<div style="margin-top: 90px;" class="srok">
					<p>рекомендуемый - <br>4 недель</p>
					<span>по 2 занятия в неделю</span>
					<span style="margin-left: 153px;"class="tooltip"><em>Однако, вы можете сами либо<br>
					сократить, либо продлить срок<br>
					прохождения программы в<br>
					зависимости от плотности<br>
					вашего расписания.<i></i></em></span></div>
				</div>
				<div style="margin-top: 225px;" id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>8 видеозаписей онлайн-занятий, <br>6 мастер-классов, <br>5 продвигающих вебинаров, </h1>
						<p>к которым вы получаете доступ в личном кабинете после оплаты. Вы можете просматривать или прослушивать их в любое удобное для вас время.</p>
					</div>
				</div>	
	</div>
	 
	 <div class="rm">
		<a href="javascript:doPopup('#rm')" class="rm-link"></a>
	 </div>
	 <div id="rm">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 126px;" class="numbers">2</span>
					<span style="margin-top: 199px;"class="numbers">3</span>
					<span style="margin-top: 253px;" class="numbers">4</span>
					<span style="margin-top: 306px;" class="numbers">5</span>
					<span style="margin-top: 361px;" class="numbers">6</span>
						<p>Проверенные методики по определению своего<br> профессионального призвания.
<br><br>
План действий по устройству на работу мечты, который вы<br> самостоятельно заточите в результате глубокой проработки<br> принципов получения желаемой должности.
<br><br>
Точечную и исчерпывающую информацию по качественному <br>составлению резюме.
<br><br>
Технологию по 100% попаданию на собеседование в <br>компанию своей мечты.
<br><br>
Важные тонкости для успешного прохождения <br>собеседования.
<br><br>
Мастер-класс с руководителем крупного московского<br> HR-агенства о том, как стать желаемым кандидатом в любой<br> нише.</p>
					</div>
					<div class="money">
					<h1>$195</h1>
					</div>
					<div style="margin-top: 12px;" class="srok">
					<p>рекомендуемый - <br>1,5 недели</p>
					</div>
				</div>
				<div style="margin-top: 128px;" id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>видеозаписи 2-х онлайн-занятий, </h1>
						<h2>1 мастер-класса,</h2>
						<h2>продвигающий вебинар <br>«Как найти свое профессиональное призвание?»,</h2>
						<h2>запись элитного вебинара «Феномен Мастерства» на тему Networking,</h2>
						<p>к которым вы получаете доступ в личном кабинете после оплаты. Вы можете просматривать или прослушивать их в любое удобное для вас время.</p>
					</div>
				</div>	
	</div>
	 
	 <div class="bc">
		<a href="javascript:doPopup('#bc')" class="bc-link"></a>
	 </div>
	 
	 <div id="bc">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 144px;" class="numbers">2</span>
					<span style="margin-top: 216px;"class="numbers">3</span>
					<span style="margin-top: 289px;" class="numbers">4</span>
					<span style="margin-top: 343px;" class="numbers">5</span>
					<span style="margin-top: 415px;" class="numbers">6</span>
					<span style="margin-top: 468px;" class="numbers">7</span>
						<p>7 мощнейших онлайн-занятий в записи, содержащих самую<br> необходимую, структурированную информацию о построении<br> и развитии бизнеса. 
<br><br>
Практические упражнения, с помощью которых вы четко<br> обозначите цели своего бизнеса. Например, вы поймёте, кто<br> является вашей главной целевой аудиторией.
<br><br>
Научитесь составлять полезный рабочий бизнес-план и<br> найдёте ключевое преимущество, которое сделает вас<br> лидером в вашей отрасли.
<br><br>
Инструменты для построения бизнес-модели, работающей<br> как часы.
<br><br>
3 мощные стратегии, которые помогут увеличить доход<br> вашего бизнеса на 30%, и 27 стратегий увеличения дохода<br> компании без вовлечения дополнительных инвестиций.
<br><br>
Узнаете о том, как внедрить ценности лидера в коллектив и о<br> том, как вырастить сильного, эффективного лидера.
<br><br>
2 мастер-класса с бизнесменами-создателями успешных <br>компаний с оборотом 1 млрд и 1,5 млрд рублей</p>
					</div>
					<div class="money">
					<h1>$630</h1>
					</div>
					<div style="margin-top: 90px;" class="srok">
					<p>рекомендуемый - <br>5 недель</p>
					<span>по 2 занятия в неделю</span>
					<span style="margin-left: 135px;" class="tooltip"><em>Однако, вы можете сами либо<br>
					сократить, либо продлить срок<br>
					прохождения программы в<br>
					зависимости от плотности<br>
					вашего расписания.<i></i></em></span></div>
				</div>
				<div style="margin-top: 225px;" id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>7 вебинаров в записи (видео- и<br> аудиоформат), </h1>
						<h2>2 мастер-класса с бизнесменами,<br> создавшими успешные компании —</h2>
						<p>доступ к этим материалам вы получаете в личном кабинете<br> после оплаты. Вы можете просматривать или прослушивать<br> их в любое удобное для вас время. </p>
					</div>
				</div>	
	</div>
	 
	 <div class="az">
		<a href="javascript:doPopup('#az')" class="az-link"></a>
	 </div>
					<div id="az">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 180px;" class="numbers">2</span>
					<span style="margin-top: 252px;"class="numbers">3</span>
					<span style="margin-top: 325px;" class="numbers">4</span>
					<span style="margin-top: 396px;" class="numbers">5</span>
					<span style="margin-top: 433px;" class="numbers">6</span>
						<p>В начале программы вы сформируете Декларацию (список<br> целей в сфере здоровья, новых привычек, которые вы хотите<br> внедрить в свою жизнь). В течение 45 дней каждое утро вы<br> будете получать статью по здоровому образу жизни на свою<br> электронную почту.
<br><br>
Доступ к записям трех уникальных мастер-классов от <br>выдающихся экспертов по здоровому образу жизни в сфере <br>науки и медицины, йоги и Аюрведы, фитнеса.
<br><br>
Набор рекомендаций и методик, которые помогут вам <br>последовательно выработать индивидуальный подход к<br> здоровому образу жизни.
<br><br>
Как результат в вашу жизнь войдут здоровые привычки,<br> которые вы так давно хотели выработать, но по каким-то<br> причинам срывались.
<br><br>
Программу по избавлению от вредных привычек. 
<br><br>
Понимание истинных потребностей своего организма и<br> составление идеального режима дня, сна и питания, <br>способствующего достижению ваших глобальных жизненных<br> целей.</p>
					</div>
					<div class="money">
					<p style="font-family: Arial;font-size: 14px;padding-top: 46px;">
					Вы выбираете такую<br> сумму, чтобы потеря<br> денег без результата стала для вас<br> ощутимой утратой -</p>
					<h1>3000 руб, <br>5000 руб, <br>7500 руб, <br>10 000 руб.</h1>
					<p>или больше</p>
					</div>
					<div style="margin-top: 90px;" class="srok">
					<p>рекомендуемый - <br>45 дней</p>
					<span>по 1 занятию в день</span></div>
				</div>
				<div style="margin-top: 225px;" id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>45 продвигающих статей,</h1> 
						<h2>видеозаписи 3 мастер-классов</h2>
						<p>с ведущими экспертами по здоровому образу жизни.<br> Материалы будут последовательно приходить на вашу<br> электронную почту.</p>
					</div>
				</div>	
	</div>
	 
	 
	 
	 
	 <div class="aef">
		<a href="javascript:doPopup('#aef')" class="aef-link"></a>
	 </div>
	 
	 <div id="aef">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 234px;" class="numbers">2</span>
					<span style="margin-top: 306px;"class="numbers">3</span>
					<span style="margin-top: 378px;" class="numbers">4</span>
						<p>В начале программы вы сформируете Декларацию - список<br> целей в сфере личной эффективности, включающий новые<br> привычки и изменения, которые вы хотите внедрить. В<br> первый день вы прослушаете запись фрагмента мощного<br> курса “Феномен Мастерства” - “Дизайн жизни мастера”, а в<br> течение следующих 20 дней вы будете получать материалы,<br> которые помогут вам закрепить новые настройки<br> эффективности.
<br><br>
Практические упражнения, с помощью которых вы научитесь<br> быстро попадать на пик своей эффективности.
<br><br>
Техники, позволяющие работать в максимально<br> эффективном режиме, лучшие тактики по оптимизированию<br> и планированию своего времени от мировых экспертов.
<br><br>
Проверенные методики соблюдения баланса эффективности<br> во всех сферах вашей жизни.</p>
					</div>
					<div class="money">
					<h1>$150</h1>
					</div>
					<div style="margin-top: 4px;" class="srok">
					<p>рекомендуемый - <br>21 день</p>
					<span>по 1 занятию в день</span>
					</div>
				</div>
				<div  id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>Данные доступа к 21 статье с<br> продвигающей информацией</h1>
						
						<p>будут открыты в личном кабинете, который мы создадим для<br> вас после оплаты курса. </p>
					</div>
				</div>	
	</div>
	 
	 <div class="ae">
		<a href="javascript:doPopup('#ae')" class="ae-link"></a>
	 </div>
	 
	 <div id="ae">
				<div class="line">
					<div class="get">
					<span style="margin-top: 73px;" class="numbers">1</span>
					<span style="margin-top: 143px;" class="numbers">2</span>
					<span style="margin-top: 252px;"class="numbers">3</span>
					<span style="margin-top: 324px;" class="numbers">4</span>
					
						<p>Каждое утро в течение 21 дня - новая статья со<br>структурированной информацией, с помощью которой вы<br>
						сможете настроить себя на счастливый, успешный день.
<br><br>
Научно обоснованные доказательства эффективности<br>
 инновационного подхода к достижению счастья, в результате<br>
 проработки и осмысления которых счастье станет<br>
 неотъемлемой частью вашей жизни, не зависящей от<br>
 обстоятельств вокруг вас.
<br><br>
Ряд практических упражнений, тактик, нацеленных на<br>
 закрепление новой парадигмы мышления и поведения<br>
 счастливого человека.
<br><br>
Возможность четко отследить свой прорыв – прохождение<br>
 специализированного теста в начале и в конце курса.</p>
					</div>
					<div class="money">
					<h1>$150</h1>
					</div>
					<div style="margin-top: -53px;" class="srok">
					<p>рекомендуемый - <br>21 день</p>
					<span>по 1 занятию в день</span>
					<span style="margin-left: 135px;"class="tooltip"><em>Доступ к следующему занятию<br>
					вы получаете после ответа на<br> 
					простой контрольный вопрос,<br> 
					который служит<br> 
					свидетельством того, что<br> 
					занятие предыдущего дня<br> 
					пройдено.<i></i></em></span></div>
				</div>
				<div id="clear"></div>
				<div class="line">	
					<div class="format">
						<h1>Вебинар в записи «Как перестать <br>надрываться и стать счастливым?»,</h1>
						<h2>онлайн тест на выявление уровня<br> счастья в жизни,</h2>
						<h2>21 занятие в форме ежедневной<br>статьи,</h2>
						<p>к которым вы получаете доступ в личном кабинете после оплаты. </p>
					</div>
				</div>	
	</div>
	 
</div><!-- #content-->
<p>&nbsp;</p>




 </div><!-- #wrapper2 --> 
 
</div>

 <div class="line_shadow"></div>
 
 <div id="wrapper3">

<div id="footer">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/about_foto.png">
				<div class="about">
					<div class="about_name">Елизавета Бабанова</div>
					<div class="about_autor">автор программ</div>
					<div class="about_desc">
						<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: 
							Экономика, Финансы, Маркетинг, Менеджмент.</p>
						<p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
						<p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
						<p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
						<p>Член Международной Ассоциации Непрерывного Образования.</p>
					</div>
	            </div>
	            <div style="width:200px;" class="vk">
						<!-- VK Widget -->
						<iframe width="200" scrolling="no" height="200" frameborder="0" name="fXD32f6b" src="//vk.com/widget_community.php?app=0&amp;width=200px&amp;gid=44023512&amp;mode=0&amp;height=400&amp;url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&amp;13b1da20c61" id="vkwidget1" style="overflow: hidden; height: 400px;"></iframe>
				</div>

			</div><!--footer-->
                     
             <div style="clear:both"></div>      
            
            <div class="footer_info">
				<div class="copy" >ИП Бабанова Елизавета Дмитриевна   ИНН 370259937869   2013 Все права защищены</div>
				<nav style="width: 483px;
margin: 0 auto;" class="bottom_menu">
					<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
					<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
					<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
				</nav>
			</div>

</div><!-- #wrapper3 -->