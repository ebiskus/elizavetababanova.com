<?php
/*
Template Name: Страница 5stages/5stages-5-phenomen
*/
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="title" content="Феномен Мастерства" />
    <meta name="description" content="В новом видео я расскажу вам о главном законе жизни, а также о том, почему большинство богатых людей несчастны, и как вам избежать их участия." />
    <meta property="fb:admins" content="100003273128680"/>
    <title>Феномен мастерства</title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/css/style.css" rel="stylesheet">
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/js/custom.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">
        VK.init({apiId:  2461907, onlyWidgets: true});
    </script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
    <link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-5/img/photo.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <section class="main">
        <header class="header">
            <div class="inner">
                <a class="logo" href="http://elizavetababanova.com/" target="_blank">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b1.png" alt=".">
                </a>
                <nav class="menu">
                    <a target="_blank" href="http://vk.com/elizaveta.babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b2.png" alt=".">
                    </a>
                    <a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b3.png" alt=".">
                    </a>
                    <a target="_blank" href="http://twitter.com/liza_babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b4.png" alt=".">
                    </a>
                    <a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b5.png" alt=".">
                    </a>
                </nav>
            </div>
            <i class="border"></i>
        </header>
        <div class="inner">
            <nav class="menu2">
                <a class="i i1 threerow" href="http://elizavetababanova.com/5stages">5 уровней<br>развития<br>человека</a>
                <a class="i i7" href="http://elizavetababanova.com/research">Исследования</a>
                <a class="i i2 tworow" href="http://elizavetababanova.com/survey">Результаты<br>опроса</a>
                <a class="i i3 tworow" href="http://elizavetababanova.com/6-moih-vectorov">6 моих<br>векторов</a>
                <a class="i i4 tworow active" href="">Феномен<br>Мастерства</a>
                <a class="i i8 threerow" href="http://elizavetababanova.com/kak-realizovat-potentsial">Как<br>реализовать<br>потенциал</a>
                <a class="i i5 tworow" href="http://elizavetababanova.com/programma-treninga">Программа<br>тренинга</a>
                <a class="i i6 tworow" href="http://elizavetababanova.com/gosti-fm">Гости<br>программы</a>
            </nav>
            <section class="block">
                <div class="i i1">
                    <p class="title"><span class="custom">Ф</span>еномен Мастерства</p>
                    <section class="body">
                    <iframe width="880" height="495" src="http://www.youtube.com/embed/2TAMJwrSB5Q?rel=0" frameborder="0" allowfullscreen></iframe>
                    </section>
                </div>
            </section>
            <center>
<!--
                <p style="padding-top: 31px;font-size: 18px;line-height: 24px;width:700px;">Регистрация на 1 сезон программы завершена. Если вы хотите приобрести 1 сезон в записи или записаться на 2 сезон, оставьте свои данные, нажав кнопку "Подать заявку". Вы будете первым, кто узнает о датах следующего сезона.</p>
-->
                <p style="padding-top: 31px;font-size: 18px;line-height: 24px;width:700px;">Регистрируйтесь на 2 сезон:</p>
                <a href="http://elizavetababanova.com/programma-treninga" target="_blank">
                    <img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-5/img/prog-but.jpg" alt="." style="padding-top: 40px;  padding-left: 10px;  padding-bottom: 40px;" width="49%">
                </a>
                <a href="http://elizavetababanova.com/form5/" target="_blank">
                    <img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-6/img/za-but.jpg" alt="." style="padding-top:40px;padding-left:30px;padding-bottom: 40px;" width="37%">
                </a>
            </center>



            <section class="block2">
                <div class="block3">
                    <span class="i">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b7.png" alt=".">
                    </span>
                    <span class="i">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b8.png" alt=".">
                    </span>
                    <span class="i">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b9.png" alt=".">
                    </span>
                    <span class="i">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b10.png" alt=".">
                    </span>
                    <span class="i">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b11.png" alt=".">
                    </span>
                    <span class="i">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b12.png" alt=".">
                    </span>
                    <p class="text">Они уже изменили свою жизнь</p>
                </div>
                <div class="block4">
                    <img class="i img" src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b13.png" alt=".">
                    <section class="i map">
                        <a href="http://elizavetababanova.com/otzivy" target="_blank"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b14.png" alt="."></a>
                    </section>
                </div>
            </section>

            <i class="border2"></i>
            <section class="block5">Не воспроизводится видео? <a href="http://get.adobe.com/flashplayer/" target="_blank">Обновите</a> ваш Flash Player</section>
            <i class="border3"></i>
            <section class="block6">
                <div class="i i1"><a href="http://vkontakte.ru/share.php?url=http://elizavetababanova.com/phenomen-masterstva" target="_blank">Расскажите</a> о видео друзьям ВКонтакте! </div>
                <div class="i i2"><a href="http://twitter.com/intent/tweet?text=Новое видео Елизаветы Бабановой о главном законе жизни, а также о том, почему большинство богатых людей несчастны: http://elizavetababanova.com/phenomen-masterstva" target="_blank">Твитнуть</a> о 5 уровнях развития человека!</div>
            </section>
            <i class="border3"></i>
            <section class="block7">
                <img class="img" src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-5/img/b15.png" alt=".">
                <div class="facebook">
                <div class="fb-like" data-href="http://elizavetababanova.com/phenomen-masterstva" data-send="false" data-width="280" data-show-faces="true"></div></div>
            </section>
            <i class="border3"></i>
            <section class="block8">
                <div class="left">
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                    VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
                    </script>
                </div>
                <div class="right">
                    <div class="fb-comments" data-href="http://elizavetababanova.com/phenomen-masterstva" data-width="400" data-num-posts="5"></div>
                </div>
            </section>
            <section class="block8">
            <?php comments_template('/comments2.php'); ?>
            </section>
            <footer class="footer">
                <i class="border6"></i>
                <p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
                <nav class="menu3">
                    <a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
                    <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
                    <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
                </nav>
            </footer>
        </div>
    </section>

</body>
</html>