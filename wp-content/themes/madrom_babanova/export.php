<?
//подключаем файл конфиг
include_once 'config.php';

//подключаем и создаем класс PHPExcel
include_once 'library/PHPExcel.php';
$pExcel = new PHPExcel();

//стили оформления

	$baseFont = array(
		'font'=>array(
			'name'=>'Arial Cyr',
			'size'=>'10',
			'bold'=>false
		),
		'borders' => array(
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
					'outline' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('argb' => 'FF000000'),
					)
	));
	$headerFont = array(
		'font'=>array(
			'name'=>'Arial Cyr',
			'size'=>'11',
			'bold'=>true
		));

	$boldFont = array(
		'font'=>array(
			'name'=>'Arial Cyr',
			'size'=>'10',
			'bold'=>true
		),
		'borders' => array(
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			'outline' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
	));
	$center = array(
		'alignment'=>array(
			'horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP
		)
	);
	$left = array(
		'alignment'=>array(
			'horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP
		)
	);

	$pExcel->setActiveSheetIndex(0);
	$aSheet = $pExcel->getActiveSheet();
	$aSheet->setTitle($name_list);       //задаем название листа (взята переменная из файла config)
	$pExcel->createSheet(1);

	//если пришли данные с формы, делаем выборку соответствующую выборку данных из бд
	if(isset($_POST['submit'])){
		switch($_POST['param']){
			case "day":
				$date = strtotime("-1 day");break;
			case "week":
				$date = strtotime("-1 week");break;
			case "month":
				$date = strtotime("-1 month");break;
			case "all":
				$date = ''; break;					

		}
		 if($date !=""){
			$date = date("Y-m-d", $date);
			$date = $date.' 00:00:00';
			//выбираем данные из бд по необходимой дате
			$sql = mysql_query("select * from form where create_date >= '$date' order by create_date asc");
			$sql_for_count = mysql_query("select count(*) from form where create_date >= '$date'");
		 }else{
			$sql = mysql_query("select * from form order by create_date asc");
			$sql_for_count = mysql_query("select count(*) from form");
		 }
	}else{
		$sql = mysql_query("select * from form order by create_date asc");
		$sql_for_count = mysql_query("select count(*) from form");
	}
	$count = mysql_fetch_row($sql_for_count);//кол-во записей

	//название полей в файле Excel
	$aSheet->setCellValue('A2', '№ п.п');
	$aSheet->setCellValue('B2', 'Имя');
	$aSheet->setCellValue('C2', 'Фамилия');
	$aSheet->setCellValue('D2', 'Город');
	$aSheet->setCellValue('E2', 'Телефон');
	$aSheet->setCellValue('F2', 'E-mail');
	$aSheet->setCellValue('G2', 'skype');
	$aSheet->setCellValue('H2', 'Откуда узнали?');
	$aSheet->setCellValue('I2', 'Дата');

	$n = 1; //счетчит для поля № п.п
	$yach = 3; //счетчик для записи в поля

	//массив соответствия вариантов ответа на вопрос "Откуда узнали?"
	$where = array(
		"1"=>"Публичные страницы Вконтакте",
		"2"=>"Приглашение на мероприятие Вконтакте",
		"3"=>"Из рассылки Алекса Яновского",
		"4"=>"Нашел блог в поисковике",
		"5"=>"Страница в Facebook",
		"6"=>"Страница в Twitter",
		"7"=>"Другое"
	);	

	if($count ==0){
	//если данных нет выведем сообщение и прекратим дальнейшую работу
	 echo "За указанный период нет данных";die;
	}

	while($row = mysql_fetch_assoc($sql)){
		list($date,$time) = explode(" ",$row['create_date']);
		list($y,$m,$d) = explode('-',$date);
		$dvDate = $d.'-'.$m.'-'.$y.' г.'.' '.$time;
		
		if($row['where']==7) $where_data = $where[7].":\n".$row['other'];
		else{$where_data = $where[$row['where']];}
		
		//заполняем ячейки самими данными
		$aSheet->setCellValue('A'.$yach, $n);
		$aSheet->setCellValue('B'.$yach, $row['name'] );
		$aSheet->setCellValue('C'.$yach, $row['lastname']);
		$aSheet->setCellValue('D'.$yach, $row['city']);
		$aSheet->setCellValue('E'.$yach, $row['phone']);
		$aSheet->setCellValue('F'.$yach, $row['email']);
		$aSheet->setCellValue('G'.$yach, $row['skype']);
		$aSheet->setCellValue('H'.$yach, $where_data);
		$aSheet->setCellValue('I'.$yach, $dvDate);

		//применяем стили оформления ячеек
		$aSheet->getStyle('A'.$yach)->applyFromArray($baseFont)->applyFromArray($center);
		$aSheet->getStyle('B'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);
		$aSheet->getStyle('C'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);
		$aSheet->getStyle('D'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);
		$aSheet->getStyle('E'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);
		$aSheet->getStyle('F'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);
		$aSheet->getStyle('G'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);
		$aSheet->getStyle('H'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);
		$aSheet->getStyle('I'.$yach)->applyFromArray($baseFont)->getAlignment()->setWrapText(true);

		$n++;$yach++;
	}

		//применяем стили оформления ячеек в которых находятся названия полей
		$aSheet->getStyle('A2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('B2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('C2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('D2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('E2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('F2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('G2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('H2')->applyFromArray($boldFont)->applyFromArray($center);
		$aSheet->getStyle('I2')->applyFromArray($boldFont)->applyFromArray($center);
		
		//устанавливаем ширину ячеек
		$aSheet->getColumnDimension('B')->setWidth(24);
		$aSheet->getColumnDimension('C')->setWidth(24);
		$aSheet->getColumnDimension('D')->setWidth(24);
		$aSheet->getColumnDimension('F')->setWidth(24);
		$aSheet->getColumnDimension('E')->setWidth(24);
		$aSheet->getColumnDimension('G')->setWidth(24);
		$aSheet->getColumnDimension('H')->setWidth(24);
		$aSheet->getColumnDimension('I')->setWidth(24);

		if($n!=1){
			//отдаем пользователю в браузер
			include("library/PHPExcel/Writer/Excel5.php");
			$objWriter = new PHPExcel_Writer_Excel5($pExcel);
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$name_file.'"');
			header('Cache-Control: max-age=0');
			$objWriter->save('php://output');
		}else{
			
			echo "За указанный период нет данных";
		}

?>