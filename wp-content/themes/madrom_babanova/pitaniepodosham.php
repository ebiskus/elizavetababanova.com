<?php
/*
Template Name: Таблица совместимости пищевых продуктов
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>

    <title>Таблица совместимости пищевых продуктов</title>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/pitaniepodosham/style.css" rel="stylesheet">

    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/custom.js"></script>

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">VK.init({apiId:  2461907, onlyWidgets: true});</script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>

    <meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/pitaniepodosham/images/pic-share-3.png">
    <meta property="og:title" content="Таблица совместимости пищевых продуктов"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://elizavetababanova.com/pitaniepodosham"/>
    <meta name="description" content="Таблица совместимости пищевых продуктов">

<meta property="og:description" content="Таблица совместимости пищевых продуктов" />


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div>
            <img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

    <!--Google-->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35488483-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!--/google-->

</head>
<body>

    <div id="fb-root"></div>
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    <div id="fb-root"></div>

    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

	<?php
		require('./wp-blog-header.php');
		require_once('header.php');
		require_once('functions.php');
	?>
	
    <section class="main">
        <div class="inner">
			<a title="<?php bloginfo('name') ?>" href="<?php bloginfo('siteurl') ?>/">
				<div id="logo_top"></div>
			</a>
			
			
			
	<div style="padding: 20px 0px 20px 0px; font-size: 15px; font-weight: bold; text-align: justify; line-height: 180%;">Рекомендации по приему пищи для основных типов конституции.

<br><br>Примечание: Рекомендации приведенные в таблице являются общими. Для удовлетворения индивидуальных потребностей следует внести специальные поправки напр. пищевые аллергии, сила агни, время года и степень доминирования доши или ухудшение состояния здоровья. 

<br><br>* нормально при умеренном потреблении
<br>** нормально при редком употреблении

<div style="display: none;"><img src=/wp-content/themes/madrom_babanova/pitaniepodosham/images/pic-share-1.png ></div>




</div>		
			
			
			
			
            <div class="image">
                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/pitaniepodosham/images/table.png">
            </div>


<div style="padding: 20px 0px 20px 0px; font-size: 15px; text-align: justify; line-height: 180%;"><i>Институт Аюрведы, штат Нью Мексико, США</i></div>			
			
			
			
			<div id="div-social">
				<?php
					require('social_button.php');
				?> 
			</div>
        </div>
		
	    <section class="block8">
			<div class="left">
				<div id="vk_comments"></div>
				<script type="text/javascript">
					VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
				</script>
			</div>
			<div class="right">
				<div class="fb-comments" data-href="http://elizavetababanova.com/pitaniepodosham/" data-width="400" data-num-posts="5"></div>
			</div>
		</section>
		<div class="clr"></div>
		<section class="block8">
			<?php comments_template('/comments2.php'); ?>
		</section>
		<footer class="footer">
			<i class="border6"></i>
			<p class="text1">ИП Бабанова Елизавета Дмитриевна &nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2014 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
		</footer>		
    </section>

<div class="lint">
        <!--LiveInternet counter--><script type="text/javascript"><!--
        document.write("<a href='http://www.liveinternet.ru/click' "+
        "target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";"+Math.random()+
        "' alt='' title='LiveInternet' "+
        "border='0' width='0' height='0'><\/a>")
        //--></script><!--/LiveInternet-->
    </div>


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>

    <noscript>
        <div>
            <img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

</body>
</html>