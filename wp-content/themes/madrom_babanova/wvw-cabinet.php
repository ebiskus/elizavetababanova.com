<?php
foreach($access_categories as $ac_cat):
	$cur_cat = get_term($ac_cat, 'videos');
	$cur_posts = new WP_Query( array(
					'post_type' => 'video',
					'tax_query' => array(
						array(
							'taxonomy' => 'videos',
							'field' => 'id',
							'terms' => $ac_cat)
							) 
					));

			 
	if($cur_posts->post_count):
		echo "<h3>$cur_cat->name</h3>"; //Название категории
		echo "<ul><style>.nomargin {margin: 0!important;padding-left: 200px;padding-top: 82px;}.page-title {
position: relative;
z-index: 2;
overflow: hidden;
padding:0;
color: black;
}.site-wrap {background: #5D6E7A;}.main-content{background: #5D6E7A;} li a{color:black;} h3{color: black;font-weight: bold;}
#social_button {

visibility: hidden;
display: none;
}</style>"; //Контейнер списка видео в категории
			while($cur_posts->have_posts() ) { 
				$cur_posts->the_post();
				echo '<li><a href="' . get_permalink() . '" title="'. get_the_title() .'">' . get_the_title() . '</a></li>'; //Вывод ссылки на видео
			}
			$print_array[] = get_the_ID();
		echo "</ul>";
		wp_reset_postdata();
	endif;
endforeach;

if(count($access_videos))
	foreach($access_videos as $bonus):
		if(!in_array(wvw_id($bonus), $print_array)) 
			$bonusess[] = wvw_id($bonus);
	endforeach;

if(count($bonusess)):
	echo "<h3>Апгрейд Эмоций</h3>"; //Вывод бонусных видео
		echo "<ul><style>.nomargin {margin: 0!important;padding-left: 200px;padding-top: 82px;}.page-title {
position: relative;
z-index: 2;
overflow: hidden;
padding:0;
color: #fff;
}.site-wrap {background: #5D6E7A;}.main-content{background: #5D6E7A;} li a{color:black;} h3{color: black;font-weight: bold;}</style>"; //Контейнер списка бонусных видео
			foreach($bonusess as $bonus):
				echo '<li><a href="' . get_permalink($bonus) . '" title="'. get_the_title($bonus) .'">' . get_the_title($bonus) . '</a></li>'; //Вывод ссылки на бонусные видео.
			endforeach;
		echo "</ul>";
endif;