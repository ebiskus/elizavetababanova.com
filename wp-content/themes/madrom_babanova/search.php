﻿<?php
require('./wp-blog-header.php');
require_once('header.php');
require_once('functions.php');
?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
 bcn_display();
}
?>
</div>
    
<div id="wrapper2"> 

      <div class="clr"> </div>   
  </div>

  <div id="wrap" class="container">
	
<div id="content" class="eleven columns">   
		
  
  <div class="news4"> 
   



<?php   
if (have_posts()) : while (have_posts()) : the_post(); ?>


<div class="category_news_block pie"> 

<div>
<div class="title_category_news_block avant_book">
<img class="mir" src="<?php bloginfo('template_directory'); ?>/image/mir.jpg"/>
<?php
if (!is_single() && !is_page() ) { 
   echo '<a href="'; 
   the_permalink() ;
   echo '">';
   }; 
the_title(); 
if ( !is_single() && !is_page() ) echo '</a>';
?>

<?php 
if (!is_page()) 
edit_post_link(' [edit]');
?>
</div>

<div class="stat_shadow"> </div>

<div class="category_news_block_text">
<?php
if ( ($post->post_excerpt != '') && (!is_single()) ) { 
	the_excerpt(); 
	} 
else the_content(''); 
?>
</div>
<div class="clr"> </div>  
</div>  
 </div> 
 
<div class="dalee_coment"> 

<div class="coment avant_book pie"> 
<img class="com" src="<?php bloginfo('template_directory'); ?>/image/com.jpg"/>
<?php comments_popup_link('Оставьте комментарий!',
    'Пока один комментарий', '% Комментариев'); ?>
<?php comments_template(); ?>  
</div>

<div class="dalee avant_book pie">    
 <?php
 
if (!is_single() && !is_page() ) { 
   echo '<a href="'; 
   the_permalink() ;
   echo ' ">Читать дальше >></a>';
   }

?>  
 </div>
<div class="clr"> </div>   
  
 </div>
<div class="clr"> </div> 
 
 
<?php endwhile; ?>  

 <div class="clr"> </div>
 
 	<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Ничего не найдено', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Простите, поиск не дал результатов. Попробуйте использовать другие слова.', 'twentyeleven' ); ?></p>
<ul class="searchbar pie">  
<?php require('searchform.php'); ?>
</ul>
<div class="clr"> </div>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
 
 
 
</div>

<?php 
if ( !is_single() && !is_page() ) {
   echo '<div class="navpage">';
   next_posts_link('&laquo; Ранее');
   echo '&nbsp;&nbsp;&nbsp;';
   previous_posts_link('Позже &raquo;');
   echo '</div >';
   }
?>


  

  
</div><!-- #content-->








   
<?php
  require('sidebar.php');
?>

 </div><!-- #wrap -->   		

<div class="line_shadow"></div>


<div id="wrapper3">

<div id="footer">
<?php require('footer.php'); ?>
</div><!-- #footer --> 

</div><!-- #wrapper3 -->