<?php
/*
 Template Name: Страница трансляции вебинара Лучший год
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta property="fb:admins" content="100003273128680"/>
		<meta property="vk:app_id" content="3398000" />

		<title>Лучший год Вашей жизни</title>

		<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/images/share.jpg">
		
		<meta property="og:title" content="Лучший год Вашей жизни"/>
		<meta property="og:type" content="article"/>
		<meta property="og:url" content="http://elizavetababanova.com/best-year-webinar"/>
		<meta name="description" content="Как сделать следующий год лучшим годом Вашей жизни?">

		<!-- <link rel="stylesheet" type="text/css" href="style/style.css"> -->
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
		<link href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/style/style.css" rel="stylesheet">

		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.js"></script>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/plugins.js"></script>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/script.js"></script>

		<script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
		<script type="text/javascript">
			VK.init({
				apiId : 2461907,
				onlyWidgets : true
			});
		</script>
		<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>

		<!-- <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script src="js/script.js"></script> -->

		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

		<!--Google-->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-35488483-1']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<!--/google-->
	</head>
	<body>

		<div id="fb-root"></div>
		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>

		<header class="header">
			<div class="topwrapper">
				<div class="inner">
					<a class="logo" href="http://elizavetababanova.com/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/images/logo.png" alt="elizavetababanova.com"></a>
					<nav class="socicon">
						<a target="_blank" href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/images/vk.png" alt="vk"></a>
						<a target="_blank" href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/images/fb.png" alt="fb"></a>
						<a target="_blank" href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/images/tw.png" alt="tw"></a>
						<a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/images/yt.png" alt="yt"></a>
					</nav>
				</div>
			</div>
		</header>

		<div class="wrapper">
			<div class="mainpic">

    <?php if ( post_password_required() ) {?>
	    <div class="pass">
            <?php
                the_content('Читать далее &raquo;');
                if ( post_password_required() ) {
                    return;
                }
            ?>
        </div>
    <?php } ?>



				<div class="video">
					<!--<iframe style="border: 0; outline: 0;" src="http://cdn.livestream.com/embed/elizavetababanova?layout=4&amp;height=415&amp;width=661&amp;autoplay=false" frameborder="0" scrolling="no" width="661" height="415"></iframe>-->
					<iframe width="661" height="422" src="//www.youtube.com/embed/95MVgyNCKC4?autohide=1" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="social">
					<?php
					require ('bestyear/best-year-webinar/social_button_web.php');
					?>
				</div>
				<div class="buttonbg">
					<a target="_blank" href="http://elizavetababanova.com/bestyear"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/images/button.png"></a>
				</div>
			</div>

			<div class="clear"></div>
			<div class="comments">
				<div class="left">
					<div id="vk_comments"></div>
					<script type="text/javascript">
						VK.Widgets.Comments("vk_comments", {
							limit : 10,
							width : "400",
							attach : "*"
						});
					</script>
				</div>

				<div class="right">
					<div class="fb-comments" data-href="http://elizavetababanova.com/best-year-webinar" data-width="400" data-num-posts="10"></div>
				</div>
			</div>

			<div class="clear"></div>
			<div class="blogcomments">
				<?php comments_template('/comments2.php'); ?>
			</div>

			<footer class="footer">
				<i class="border"> </i>
				<p class="text">
					ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены
				</p>
				<nav class="menu3">
					<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> | <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> | <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
				</nav>
			</footer>

		</div>
	</body>
</html>
