<?php
/*
Template Name: Страница 5stages-4-vector
*/
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>
    <title>6 моих устремлений</title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/css/style.css" rel="stylesheet">
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/js/custom.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">
        VK.init({apiId:  2461907, onlyWidgets: true});
    </script>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
    <link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-4/img/img6vec.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <section class="main">
        <header class="header">
            <div class="inner">
                <a class="logo" href="http://elizavetababanova.com/" target="_blank">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/img/b1.png" alt=".">
                </a>
                <nav class="menu">
                    <a target="_blank" href="http://vk.com/elizaveta.babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/img/b2.png" alt=".">
                    </a>
                    <a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/img/b3.png" alt=".">
                    </a>
                    <a target="_blank" href="http://twitter.com/liza_babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/img/b4.png" alt=".">
                    </a>
                    <a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/img/b5.png" alt=".">
                    </a>
                </nav>
            </div>
            <i class="border"></i>
        </header>
        <div class="inner">
            <nav class="menu2">
                <a class="i i1 threerow" href="http://elizavetababanova.com/5stages">5 уровней<br>развития<br>человека</a>
                <a class="i i7" href="http://elizavetababanova.com/research">Исследования</a>
                <a class="i i2 tworow" href="http://elizavetababanova.com/survey">Результаты<br>опроса</a>
                <a class="i i3 tworow active" href="">6 моих<br>векторов</a>
                <a class="i i4 tworow" href="http://elizavetababanova.com/phenomen-masterstva">Феномен<br>Мастерства</a>
                <a class="i i8 threerow" href="http://elizavetababanova.com/kak-realizovat-potentsial">Как<br>реализовать<br>потенциал</a>
                <a class="i i5 tworow" href="http://elizavetababanova.com/programma-treninga">Программа<br>тренинга</a>
                <a class="i i6 tworow" href="http://elizavetababanova.com/gosti-fm">Гости<br>программы</a>
            </nav>
            <section class="block">
                <div class="i i1">
                    <p class="title"><span class="custom">6</span> моих векторов</p>
                    <section class="body" style="width: 800px;margin: 0 auto;text-align: justify;">
                                     <div class="text2">

    <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-4/img/ostrov.png" alt="." style="padding-top:30px;"></center>

    <br />
    <p style="font-size:17px;line-height: 22px;">Последнее время я много думаю о том, чем я живу, что мною движет, что вдохновляет. Многие мои интересы и устремления я уже раскрыла в своих статьях. </p>
    <br />
    <p style="font-size:17px;line-height: 22px;">В этом посте я хочу поделиться с вами моими главными ценностями, моими векторами развития. Михаил Задорнов шутит, что у русских людей много энергии, но она без вектора, и в этом наша беда. </p>
    <br />
    <p style="font-size:17px;line-height: 22px;">Возможно, именно поэтому я определила свои основные ценности, чтобы движение моей энергии всегда имело направление. Эти 6 векторов – мои главные указатели того, в каком направлении мне необходимо развиваться, чтобы быть счастливой. </p>
    <br />
    <section style="width:660px; margin:0 auto;">
    <h5 style="color:#1AA6E8;font-size:1,5em;font-family:'avantgardegothicc-book';">1 Вектор: Смысл</h5>
    <br />

    <p>С 10 лет я озадачилась поиском смысла жизни. Этот поиск заставил меня исследовать себя и мир вокруг. Именно благодаря поиску чего-то большего, чем то, что было доступно мне в моем городе, в 15 лет я самостоятельно уехала в Штаты.</p>
    <br />


    <p>Я искала смысл в религии и в различных философских направлениях, в путешествиях по экзотическим местам и в экстремальных ситуациях (вспоминаются ночи в дикой природе в Панаме и Коста-Рике под рев блуждающих тигров).</p>
    <br />

    <p>Во всем, что бы я ни делала сегодня – в работе, в здоровье, в отношениях, я сначала ищу смысл. Когда есть ответ на главный вопрос «зачем», то все остальное легко становится на свои места.</p>
    <br />


    <h5 style="color:#1AA6E8;font-size:1,5em;font-family:'avantgardegothicc-book';">2 вектор: Любовь как равновесие Эгоизма и Альтруизма</h5>
    <br />

    <p>Эгоизм – это «умение получать». Альтруизм – «умение отдавать». </p>
    <br />

    <p>Если человек подчиняет жизнь других удовлетворению его личных интересов, не заботясь в равной степени об их интересах, у него происходит перекос в эгоизм. Если человек отдает другому (свои силы, энергию, знания, заботу), и при этом становится опустошенным, то происходит перекос в альтруизм. </p>
    <br />

    <p>Если получение или отдача идут во благо каждому, то это становится любовью.</p>
    <br />

    <p>И эгоизм, и альтруизм в чистом виде – отклонение от любви. Мудрость – это способность сбалансировать в себе эгоистические и альтруистические свойства, чтобы с одной стороны, не быть лишь потребителем, а с другой не быть жертвой.  </p>
    <br />

    <p>Сейчас одна из моих главных задач – тонко чувствовать и понимать каждого человека, и правильно отражать его. </p>
    <br />
    <p>Я называю это моим вектором Любви.</p><br />

    <h5 style="color:#1AA6E8;font-size:1,5em;font-family:'avantgardegothicc-book';">3 вектор: Желание объединить людей с одинаковыми ценностями с целью принесения пользы</h5>
    <br />

    <p>Вместе с моей командой мы создаем платформу для объединения людей, которые, так же, как и мы, настроены на смысл, реализацию и высокий уровень отдачи другим.</p>
    <br />

    <p>Когда я оставила работу в хедж фонде, я почти два года не могла «найти себя». Меня мучали страхи, что я уже никогда не буду нужна окружающему миру, хотя я не могла понять, почему Бог не находит пристанище таким людям, как я: очень сильно стремящихся к служению другим и обладающим довольно неплохим образованием и профессиональным опытом, который можно применить для благородной цели. </p>
    <br />

    <p>Видимо, он испытывал на прочность мое желание, то и дело подсовывая мне интригующие предложения поработать в финансовой индустрии то в Москве, то в Киеве, то в Майами. Но я продолжала верить в то, что рано или поздно я найду свое дело.</p>
    <br />

    <p>Для меня нет ничего более важного в “материальном” мире, чем принесение пользы окружающим людям. Поэтому я испытываю колоссальный прилив энергии, когда вижу по вашей обратной связи, что материал попал в точку и помог вам что-то понять, что-то переосмыслить и что-то изменить. </p>
    <br />

    <p>Каждое утро я начинаю с просмотра комментариев на сайте и в соц. сетях, и это заряжает меня на новый день работы.</p>
    <br />

    <p>Понимая важность и необходимость группы, резонирующей мне по ценностям, я делаю все от меня зависящее, чтобы у каждого, кто попадает на наш сайт, была возможность ощутить нашу поддержку.</p>
    <br />

    <h5 style="color:#1AA6E8;font-size:1,5em;font-family:'avantgardegothicc-book';">4 вектор: Мастерство</h5>
    <br />

    <p>В серии новых материалов о мастерстве я реализую одну из моих основных ценностей.</p>
    <br />

    <p>Если вы смотрели видео о 5 уровнях, то вы уже знаете, что желание стать мастером может появиться только когда ты уже понял, что успех тебе по плечу.</p>
    <br />

    <p>Но успех без любви ничего не значит. А настоящая любовь к профессии призывает человека к тому, чтобы достичь в ней уровня мастерства. Иначе это не любовь, а увлечение, мимолетная страсть. Ведь поэтому только настоящее призвание называют «любимым» делом.</p>
    <br />

    <p>На развитие мастерства мирового уровня скорее всего и уйдет вся моя профессиональная жизнь, и ничего лучшего для себя в качестве стимула к развитию в любимом деле я еще не придумала.</p>
    <br />

    <h5 style="color:#1AA6E8;font-size:1,5em;font-family:'avantgardegothicc-book';">5 вектор: Прогресс</h5>
    <br />

    <p>Как я уже написала в ответе на <a href="http://elizavetababanova.com/survey">одно из предубеждений</a> касательно личностного и профессионального роста, я испытываю колоссальных страх от стагнации и отката назад. Для меня главное - не останавливаться и идти вперед, даже если это будут совсем маленькие, «baby steps». </p>
    <br />

    <p>Без постоянного прогресса во всех сферах жизни я не представляю своего счастья. </p>
    <br />

    <p>Джордж Леонард прекрасно соединил две моих ценности (Мастерство и Прогресс) в этом отрывке своей книги «Истинное Мастерство»:</p>
    <br />

    <p><span style="font-style:italic;">«Со временем мы поняли, что истинное мастерство – не есть совершенство по своей сути, но путь к нему, сам процесс, а не конечный результат. Истинный мастер своего дела, таким образом – тот, кто остаётся на своем посту день за днем, год за годом; тот, кто действует – пусть и ошибается – но пробует вновь и вновь, пока бьется его сердце. Стремление людей к быстрым, беспрекословным и наглядным результатам сегодня – пожалуй, главный враг истинного мастерства».</span></p>
    <br />

    <h5 style="color:#1AA6E8;font-size:1,5em;font-family:'avantgardegothicc-book';">6 вектор: Баланс духовного и материального, логики и чувств </h5>
    <br />

    <p>Один из самых больших challenges для меня сегодня – это постоянное стремление к равновесию. </p>
    <br />

    <p>Быть с одной стороны в медитации, с другой - твердо стоять на ногах и заниматься бизнесом. Придумывать и генерировать идеи и незамедлительно их реализовывать.  Во всех своих решениях руководствоваться и логикой, и чувствами, интуицией. </p>
    <br />

    <p>Чем более я сбалансирована, тем легче мне реализовывать все мои задачи и тем приятнее я становлюсь в общении для окружающих. Человеку с многолетней опорой на логику очень трудно ежедневно переключаться на развитие чувственности, но это прекрасный challenge, который открывает совершенно новые состояния.</p>
    <br />

    ***

    <p>Последнее, о чем я хочу сказать, не является «вектором», ибо я не вижу, как к этому можно «устремиться». Но это можно назвать ценностью, которая для меня не менее важна, чем все вышеперечисленные 6 пунктов.</p>
    <br />

    <p>Эта ценность  - смирение. Вы наверняка знаете, что большинство людей учится не от радости, а от боли. Вот и смирение стало для меня важным не потому, что мне так хочется быть смиренной, а потому, что я вижу, насколько гротескны становятся характеры людей, если по мере достижения своих целей они теряют это качество.</p>
    <br />

    <p>Так как на моих глазах происходили подобные отрицательные трансформации, мне страшно от одной мысли, что я могу сильно возгордиться и забыть о том, для чего я вообще пришла в этот мир (ну точно не для того, чтобы развить свое самомнение). </p>
    <br />

    <p>Поэтому, смирение и смиренность – слова, которые так же для меня важны, как и счастье, успех и самореализация.</p>
    <br />

    <p>Любить Бога, моих близких и людей, приносить пользу окружающему миру через мою работу и смиренно ежедневно стремиться к мастерству – вот, пожалуй, краткая формулировка моих ценностей.</p>
    <br />
    <p><span style="font-style:italic;">Если вы хотите познакомиться со мной поближе, то вы можете изучить мою <a href="http://elizavetababanova.com/about" target="_blank">подробную биографию</a>, а также посмотреть <a href="http://elizavetababanova.com/video-obomne" target="_blank">видео</a> и <a href="http://elizavetababanova.com/photo" target="_blank">фото</a>.</span></p><br />
    <p>Спасибо вам за то, что вы со мной.</p>
    <br />
    <div class="text2">


            <br /><center>         <?php
        if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )
            echo "";
        else
            require('social_button.php');
    ?> </center>
            </div>
    </section>
    </div>

                    </section>
                </div>
            </section>

            <section class="block14" style="width: 800px;margin: 0 auto;">
            <div class="text2">


            <br /><center>
            </center>
            </div>
            </section>
            <i class="border3" style="width: 800px;margin: 0 auto;"></i>
            <section class="block8">
                <div class="left">
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                    VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
                    </script>
                </div>
                <div class="right">
                    <div class="fb-comments" data-href="http://elizavetababanova.com/6-moih-ustremlenij" data-width="400" data-num-posts="5"></div>
                </div>
            </section>
            <section class="block8">
            <?php comments_template('/comments2.php'); ?>
            </section>
            <footer class="footer">
                <i class="border6"></i>
                <p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
                <nav class="menu3">
                    <a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
                    <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
                    <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
                </nav>
            </footer>
        </div>
    </section>

</body>
</html>