<?php
/*
Template Name: PhotoPage
*/
?> 

<?php
require('./wp-blog-header.php');
require_once('header.php');
require_once('functions.php');
?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
 bcn_display();
}
?>
</div>

<div class="singl">

<div id="wrap" class="container singl" style="width: 820px;">
 
<div id="content">	
     
					<?php if (have_posts()) : ?>	
						<?php while (have_posts()) : the_post(); ?>
						<div class="page_post" <?php post_class() ?> id="post-<?php the_ID(); ?>">
							
						
          		
							<div class="postdate avant_book">  
              <?php 
    if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )  
        echo ""; 
    else
        
?>
              
              <?php if (current_user_can('edit_post', $post->ID)) { ?> <?php edit_post_link('Edit', '', ''); } ?>
              
              </div>
			
			
			
							<div class="entry">
                                <?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(300,225), array("class" => "alignleft post_thumbnail")); } ?>
								<?php the_content('Читать далее &raquo;'); ?>
								<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							</div> 
							
              
              
                          						 
						
							<?php edit_post_link('Редактировать эту запись','','.'); ?>
							
						</div><!--/post-<?php the_ID(); ?>-->  
				
				
				<?php endwhile; ?>
			
				<?php endif; ?>
				
</div><!-- #content-->

   


 </div><!-- #wrapper2 --> 
</div>

 <div class="line_shadow"></div>
 
 <div id="wrapper3">

<div id="footer">
<?php require('footer.php'); ?>
</div><!-- #footer --> 

</div><!-- #wrapper3 -->