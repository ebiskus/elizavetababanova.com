<?php
/*
Template Name: Лучший год Модуль 13
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/module.css">

	<title>Деловой Этикет</title>

	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/share13.jpg"> 
	<meta property="og:title" content="Деловой Этикет."/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear-module13"/>
	<meta name="description" content="Годовая программа «Лучший год Вашей жизни». Многие буквально подрезают себе крылья, срывая сделки и буксуя свой карьерный рост из-зa банального отсутствия навыков общения.">

	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script>
	<!--/LiveInternet-->	
</head>

<body>
    <div id="bym-wrapper" align="center">
		<div id="bym-social">
			<div id="bym-logo">
			   <a href="/"><img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/vk_b.gif"></a>
				<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/facebook_b.gif"></a>
				<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tweeter_b.gif"></a>
				<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/youtube_b.gif"></a>
			</div>
		<div style="clear:both"></div>

		</div>
		<div id="bym-header" align="center">
			<div class="bym-all_year">
				<div class="bym-mounth"><p class="bym-digit" style="color:#ffd98c;">13</p><p class="bym-name" style="color:#ffd98c;">БОНУС</p></div>
				<div class="bym-description"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/title13.png">
					<p class="bym-text">
					Многие буквально подрезают себе крылья, срывая сделки и буксуя свой карьерный рост из-зa банального отсутствия навыков общения. Но незнание этических законов не освобождает от горьких последствий. На этом модуле вы узнаете фундаментальные правила делового этикета, бизнес-корреспонденции и освоите высокие стандарты общения. Устранив ошибки, о которых сегодня, возможно, вы даже не догадываетесь, вы начнете гораздо легче завязывать знакомства и выстраивать отношения. В результате, ваши планы начнут реализовываться гораздо быстрее. Ведь теперь ваши коммуникации будут соответствовать вашим целям и задачам.
					</p>
					</p>
				</div>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/13.png">
			</div>
			<div style="clear:both"></div>

		</div>
		<div id="bym-content">
			<div id="bym-mon">
				<div id="bym-mon1-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m13mon1.jpg" ></div>
				<div id="bym-mon1">
				<ul>
					<li>Первое и самое главное впечатление</li>
					<li>Правила хорошего тона
							<ul>
								<li>Деловая переписка</li>
								<li>Телефонный разговор</li>
								<li>Социальные сети</li>
							</ul>
					</li>
					<li>Искусство «small talk»  </li>
					<li>Что необходимо делать на профессиональных мероприятиях и корпоративах, чтобы от них была польза
							<ul>
								<li>Критически важные рекомендации и для тех, кто работает в организации и для тех, кто управляет своим собственным бизнесом </li>
							</ul>
					</li>

				</ul>
				</div>
			  
				<div id="bym-mon2" style="padding-top:100px;">
				<ul>
					
					<li>Манеры
							<ul>
								<li>Как вести себя 
									<ul>
										<li>на коктейльной вечеринке</li>
										<li>в гостях</li>
										<li>на формальном ужине</li>
									</ul>
								</li>
								<li>Какие темы уместны в каких кругах</li>
								<li>Как правильно использовать приборы в 5-звездочном ресторане</li>
								<li>Как не опростоволоситься в чужой стране или ресторане иностранной кухни?</li>
							</ul>
					</li>
					<li>Правила хорошего тона в одежде
							<ul>
								<li>Законы совмещения цветов, тканей и аксессуаров</li>
							</ul>
					</li>
					<li>Правила хорошего тона в подарках
							<ul>
								<li>Что можно, а что категорически нельзя дарить разным типам ваших контактов</li>
							</ul>
					</li>

				</ul>
				</div>
				<div id="bym-mon2-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m13mon2.jpg"></div>
			</div>
		</div>
		<div style="clear:both"></div>
		<div id="bym-reg">
			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>
			<div style="clear:both"></div>

		</div>

		<div id="bym-footer">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/aboutfoto.png">
			<div class="bym-about">
			   <div class="bym-about_name">Елизавета Бабанова</div>
			   <div class="bym-about_autor">автор семинара</div>
			   <div class="bym-about_desc">
					<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
				   <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
				   <p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
				   <p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
				  <p>Член Международной Ассоциации Непрерывного Образования.</p>
			   </div>
			</div>
			<div class="bym-vk">
			<iframe id="vkwidget1" height="350" frameborder="0" width="200" scrolling="no" style="overflow: hidden; height: 350px;" src="//vk.com/widget_community.php?app=0&width=200px&gid=44023512&mode=0&height=350&url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&13b1da20c61" name="fXD32f6b"></iframe>
			</div>
		</div>


		<div class="bym-footer_info">
			<div class="bym-copy">ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены</div>
			<nav class="bym-bottom_menu">
			<a href="/privacy-policy.html">Политика конфиденциальности</a> | <a href="/terms.html">Правила пользования</a> | <a href="/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div class="bym-site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></div>
		</div>


	</div>
</body>