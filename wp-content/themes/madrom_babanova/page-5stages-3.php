<?php
/*
Template Name: Страница 5stages-3
*/
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>
    <title>Результаты опроса</title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/css/style.css" rel="stylesheet">
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/js/custom.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">
        VK.init({apiId:  2461907, onlyWidgets: true});
    </script>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
    <link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-3/img/share.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <section class="main">
        <header class="header">
            <div class="inner">
                <a class="logo" href="http://elizavetababanova.com/" target="_blank">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/b1.png" alt=".">
                </a>
                <nav class="menu">
                    <a target="_blank" href="http://vk.com/elizaveta.babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/b2.png" alt=".">
                    </a>
                    <a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/b3.png" alt=".">
                    </a>
                    <a target="_blank" href="http://twitter.com/liza_babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/b4.png" alt=".">
                    </a>
                    <a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/b5.png" alt=".">
                    </a>
                </nav>
            </div>
            <i class="border"></i>
        </header>
        <div class="inner">
            <nav class="menu2">
                <a class="i i1 threerow" href="http://elizavetababanova.com/5stages">5 уровней<br>развития<br>человека</a>
                <a class="i i7" href="http://elizavetababanova.com/research">Исследования</a>
                <a class="i i2 tworow  active" href="">Результаты<br>опроса</a>
                <a class="i i3 tworow" href="http://elizavetababanova.com/6-moih-vectorov">6 моих<br>векторов</a>
                <a class="i i4 tworow" href="http://elizavetababanova.com/phenomen-masterstva">Феномен<br>Мастерства</a>
                <a class="i i8 threerow" href="http://elizavetababanova.com/kak-realizovat-potentsial">Как<br>реализовать<br>потенциал</a>
                <a class="i i5 tworow" href="http://elizavetababanova.com/programma-treninga">Программа<br>тренинга</a>
                <a class="i i6 tworow" href="http://elizavetababanova.com/gosti-fm">Гости<br>программы</a>
            </nav>
            <section class="block">
                <div class="i i1">
                    <p class="title"><span class="custom">Р</span>езультаты опроса</p>
                    <section class="body" style="width: 800px;margin: 0 auto;text-align: justify;">
                                     <div class="text2">
    <p style="padding-top:30px;">Мы благодарим всех тех, кто принял участие в опросе. Он помог нам определить, какой процент читателей этого блога реализуется через свое дело, и узнать главные факторы, препятствующие стремительному профессиональному росту. Наша статистика, безусловно, намного лучше статистики по России, так как темой саморазвития и бизнеса увлечены люди определенного уровня развития.</p>
    <br />
    <p> Ниже мы делимся с вами результатами, а во второй части вы увидите самые распространенные ограничения, препятствующие профессиональной реализации наших читателей. Сравните их со своими.</p>
    <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/result-1.png" alt="."></center>
    <p>Мы видим, что только 7,2% людей считают себя абсолютно счастливыми в профессии. Большинство людей поставили оценки от 5 до 8, а 27,2% оценили уровень счастья на 4 балла и ниже.</p>
    <br />
    <i class="border3"></i>
    <br />

    <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/result-2.png" alt="."></center>

    <p>Опрос показал, что более 50% выбирали профессию не по любви, а по расчету. К сожалению, это реалии современности. Большинство людей делают свой выбор в пользу нелюбимой работы. Многие просто не могут понять, как научиться достойно зарабатывать, занимаясь любимым делом.</p>
    <br />
    <p style="margin-bottom:30px;">Мое мнение - человек в принципе не может стать счастливым, если не любит свою работу. То есть у нас с вами всего два пути: либо полюбить то, что мы уже делаем, либо найти то, что мы будем любить.</p>
    <i class="border3"></i>
    <br />

    <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/result-3.png" alt="."></center>
    <p>Всего лишь 3,3% читателей блога включают себя в топ 10% своей индустрии.
    Моя задача - это исправить.</p>
    <br />
    <p style="margin-bottom:30px;">Я желаю, чтобы каждый из вас вошел в топ 5% специалистов вашей ниши или профессии. Я проповедую и буду продолжать проповедовать эту идею в своих будущих материалах и надеюсь, что вы, так же, как и я, скоро загоритесь идеей работы на мировом уровне.</p>
    <i class="border3"></i>
    <br />

    <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/result-4.png" alt="."></center>
    <p style="margin-bottom:30px;">Вот здесь становится уже очень интересно. Мы видим, что 73% из вас считают, что вам чего-то не хватает, чтобы  попасть в высшую лигу своей профессии. Но первый шаг уже сделан, так как многие признались самим себе в необходимости получения дополнительных знаний и совершенствования своих стратегических усилий. Это первый шаг на пути к мастерству.</p>
    <i class="border3"></i>
    <br />



    <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/result-5.png" alt="."></center>

    <p>Я безмерно счастлива увидеть такой высокий уровень доверия.  Это значит, что я правильно выбрала свое дело. :)</p>
    <br />
    <p>А теперь начинается самое интересное -  распространенные причины, которые мешают вашему стремительному профессиональному росту. Я постаралась нокаутировать каждую из них, чтобы она уже не могла "подняться" и занять место в вашей голове после того, как вы прочтете мои аргументы.</p>
    <br />
    <p>&nbsp;</p>
    <p class="title"><span class="custom">Г</span>лавные причины, препятствующие профессиональному развитию</p>
    <p>&nbsp;</p>
    <br />
    <p style="font-weight:bold;">Я не очень люблю дело, которым занимаюсь, и пока не нашел свое</p>
    <br />
    <p>Любимое дело находится на пересечении 5 составляющих. Каждую из этих составляющих мы глубоко прорабатываем на тренинге "<a href="http://elizavetababanova.com/zhivi-po-polnoj" target="_blank">Живи по Полной!</a>", и в результате вы получаете ответ на этот вопрос.
    </p>
    <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-3/img/krug.png" height="50%" width="50%" alt="."></center>


    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">
    У меня нет времени</p>
    <br />
    <p>Я верю, что 1-2% людей сейчас могут находиться на стадии, когда им “не до себя”. Кто-то, возможно, ухаживает за больными родителями, у кого то новорожденный ребенок, но если ваша отговорка “нет времени”, и при этом вы погрязли в текучке, значит, вы не применяете один из самых главных законов успеха - закон лазерного фокуса. </p>
    <br />
    <p>Если у вас есть время на Интернет, даже на пару часов телевидения в неделю и на общение с друзьями, значит отговоркой “нет времени” вы дурите сами себя.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">У меня нет денег</p>
    <br />
    <p>Составьте список того, что вы купили за последний месяц. Распишите их по трем категориям: 1-ая необходимость, 2-ая необходимость,  3-я необходимость. В категории 1-ой необходимости будут такие пункты, как оплата жилья, покупка еды, одежда может быть во 2-ой. Рестораны и развлечения в 3-ем.  </p>
    <br />
    <p>Большинство людей, к сожалению, считают, что инвестирование в свое профессиональное развитие входит именно в 3-ю категорию - в которой мы тратим то, что у нас осталось после покупки игрушек и походов в рестораны. Моя же цель - донести до вас идею о том, что инвестирование в себя должно входить в категорию 1-ой необходимости.</p>
    <br />
    <p>Вы были бы готовы дополнительно трудиться, если бы под угрозой стояло ваше выживание? Профессиональное и личностное развитие, на самом деле, это залог не только нашего успеха, но и нашего существования в будущем.</p>
    <br />
    <p style="margin-bottom:30px">На тренинге Business Mastery Тони Роббинс пригласил выступить несколько гостей со своими материалами. Меня поразило то, что практически каждый из них начал изменять свою жизнь с посещения его тренинга. Но, что еще более поразительно, даже такие люди, как Кит Каннингем, будучи на грани полного банкротства, покупали программы Роббинса на последние деньги. А некоторым из них даже приходилось занимать. Со слезами на глазах эти состоявшиеся мастера вспоминали тот ключевой момент, когда они приняли своевременное решение инвестировать деньги в свое обучение и развитие.</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Я боюсь потерять ту работу, которая меня кормит сейчас, пусть даже она и нелюбимая</p>
    <br />
    <p>Это очень распространенный страх. У меня он тоже был. Пожалуй, это единственный веский аргумент. Как я решилась оставить высокодоходную работу в инвестиционном фонде с практически безграничным карьерным ростом? Я просто в один день проснулась с ощущением, что не могу больше заниматься нелюбимым делом. Все надо делать по любви - без нее вы не сможете ни построить счастливые отношения, ни преуспеть в профессии.</p>
    <br />
    <p>Что же делать? Ждите того дня, когда вы проснетесь и скажете себе “хватит!”. Тогда вы откроете новую дверь и войдете в новый мир. Я буду терпеливо ждать вас “по ту сторону”. :)</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">У меня не хватает знаний, образования, опыта, умений, навыков</p>
    <br />
    <p style="padding-bottom: 30px;">Вот это как раз вы и получаете у тех, кто обучает стратегиям достижения целей. Не пренебрегайте опытом других людей, особенно когда эти люди с огромным желанием делятся им с вами. Сэкономите огромное количество усилий.</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Мне лень</p>
    <br />
    <p>Если вам лень сделать что то хорошее для себя и для человечества, для самой Жизни, не ожидайте, что вы от нее получите многое. На вашу лень Жизнь, скорее всего, ответит взаимностью и поленится дать вам все то, о чем вы мечтаете.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">У меня нет достойной поддержки и окружения</p>
    <br />
    <p style="padding-bottom: 30px;">Это очень серьезное испытание для любого человека. Когда вам кажется, что “все против вас” и ваших внутренних ресурсов для преодоления препятствий недостаточно, тогда вам больше всего и нужно окружение, которое, так же, как и вы, будет настроено и нацеленно на успех и на раскрытие своего потенциала. Посещение продвигающих курсов онлайн и оффлайн как раз решает эту задачу.</p>
    <i class="border3"></i>
    <br />

    <p style="font-weight:bold;"> Мне не хватает самодисциплины (фокусировки, сосредоточенности, энергии, мотивации и т.д.)</p>
    <br />
    <p>У каждого из нас в какой-то момент времени отсутствует один из этих важных компонентов успешного человека, и именно для этого нужен тренер, или даже несколько. Они обеспечат вас как раз всем тем, чего вам так не хватает.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">У меня нет наставника</p>
    <br />
    <p style="padding-bottom: 30px;">Найти профессионального наставника не так уж и сложно, если инвестировать в это немного времени. В каждой профессии есть один или несколько выдающихся личностей - лидеров. Найдите их в своей индустрии и начните учиться тому, как они стали мастерами. Если вы только начинаете, то наблюдайте за тем, что они делают и учитесь у них со стороны. Если вы уже можете позволить себе учиться у них в группе или лично, то связывайтесь с ними и делайте все возможное, чтобы стать частью их окружения.</p>


    <p class="title" style="line-height:24px;padding:12px;"><span class="custom">Н</span>у а теперь несколько причин, которые препятствуют учиться именно на моих программах:</p>

    <br />
    <p style="font-weight:bold;">Я не уверен в себе</p>
    <br />
    <p>Если у вас есть один большой внутренний или внешний барьер, или целый букет, то решить эту проблему вам поможет моя программа "Живи по Полной!". Теме преодоления барьеров посвящен целый модуль, в котором мы разбираем все возможные ограничения и в итоге вы получаете ясное понимание, как убрать их из жизни.</p>

    <br />
    <p>Если же вас больше всего угнетает то, что вы не работаете и не живете на том уровне, на котором вам хотелось бы, то есть, вы еще не лидер в своей профессии, то для вас скоро выйдет новая программа, в которой мы будем работать над тем, как выйти на уровень мастерства.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Я не верю в успех</p>
    <br />
    <p>Если вы не будете в себя верить, то кто будет верить в вас?
    Правильно. Я буду верить. И сделаю все возможное, чтобы и вы поверили в себя сами. А когда это случится, мир отреагирует соответственно.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />

    <p style="font-weight:bold;">Я боюсь изменений</p>
    <br />
    <p>Боитесь стать умнее, сильнее, мудрее, опытнее?
    Я лично не представляю своей жизни без постоянного прогресса и изменений к лучшему. Страх стагнации, остановки, отката назад лично у меня гораздо сильнее, чем страх изменений. Страх прожить свою жизнь впустую - вот, что меня реально мотивирует.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Я не понимаю свои цели</p>
    <br />
    <p>Программа "Живи по Полной!" разработана таким образом, чтобы помочь вам раскопать в себе ваши главные жизненные цели. Вы их знаете, я вам гарантирую. Просто за “белым шумом” социума, своих ограничивающих убеждений, вы перестали слышать себя и то, куда вы действительно хотите прийти. Если вы готовы узнать, то мы всегда ждем вас.  </p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Не знаю, как правильно выбрать наставника</p>
    <br />
    <p>Как и во всем другом, чтобы правильно сделать выбор, нужно, чтобы и мозг, и сердце сказали громкое “да”. Если одно или другое говорит вам “нет”, то ищите другого, своего. Обязательно найдете. :) </p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Сомневаюсь, не знаю нужно ли мне это, не созрел </p>
    <br />
    <p>Чаще всего, когда мы думаем, что “не созрели” к чему то, это либо лень, либо комплексы. Почитайте еще раз ответы на эти установки выше и перепроверьте себя. </p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Учусь много, но занятия не превращаются в навыки</p>
    <br />
    <p>Занимаясь по моим программам, вы не останетесь на уровне теоретика. Так как для меня самое главное в жизни - это не мечтания, не цели, не теория, а практика и реализация задуманного, я мягко заставлю вас действовать. По-другому мои курсы пройти невозможно.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Не уверена, что смогу выполнять все задания курса</p>
    <br />
    <p>Сможете. Ничего сверхъестественного я не даю, все рассчитано на занятого, профессионально развивающегося человека, с огромным количеством дел и обязательств. Я не перегружу вас ни информацией, ни упражнениями. Но дам именно столько, чтобы вы а) сделали колоссальный прорыв и б) почувствовали грандиозный драйв от процесса.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Курсы не подойдут конкретно для моей профессии</p>
    <br />
    <p>Единственная программа, которая заточена под определенную профессию - это <a href="http://elizavetababanova.com/businessclass" target="_blank">Бизнес Класс</a>, рассчитанный на предпринимателей и бизнесменов.</p>
    <br />
    <p>"Живи по Полной!" продвинет вас, чем бы вы ни занимались. Законы успеха работают повсеместно.</p>
    <br />
    <p>Ну а новый курс рассчитан на то, чтобы вывести вас в топ 1-5% вашей индустрии. Мне неважно, кто вы - писатель, программист или юрист. Я помогу вам вскрыть чип-код вашего гения.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />

    <p style="font-weight:bold;">Живу в другой стране</p>
    <br />
    <p>Все программы проходят онлайн, и вы можете участвовать в них из любой точки мира, где есть Интернет. Если даже вам будет неудобно участвовать в прямом эфире, то у нас всегда есть запись занятий.</p>
    <p>&nbsp;</p>
    <i class="border3"></i>
    <br />
    <p style="font-weight:bold;">Нет технической возможности участвовать </p>
    <br />
    <p style="padding-bottom: 30px;">Для участия в наших программах вам не нужно быть технарем-гением. Вам понадобится только высокоскоростной Интернет и наушники с микрофоном, и то, только в том случае, если вы захотите задать вопрос. А нужную программу для просмотра вам поможет установить наша заботливая команда.</p>
    <i class="border3"></i>
    <br />
    <p>Ну вот, друзья, вы и прочитали самые распространенные предубеждения и сомнения касательно личностного и профессионального роста в целом и моих программ в частности. Я надеюсь, что мои ответы показали вам, что все они беспочвенны и не остановят вас, если вы действительно настроены на то, чтобы создать более счастливую, успешную и насыщенную жизнь.</p>
    <br />
    <p>Если у вас остались какие-то предубеждения, напишите их в комментариях. А если вам понравился материал, я буду рада получить вашу обратную связь.<p>
    </center>
    </div>

                    </section>
                </div>
            </section>

            <section class="block14" style="width: 800px;margin: 0 auto;">
            <div class="text2">


            <br /><center>         <?php
        if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )
            echo "";
        else
            require('social_button.php');
    ?> </center>
            </div>
            </section>
            <i class="border3" style="width: 800px;margin: 0 auto;"></i>
            <section class="block8">
                <div class="left">
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                    VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
                    </script>
                </div>
                <div class="right">
                    <div class="fb-comments" data-href="http://elizavetababanova.com/survey" data-width="400" data-num-posts="5"></div>
                </div>
            </section>
            <section class="block8">
            <?php comments_template('/comments2.php'); ?>
            </section>
            <footer class="footer">
                <i class="border6"></i>
                <p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
                <nav class="menu3">
                    <a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
                    <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
                    <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
                </nav>
            </footer>
        </div>
    </section>

</body>
</html>