<?php
/*
Template Name: Страница трансляции вебинара 45 дней
*/
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Как прийти к здоровому образу жизни за 45 дней?</title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
	<link href="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/css/style.css" rel="stylesheet">
	<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/js/jquery.js"></script>
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?98"></script>
	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/lemon.jpg"/>

	<meta property="og:description" content="Бесплатный вебинар, на котором вы получите конкретные инструменты для того, чтобы всего за 45 дней прийти к тому образу жизни, о котором Вы давно мечтали"/>
	<script type="text/javascript">
		VK.init({apiId: 3398000, onlyWidgets: true});
	</script>
	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>


	<div class="header">
		<div class="logo">
			<a href="http://elizavetababanova.com/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/img/logo.png"></a>
		</div>
	
		<div class="social">
			<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/img/b2.png"></a>
			<a href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/img/b3.png"></a>
			<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/img/b4.png"></a>
			<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/img/b5.png"></a>
		</div>
	</div>

    <?php if ( post_password_required() ) {?>
	    <div class="pass">
            <?php
                the_content('Читать далее &raquo;');
                if ( post_password_required() ) {
                    return;
                }
            ?>
        </div>
    <?php } ?>
	
	<div class="video">
		<iframe width="660" height="390" src="http://www.youtube.com/embed/_UM8wieloZE?rel=0&autoplay=1&start=1&disablekb=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
		<p style="padding-top: 285px;"><a href="http://elizavetababanova.com/health-upgrade/" target="_blank"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/webinar/img/45dney.png" /></a></p>
	</div>
	
	<div class="header2">
		<div class="block6">
			<div class="i i1"><a href="http://vkontakte.ru/share.php?url=http://elizavetababanova.com/webinar-health/" target="_blank">Расскажите</a> о видео друзьям ВКонтакте! </div>
			<div class="i i2"><a href="http://twitter.com/intent/tweet?text=Как прийти к здоровому образу жизни за 45 дней? Бесплатный онлайн-семинар:&url=http://elizavetababanova.com/webinar-health/" target="_blank">Твитнуть</a> о вебинаре по здоровью!</div>
		</div>

		<div class="block7">
			<img class="img" src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/webinar/img/b15.png" alt=".">
			<div class="facebook">
				<div class="fb-like" data-href="http://elizavetababanova.com/webinar-health/" data-send="false" data-width="280" data-show-faces="true"></div>
			</div>
		</div>
		
		<div style="clear:both;"></div>
		<div class="left">
			<div id="vk_comments"></div>
			<script type="text/javascript">
				VK.Widgets.Comments("vk_comments", {limit: 10, width: "400", attach: "*"});
			</script>
		</div>
	
		<div class="right">
			<div class="fb-comments" data-href="http://elizavetababanova.com/webi" data-width="400" data-num-posts="10"></div>
		</div>
	</div>
	
	<div style="clear:both;"></div>
	<section class="block8">
		<?php comments_template('/comments2.php'); ?>
	</section>
		
	<div class="footer">
		<p class="cent">
			ИП Бабанова Елизавета Дмитриевна &nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;2013 Все права защищены<br>
			<a target="_blank" href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a>  |  <a target="_blank" href="http://elizavetababanova.com/terms.html">Правила пользования</a>   |  <a target="_blank" href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a><br>
			<a style="color:#747474; text-decoration:none;" target="_blank" href="http://elizavetababanova.com">www.elizavetababanova.com</a>
		</p>	
	</div>
</body>
</html>