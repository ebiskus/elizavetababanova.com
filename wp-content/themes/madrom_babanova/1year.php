<?php
/*
Template Name: Страница 1year
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>

    <title>Первый год в цифрах</title>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/1year/css/style.css" rel="stylesheet">

    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/custom.js"></script>

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">VK.init({apiId:  2461907, onlyWidgets: true});</script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>

    <meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/1year/img/pic-share.png">
    <meta property="og:title" content="Первый год в цифрах"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://elizavetababanova.com/1year"/>
    <meta name="description" content="Первый год Системы Сферического Развития в цифрах">

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div>
            <img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

    <!--Google-->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35488483-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!--/google-->

</head>
<body>

    <div id="fb-root"></div>
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    <div id="fb-root"></div>

    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <section class="main">
        <div class="inner">
            <div>
                <img class="image" src="<?php bloginfo( 'stylesheet_directory' ); ?>/1year/img/god-v-cifrah-clear.jpg">
            </div>
            <section class="block6">
                <div class="i i1">
                    <a href="http://vkontakte.ru/share.php?url=http://elizavetababanova.com/1year/" target="_blank">Расскажите</a> друзьям ВКонтакте!
                </div>
                <div class="i i2">
                    <a href="http://twitter.com/intent/tweet?text=Поздравляю Систему Сферического Развития с д.р. и участвую в акции: ':&url=http://elizavetababanova.com/1year/" target="_blank">Твитнуть</a>!
                </div>
            </section>
            <i class="border3"></i>
            <section class="block7">
                <img class="img" src="<?php bloginfo( 'stylesheet_directory' ); ?>/1year/img/b1.png" alt=".">
                <div class="facebook">
                    <div class="fb-like" data-href="https://www.facebook.com/elizavetababanova.channel" data-send="false" data-width="280" data-show-faces="true"></div>
                </div>
            </section>
            <i class="border3"></i>
            <section class="block8">
                <div class="left">
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                        VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
                    </script>
                </div>
                <div class="right">
                    <div class="fb-comments" data-href="http://elizavetababanova.com/1year/" data-width="400" data-num-posts="5"></div>
                </div>
            </section>
            <section class="block8">
                <?php comments_template('/comments2.php'); ?>
            </section>
            <footer class="footer">
                <i class="border6"></i>
                <p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
                <nav class="menu3">
                    <a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
                    <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
                    <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
                </nav>
            </footer>
        </div>
    </section>
</body>
</html>