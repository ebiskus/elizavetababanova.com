<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main">

			<article id="post-0" class="post error404 not-found">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( '<strong>Запрашиваемая Вами страница не найдена</strong>', 

'twentyeleven' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( '', 'twentyeleven' ); ?></p>

					

				
					<div class="widget">
						<h2 class="widgettitle"><?php _e( '<b>Вы можете выбрать нужную запись из категорий сайта:</b>', 'twentyeleven' ); 

?></h2>
						<ul>
						<?php wp_list_categories( array( 'orderby' => 'count', 'order' => 'DESC', 

'show_count' => 1, 'title_li' => '', 'number' => 10 ) ); ?>
						</ul>
					</div>

					
					

				<div id="footer">
<?php require('footer2.php'); ?>
</div>