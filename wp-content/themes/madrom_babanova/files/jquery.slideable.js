/**
 * @category	Individ
 * @copyright	2012 Individ LTD (http://individ.ru)
 */

/**
 * Slideable jQuery plugin with CSS3 transitions support
 * application.js required
 */
$.fn.slideable = function(config){
	config = $.extend({
		wheelable: true,
		draggable: true,
		native: false,
		strict: true,
		wheelSensitivity: 1,
		axis: 'x',//[x|y]
		cssProperties: {//CSS property for apply position (if not 3d transform used)
			x: 'left',//[left|margin-left|...]
			y: 'top'//[top|margin-top|...]
		},
		cssPosition: 'relative',//[relative|absolute]
		animation: {
			timingFunction: 'linear',
			duration: 400,//Milliseconds
			delay: 0//Milliseconds
		},
		detectionOfDimensions: 'children',//[children|container]
		containerSelector: '> *',
		childSelector: '> *',
		onChangePosition: null,
		onMoveStart: null,
		onMove: null,
		onMoveStop: null
	}, config || {});
	
	var containerNode = this.find(config.containerSelector);
	var position = 0;
	var positionOffset = 0;
	var width = 0;
	var height = 0;
	var childrenWidth = 0;
	var childrenHeight = 0;
	var hiddenWidth = 0;
	var hiddenHeight = 0;
	var childrenCount = 0;
	var minStepX = 0;
	var minStepY = 0;
	var strictSave = config.strict;
	var moveable = null;
	var instance = this;
	
	/**
	 * Resize handler
	 *
	 * @return void
	 */
	var onResize = function(){
		width = instance.innerWidth();
		height = instance.innerHeight();
		
		childrenWidth = 0;
		childrenHeight = 0;
		
		var visibleCountX = 0;
		var visibleCountY = 0;
		
		switch(config.detectionOfDimensions){
			case 'container':
				childrenWidth = containerNode.innerWidth();
				childrenHeight = containerNode.innerHeight();
				break;
		}
		containerNode.find(config.childSelector).each(function(){
			if(childrenWidth <= width)
				visibleCountX++;
			if(childrenHeight <= height)
				visibleCountY++;
			
			switch(config.detectionOfDimensions){
				case 'children':
					childrenWidth += $(this).outerWidth(true);
					childrenHeight += $(this).outerHeight(true);
					break;
			}
		});
		
		hiddenWidth = childrenWidth - width;
		hiddenHeight = childrenHeight - height;
		
		var hiddenCountX = childrenCount - visibleCountX;
		var hiddenCountY = childrenCount - visibleCountY;
		minStepX = hiddenCountX > 0 ? 1 / hiddenCountX: 0;
		minStepY = hiddenCountY > 0 ? 1 / hiddenCountY : 0;
	};
	
	/**
	 * Initialization
	 *
	 * @return void
	 */
	var initialize = function(){
		childrenCount = containerNode.find(config.childSelector).length;
		
		onResize();
	};
	
	/**
	 * Re-initialization
	 *
	 * @return object
	 */
	this.reinitialize = function(){
		initialize();
		
		return this;
	};
	
	/**
	 * Enable animation
	 *
	 * @return void
	 */
	this.enableAnimation = function(){
		moveable.enableAnimation();
	};
	
	/**
	 * Disable animation
	 *
	 * @return void
	 */
	this.disableAnimation = function(){
		moveable.disableAnimation();
	};
	
	/**
	 * Convert offset in pixels to position
	 *
	 * @param integer offset Offset in pixels
	 * @return float
	 */
	this.offsetToPosition = function(offset){
		switch(config.axis)
		{
			case 'x':
				return hiddenWidth > 0 ? -offset / hiddenWidth : 0;
				break;
			
			default:
				return hiddenHeight > 0 ? -offset / hiddenHeight : 0;
				break;
		}
	};
	
	/**
	 * Store scroll position
	 *
	 * @param float pos New position (0-1)
	 * @param boolean createEvents Generate events
	 * @return void
	 */
	this.storePosition = function(pos, createEvents){
		createEvents = createEvents === undefined ? true : createEvents;
		var direction = pos > position;
		
		switch(config.axis)
		{
			case 'x':
				var total = hiddenWidth;
				break;
				
			default:
				var total = hiddenHeight;
				break;
		}
		
		if(config.strict && pos > 0){
			var currentOffset = 0;
			var skip = false;
			var searchableOffset = pos * total;
			containerNode.find(config.childSelector).each(function(index){
				if(skip)
					return;
				
				switch(config.axis)
				{
					case 'x':
						var itemOffset = $(this).outerWidth(true);
						break;
					
					default:
						var itemOffset = $(this).outerHeight(true);
						break;
				}
				var halfOffset = itemOffset / 2;
				
				if(currentOffset + halfOffset > searchableOffset){
					pos = currentOffset / total;
					skip = true;
				}
				
				currentOffset += itemOffset;
			});
		}
		
		position = Math.max(0, Math.min(pos, 1));
		positionOffset = Math.round(position * total);
		
		if(createEvents && config.onChangePosition)
			config.onChangePosition.call(instance, position, positionOffset);
	};
	
	/**
	 * Return scroll position (0-1)
	 *
	 * @return float
	 */
	this.getPosition = function(){
		return position;
	};
	
	/**
	 * Set scroll position
	 *
	 * @param float pos New position (0-1)
	 * @param boolean createEvents Generate events
	 * @return void
	 */
	this.setPosition = function(pos, createEvents){
		this.storePosition(pos, createEvents);
		
		moveable.setPosition({
			x: config.axis == 'x' ? -positionOffset : 0,
			y: config.axis == 'x' ? 0 : -positionOffset
		});
	};
	
	/**
	 * Slide to next item
	 *
	 * @return void
	 */
	this.goToNext = function(){
		this.switchItem(1);
	};
	
	/**
	 * Slide to previous item
	 *
	 * @return void
	 */
	this.goToPrev = function(){
		this.switchItem(-1);
	};
	
	/**
	 * Slide to count items
	 *
	 * @param integer count Items count
	 * @return void
	 */
	this.switchItem = function(count){
		switch(config.axis)
		{
			case 'x':
				var step = minStepX;
				break;
				
			default:
				var step = minStepY;
				break;
		}
		
		instance.setPosition(position + count * step);
	}
	
	/**
	 * Node move start handler
	 *
	 * @param object event
	 * @return void
	 */
	var onMoveStart = function(event)
	{
		config.strict = false;
		if(config.onMoveStart)
			config.onMoveStart.call(instance);
	};
	
	/**
	 * Node move handler
	 *
	 * @param object event
	 * @param object position
	 * @return void
	 */
	var onMove = function(event, position)
	{
		switch(config.axis)
		{
			case 'x':
				position.x = Math.min(position.x, 0);
				position.x = Math.max(position.x, -hiddenWidth);
				instance.storePosition(instance.offsetToPosition(position.x));
				break;
			
			default:
				position.y = Math.min(position.y, 0);
				position.y = Math.max(position.y, -hiddenHeight);
				instance.storePosition(instance.offsetToPosition(position.y));
				break;
		}
		
		if(config.onMove)
			config.onMove.call(instance, position);
	};
	
	/**
	 * Node move stop handler
	 *
	 * @param object position
	 * @return void
	 */
	var onMoveStop = function(position)
	{
		config.strict = strictSave;
		if(config.strict)
			instance.setPosition(instance.getPosition());
		
		if(config.onMoveStop)
			config.onMoveStop.call(instance);
	};
	
	/**
	 * Mouse wheel handler
	 *
	 * @param object event
	 * @param integer delta
	 * @param object movePosition
	 * @return boolean
	 */
	var onWheelRotate = function(event, delta, movePosition)
	{
		movePosition.x = null;
		
		if(childrenCount == 0)
			return true;
		
		instance.switchItem(config.wheelSensitivity * -delta);
		
		var pos = instance.getPosition();
		return config.native ? pos < 0.02 || pos > 0.98 : false;
	};
	
	/**
	 * Initialization
	 */
	containerNode.css({
		'position': config.cssPosition
	});
	
	$(window).resize(onResize);
	
	moveable = new application.moveable({
		domNode: containerNode,
		draggable: config.draggable,
		wheelable: config.wheelable,
		axis: config.axis,
		cssProperties: config.cssProperties,
		cssPosition: config.cssPosition,
		animation: config.animation,
		onMoveStart: onMoveStart,
		onMove: onMove,
		onMoveStop: onMoveStop,
		onWheelRotate: onWheelRotate
	});
	
	initialize();
	
	this.setPosition(0, false);
	
	return this;
};
