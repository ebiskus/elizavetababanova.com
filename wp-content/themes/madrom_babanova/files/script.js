/**
 * @category	Individ
 * @copyright	2012 Individ LTD (http://individ.ru)
 */


/**
 * Store
 */
application.blocks.store = function()
{
	/**
	 * Link to "Main" block instance
	 *
	 * @var object
	 */
	var store = this;
	
	
	/**
	 * Check block
	 *
	 * @return boolean
	 */
	this.exists = function()
	{
		return $('section.store').length > 0;
	};
	
	/**
	 * Init block
	 *
	 * @return void
	 */
	this.init = function()
	{
		/* Vars */
		store.storeSections = $('section.store');
		var storeSectionsLink = $('h1', store.storeSections);
		var storeSectionsCont = $('.cont', store.storeSections);
		
		store.storeTabs = $('li.tabitem');
		store.storeTabsLinks = $('a', store.storeTabs);
		
		var overlay = $('#overlay');
		var trainingForm = $('#training-form');
		var trainingBuy = $('#training-buy');
		var trainingFormClose = $('a.close', trainingForm);
		
		var graytabs = $('div.graytabs');
		var graytabs_links = $('h3 a', graytabs);
		var graytabs_panes = $('.grayblock section', graytabs);
		
		store.trainingSum = 0;
		store.trainingCnt = 0;
		
		store.activeSection = store.storeSections.filter('.active').data('url');
		store.activeID = parseInt(store.storeTabs.filter('.active').data('id'));
		
		var history = new application.history('application.blocks.store.loadPageHistory', {section: store.activeSection, id: store.activeID});
		//application.history.init('application.blocks.store.loadPageHistory', {section: store.activeSection, id: store.activeID});
		
		
		store.storeTabsLinks.click(function()
		{
			store.activeSection = $(this).parents('section.store').data('url');
			store.activeID = parseInt($(this).data('id'));
			history.push($(this).attr('href'), {section: store.activeSection, id: store.activeID});
		});
		
		
		
		/* Sections */
		storeSectionsLink.click(function(){
			var index = storeSectionsLink.index(this);
			
			if(!store.storeSections.eq(index).is('.active'))
			{
				storeSectionsCont.filter(':visible').slideUp('fast');
				storeSectionsCont.eq(index).slideDown({
					duration: 'fast',
					step: function(){
						var scrollTop = $('html,body').scrollTop();
						var thisSectionPos = $(this).parent().position();
						if(thisSectionPos.top < scrollTop)
						{
							$('html,body').scrollTop(thisSectionPos.top);
						}
					},
					complete: function(){
						/*var thisSection = $(this).parents('section.store');
						store.activeSection = thisSection.data('url');
						store.activeID = parseInt(thisSection.find('li.tabitem.active a').data('id'));
						application.history.push($(this).attr('href'), {section: store.activeSection, id: store.activeID});*/
					}
				});
				
				store.storeSections.removeClass('active');
				store.storeSections.eq(index).addClass('active');
			}
			return false;
		});
		
		
		
		/* Gray tabs */
		graytabs_links.click(function(){
			var thisGrayTab = $(this).parents('div.graytabs');
			var thisGrayTabLinks = $('h3 a', thisGrayTab);
			var thisGrayTabPanes = $('.grayblock section', thisGrayTab);
			
			var index = thisGrayTabLinks.index(this);
			
			thisGrayTabLinks.removeClass('active');
			$(this).addClass('active');
			
			thisGrayTabPanes.removeClass('active');
			thisGrayTabPanes.eq(index).addClass('active');
			
			return false;
		});
		
		
		
		
		/* Order */
		/* Buy button */
		$('section.store button').click(function()
		{
			application.blocks.common.onResize();
			overlay.height(application.vars.docHeight).fadeIn('fast');
			var trainingFormTop = $(window).scrollTop() + 100;
			$('html,body').animate({scrollTop: 0});
			//trainingForm.css({top: trainingFormTop}).fadeIn('fast');
			trainingForm.fadeIn('fast');
			
			var thisSection = $(this).parents('section.store');
			var thisCode = thisSection.data('code');
			var itemID = parseInt($(this).data('id'));
			
			if(itemID > 0)
			{
				if(thisSection.is('.TRAININGS'))
				{
					application.loading.show('#training-buy');
					$.post(
						application.vars.SITE_TEMPLATE_PATH + '/ajax/training_buy.php',
						{
							id: itemID
						},
						function(data)
						{
							trainingBuy.html(data);
							
							store.initBuyForm();
							store.trainingSum = 0;
							store.trainingCnt = 0;
							
							application.loading.hide('#training-buy');
						}
					);
				}
				else
				{
					application.loading.show('#training-buy');
					
					var thisArticle = $(this).parents('article.pane');
					var buyItems = $('select.buy-item', thisArticle).val();
					
					$.post(
						application.vars.SITE_TEMPLATE_PATH + '/ajax/store_order.php',
						{
							id: itemID,
							buyItems: buyItems,
							code: thisCode
						},
						function(data)
						{
							trainingBuy.html(data);
							
							store.initPersonTypeLink();
							
							application.loading.hide('#training-buy');
						}
					);
				}
			}
		});
		
		
		$('select.buy-item').change(function(){
			var thisPane = $(this).parents('article.pane');
			
			if($(this).val() == 'all')
				thisPane.addClass('buyall');
			else
				thisPane.removeClass('buyall');
		});
		
		
		trainingFormClose.click(function()
		{
			overlay.fadeOut('fast');
			trainingForm.fadeOut('fast');
			return false;
		});
		/*overlay.click(function()
		{
			trainingFormClose.trigger('click');
		});*/
		
		
		/* training buy form */
		trainingForm.on('submit', '#training-buy-form', function()
		{
			if(store.trainingCnt > 0)
			{
				application.loading.show('#training-form');
				$.post(
					application.vars.SITE_TEMPLATE_PATH + '/ajax/training_order.php',
					$(this).serialize(),
					function(data)
					{
						$('#training-order').html(data).show();
						
						if(store.trainingCnt > 2)
						{
							/* slideable */
							regList = $('#reg-list');
							var regSlider = regList.find('.list').slideable({
								strict: false,
								wheelable: store.trainingCnt > 3 ? true:false,
								onChangePosition: function(pos){
									regScrollbar.setPosition(pos, false);
								},
								onMoveStart: function(){
									regSlider.hasBeenDragged = true;
								}
							});
							regSlider.hasBeenDragged = false;
							
							var regScrollbar = regList.find('.scrollbar').scrollbar({
								onChangePosition: function(pos){
									regSlider.setPosition(pos, false);
								}
							});
						}
						
						application.blocks.common.onResize();
						overlay.height(application.vars.docHeight);
						
						application.loading.hide('#training-form');
					}
				);
			}
			return false;
		});
		
		
		/* training buy form - step 2 */
		trainingForm.on('submit', '#training-order-form', function()
		{
			if(!$(this).is('.disabled') )
			{
				application.loading.show('#training-form');
				
				$('#training-buy-form').addClass('disabled');
				$('#training-buy-form input').attr({disabled: 'disabled'});
				
				$.post(
					application.vars.SITE_TEMPLATE_PATH + '/ajax/training_order.php',
					$(this).serialize(),
					function(data)
					{
						$('#training-order').html(data);
						
						//application.widgets.init('#training-order');
						store.initPersonTypeLink();
						
						application.loading.hide('#training-form');
					}
				);
				
				$('html,body').animate({scrollTop: 0});
			}
			return false;
		});
		
		
		/* store buy form - step 2 */
		trainingForm.on('submit', '#store-order-form', function()
		{
			if($('input.delivery-inp:checked').length)
			{
				application.loading.show('#training-form');
				
				$.post(
					application.vars.SITE_TEMPLATE_PATH + '/ajax/store_order.php',
					$(this).serialize(),
					function(data)
					{
						trainingBuy.html(data);
						
						store.initPersonTypeLink();
						
						application.loading.hide('#training-form');
					}
				);
				
				$('html,body').animate({scrollTop: 0});
			}
			return false;
		});
		/* end Order */
	};
	
	this.showItem = function(section, id)
	{
		var thisSection = store.storeSections.filter('.'+section);
		if(thisSection.length && !thisSection.is('.active'))
		{
			thisSection.find('h1').trigger('click');
		}
		
		if(id > 0)
		{
			store.storeTabsLinks.filter('.tablink_'+id).trigger('click');
			
		}
	};
	
	this.loadPageHistory = function(url, state)
	{
		application.blocks.store.showItem(state.section, state.id);
	};
	
	
	this.initBuyForm = function()
	{
		var trainingBuyForm = $('#training-buy-form');
		
		/* spinner */
		$("input.spinner", trainingBuyForm).each(function(){
			var minVal = $(this).attr('data-min') ? $(this).attr('data-min') : null;
			var maxVal = $(this).attr('data-max') ? $(this).attr('data-max') : null;
			
			$(this).spinner({
				min: minVal,
				max: maxVal,
				change: store.calcTotal,
				stop: store.calcTotal
			});
		});
		
		$("input.spinner", trainingBuyForm).change(function(){
			store.calcTotal();
		});
		
		// vip detail
		$('#vipdetaillink', trainingBuyForm).click(function()
		{
			$(this).hide();
			$('#vipdetail').slideDown('fast');
		});
		$('#vipdetailhide', trainingBuyForm).click(function()
		{
			$('#vipdetaillink', trainingBuyForm).show();
			$('#vipdetail').slideUp('fast');
		});
		
		
		/* Coupon */
		var couponTimeOut = false;
		$('#COUPON').on('change keyup', function()
		{
			if(couponTimeOut)
				clearTimeout(couponTimeOut);
			
			couponTimeOut = setTimeout(function(){store.checkCoupon($('#COUPON').val());}, 500);
		});
	};
	
	
	this.checkCoupon = function(coupon)
	{
		var couponField = $('#COUPON');
		$.getJSON(
			application.vars.SITE_TEMPLATE_PATH + '/ajax/check_coupon.php',
			{coupon: coupon},
			function(data){
				if(data.result)
					couponField.addClass('ok');
				else
					couponField.removeClass('ok');
				
				store.calcTotal();
			}
		);
		
	}
	
	
	this.calcTotal = function()
	{
		var trainingBuyTotal = $('#buy-total');
		var trainingBuyItemTotal = $('span.item-total');
		
		trainingBuyTotal.text('---');
		trainingBuyItemTotal.text('-');
		
		$.getJSON(
			application.vars.SITE_TEMPLATE_PATH + '/ajax/calc_price.php',
			$('#training-buy-form').serialize(),
			function(data)
			{
				trainingBuyTotal.text(data.PRICE_SUM);
				for(k in data.PRICE_TYPES)
				{
					trainingBuyItemTotal.filter('.item-total-'+k).text(data.PRICE_TYPES[k]);
				}
				store.trainingSum = data.PRICE_SUM;
				store.trainingCnt = data.CNT;
			}
		);
	}
	
	
	this.initPersonTypeLink = function()
	{
		var person_type_link = $('.person_type_link a');
		var person_type_areas = $('div.person_type_div');
		var person_type_inputs = $('.required', person_type_areas);
		
		person_type_link.click(function()
		{
			var index = person_type_link.index(this);
			
			person_type_link.removeClass('active');
			$(this).addClass('active');
			
			var persontype = $(this).data('persontype');
			$('#person-type-inp').val(persontype);
			
			person_type_areas.hide();
			
			var person_type_area = $('#person_type_'+persontype);
			person_type_area.show();
			
			person_type_inputs.removeAttr('required');
			$('.required', person_type_area).attr('required', 'required');
			
			return false;
		});
		
		person_type_inputs.removeAttr('required');
		person_type_areas.filter('.active').find('.required').attr('required', 'required');
	}
	
};
