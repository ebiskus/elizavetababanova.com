/**
 * @category	Individ
 * @copyright	2012 Individ LTD (http://individ.ru)
 */

/**
 * Приложение
 */
var application = new function()
{
	/**
	 * Сообщения локали
	 *
	 * @var object
	 */
	this.locale = {
		dateFormat: 'dd.mm.yy',
		timeFormat: 'hh:mm',
		dateTimeFormat: 'dd.mm.yy hh:mm',
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
		dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
		dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		firstDay: 1,
		isRTL: false,
		close: 'Закрыть',
		datePicker: {
			prev: '<Пред',
			next: 'След>',
			current: 'Сегодня',
			showMonthAfterYear: false,
			weekHeader: 'Не',
			yearSuffix: ''
		},
		timePicker: {
			timeOnlyTitle: 'Выберите время',
			timeText: 'Время',
			hourText: 'Часы',
			minuteText: 'Минуты',
			secondText: 'Секунды',
			millisecText: 'миллисекунды',
			currentText: 'Сейчас',
			ampm: false
		},
		messageDialog: {
			title: 'Системное сообщение',
			ok: 'Ok'
		}
	};
	
	/**
	 * Функциональные блоки шаблона (общие для всех страниц)
	 *
	 * @var object
	 */
	this.blocks = {};
	
	/**
	 * Ф-ии обратного вызова готовности блоков
	 *
	 * @var array
	 */
	var blocksInitCallbacks = [];
	
	/**
	 * Настраивает локаль
	 *
	 * @return void
	 */
	var setupLocale = function()
	{
		if($.datepicker)
		{
			$.datepicker.regional['ru'] = {
				closeText: this.locale.close,
				prevText: this.locale.datePicker.prev,
				nextText: this.locale.datePicker.next,
				currentText: this.locale.datePicker.current,
				monthNames: this.locale.monthNames,
				monthNamesShort: this.locale.monthNamesShort,
				dayNames: this.locale.dayNames,
				dayNamesShort: this.locale.dayNamesShort,
				dayNamesMin: this.locale.dayNamesMin,
				weekHeader: this.locale.datePicker.weekHeader,
				dateFormat: this.locale.dateFormat,
				firstDay: this.locale.firstDay,
				isRTL: this.locale.isRTL,
				showMonthAfterYear: this.locale.datePicker.showMonthAfterYear,
				yearSuffix: this.locale.datePicker.yearSuffix
			};
			$.datepicker.setDefaults($.datepicker.regional['ru']);
		}
		
		if($.timepicker)
		{
			$.timepicker.regional['ru'] = {
				timeOnlyTitle: this.locale.timePicker.timeOnlyTitle,
				timeText: this.locale.timePicker.timeText,
				hourText: this.locale.timePicker.hourText,
				minuteText: this.locale.timePicker.minuteText,
				secondText: this.locale.timePicker.secondText,
				millisecText: this.locale.timePicker.millisecText,
				currentText: this.locale.timePicker.currentText,
				closeText: this.locale.close,
				ampm: this.locale.timePicker.ampm
			};
			$.timepicker.setDefaults($.timepicker.regional['ru']);
		}
	};
	
	/**
	 * Внутренние обработчики для метода alert()
	 *
	 * @var object
	 */
	var alertHandlers = {};
	
	/**
	 * Отображает всплывающий диалог с сообщением
	 *
	 * @param string messge Текст сообщения
	 * @param object config Конфигурация диалога
	 * @return void
	 */
	this.alert = function(message, config)
	{
		config = $.extend({
			appDriver: '',
			appType: 'info'
		}, config || {});
		
		var alertClass = 'application-alert ' + config.appType;
		
		if(!config.appDriver){
			if($.fancybox)
				config.appDriver = 'fancybox';
			else if($.dialog)
				config.appDriver = 'dialog';
		}
		
		switch(config.appDriver)
		{
			/* Fancybox driver */
			case 'fancybox':
				config = $.extend({
					title: application.locale.messageDialog.title,
					titleShow: false,
					onComplete: function(){
						$('#fancybox-wrap').addClass(alertClass);
					},
					onClosed: function(){
						$('#fancybox-wrap').removeClass(alertClass);
					}
				}, config);
				message =
					'<div class="title">' + config.title + '</div>' + 
					'<div class="message">' + message + '</div>' + 
					'<div class="toolbar"><button class="ok">' + application.locale.messageDialog.ok + '</button></div>'
				
				$.fancybox(message, config);
				
				if(!alertHandlers.fancyboxToolbarHandler){
					alertHandlers.fancyboxToolbarHandler = function(){
						if($(this).hasClass('ok')){
							$.fancybox.close();
						}
					};
					$('body').on('click', '#fancybox-content .toolbar :input', alertHandlers.fancyboxToolbarHandler);
				}
				
				break;
			
			/* jQuery UI "dialog" driver */
			case 'dialog':
				config = $.extend({
					modal: true,
					dialogClass: alertClass,
					title: application.locale.messageDialog.title,
					width: 500,
					buttons: {
						Ok: function(){
							$(this).dialog("close");
						}
					},
					open: function(event, ui){
						application.widgets.init($(this).parents('.ui-dialog'));
					},
					close: function(event, ui){
						dialog.dialog('destroy');
						dialog.detach();
						delete dialog;
					}
				}, config);
				
				if(config.width == 'auto')
					config.width = $(document).width() - 100;
				
				var dialog = $('<div class="dialog-message">' + message + '</div>').dialog(config);
				
				break;
		}
	};
	
	/**
	 * Добавляет ф-ю обратного вызова готовности инициализации блоков
	 *
	 * @param object callback Ф-я обратного вызова
	 * @param object context Контекст вызова ф-ии
	 * @return void
	 */
	this.onBlocksInitComplete = function(callback, context)
	{
		context = context || this;
		blocksInitCallbacks.push({
			callback: callback,
			context: context
		});
	};
	
	
	
	
	/* Инициализация */
	$(function()
	{
		setupLocale.call(application);
		application.widgets.init();
		
		// Инициализируем блоки шаблона
		$.each(application.blocks, function(index)
		{
			var block = new this();
			block._constructor = this;
			application.blocks[index] = block;
		});
		$.each(application.blocks, function(index)
		{
			if((this.exists ? this.exists() : true) && this.init && !this.initialized)
			{
				this.init();
				this.initialized = true;
			}
		});
		for( var i = 0; i < blocksInitCallbacks.length; i++)
		{
			blocksInitCallbacks[i].callback.call(blocksInitCallbacks[i].context);
		};
	});
};




/**
 * Виджеты
 */
application.widgets =  new function()
{
	/**
	 * Конструкторы виджетов
	 *
	 * @var object
	 */
	this.items = {};
	
	/**
	 * Создает стилизованную кнопку
	 *
	 * @param string|object selector Селектор узлов DOM
	 * @return void
	 */
	this.items['button'] = function(selector)
	{
		$(selector).each(function()
		{
			var field = $(this);
			if(field.find('> span.wrap').length)
				return;
			
			field.addClass('widget button');
			
			if(field.is('button'))
			{
				field.wrapInner('<span class="wrap"><span class="wrap"><nobr></nobr></span></span></button>');
			}
			else
			{
			
				var attr = [];
				for(var i =0; i < this.attributes.length; i++)
					attr.push(this.attributes[i].nodeName + '="' + this.attributes[i].nodeValue + '"');
				
				field.replaceWith('<button ' + attr.join(' ') + '><span class="wrap"><span class="wrap"><nobr>' + field.attr('value') + '</nobr></span></span></button>');
			}
		});
	};
	
	/**
	 * Создает стилизованное поле загрузки файла
	 *
	 * @param string|object selector Селектор узлов DOM
	 * @return void
	 */
	this.items['uploadpicker'] = function(selector)
	{
		$(selector).each(function()
		{
			var field = $(this);
			
			field
				.addClass('original')
				.wrap('<span class="widget upload"/>')
				.after('<input class="emulator" name="' + field.attr('name') + '-emu" value="" readonly /><a class="icon" href="#" title="Выбрать файл"></a>');
			
			var block = field.parent('.upload-field');
			field.bind("change",function(){
				block.find('.file-upload-emu').val($(this).val());
			});
		});
	};
	
	/**
	 * Создает поле выбора даты
	 *
	 * @param string|object selector Селектор узлов DOM
	 * @return void
	 */
	this.items['datepicker'] = function(selector)
	{
		if(!$.datepicker)
			return;
		
		var currentDate = new Date();
		$(selector).each(function(){
			var element = $(this);
			var dateMin = element.data('date-min') || new Date(1900, 1, 1);
			var dateMax = element.data('date-max') || currentDate;
			var yearFrom = element.data('year-from') || currentDate.getFullYear() - 10;
			var yearTo = element.data('year-to') || currentDate.getFullYear();
			
			element.datepicker({
				changeMonth: true,
				changeYear: true,
				minDate: dateMin,
				maxDate: dateMax,
				yearRange: yearFrom + ':' + yearTo
			})
			.keypress(function(event){
				switch(event.keyCode){
					case 13:
						//Если Enter нажимается при открытом календаре - убираем каленрарь и сабмитим форму
						if($(this).datepicker("widget").css("display") != "none"){
							event.stopPropagation();
							$(this).datepicker("hide");
							if(this.form)
								$(this.form).trigger("submit");
						}
						break;
				}
			})
			.after('<span class="datepicker-icon" title="Выбрать дату"></span>')
			.next('.datepicker-icon')
				.click(function(){
					$(this).prev('.datepicker').datepicker("show");
				});
		});
	};
	
	/**
	 * Создает поле выбора времени
	 *
	 * @param string|object selector Селектор узлов DOM
	 * @return void
	 */
	this.items['timepicker'] = function(selector)
	{
		if(!$.timepicker || !$.mask)
			return;
		
		$(selector)
			.timepicker()
			.mask('99:99');
	};
	
	/**
	 * Создает переключатель табов
	 * Примерный markup:
	 * <div class="widget tabpane">
	 * 	<ul class="tabs">
	 * 		<li><a class="fake" href="#tab-1">Tab 1</a></li>
	 * 		<li><a class="fake" href="#tab-2">Tab 2</a></li>
	 * 	</ul>
	 * 	<div class="panes">
	 * 		<div id="tab-1" class="pane">Content 1</div>
	 * 		<div id="tab-2" class="pane">Content 2</div>
	 * 	</div>
	 * </div>
	 *
	 * @param string|object selector Селектор узлов DOM
	 * @return void
	 */
	this.items['tabpane'] = function(selector)
	{
		var tabsSelector = '.tabs';
		var panesSelector = '.pane';
		
		$(selector).each(function()
		{
			var tabPane = $(this);
			tabPane.find(tabsSelector + ' a').click(function()
			{
				var item = $(this).parent('li').length ? $(this).parent('li') : $(this);
				item.siblings().removeClass('active');
				item.addClass('active');
				
				tabPane.find(panesSelector).hide();
				var obId = $(this).attr('rel') ? $(this).attr('rel') : $(this).attr('href');
				$(obId)
					.show()
					.trigger('.widget.tabpane:show');
				
				return false;
			});
			
			if($(tabsSelector + ' a.active', tabPane).length)
				$(tabsSelector + ' a.active', tabPane).click();
			else
				tabPane.find(tabsSelector + ' a:first').click();
		});
	};
	
	/**
	 * Accordion
	 * Markup:
	 * <a class="widget accordion" href="#blockID">show/hide block</a>
	 * <div id="blockID">Content</div>
	 *
	 * @param string|object selector Селектор узлов DOM
	 * @return void
	 */
	this.items['accordion'] = function(selector)
	{
		$(selector).click(function()
		{
			var targetBlock = $($(this).attr('href'));
			if(targetBlock.length)
			{
				if(targetBlock.is(':visible'))
				{
					$(this).removeClass('active');
					targetBlock.slideUp('fast').removeClass('active');
				}
				else
				{
					$(this).addClass('active');
					targetBlock.slideDown('fast').addClass('active');
				}
			}
			return false;
		});
	};
	
	/**
	 * Создает таблицу с интерлейсом
	 *
	 * @param string|object selector Селектор узлов DOM
	 * @return void
	 */
	this.items['interlace'] = function(selector)
	{
		$(selector).find('tr:even').addClass('even'); 
	}
	
	/**
	 * Инициализирует виджеты
	 *
	 * @param string selector Родительский элемент
	 * @return void
	 */
	this.init = function(selector)
	{
		if(selector === undefined)
			selector = $('body');
		else
			selector = $(selector);
		
		$.each(this.items, function(name){
			this.call(this, selector.find('.widget.' + name));
		}); 
	};
};




/**
 * Индикатор загрузки
 */
application.loading = new function()
{
	/**
	 * Последний объект, на котором отображался индикатор загрузки
	 *
	 * @var string|object
	 */
	var lastObject = '';
	
	/**
	 * Отображает индикатор загрузки
	 *
	 * @param string|object selector Селектор элемента, у которого отображается индикатор
	 * @return void
	 */
	this.show = function(selector)
	{
		lastObject = selector = selector || 'body';
		$(selector)
			.addClass('loading-indicator')
			.append('<div class="loading-layer"></div><div class="loading-icon"></div>');
	};
	
	/**
	 * Скрывает индикатор загрузки
	 *
	 * @param string|object selector Селектор элемента, у которого отображается индикатор
	 * @return void
	 */
	this.hide = function(selector)
	{
		selector = selector || lastObject;
		$(selector)
			.removeClass('loading-indicator')
			.find('> .loading-layer, > .loading-icon').remove();
	};
};




/**
 * Работа с историей браузера
 * 
 * @param string callback Имя ф-ии обратного вызова, вызываемой при переходах
 * @param object data Данные текущей страницы
 */
application.history = function(callback, data)
{
	/**
	 * Формирует history state
	 *
	 * @param object data State data
	 * @return object
	 */
	var buildState = function(data)
	{
		var state = {
			data: data instanceof Object ? data : {}
		};
		
		if(state.data.title === undefined)
			state.data.title = document.title;
		
		state.generatedByApplicationHistory = true;
		state.handledBy = callback;
		
		return state;
	};
	
	/**
	 * Добавляет обработчик браузерного события
	 *
	 * @return void
	 */
	var addListener = function()
	{
		if(window.addEventListener)
		{
			window.addEventListener('popstate', function(event)
			{
				//Пропускаем события, созданные не через application.history (например, Chrome такие генерирует)
				if(!(event.state instanceof Object) || event.state.generatedByApplicationHistory === undefined)
					return;
				
				if(event.state.data !== undefined && event.state.data.title !== undefined)
					document.title = event.state.data.title;
				
				//Дергаем ф-ии обратного вызова только для своих хозяев
				if(event.state.handledBy && event.state.handledBy == callback)
					(new Function('url, data', event.state.handledBy + '(url, data);'))(document.location.href, event.state.data);
			}, false);
		}
	};
	
	/**
	 * Возвращает текущий пункт истории
	 *
	 * @return object
	 */
	this.current = function()
	{
		return history.state;
	};
	
	/**
	 * Обновляет текущий пункт истории
	 *
	 * @param string url URL
	 * @param object data Данные
	 * @return void
	 */
	this.replace = function(url, data)
	{
		if(history.replaceState)
		{
			var state = buildState(data);
			
			history.replaceState(state, state.data.title, application.utils.url.getFull(url));
		}
	};
	
	/**
	 * Добавляет новый пункт в историю
	 *
	 * @param string url URL
	 * @param object data Данные
	 * @return void
	 */
	this.push = function(url, data)
	{
		if(history.pushState)
		{
			var state = buildState(data);
			
			history.pushState(state, state.data.title, application.utils.url.getFull(url));
		}
	};
	
	
	
	
	/**
	 * Инициализия
	 */
	this.replace(document.location.href, data);
	
	addListener();
};

/**
 * Позволяет получить singleton instance application.history
 *
 * @param string callback Имя ф-ии обратного вызова, вызываемой при переходах
 * @param object data Данные текущей страницы
 * 
 * @return application.history
 */
application.history.getInstance = function(callback, data)
{
	if(this.instances === undefined)
		this.instances = {};
	
	if(this.instances[callback] === undefined)
		this.instances[callback] = new application.history(callback, data);
	
	return this.instances[callback];
}




/**
 * Утилиты для работы с Cookie
 */
application.cookie =
{
	/**
	 * Set cookie
	 *
	 * @param string name Cookie name
	 * @param string value Cookie value
	 * @param string time Lifetime
	 * @param string path
	 * @return void
	 */
	set: function(name, value, time, path)
	{
		var time = this.is_undefined(time) ? 0 : time;
		var path = this.is_undefined(path) ? '/' : path;
		var expires = new Date();
		time = expires.getTime()+(time*1000);
		
		expires.setTime(time);
		document.cookie = name+'='+value+'; expires='+expires.toGMTString() +"; path="+path;
	},
	
	/**
	 * Get cookie
	 *
	 * @param string name Cookie name
	 * @return string Cookie value
	 */
	get: function(name)
	{
		var cookie = " " + document.cookie;
		var search = " " + name + "=";
		var setStr = null;
		var offset = 0;
		var end = 0;
		if (cookie.length > 0) {
			offset = cookie.indexOf(search);
			if (offset != -1) {
				offset += search.length;
				end = cookie.indexOf(";", offset)
				if (end == -1) {
					end = cookie.length;
				}
				setStr = unescape(cookie.substring(offset, end));
			}
		}
		return(setStr);
	}
};




/**
 * User-agent пользователя
 */
application.ua = new function()
{
	/**
	 * User agent  - iPad
	 *
	 * @var boolean
	 */
	this.isIPad = /iPad/i.test(navigator.userAgent);
	
	/**
	 * User agent  - iPhone
	 *
	 * @var boolean
	 */
	this.isIPhone = /iPhone OS/i.test(navigator.userAgent);
	
	/**
	 * User agent  - Android
	 *
	 * @var boolean
	 */
	this.isAndroid = /Android/i.test(navigator.userAgent);
	
	/**
	 * User agent поддерживает WebKit-фишки
	 *
	 * @var boolean
	 */
	this.isWebKit = /WebKit/i.test(navigator.userAgent);
	
	/**
	 * User agent имеет тоuch интерфейс
	 *
	 * @var boolean
	 */
	this.isTouchable = function(){
		var result = (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
		this.isTouchable = function(){
			return result;
		};
		return result;
	};
};




/**
 * Moveable DOM node with support animation, drag, touch wipe and mouse wheel
 * 
 * For mousewheel jQuery Mousewheel plugin required
 * For drag "jQuery UI Draggable" required
 * 
 * @param object config Config
 */
application.moveable = function(config){
	config = $.extend({
		domNode: '',//DOM selector or jQuery object
		animated: true,//Enable animation
		draggable: true,//Enable drag
		touchable: application.ua.isTouchable(),//Enable touch
		wheelable: true,//Enable mouse wheel
		axis: '',//[x|y|<empty>]
		position: {//node position
			x: 0,
			y: 0
		},
		wheelStep: {
			x: 100,
			y: 100
		},
		wheelConnectDomNode: null,//Selector or jQuery object to connect mouse wheel handler
		wheelConnectToParent: true,//Connect mouse wheel handler to parenr DOM node (if wheelConnectDomNode is not defined)
		cssProperties: {//CSS property for apply position (if not 3d transform used)
			x: 'left',//[left|margin-left|...]
			y: 'top'//[top|margin-top|...]
		},
		cssPosition: 'relative',//CSS position for container [relative|absolute|<empty>]
		animation: {
			timingFunction: 'linear',//http://www.w3.org/TR/css3-transitions/#transition-timing-function
			duration: 400,//Milliseconds
			delay: 0//Milliseconds
		},
		moveDetectionRatio: 1.5,//Axis move detection ratio
		useTransition: application.utils.css.supportTransition(),
		useTransition3dTransform: application.ua.isTouchable() && application.utils.css.support3dTransform(),
		onMoveStart: null,
		onMove: null,
		onMoveStop: null,
		onWheelRotate: null,
		onChangePosition: null
	}, config || {});
	
	if(config.touchable)
		config.draggable = false;
	
	switch(config.axis){
		case 'x':
			config.cssProperties.y = '';
			break;
		case 'y':
			config.cssProperties.x = '';
			break;
	}
	
	config.domNode = $(config.domNode);
	var instance = this;
	
	/**
	 * Return config
	 *
	 * @return object
	 */
	this.getConfig = function(){
		return config;
	};
	
	/**
	 * Enable animation
	 *
	 * @return void
	 */
	this.enableAnimation = function(){
		config.animated = true;
		
		var transition = config.animation.duration + 'ms ' + config.animation.timingFunction + ' ' + config.animation.delay + 'ms';
		if(config.useTransition3dTransform){
			config.domNode.crossCss({
				'transitionTransform': transition
			});
		}else if(config.useTransition){
			var transitions = [];
			for(var key in config.cssProperties){
				if(config.cssProperties[key]){
					transitions.push(config.cssProperties[key] + ' ' + transition);
				}
			}
			config.domNode.crossCss({
				'transition': transitions.join(',')
			});
		}
	};
	
	/**
	 * Disable animation
	 *
	 * @return void
	 */
	this.disableAnimation = function(){
		config.animated = false;
		
		if(config.useTransition3dTransform){
			config.domNode.crossCss({
				'transitionTransform': '0ms linear'//Important or Android built-in browser
			});
		}else if(config.useTransition){
			config.domNode.crossCss({
				'transition': 'none 0ms'
			});
		}
	};
	
	/**
	 * Return position
	 *
	 * @return object
	 */
	this.getPosition = function(){
		return config.position;
	};
	
	/**
	 * Set position
	 *
	 * @param object|null position New position {x: n, y: n}
	 * @return void
	 */
	this.setPosition = function(position){
		if(position)
			config.position = position;
		
		if(config.useTransition3dTransform){
			config.domNode.crossCss({
				'transform': 'translate3d(' + config.position.x + 'px, ' + config.position.y + 'px, 0px)'
			});
		}else{
			var cssParams = {};
			for(var key in config.cssProperties){
				if(config.cssProperties[key])
					cssParams[config.cssProperties[key]] = config.position[key] + 'px';
			}
			
			if(config.useTransition){
				config.domNode.css(cssParams);
			}else if(config.animated){
				config.domNode
					.stop()
					.animate(cssParams, config.animation.duration);
			}else{
				config.domNode.css(cssParams);
			}
		}
		
		if(config.onChangePosition)
			config.onChangePosition.call(this, config.position);
	};
	
	/**
	 * Set drag handler if supported
	 *
	 */
	if(config.draggable && config.domNode.draggable){
		var dragStartPosition = {};
		var resetLeft = config.useTransition3dTransform || config.cssProperties.x != 'left';
		var resetTop = config.useTransition3dTransform || config.cssProperties.y!= 'top';
		
		config.domNode.draggable({
			axis: config.axis,
			start: function(event, ui){
				instance.disableAnimation();
				
				dragStartPosition = {x: 0, y: 0};
				if(resetLeft)
					dragStartPosition.x = config.position.x;
				if(resetTop)
					dragStartPosition.y = config.position.y;
				
				if(config.onMoveStart)
					config.onMoveStart.call(instance, event);
			},
			drag: function(event, ui){
				var dragPosition = {x: dragStartPosition.x + ui.position.left, y: dragStartPosition.y + ui.position.top};
				
				var handlerResult = config.onMove ? config.onMove.call(instance, event, dragPosition) : true;
				if(handlerResult !== false)
					instance.setPosition(dragPosition);
				
				ui.position.left = resetLeft ? 0 : dragPosition.x;
				ui.position.top = resetTop ? 0 : dragPosition.y;
			},
			stop: function(event, ui){
				instance.enableAnimation();
				
				if(config.onMoveStop)
					config.onMoveStop.call(instance, event);
			}
		});
	};
	
	/**
	 * Set touch handler if supported
	 *
	 */
	if(config.touchable){
		config.domNode.each(function(){
			var touchStartData = {};
			
			this.ontouchstart = function(event){
				instance.disableAnimation();
				
				touchStartData.page = {x: 0, y: 0};
				touchStartData.position = config.position;
				if(event.changedTouches.length){
					var touch = event.changedTouches[0];
					touchStartData.page.x = touch.pageX;
					touchStartData.page.y = touch.pageY;
				};
				
				if(config.onMoveStart)
					config.onMoveStart.call(instance, event);
				
			};
			
			this.ontouchmove = function(event){
				var touchPosition = {x: 0, y: 0};
				var deltaX = 0;
				var deltaY = 0;
				var ratio = {x: 0, y: 0};
				if(event.changedTouches.length){
					var touch = event.changedTouches[0];
					
					if(config.axis != 'y'){
						deltaX = touch.pageX - touchStartData.page.x;
						touchPosition.x = touchStartData.position.x + deltaX;
						ratio.x = Math.abs(deltaX) / Math.max(Math.abs(deltaY), 0.001);
					}
					
					if(config.axis != 'x'){
						deltaY = touch.pageY - touchStartData.page.y;
						touchPosition.y = touchStartData.position.y + deltaY;
						ratio.y = Math.abs(deltaY) / Math.max(Math.abs(deltaX), 0.001);
					}
				};
				
				if(!config.axis || ratio[config.axis] > config.moveDetectionRatio)
					event.preventDefault();
				
				if(deltaX != 0 || deltaY != 0){
					var handlerResult = config.onMove ? config.onMove.call(instance, event, touchPosition) : true;
					if(handlerResult !== false)
						instance.setPosition(touchPosition);
				}
			};
			
			this.ontouchend = function(e){
				instance.enableAnimation();
				
				if(config.onMoveStop)
					config.onMoveStop.call(instance, event);
			};
		});
	};
	
	/**
	 * Set mouse wheel handler if supported
	 *
	 */
	if(config.wheelable && config.domNode.mousewheel){
		var onMouseWheel = function(event, delta){
			var position = instance.getPosition();
			
			if(config.axis != 'y')
				position.x += delta * config.wheelStep.x;
			if(config.axis != 'x')
				position.y += delta * config.wheelStep.y;
			
			var handlerResult = false;
			if(config.onWheelRotate)
				handlerResult = config.onWheelRotate.call(instance, event, delta, position);
			
			if(position.x !== null && position.y !== null)
				instance.setPosition(position);
			
			return handlerResult;
		};
		config.wheelConnectDomNode ? $(config.wheelConnectDomNode).mousewheel(onMouseWheel) : (
			config.wheelConnectToParent ? config.domNode.parent().mousewheel(onMouseWheel) : config.domNode.mousewheel(onMouseWheel)
		);
	};
	
	/**
	 * Initialization
	 *
	 */
	if(config.useTransition3dTransform){
		config.domNode.crossCss({
			'transform': 'translate3d(' + config.position.x + 'px, ' + config.position.y + 'px, 0px)'
		});
	}
	if(config.cssPosition){
		config.domNode.css({
			'position': config.cssPosition
		});
	}
	this.setPosition();
	if(config.animated)
		this.enableAnimation();
};




/**
 * Утилиты
 */
application.utils = new function()
{
	/**
	 * Наследует один класс от другого
	 *
	 * @param object child Потомок
	 * @param object parent Родитель
	 * @param object overrides Перезаписываемые св-ва и методы
	 * @return object Потомок
	 */
	this.extend = function(child, parent, overrides)
	{
		if(typeof parent == 'object')
		{
			overrides = parent;
			parent = child;
			child = function(){child.superclass.constructor.apply(this, arguments)};
		}
		
		var f = function(){};
		f.prototype = parent.prototype;
		child.prototype = new f();
		child.prototype.constructor = child;
		child.superclass = parent.prototype;
		
		this.override(child, overrides);
		
		return child;
	};
	
	/**
	 * Перезаписывает св-ва и методы класса
	 *
	 * @param object clss Перезаписываемый класс
	 * @param object parent Родитель
	 * @param object overrides Перезаписываемые св-ва и методы
	 * @return void
	 */
	this.override = function(clss, overrides)
	{
		this.apply(clss.prototype, overrides);
	};
	
	/**
	 * Расширяет сво-ва одного объекта другим
	 *
	 * @param object object Расширяемый объект
	 * @param object overrides Перезаписываемые св-ва
	 * @return void
	 */
	this.apply = function(object, overrides)
	{
		overrides = overrides || {};
		var primitiveObject = {};
		for(var i in overrides){
			if(typeof primitiveObject[i] == 'undefined' || primitiveObject[i] != overrides[i])
				object[i] = overrides[i];
		}
	};
	
	/**
	 * Возвращает текст в атрибуте href гиперссылки, чтоящий после #
	 * Учитывает баг IE, который выдает полный URL для атрибута href
	 *
	 * @param object|string link Элемент-ссылка
	 * @return string
	 */
	this.getHashWord = function(link)
	{
		return $(link).attr('href').replace(/^.*#/, '#');
	};
	
	/**
	 * Загруженные изображения
	 *
	 * @var object
	 */
	var preloadedImages = {};
	
	/**
	 * Предзагрузчик изображений с поддержкой jQuery deferred
	 * Использование:
	 * $.when(
	 *     application.utils.preloadImage(src1),
	 *     application.utils.preloadImage(src2)
	 * ).done(function(){
	 *     //Images are preloaded
	 * });
	 * 
	 * @param string src Адрес изображения
	 * @return object
	 */
	this.preloadImage = function(src)
	{
		var id = 'id' + src.replace(/[^A-z0-9]/g, '');
		var defer = $.Deferred();
		
		if(preloadedImages[id] === undefined)
			preloadedImages[id] = new Image();
		
		preloadedImages[id].src = '';
		preloadedImages[id].onload = function()
		{
			defer.resolve();
		};
		preloadedImages[id].src = src;
		
		return defer.promise();
	};
	
	/**
	 * Склоняет существительное с числительным
	 *
	 * @param integer number Число
	 * @param array cases Варианты существительного в разных падежах и числах (nominativ, genetiv, plural). Пример: ['комментарий', 'комментария', 'комментариев']
	 * @param boolean incNum Добавить само число в результат
	 * @return string
	 */
	this.getNumEnding = function(number, cases, incNum)
	{
		var numberMod = number % 100;
		incNum = incNum === undefined ? true : incNum;
		
		if(numberMod >= 11 && numberMod <= 19)
		{
			result = cases[2];
		}
		else
		{
			numberMod = numberMod % 10;
			switch(numberMod)
			{
				case 1:
					result = cases[0];
					break;
				case 2:
				case 3:
				case 4:
					result = cases[1];
					break;
				default:
					result = cases[2];
			}
		}
		
		return incNum ? number + ' ' + result : result;
	};
	
	/**
	 * Check undefined
	 *
	 * @param string xvar Variable to check
	 * @return boolean
	 */
	this.isUndefined = function(xvar)
	{
		return typeof xvar == 'undefined' ? true : false;
	};
};




/**
 * Утилиты для работы с CSS
 */
application.utils.css =
{
	/**
	 * Тестирует стиль
	 *
	 * @param array checks Тесты
	 * @param object callback Функция проверки
	 * @return boolean
	 */
	test: function(checks, callback){
		var success = false;
		for(var key = 0; key < checks.length; key++){
			var check = checks[key];
			var div = document.createElement('div');
			div.style.cssText = check.style;
			
			document.body.appendChild(div);
			success = callback(div, check, key);
			document.body.removeChild(div);
			
			if(success){
				break;
			}
		}
		
		return success;
	},
	
	/**
	 * User agent поддерживает 2d transform
	 *
	 * @return boolean
	 */
	supportTransform: function()
	{
		var result = (
			typeof document.body.style.transform != 'undefined' ||
			typeof document.body.style.webkitTransform != 'undefined' ||
			typeof document.body.style.mozTransform != 'undefined' ||
			typeof document.body.style.msTransform != 'undefined' ||
			typeof document.body.style.oTransform != 'undefined'
		);
		this.supportTransform = function(){
			return result;
		};
		return result;
	},
	
	/**
	 * User agent поддерживает 3d transform
	 *
	 * @return boolean
	 */
	support3dTransform: function()
	{
		var result = this.test([{
				'style': 'transform: translate3d(0, 0, 0)',
				'property': 'transform'
			},{
				'style': 'transform: -moz-translate3d(0, 0, 0)',
				'property': 'transform'
			},{
				'style': 'transform: -ms-translate3d(0, 0, 0)',
				'property': 'transform'
			},{
				'style': 'transform: -o-translate3d(0, 0, 0)',
				'property': 'transform'
			},{
				'style': 'transform: -webkit-translate3d(0, 0, 0)',
				'property': 'transform'
			},{
				'style': '-moz-transform: translate3d(0, 0, 0)',
				'property': 'MozTransform'
			},{
				'style': '-ms-transform: translate3d(0, 0, 0)',
				'property': 'MSTransform'
			},{
				'style': '-o-transform: translate3d(0, 0, 0)',
				'property': 'OTransform'
			},{
				'style': '-webkit-transform: translate3d(0, 0, 0)',
				'property': 'webkitTransform'
			}],
			function(node, check){
				return node.style[check.property] ? true : false;
			}
		);
		
		this.support3dTransform = function(){
			return result;
		};
		return result;
	},
	
	/**
	 * User agent поддерживает transitions
	 *
	 * @return boolean
	 */
	supportTransition: function()
	{
		var result = (
			typeof document.body.style.transition != 'undefined' ||
			typeof document.body.style.webkitTransition != 'undefined' ||
			typeof document.body.style.mozTransition != 'undefined' ||
			typeof document.body.style.msTransition != 'undefined' ||
			typeof document.body.style.oTransition != 'undefined'
		);
		this.supportTransition = function(){
			return result;
		};
		return result;
	}
}



/**
 * Утилиты для работы с URL
 */
application.utils.url =
{
	/**
	 * Возвращает полный URL текущей страницы, включая домен и протокольный префикс
	 *
	 * @param string path Pathname
	 * @return string
	 */
	getFull: function(path)
	{
		return path.search('//') == -1 ?
			document.location.protocol + '//' + document.location.host + path
		:
			path;
	},
	
	/**
	 * Разбирает URL на части
	 *
	 * @param string url URL
	 * @return object {protocol, host, port, pathname, query, hash}
	 */
	parse: function(url)
	{
		var result = {
			protocol: '',
			host: '',
			port: '',
			pathname: '',
			query: '',
			hash: ''
		}
		
		//Get protocol
		url = url.split('://');
		if(url.length > 1)
		{
			result.protocol = url.shift();
			url[0] = '//' + url[0];
		}
		url = url.join('://');
		
		//Get host
		url = url.split('//');
		if(url.length > 1)
		{
			url.shift();
			url = url.join('//').split('/');
			result.host = url.shift();
			url = '/' + url.join('/');
		}
		else
		{
			url = url.join('//');
		}
		
		//Get port
		result.host = result.host.split(':');
		if(result.host.length > 1)
			result.port = result.host.pop();
		result.host = result.host.join(':');
		
		//Get hash
		url = url.split('#');
		if(url.length > 1)
			result.hash = url.pop();
		url = url.join('#');
		
		//Get query
		url = url.split('?');
		if(url.length > 1)
			result.query = url.pop();
		url = url.join('?');
		
		//Get path
		result.pathname = url;
		
		return result;
	}
};



 /**
 * CSS3 crossbrowser support jQuery plugin
 */
$.fn.crossCss = function(config)
{
	config = config || {};
	var nodes = this;
	
	var applyCommon = function(key, value, prefixedValue){
		var map = {};
		map['-moz-' + key] = (prefixedValue ? '-moz-' : '') + value;
		map['-ms-' + key] = (prefixedValue ? '-ms-' : '') + value;
		map['-o-' + key] = (prefixedValue ? '-o-' : '') + value;
		map['-webkit-' + key] = (prefixedValue ? '-webkit-' : '') + value;
		map[key] = value;
		
		var str = [];
		for(var key in map){
			str.push(key + ': ' + map[key]);
		}
		
		nodes.css(map);
	};
	
	$.each(config, function(key){
		switch(key){
			case 'transformTranslate3d':
				applyCommon('transform', 'translate3d('+ this.x +', '+ this.y +', '+ this.z +')');
				break;
			case 'transitionTransform':
				applyCommon('transition', 'transform ' + this, true);
				break;
			default:
				applyCommon(key, this);
				break;
		}
	});
	
	return this;
};