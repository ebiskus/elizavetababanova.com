/**
 * @category	Individ
 * @copyright	2012 Individ LTD (http://individ.ru)
 */

/**
 * Scrollbar jQuery plugin
 * application.js required
 */
$.fn.scrollbar = function(config){
	config = $.extend({
		wheelable: true,
		draggable: true,
		native: false,
		wheelSensitivity: 0.1,
		axis: 'x',//[x|y]
		cssProperties: {//CSS property for apply position (if not 3d transform used)
			x: 'left',//[left|margin-left|...]
			y: 'top'//[top|margin-top|...]
		},
		cssPosition: 'relative',//[relative|absolute]
		animation: {
			timingFunction: 'linear',
			duration: 200,//Milliseconds
			delay: 0//Milliseconds
		},
		onChangePosition: null,
		onMoveStart: null,
		onMove: null,
		onMoveStop: null
	}, config || {});
	
	var position = 0;
	var positionOffset = 0;
	var railNode = $('<div class="jq-scrollbar-rail">');
	var railWidth = 0;
	var railHeight = 0;
	var barNode = $('<div class="jq-scrollbar-bar">');
	var barWidth = 0;
	var barHeight = 0;
	var moveable = null;
	var instance = this;
	
	/**
	 * Resize handler
	 *
	 * @return void
	 */
	var onResize = function(){
		railWidth = railNode.width();
		railHeight = railNode.height();
		
		barWidth = barNode.width();
		barHeight = barNode.height();
	};
	
	/**
	 * Enable animation
	 *
	 * @return void
	 */
	this.enableAnimation = function(){
		moveable.enableAnimation();
	};
	
	/**
	 * Disable animation
	 *
	 * @return void
	 */
	this.disableAnimation = function(){
		moveable.disableAnimation();
	};
	
	/**
	 * Convert offset in pixels to position
	 *
	 * @param integer offset Offset in pixels
	 * @return float
	 */
	this.offsetToPosition = function(offset){
		switch(config.axis)
		{
			case 'x':
				return railWidth > barWidth ? offset / (railWidth - barWidth) : 0;
				break;
			
			default:
				return railHeight > barHeight ? offset / (railHeight - barHeight) : 0;
				break;
		}
	};
	
	/**
	 * Store scrollbar position
	 *
	 * @param float pos New position (0-1)
	 * @param boolean createEvents Generate events
	 * @return void
	 */
	this.storePosition = function(pos, createEvents){
		createEvents = createEvents === undefined ? true : createEvents;
		
		switch(config.axis)
		{
			case 'x':
				var total = railWidth - barWidth;
				break;
				
			defaultStatus:
				var total = railHeight - barHeight;
				break;
		}
		
		position = Math.max(0, Math.min(pos, 1));
		positionOffset = Math.round(position * total);
		
		if(createEvents && config.onChangePosition)
			config.onChangePosition.call(instance, position, positionOffset);
	};
	
	/**
	 * Return scrollbar position (0-1)
	 *
	 * @return float
	 */
	this.getPosition = function(){
		return position;
	};
	
	/**
	 * Set scrollbar position
	 *
	 * @param float pos New position (0-1)
	 * @param boolean createEvents Generate events
	 * @return void
	 */
	this.setPosition = function(pos, createEvents){
		this.storePosition(pos, createEvents);
		
		moveable.setPosition({
			x: config.axis == 'x' ? positionOffset : 0,
			y: config.axis == 'x' ? 0 : positionOffset
		});
	};
	
	/**
	 * Node move start handler
	 *
	 * @param object event
	 * @return void
	 */
	var onMoveStart = function(event)
	{
		if(config.onMoveStart)
			config.onMoveStart.call(instance);
	};
	
	/**
	 * Node move handler
	 *
	 * @param object event
	 * @param object position
	 * @return void
	 */
	var onMove = function(event, position)
	{
		switch(config.axis)
		{
			case 'x':
				position.x = Math.max(position.x, 0);
				position.x = Math.min(position.x, railWidth - barWidth);
				instance.storePosition(instance.offsetToPosition(position.x));
				break;
			
			default:
				position.y = Math.max(position.y, 0);
				position.y = Math.min(position.y, railHeight - barHeight);
				instance.storePosition(instance.offsetToPosition(position.y));
				break;
		}
		
		if(config.onMove)
			config.onMove.call(instance, position);
	};
	
	/**
	 * Node move stop handler
	 *
	 * @param object position
	 * @return void
	 */
	var onMoveStop = function(position)
	{
		if(config.onMoveStop)
			config.onMoveStop.call(instance);
	};
	
	/**
	 * Mouse wheel handler
	 *
	 * @param object event
	 * @param integer delta
	 * @param object movePosition
	 * @return boolean
	 */
	var onWheelRotate = function(event, delta, movePosition)
	{
		movePosition.x = null;
		
		var pos = position - config.wheelSensitivity * delta;
		instance.setPosition(pos);
		
		return config.native ? pos < 0.02 || pos > 0.98 : false;
	};
	
	/**
	 * Initialization
	 */
	this.addClass('jq-scrollbar jq-scrollbar-' + config.axis);
	railNode.appendTo(this);
	barNode.appendTo(railNode)
	
	$(window).resize(onResize);
	
	moveable = new application.moveable({
		domNode: barNode,
		draggable: config.draggable,
		wheelable: config.wheelable,
		wheelConnectDomNode: this,
		axis: config.axis,
		cssProperties: config.cssProperties,
		cssPosition: config.cssPosition,
		animation: config.animation,
		onMoveStart: onMoveStart,
		onMove: onMove,
		onMoveStop: onMoveStop,
		onWheelRotate: onWheelRotate
	});
	
	onResize();
	
	this.setPosition(0, false);
	
	return this;
}