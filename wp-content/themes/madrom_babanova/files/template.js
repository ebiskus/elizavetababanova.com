/**
 * @category	Individ
 * @copyright	2012 Individ LTD (http://individ.ru)
 */

 
/**
* Global vars
*/
application.vars = {
	SITE_TEMPLATE_PATH: '/bitrix/templates/radislavgandapas',
	SITE_TEMPLATE_IMG: '/bitrix/templates/radislavgandapas/images'
};



/**
 * Common
 */
application.blocks.common = function()
{
	/**
	 * Link to "Common" block instance
	 *
	 * @var object
	 */
	var commonPage = this;
	
	
	/**
	 * Page resize handler
	 *
	 * @return void
	 */
	this.onResize = function()
	{
		application.vars.wHeight = $(window).height();
		application.vars.wWidth = $(window).width();
		application.vars.docHeight = $(document).height();
		application.vars.docWidth = $(document).width();
		
		application.vars.miniparallax = $('body').is('.miniparallax') ? true : false
		
		application.vars.backHeight = 1926;
		application.vars.backDef = application.vars.docHeight - application.vars.backHeight;
		if(application.vars.backDef < 0) application.vars.backDef = 200;
		if(application.vars.miniparallax)application.vars.backDef = 2000;
		
		application.vars.scrollLim = application.vars.docHeight - application.vars.wHeight;
	};
	
	
	/**
	 * Init block
	 *
	 * @return void
	 */
	this.init = function()
	{
		$('input[placeholder], textarea[placeholder]').placeholder();
		
		$(window).resize(function()
		{
			commonPage.onResize();
		});
		commonPage.onResize();
		
		// fancybox
		$('a.fancybox').fancybox({
			padding: 0,
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
		
		//Scroll handler
		var wrapper = $('#wrapper');
		if(!application.vars.miniparallax && !application.ua.isTouchable())
		{
			$(window).scroll(function()
			{
				var scrollTop = $(this).scrollTop();
				var backPos = - ( application.vars.backDef * scrollTop / application.vars.scrollLim );
				
				wrapper.css({'background-position': 'center '+backPos+'px'});
			});
		}
	};
};




/**
 * Блок шаблона: шапка
 */
application.blocks.header = function()
{
	/**
	 * Проверяет наличие блока
	 *
	 * @return boolean
	 */
	this.exists = function()
	{
		return true;
	};
	
	/**
	 * Init block
	 *
	 * @return void
	 */
	this.init = function()
	{
	};
};




/**
 * Main page
 */
application.blocks.mainPage = function()
{
	/**
	 * Link to "Main" block instance
	 *
	 * @var object
	 */
	var mainPage = this;
	
	
	/**
	 * Check block
	 *
	 * @return boolean
	 */
	this.exists = function()
	{
		return $('#main-trainings').length > 0;
	};
	
	/**
	 * Init block
	 *
	 * @return void
	 */
	this.init = function()
	{
		if($('#trainings-car section article').length > 1)
		{
			$('#trainings-car section')
				.touchSlider({
					draggable: false,
					wheelable: false,
					containerNode: 'div.list',
					itemNode: 'article',
					autoInterval: 5000,
					applyWidths: false,
					controlsContainer: '#main-controls'
				});
		}
		
		var overlay = $('#overlay');
		var clientsAllLink = $('#clients-all-link');
		var logosAll = $('#logos-all');
		
		clientsAllLink.click(function()
		{
			var scrollTop = $(window).scrollTop();
			if(scrollTop < 940)
			{
				$('html,body').animate({scrollTop: 940});
			}
			
			if($(this).is('.active')){
				$(this).removeClass('active');
				overlay.fadeOut('fast');
				logosAll.fadeOut('fast');
			}else{
				application.blocks.common.onResize();
				$(this).addClass('active');
				overlay.height(application.vars.docHeight).fadeIn('fast');
				logosAll.fadeIn('fast');
			}
		});
		overlay.click(function(){
			clientsAllLink.trigger('click');
		});
	};
};
