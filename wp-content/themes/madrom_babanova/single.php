<?php
require('./wp-blog-header.php');
require_once('header.php');
require_once('functions.php');
?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
 bcn_display();
}
?>
</div>


<div class="singl">

<div id="wrap" class="container singl">

<div id="content" class="eleven columns">


	
					<?php if (have_posts()) : ?>	
						<?php while (have_posts()) : the_post(); ?>
						<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				
        	

							<h1 class="title avant_book"><?php the_title(); ?></h1>
							<div class="postdate avant_book">Опубликовано <?php the_time('d.m.y') ?><?php if (current_user_can('edit_post', $post->ID)) { ?> <?php edit_post_link('Edit', '', ''); } ?></div>
			
							<div class="entry">
								<!--
								<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(300,225), array("class" => "alignleft post_thumbnail")); } ?>
								-->
								<?php the_content('Читать далее &raquo;'); ?>
								<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
								 <div class="clr"></div>
						</div>

<div class="navigation" style="margin-top:30px;">
<div class="alignleft"  style="border:none;">
<?php previous_post_link('&laquo;&laquo; %link', 'Предыдущая статья', FALSE, '30 and 23'); ?>
</div>
<div class="alignright" style="border:none;">
<?php next_post_link('%link &raquo; &raquo; ',
 'Следующая статья', FALSE, '30 and 23'); ?>
</div>
</div> <!-- end navigation -->

							<div class="postmeta">
              <i>Рубрика</i>: <?php the_category(', ') ?>
              <!-- 
              <?php if(get_the_tags()) { ?><?php  the_tags('Метки: ', ', '); } ?>
              -->
              </div>
             



						   <?php require('social_button.php'); ?>   	
					 
						<div class="podpiska_single">
<p>Понравился материал? Подпишитесь на бесплатную рассылку блога!</p>
<img src="<?php bloginfo('template_directory'); ?>/image/arrow-podpiska.png" class="left">
<form accept-charset="utf-8" action="https://app.getresponse.com/add_contact_webform.html" method="post">
        <input type="hidden" name="webform_id" value="105832">  
			<div class="iwrapu">
				<input type="text" placeholder="Ваше имя..." class="field text placeholder" name="name" id="oa-firstname" style="width: 190px; height: 28px; border: 2px solid #ccc; border-radius: 4px;padding:0px 5px;">
			</div>
			<div class="iwrapu">
              <input type="text" placeholder="Ваш email..." class="field text placeholder" name="email" id="oa-email" style="width: 190px;  height: 28px;  border: 2px solid #ccc;  border-radius: 4px;padding:0px 5px;">   
			</div>
			<div style="width: 130px; display: block; float: right;margin-top:3px;" class="button_foot">
			  <button value="1" name="submit" type="submit" style="height: 28px;width: 130px;cursor: pointer;border: none;padding: 0;background: url(/wp-content/themes/madrom_babanova/image/podpiska-button.png) center center no-repeat;"></button>
			          
            
              <input type="hidden" name="webform_id" value="105832" />
          </form> 
          <script type="text/javascript" src="http://app.getresponse.com/view_webform.js?wid=105832&mg_param1=1"></script>
			  
			</div>
</div>
						
						<!--
						<div class="navigation clearfix">
								<div class="alignleft"><?php previous_post_link('&laquo; %link') ?></div>
								<div class="alignright"><?php next_post_link('%link &raquo;') ?></div>
						
					 
              
	          
							<?php edit_post_link('Редактировать эту запись','','.'); ?>
							--> 	


						</div><!--/post-<?php the_ID(); ?>-->
				<?php comments_template(); ?>
				
				<?php endwhile; ?>
			
				<?php endif; ?>
				
</div><!-- #content-->

   
<?php
  require('sidebar.php');
?>

 </div><!-- #wrapper2 --> 
</div>



<div id="footer">
<?php require('footer.php'); ?>
</div><!-- #footer --> 

</div><!-- #wrapper3 -->