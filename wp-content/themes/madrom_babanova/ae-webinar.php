<?php
/*
 Template Name: Страница трансляции вебинара АЭ
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<link rel="stylesheet" type="text/css" href="style2.css">
	<meta property="fb:admins" content="100003273128680"/>
		<meta property="vk:app_id" content="3398000" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
		<link href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/style/style2.css" rel="stylesheet">
	<title>Открытый онлайн семинар. Как перестать надрываться и стать счастливым. "</title>
	
	<meta property="og:title" content="Открытый онлайн семинар. Как перестать надрываться и стать счастливым. "/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://www.elizavetababanova.com/happiness-seminar/"/>
	<meta name="description" content="7 успешных стратегий, найденных одновременно в науке и религии.">
   <meta property="og:image" content="http://elizavetababanova.com/happiness-seminar2/img/share2.jpg">
   
	<script src="http://www.elizavetababanova.com/wp-content/themes/madrom_babanova/js/jquery-1.7.1.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="http://www.elizavetababanova.com/wp-content/themes/madrom_babanova/js/jquery.fancybox.css" type="text/css" media="screen">
<script type="text/javascript" src="http://www.elizavetababanova.com/wp-content/themes/madrom_babanova/js/jquery.fancybox.pack.js"></script> 
<script src="http://www.elizavetababanova.com/wp-content/themes/madrom_babanova/js/waypoints.min.js"></script>	
	 <style>@font-face{font-family: Myriad Pro; src: url('asd.otf'); font-weight:normal; font-style: normal;} 
	@font-face{font-family: Myriad Pro; src: url('myriad.woff'); font-weight:normal; font-style: normal;}
@font-face{font-family: Myriad Pro; src: url('MyriadPro-Regular.eot'); font-weight:normal; font-style: normal;}
	@font-face{font-family: Myriad Pro; src: url('MyriadPro-Regular.svg'); font-weight:normal; font-style: normal;}
	@font-face{font-family: Myriad Pro; src: url('MyriadPro-Regular.ttf'); font-weight:normal; font-style: normal;}</style>
	<!-- Yandex.Metrika counter -->
	
	<style type="text/css"></style>
	<script type="text/javascript" vksploader_acfg="undefined">window._ext_ldr_vksploader_acfg=true;</script>
	<script type="text/javascript" vksploader_acfg="dppm_id.js">window.dppm_extensions = window.dppm_extensions || {}; window.dppm_extensions['vksploader_acfg']=true;</script>
	<noscript>
		&lt;div&gt;
			&lt;img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /&gt;
		&lt;/div&gt;
	</noscript>
	<!-- /Yandex.Metrika counter -->
	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script><style type="text/css">.fancybox-margin{margin-right:15px;}</style>
	
	<div id="fb-root"></div>
		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
		<script type="text/javascript">
			VK.init({
				apiId : 2461907,
				onlyWidgets : true
			});
		</script>
		<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
		

<script type="text/javascript">
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    $(document).ready(function () {
        var referrer = document.referrer;

 		$('.various').fancybox({ height: '360px', width: '660px'});

        $('#holod').waypoint(function () {
            $('#demoplay').show('slow', 'easeInOutBack');
        }, { offset: 200, triggerOnce: true });
    });
    </script>

			<style>
			#demoplay
			{
				width: 150px;
margin: 0 auto;margin-bottom: 180px;
			}

			#demoplay img
			{
				margin-bottom: 0px;
			}

		
			#demoplay a { color: white; }
			#demoplay .close
			{
				position: absolute;
				right: 5px;
				top: 0;
				color: black;
			}
			.fancybox-opened .fancybox-skin {

padding: 20px !important;
}
.fancybox-overlay-fixed {
background-color: rgba(0, 0, 0, 0.6);
}
.pass{margin-top: 270px;
}
			</style>
		
	</head>
	
<body>

    

<!--/LiveInternet-->	
 
<div id="bym-wrapper" align="center">
	<div id="bym-header" align="center">
		<div style="width:940px; margin: 0 auto;"><div id="bym-social">
			<div id="bym-logo">
			   <a href="http://elizavetababanova.com/"><img align="left" src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a target="_blank" href="http://vk.com/elizaveta.babanova"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/vk_b.png"></a>
				<a target="_blank" href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/facebook_b.png"></a>
				<a target="_blank" href="http://twitter.com/liza_babanova"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/tweeter_b.png"></a>
				<a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/youtube_b.png"></a>
			</div>
			<div style="clear:both"></div>
			<a style="display: inline-block;width: 173px;height: 43px;margin-left: 165px;margin-top: -18px;cursor: pointer;" href="http://elizavetababanova.com/bestyear-module4"></a>
			<a style="display: inline-block;width: 158px;height: 43px;margin-left: 5px;margin-top: -18px;cursor:pointer;" href="http://elizavetababanova.com/emotions-upgrade"></a>
		</div>	</div>
		
		<?php if ( post_password_required() ) {?>
						<div class="pass">
							<?php
								the_content('Читать далее &raquo;');
								if ( post_password_required() ) {
									return;
								}
							?>
						</div>
					<?php } ?>
	
		<div style="width:696px;  margin:0 auto;margin-top: 350px;">
		<div id="demoplay" style="">
    	
        <a class="various fancybox.iframe" href="http://www.youtube.com/embed/jy3qLHIoQyo?rel=0&border=0&showinfo=0" "/wp-content/themes/madrom_babanova/js/frame1.html"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/play.png">
       </a>
    </div>
		
		</div>
	



<div class="social">
	<div class="social2">
			<div id="social_button">
	<noindex>

		<!-- Facebook -->
				<div id="bt_facebookr">
					<iframe src="http://www.facebook.com/plugins/like.php?href=http://www.elizavetababanova.com/happiness-seminar/ ?>&amp;layout=button_count&amp;show_faces=true&amp;width=140&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:140px; height:21px;" allowTransparency="true"></iframe>
				</div>

				<!-- Facebook END -->

				<!-- Вконтакте -->
				<div id="bt_vk">
					<div id="vk_like"></div>
					<script type="text/javascript">
						VK.Widgets.Like("vk_like", {
							type : "mini"
						});
					</script>
				</div>
				<!-- Вконтакте END -->

				<!-- Twitter -->
				<div id="bt_twitter">
					<a rel="nofollow" href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-lang="ru">Твитнуть</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

					<!-- <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Твитнуть</a>
					<script>
					! function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
					if (!d.getElementById(id)) {
						js = d.createElement(s);
						js.id = id;
						js.src = p + '://platform.twitter.com/widgets.js';
						fjs.parentNode.insertBefore(js, fjs);
					}
					}(document, 'script', 'twitter-wjs');
					</script> -->
				</div>
				<!-- Twitter END -->

	</noindex>
			</div>
	</div>
	<div style="background: url('http://elizavetababanova.com/emotions-upgrade/button-bg.png') no-repeat center center;" class="btns">
						<a style="display: inline-block;
					height: 75px;
					width: 338px;
					margin: 0 auto;
					background: url('http://elizavetababanova.com/emotions-upgrade/reg2.jpg') no-repeat top center;
					margin-top: 50px;
					cursor: pointer;" href="http://elizavetababanova.com/bestyear-module4" target="_blank" class="reg"></a>

					<a style="display: inline-block;
					height: 75px;
					margin: 0 auto;
					width: 338px;
					background: url('http://elizavetababanova.com/emotions-upgrade/reg.jpg') no-repeat top center;
					cursor: pointer;" href="http://elizavetababanova.com/emotions-upgrade/" target="_blank" class="reg"></a>
</div>
</div>
	</div>
		<div style="margin-top: 200px;" id="bym-header" align="center">
		<div class="comments">
				<div class="left">
					<div id="vk_comments"></div>
					<script type="text/javascript">
						VK.Widgets.Comments("vk_comments", {
							limit : 10,
							width : "400",
							attach : "*"
						});
					</script>
				</div>

				<div class="right">
					<div class="fb-comments" data-href="http://www.elizavetababanova.com/happiness-seminar/" data-width="400" data-num-posts="10"></div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="clear"></div>
			<div style="width: 940px; margin: 0 auto;" class="blogcomments">
				<?php comments_template('/comments2.php'); ?>
			</div>
			<div style="clear:both"></div>
		<div id="footer" class="tp-38">
		
		
			<p class="text fs13 col9">ИП Бабанова Елизавета Дмитриевна 2014 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<center><div style="width: 200px;"><span id="site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></span></div></center>
		</div>
		</div>
		</div>
</body>

</html>