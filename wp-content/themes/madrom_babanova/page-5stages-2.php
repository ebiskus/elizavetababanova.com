<?php
/*
Template Name: Страница 5stages-2
*/
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>
    <title>Исследования</title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/css/style.css" rel="stylesheet">
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/js/custom.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">
        VK.init({apiId:  2461907, onlyWidgets: true});
    </script>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
    <link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-2/img/share3.jpg" />
	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <section class="main">
        <header class="header">
            <div class="inner">
                <a class="logo" href="http://elizavetababanova.com/" target="_blank">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/img/b1.png" alt=".">
                </a>
                <nav class="menu">
                    <a target="_blank" href="http://vk.com/elizaveta.babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/img/b2.png" alt=".">
                    </a>
                    <a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/img/b3.png" alt=".">
                    </a>
                    <a target="_blank" href="http://twitter.com/liza_babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/img/b4.png" alt=".">
                    </a>
                    <a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/img/b5.png" alt=".">
                    </a>
                </nav>
            </div>
            <i class="border"></i>
        </header>
        <div class="inner">
            <nav class="menu2">
                <a class="i i1 threerow" href="http://elizavetababanova.com/5stages">5 уровней<br>развития<br>человека</a>
                <a class="i i7 active" href="">Исследования</a>
                <a class="i i2 tworow" href="http://elizavetababanova.com/survey">Результаты<br>опроса</a>
                <a class="i i3 tworow" href="http://elizavetababanova.com/6-moih-vectorov">6 моих<br>векторов</a>
                <a class="i i4 tworow" href="http://elizavetababanova.com/phenomen-masterstva">Феномен<br>Мастерства</a>
                <a class="i i8 threerow" href="http://elizavetababanova.com/kak-realizovat-potentsial">Как<br>реализовать<br>потенциал</a>
                <a class="i i5 tworow" href="http://elizavetababanova.com/programma-treninga">Программа<br>тренинга</a>
                <a class="i i6 tworow" href="http://elizavetababanova.com/gosti-fm">Гости<br>программы</a>
            </nav>
            <section class="block">
                <div class="i i1">
                    <p class="title"><span class="custom">И</span>сследования</p>
                    <section class="body">
                    <center><p style="padding-top:24px; line-height: 24px;">Если вы пропустили предыдущее видео о 5 уровнях развития человека, <a href="http://elizavetababanova.com/5stages" target="_blank">посмотрите его</a>, прежде чем читать дальше. <br />Тогда вы получите максимальную пользу от этого материала.</p></center>
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/img/stat.png" alt=".">
                    <br />
                    <p style="font-size:12px; text-align:center; padding-bottom: 12px;">Источники: Федеральная служба государственной статистики www.gks.ru, HeadHunter www.hh.ru, 5 канал www.5-tv.ru</p>
                    </section>
                </div>
            </section>
            <section class="block8">
            <div class="text">

    <p>И это лишь данные официальной статистики. Если посмотреть на реальное положение вещей, то очевидно, что более 90% людей в какой-то мере недовольны своей работой. </p>
    <br />
    <p>Почему, как вы думаете? Ответ, на самом деле, очень прост.</p>
    <br />
    <p>Большинство людей, даже если они заработали много денег, рано или поздно начинают чувствовать себя крайне несчастными в своей профессии только по одной причине: если они строили карьеру не по любви, а по расчету. Когда причиной выбора профессии становятся деньги, а не призвание, то в конце концов деньги перестают наполнять нематериальную составляющую человека.</p>
    <br />
    <p>А если человек выбирает профессию по любви, то в результате, если он правильно работает, он может получить все - и славу, и богатство, и успех. И пример этому - состоявшиеся в своей работе люди, которые отличаются от большинства особым ответственным отношением к своему творческому потенциалу.</p>
    <br />
    <p>Давайте посмотрим, что говорят такие люди, как Ирина Хакамада, Ричард Брэнсон, Земфира, Артем Агабеков, Опра Уинфри, Тони Роббинс, Стинг, Робин Шарма о деньгах и выборе профессии.</p>

            </div>
            </section>
            <section style="margin: 33px auto;">
                <center><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-2/img/smotrite.png" alt="."></center>
            </section>
            <section style="width: 700px;margin: 20px auto;">
                <iframe width="700" height="394" src="http://www.youtube.com/embed/ei5fwCOdtrI?rel=0" frameborder="0" allowfullscreen></iframe>
            </section>
            <section class="block14">
            <div class="text2">

            <p>Ну а теперь, по традиции, :) у меня несколько вопросов к вам:</p>
            <br />
            <p>1. Каким образом вы выбрали свою профессию: по любви или по расчету? Поделитесь в комментариях, чем вы занимаетесь и почему вы выбрали именно эту стезю? Если у вас были трудности в выборе своего любимого дела, поделитесь своими примерами и тем, как вы их преодолели.</p>
            <br />
            <p>2. Примите участие в нашем собственном небольшом <span style="font-size:25px;" text><a href="http://www.surveymonkey.com/s/XW5MVZG" target="_blank">исследовании</a></span>. Я хочу узнать, какой процент моих читателей реализуется через свое дело, и сравнить это со средними показателями по России. Также мне очень важно узнать, а) что вам препятствует в  выборе своего профессионального призвания, и б) если вы его уже определили, что сдерживает ваш стремительный рост в этой сфере?</p>
            <br />
            <p>Результаты статистики помогут мне заточить будущие материалы конкретно под ваши запросы, поэтому, если вы чувствуете, что я могу вам помочь, обязательно примите участие в <a href="http://www.surveymonkey.com/s/XW5MVZG" target="_blank">опросе</a> (всего 7 пунктов).</p>
            <br />
            <p>Жду вашей обратной связи!</p>
            <br /><center>         <?php
        if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )
            echo "";
        else
            require('social_button.php');
    ?> </center>
            </div>
            </section>
            <i class="border3"></i>
            <section class="block8">
                <div class="left">
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                    VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
                    </script>
                </div>
                <div class="right">
                    <div class="fb-comments" data-href="http://elizavetababanova.com/research" data-width="400" data-num-posts="5"></div>
                </div>
            </section>
            <section class="block8">
            <?php comments_template('/comments2.php'); ?>
            </section>
            <footer class="footer">
                <i class="border6"></i>
                <p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
                <nav class="menu3">
                    <a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
                    <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
                    <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
                </nav>
            </footer>
        </div>
    </section>
</body>
</html>