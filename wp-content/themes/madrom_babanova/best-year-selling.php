<?php
/*
Template Name: Продающая страница Лучший год
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!--<link rel="stylesheet" type="text/css" href="style.css">-->

	<link href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/style.css" rel="stylesheet" type="text/css">
	<script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.js"></script>
	<!--<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>-->

	<title>Лучший год Вашей жизни</title>

	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-selling/images/share1.jpg">
	<meta property="og:title" content="Лучший год Вашей жизни"/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear"/>
	<meta name="description" content="Как сделать следующий год лучшим годом Вашей жизни?">

	<meta property="fb:admins" content="100003273128680"/>
	<meta property="vk:app_id" content="3398000"/>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

</head>

<body>
	<div id="wrapper" align="center">
		<div id="header">
			<div id="social">
				<div id="logo">
					<a href="/">
						<img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/logo.png">
					</a>
				</div>
				<div id="buttons">
					<a href=http://vk.com/elizaveta.babanova><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/V.png"></a>
					<a href=https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/F.png"></a>
					<a href=http://twitter.com/liza_babanova><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/T.png"></a>
					<a href=http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/Y.png"></a>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>

		<div id="content">
			<div class="banner1">
				<img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner1.png">
				<p>Для тех, кто хочет и готов «достать до звезд», но не за счет резких рывков, которые обычно заканчиваются срывом и обессиливанием, а за счет постепенного наращивания скорости, профессионализма и<br />суперпродуктивности.
				</p>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="banner2"><img align="right" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner2.png">
				<p>Для тех, кто уже многого достиг и понял: чтобы оставаться<br />в седле длительное время, вам критически необходим<br />тренер, который обеспечит постоянное вдохновение,<br />мотивацию и обучит лучшим стратегическим и<br />тактическим приемам по достижению целей.
				</p>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>


			<p class="how">Как будет проходить программа?</p>
			<p class="modules12">12 месяцев – 12 феноменальных модулей,<br/>которые обеспечат вас максимальными<br/>результатами за год</p>
			<div class='modules'>
				<div class="master">
						<p class="magenta">мастер-классов</p>
						<p class="grey">в прямом эфире </p>
						<p class="regular">Теория + практика.  На каждом мастер-классе вы получите конкретные задачи для получения результатов по каждой сфере.</p>
				</div>
				<div class="webinar">
						<p class="magenta">вебинаров</p>
						<p class="magenta">«вопрос/ответ»</p>
						<p class="grey">в прямом эфире </p>
						<p class="regular">Это ваша возможность задать свой вопрос или разобрать вашу конкретную ситуацию, чтобы усилить эффект от мастер-класса, и получить дополнительный эмоциональный и интеллектуальный здоровый допинг.</p>
				</div>
			</div>
		</div>


			<div class="enstein">
				<div class="ens_inner">
				<p class="citate">«Безумие – это делать тоже самое и <br> надеяться на другой результат».</p>
  				<p class="autor">Альберт Эйнштейн</p>
  				</div>
			</div>
		<div id="content2">
			<div class="question">
				<p>А то, что делали вы все прошедшие годы<br>сможет помочь вам сделать следующий год</p>
				<p class="blue">лучшим годом<br><br />вашей жизни?</p>
			</div>	
			<div class="all_year">
				<div class="mounth"><p class="digit">01</p><p class="name">ЯНВАРЬ</p></div>
				<div class="description"><p class="title">Воздушный Старт.<br>Система Запуска.</p>
					<p class="text">
					1 модуль начнется с четкого определения, что вы хотите достичь в этом году. Вы получите отлаженную систему "запуска полета” плюс необходимые "ускорители". В космической индустрии появился новый способ запуска корабля в космос - не с Земли, а с воздуха, что позволяет сократить ресурсы и усилия на самом ответственном начальном этапе пути. Мы будем действовать аналогично. В этом модуле вы получите инструменты, которые не заберут 80% ваших усилий и ресурсов во время запуска вашей ракеты в космос. Вместо этого вы овладеете самыми передовыми технологиями massive action и внедрите их в свою жизнь.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/01.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module1">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="all_year">
				<div class="mounth"><p class="digit">02</p><p class="name">ФЕВРАЛЬ</p></div>
				<div class="description"><p class="title">Иммунитет к препятствиям,<br>комплексам и страхам</p>
					<p class="text">
					В этом высокоинтенсивном модуле вы проработаете свои ограничивающие убеждения и определите корень своего основного страха, который мешает вам лететь на нужной вам скорости. В результате прохождения этого модуля вы станете самым осознанно бесстрашным человеком из всех, кого вы знаете.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/02.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module2">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="all_year">
				<div class="mounth"><p class="digit">03</p><p class="name">МАРТ</p></div>
				<div class="description"><p class="title">Вечный Двигатель</p>
					<p class="text">
					Успешные люди отличаются тем, что они умеют правильно распределять свои усилия и ресурсы, чтобы не выгорать. В этом модуле вы получите формулу оптимального поддержания себя в тонусе и узнаете, как заточить ее под свои индивидуальные потребности. Так как весна - идеальное время для обновления энергии, вы научитесь использовать природные механизмы для максимального наполнения и сумеете наладить работу своего "вечного двигателя". 
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/03.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module3">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="all_year" style="height: 439px;">
				<div class="mounth">
					<p class="digit">04</p>
					<p class="name">АПРЕЛЬ</p>
				</div>
				<div class="description">
					<p class="title">Эмоции. Как с помощью счастья прийти к успеху и процветанию?</p>
					<p class="text">
						Весна в разгаре. Природа начинает просыпаться, и вместе с ней возникает необходимость в приливе положительной энергии. В этом модуле вы освоите искусство поддержания мотивации и хорошего расположения духа при любых обстоятельствах.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/04.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module4">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
						<div class="all_year">
				<div class="mounth"><p class="digit">05</p><p class="name">МАЙ</p></div>
				<div class="description"><p class="title">Личная Сила и Харизма</p>
					<p class="text">
					Личная Сила отдельного человека - вот на чем основывается весь прогресс человечества. Если у вас недостаточно развит внутренний стержень, если в вас нет внутреннего огня, если вы не притягиваете людей своей харизмой, то вы не задействуете свои самые мощные ресурсы. Харизма - способность "заряжать" людей своей энергией и внутренней силой. С помощью чего развиватюся личная сила и харизма, вы откроете для себя в этом модуле.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/05.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module5">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
						<div class="all_year">
				<div class="mounth"><p class="digit">06</p><p class="name">ИЮНЬ</p></div>
				<div class="description"><p class="title">Как раскрыть Талант?</p>
					<p class="text">
					Чтобы ваш полет и ваша жизнь была полноценной, вы должны иметь два развитых полушария мозга. Тогда вы можете и генерировать творческие идеи, и реализовывать их с помощью логических действий. В этом модуле вы получите фантастическую методику по развитию творческого гения.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/06.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module6">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
						<div class="all_year" style="height: 439px;">
				<div class="mounth">
					<p class="digit">07</p>
					<p class="name">Июль</p>
				</div>
				<div class="description"><p class="title">Ваше Созвездие</p>
					<p class="text">
						В этом месяце мы сфокусируемся на улучшении отношений и формировании продвинутого социального круга. Вы узнаете, как окружить себя достойными людьми, способными составить вам компанию в вашем полете. Пройдя этот модуль, вы сможете 1) укрепить отношения с близкими людьми и 2) сделать апгрейд своего окружения, создав свое собственное "созвездие".
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/08.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module7">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="all_year">
				<div class="mounth"><p class="digit">08</p><p class="name">Август</p></div>
				<div class="description"><p class="title">Интуиция и Осознанность</p>
					<p class="text">
					В этом месяце мы будем не только бороздить космическое пространство внешнего мира, но и открывать Космос внутри себя. Так как ближе к осени обостряется связь с подсознательным, это будет идеальное время для определения дальнейшей цели вашего путешествия и траектории полета. Вы научитесь слышать самих себя и подключаться к Источнику. Вы получите методики расширения сознания и развития интуиции, рекомендуемые передовыми учеными. 
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/09.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module8">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
						<div class="all_year">
				<div class="mounth"><p class="digit">09</p><p class="name">Сентябрь</p></div>
				<div class="description"><p class="title">Деньги</p>
					<p class="text">
					Что мешает вам зарабатывать столько, сколько вам нужно? В сентябре мы глубоко проработаем сферу личных финансов, выявим ваши подсознательные установки, которые мешают вам обеспечивать себя в полной мере. Я познакомлю вас с холистическими  инструментами, которые помогут вам прийти к изобилию во всех сферах жизни.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/07.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module9">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
						<div class="all_year" style="height: 439px;">
				<div class="mounth">
					<p class="digit">10</p>
					<p class="name">Октябрь</p>
				</div>
				<div class="description"><p class="title">Влияние</p>
					<p class="text">
					Октябрь - обычно унылое время, но не у вас! Вы работали над собой уже 9 месяцев, достигли огромных результатов и теперь готовы узнать секреты влияния на людей. Без магии и хитроумства вы научитесь быть авторитетом и источником вдохновения. Вы получите набор технологий, с помощью которых самые сильные люди этого мира способны заряжать других своими идеями и сподвигать на действия.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/11.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module10">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="all_year">
				<div class="mounth"><p class="digit">11</p><p class="name">Ноябрь</p></div>
				<div class="description"><p class="title">Версия "Я 2.0" или<br>Человек-WOW</p>
					<p class="text">
					Если вы хотите, чтобы все, что бы вы ни делали, вызывало и у вас, и у вашего созвездия ощущение WOW, то это будет самый мощный модуль для вас. Это будет новый рывок вверх для тех, кто хочет добиться самых больших высот в своем деле и научиться работать на уровне мирового класса.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/10.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module11">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
						<div class="all_year">
				<div class="mounth"><p class="digit">12</p><p class="name">ДЕКАБРЬ</p></div>
				<div class="description"><p class="title">Найди свой Edge</p>
					<p class="text">
					Лучший год Вашей жизни подходит к концу, но все только начинается. В этом модуле мы проанализируем все то, что работало для вас за последние 11 месяцев, закрепим результаты и усилим эффект, чтобы вы вошли в следующий год с еще большим объемом внутренней энергии. Вы определите, в чем ваш Edge - уникальность, которая может стать вашим конкурентным преимуществом в последующих "полетах".
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/12-1.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module12">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/plus.png" style="float:left;padding-bottom:50px;padding-top:50px;">
			<div style="clear:both"></div>
			<div class="all_year" style="height: 360px;">
				<div class="mounth"><p class="digit" style="color:#ffcc66">13</p><p class="name" style="color:#ffcc66">БОНУС</p></div>
				<div class="description"><p class="title" style="color:#ff9900">Деловой Этикет</p>
					<p class="text">
						Многие буквально подрезают себе крылья, срывая сделки и буксуя свой карьерный рост из-зa банального отсутствия навыков общения. Но незнание этических законов не освобождает от горьких последствий. На этом модуле вы узнаете фундаментальные правила делового этикета, бизнес-корреспонденции и освоите высокие стандарты общения. Устранив ошибки, о которых сегодня, возможно, вы даже не догадываетесь, вы начнете гораздо легче завязывать знакомства и выстраивать отношения. В результате, ваши планы начнут реализовываться гораздо быстрее. Ведь теперь ваши коммуникации будут соответствовать вашим целям и задачам.
					</p>
				</div>
				<div class="img_but">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/13.png">
					<div class="but_link">
						<a href="http://elizavetababanova.com/bestyear-module13">
							<div class="but_more">Программа модуля</div>
						</a>
					</div>
				</div>
			</div>	
<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/plus.png" style="float:left;padding-bottom:50px;padding-top:50px;">
			<div style="clear:both"></div>			
			<div class="all_year" style="height: 692px;">
			<p class="title" style="color:#ff9900; font-size: 32px;
margin-bottom: 20px;">Выдающиеся гости</p>
				<img src="http://elizavetababanova.com/best-year/guests/img/askerov2.png"></img>
				<img src="http://elizavetababanova.com/best-year/guests/img/gandapas2.png"></img>
				<img src="http://elizavetababanova.com/best-year/guests/img/kartunkova2.png"></img>
				<img src="http://elizavetababanova.com/best-year/guests/img/kem.png"></img>
				<img src="http://elizavetababanova.com/best-year/guests/img/mann.png"></img>
				<img src="http://elizavetababanova.com/best-year/guests/img/odels.png"></img>
						<a href="http://elizavetababanova.com/best-year/guests/">
							<div style="width: 274px;
height: 34px;
margin: 0 auto;" class="but_more">Узнать подробнее о гостях</div>
						</a>
					
			</div>	
			<div style="clear:both"></div>
			<img class="whatineed" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/whatineed.png">
			<div class="whatsay ws_expert">
				<div><img class="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
				<div>Отзывы экспертов</div>
				<div><img class="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
			</div>
			<div style="clear:both"></div>
			
			
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Я благодарю Вас, потому что когда складываются энтузиазм и технологии, можно преодолевать границы».</p>
					<p class="expert_say">Из <a href="http://www.youtube.com/watch?v=DOPhNi5pwIQ" target="_blank">мастер-класса</a> программы «Феномен Мастерства»</p>
					<p class="expert_autor">Ирина Хакамада</p>
					<div class="expert_who">Российский политик, кандидат экономических наук, писатель, радио- и телеведущая</div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/hakamada.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Проекты Елизаветы соответствуют мировым стандартам качества».</p>
					<p class="expert_say">Когда я познакомился с Елизаветой на научной конференции в Санкт-Петербурге, которую я возглавлял, я сразу увидел в ней способность «сдвигать горы». Поэтому, уже через несколько недель после конференции, я пригласил ее в состав рабочей группы экспертов дистанционного обучения при Министерстве образования и науки России.  Проекты Елизаветы соответствуют мировым стандартам качества, поэтому я надеюсь на долгосрочное сотрудничество с ней в рамках Международной Ассоциации Непрерывного Образования.</p>
					<p class="expert_autor">Евгений Сжёнов</p>
					<div class="expert_who">Президент Междунаройной Ассоциации Непрерывного Образования, член Комиссии Минобрнауки России по развитию ДПО, руководитель Центров дополнительного профессионального образования МГУ</div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/szhenov.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Благодаря консультациям с Елизаветой мы смогли перевести работу нашей компании на совершенно новый уровень».</p>
					<p class="expert_say">Благодаря консультациям с Елизаветой мы смогли перевести работу нашей компании на совершенно новый уровень. За 4 года существования нашей компании количество наших сотрудников выросло с 3 человек до 12 и без опыта правильной организации работы у нас начался небольшой хаос. Лиза указала на наши слабые места и пробелы, помогла нам быстро и очень професионально расставить приоритеты и задачи, научила как правильно работать с командой. После консультаций с Елизаветой мы стали работать гораздо четче, появилась ясность, куда мы хотим прийти, и мы начали продвигаться к намеченным целям гораздо быстрее. Очень благодарны Лизе за проделанную работу и надеемся на продуктивное сотрудничество в будущем!</p>
					<p class="expert_autor">Ирина Хлимоненко</p>
					<div class="expert_who">Со-учредитель Mindvalley Russian. Mindvalley - самая крупная издательская компания программ личностного роста с оборотом в дясятки миллионов долларов,<br /><a href="http://www.mindvalleyrussian.com/" target="_blank">www.mindvalleyrussian.com</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/hlimonenko.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Это реально здорово! Елизавета открывает женщинам мир, показывает своим результатом, своей жизнью возможности, вдохновляя их».</p>
					<p class="expert_autor">Дмитрий Шаменков</p>
					<div class="expert_who">Ученый, автор Системы Управления Здоровья,<br /><a href="http://shamenkov.ru" target="_blank">www.shamenkov.ru</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/shamenkov.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Что можно сделать за год? Можно колоссально изменить всю свою жизнь! Но как же приятно пройти этот год с правильным тренером успеха - Елизаветой Бабановой!»</p>
					<p class="expert_say">Вы мечтаете прийти к успеху без перегорания и без надрыва? Вы мечтаете реализоваться в жизни, придя к гармонии внутреннего и внешнего мира? Как вы считаете, возможно ли сделать взлет за месяц? Видимо, да! А что можно сделать за год? Можно колоссально изменить всю свою жизнь! Но как же приятно пройти этот год с правильным тренером успеха - Елизаветой Бабановой! Мягкая, нежная, женственная, харизматичная. Умеющая донести вопросы до самого вашего сердца! Которые позволяют найти вам истинные ответы и идти к вашим дерзким и смелым целям, кардинально улучшающие вашу жизнь день ото дня! С уважением к данному тренеру успеха.</p>
					<p class="expert_autor">Лилия Осия</p>
					<div class="expert_who">Чемпионка мира по фитнесу, тренер чемпионов России, Европы и мира по фитнесу, коуч министров, бизнесменов, топ-менеджеров высшего звена по здоровью и личной эффективности. Автор метода «Архитектура мысли, тела и жизни».<br /><a href="http://www.fitnessidea.ru" target="_blank">www.fitnessidea.ru</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/osia.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Ее система работает».</p>
					<p class="expert_say">Елизавета является одним из ведущих мировых экспертов в сфере личностного развития. Я нахожу ее инструменты, мудрость и проницательность бесценным источником для моего личностного и профессионального роста. Я очень рекомендую ее программы всем, кто хочет вывести свою жизнь на следующий уровень. Ее система работает.</p>
					<p class="expert_autor">Крис Кей</p>
					<div class="expert_who">Американский предприниматель, владелец трех успешных бизнесов с оборотом в десятки миллионов долларов,<br /><a href="http://www.blueglobalmedia.com" target="_blank">www.blueglobalmedia.com</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/chriskay.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Курс для практикующих специалистов разного уровня и руководителей. Для тех, кто готов выйти за рамки линейного мышления, классической науки и традиционного подхода к организованным вопросам».</p>
					<p class="expert_say">Программа «Лучший год Вашей жизни» опирается на сильную теоретическую основу и практику, учит упрощать сложное, управлять взаимосвязями и делать выбор, используя итеративное моделирование. Раскрывает вопрос философии и практического применения систем, переплетая менеджмент, психологию и социологию. Рассматривает конкретные реализованные проекты и демонстрирует решения задач бизнес-моделирования, социальных и культурных вопросов.<br />
					<p class="expert_say">Курс о способности видеть, действовать и жить по-новому, о чётком и ясном мышлении среди хаоса.</p>
					<p class="expert_say">Программа построена с учетом анализа современных подходов к решаемым проблемам. Контролировать свое время – значит владеть своей жизнью и использовать ее наилучшим образом. Гармоничное сочетание этих необходимых каждому сторон жизни возможно, если научиться правильно распределять свои временные ресурсы.</p>
					<p class="expert_say">Курс предназначен для практикующих специалистов разного уровня и руководителей. Для тех, кто готов выйти за рамки линейного мышления, классической науки и традиционного подхода к организованным вопросам. Этот курс для специалистов, которые должны быть сильной личностью в профессии, взаимоотношениях, управлении.</p>
					<p class="expert_say">Программа заслуживает положительной оценки и широкого внедрения в  образовательную практику.</p>
					<p class="expert_autor">Владимир Рясин</p>
					<div class="expert_who">Доктор экономических наук, Профессор, член-корреспондент РАЕН им. В.И. Вернадского<br />
					Председатель Совета областного отделения межрегионального фонда «Вознесение»</div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/vladimir-ryasin.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Если вам нужна поддержка, знания, вдохновение, и хочется раскрыть внутренний потенциал, то подключайтесь к замечательным программам Елизаветы!»</p>
					<p class="expert_say">Елизавета - одна из редких людей, кто на самом деле хочет помочь другим людям от всей души. Она болеет темой саморазвития и самопонимания. Ее работа вдохновляет, помогает двигаться вперед и создавать красивую жизнь. Если вам нужна поддержка, знания, вдохновение, и хочется раскрыть внутренний потенциал, то подключайтесь к замечательным программам Елизаветы!</p>
					<p class="expert_autor">Надя Андреева</p>
					<div class="expert_who">Ведущий эксперт в США по Аюрведе, автор популярных статей и книги,<br /><a href="http://www.spinachandyoga.com" target="_blank">www.spinachandyoga.com</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/andreeva.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Елизавета пробуждает любовь к жизни и дремлющие мечты».</p>
					<p class="expert_say">Елизавета пробуждает любовь к жизни и дремлющие мечты, заряжает энергией и отвагой координально изменить свою жизнь. Советы Елизаветы конкретны и полны мудрости, во многом, благодаря ее опыту работы в финансовом центре мира. Елизавета знакомит с замечательными людьми с разных континентов, с легкостью превращая их в друзей и партнеров благодаря своей искренности и культуре глобального человека.</p>
					<p class="expert_autor">Валентин Преображенский</p>
					<div class="expert_who">Управляющий партнер, Avega Capital</div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/preobrazhenskiy.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Глубина подачи материала сочетается с доступной, образной формой его изложения, что облегчает восприятие информации».</p>
					<p class="expert_say">Программа адресована широкому кругу слушателей, занимающихся профессиональным, дополнительным и самообразованием. Большой научно-практический интерес представляет авторский подход к постановке и решению актуальных для современного социума вопросов.</p>
					<p class="expert_say">Содержание программы построено с учетом анализа современных подходов к решаемым проблемам. Глубина подачи материала сочетается с доступной, образной формой его изложения, что облегчает восприятие информации.</p>
					<p class="expert_say">Отличительной чертой программы, повышающей её ценность, является умелая интеграция различных областей знаний. Именно это обеспечивает холистический подход к обучению и повышает его эффективность.</p>
					<p class="expert_say">Модульный характер построения программы отвечает современным требованиям методики и дидактики образования взрослых. Логическая и содержательная взаимосвязь модулей обеспечивает преемственность на последовательных этапах личностно-профессионального совершенствования.</p>
					<p class="expert_say">Таким образом, программа заслуживает положительной оценки и широкого внедрения в образовательную практику.</p>
					<p class="expert_autor">Сергей Толстов</p>
					<div class="expert_who">Профессор, доктор медицинских наук, доктор психологических наук, почетный работник высшего профессионального образования<br />Дважды доктор наук, под его руководством защищено 17 кандидатских диссертаций. Научный опыт С.Н. Толстова обобщен в 400 научных работах, в том числе центральных публикациях, 10 монографиях и 18 учебниках с грифами министерств и УМО</div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/sergey-tolstov.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Программа отличается комплексным характером, основанным на интеграции психолого-педагогических, медико-социальных и философских подходов».</p>
					<p class="expert_say">Данная программа посвящена актуальной тематике, отражает информационные потребности и запросы современного общества. Отличительной особенностью программы является комплексный характер, основанный на интеграции психолого-педагогических, медико-социальных и философских подходов. Основные модули программы содержат взаимосвязанный комплекс знаний, умений и навыков, определяющих достижение вершин личностно-профессионального развития.</p>
					<p class="expert_say">Автором четко обозначены цели и задачи обучения, сформулированные часто в виде вопросов, что значительно облегчает восприятие материала.</p>
					<p class="expert_say">Программа отражает современные подходы и научно-практические достижения в сфере бизнес-образования и дистанционного обучения.</p>
					<p class="expert_say">С учетом вышесказанного, данная программа в целом, и каждый её модуль в отдельности, могут быть рекомендованы для широкого практического внедрения в условиях стационарного, заочного и дистанционного образования.</p>
					<p class="expert_autor">Татьяна Карасёва</p>
					<div class="expert_who">Профессор, доктор медицинских наук, почетный работник высшего профессионального образования<br />Является автором около 500 публикаций, в том числе 14 учебно-методических пособий и учебников с грифами министерств и УМО и 8 монографий. Под её руководством защищено 14 кандидатских диссертаций по медицинским, педагогическим и психологическим аспектам образа жизни и культуры здоровья</div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/tatiana-karasiova.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«В программах Елизаветы я получаю выжимку инструментов по повышению своей эффективности, которые реально работают».</p>
					<p class="expert_say">В программах Елизаветы я получаю выжимку инструментов по повышению своей эффективности, которые реально работают. Я чувствую, что они опробованы на себе и отобраны с любовью и желанием помочь. Все то, что некогда читать, смотреть, выбирать из книг, видео можно получить здесь. Начинаешь это применять в своей жизни, и постепенно, как по мановению волшебной палочки, все меняется неуловимым непостижимым образом. Просто волшебство! И не только для женщин.</p>
					<p class="expert_autor">Ксения Сиротина</p>
					<div class="expert_who">Директор рекрутинговых агентств Sirotina Personnel <a href="http://www.sirotina.net" target="_blank">www.sirotina.net</a><br />и Рабочие руки <a href="http://www.rabochie-ruki.com" target="_blank">www.rabochie-ruki.com</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/sirotina.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Стремления Елизаветы являются не просто декларациями, а подкрепляются реальными долгосрочными, не сиюминутными результатами бизнес-проектов».</p>
					<p class="expert_say">Лиза принадлежит к тому редкому типу женщин, которые красивы внешне и еще более красивы внутренне - своим желанием развиваться самой и сделать лучше жизнь людей вокруг. Эти стремления не являются просто декларациями, а подкрепляются реальными долгосрочными, не сиюминутными результатами бизнес-проектов, созданных и работавших при ее участии. Лиза, успехов и вдохновения тебе, а многочисленным участникам твоей программы - реальных знаний и результатов!</p>
					<p class="expert_autor">Вячеслав Юнев</p>
					<div class="expert_who">Бизнес-тренер, <a href="http://www.yunev.com" target="_blank">www.yunev.com</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/yunev.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="expert">
				<div class="expert_text">
					<p class="expert_title">«Она помогает делать этот мир лучше».</p>
					<p class="expert_say">Я знаком с Лизой уже около двух лет, и то, что мне нравится больше всего - как она делает свою работу, как она реализовывает свое предназначение. Те материалы, программы, тренинги, которыми она делится с людьми, несут в себе огромную ценность, они вдохновляют и мотивируют других людей достигать больших успехов в жизни, стремиться к чему-то лучшему. Так как я сам непосредственно работаю в такой же сфере, я знаю насколько это тяжелый труд, насколько это сильная отдача. И за это я хочу поблагодарить Лизу, так как она помогает делать этот мир лучше.</p>
					<p class="expert_autor">Андрей Шарук</p>
					<div class="expert_who">Бизнес-тренер, <a href="http://www.andreysharuk.com" target="_blank">www.andreysharuk.com</a></div>
				</div>
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/sharuk.jpg" >
				</div>
			</div>
			<div style="clear:both"></div>

			<div class="whatsay ws_comment">
				<div><img id="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
				<div>Отзывы выпускников программ<br>Системы Сферического Развития</div>
				<div><img id="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
			</div>
			<div style="clear:both"></div>
			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment1.png" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Заряд, пинок, толчок, прорыв! Доход увеличился в 3 раза, а времени стало больше».</p>
					<p>Объем и ценность полученной информации колоссальны! Заряд, пинок, толчок, прорыв, как угодно! Если честно, просто открылись глаза на жизнь, работу и мир вокруг. И главное, я почувствовала два таких момента как "хочу" и "могу" изнутри, из своей души, из своего сердца, из своих сил и энергии, а не как это было раньше - от внешних факторов и окружения. Ответ Елизаветы на мой вопрос на вебинаре касательно супруга и наших с ним разных уровнях просто поразил своей простотой и истинностью. Как только я его услышала и осознала, очень изменилось отношение к мужу, появилось больше любви и искренности.
					</p>
					<p>К слову, за время курса мой доход увеличился в три раза в большей степени из-за того, что я стала применять советы из модулей. И появилось больше свободного времени, что даже странно, я теперь могу позволить себе выходной. Последний год выходной был для меня непозволительной роскошью, а за последний месяц их было аж 2 :)</p>
					<p>Я даже не могу перевести в слова все свои эмоции и чувства от того, что дает Елизавета. Это потрясающе!</p>
					<p>Просто спасибо за вашу работу, и Елизавета, и команда. И спасибо судьбе, что однажды я нечаянно наткнулась в фейсбуке на рекламный (наверное) пост.</p>
					<p>Следующий год начну с Живи по полной, в качестве подарка на Новый год.</p>
					<p class="autor">Екатерина Тимохина</p>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment2.png" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Я начала действительно делать то, что запланировала, а не откладывать дела на завтрашний день».</p>
					<p>Самое лучшее, что произошло со мной во время прохождения курса, - это то, что я начала действительно делать то, что запланировала, а не откладывать дела на завтрашний день. </p>
					<p>Я научилась чувствовать свое тело, у меня появилось больше энергии и веры в себя. Я поняла, что нет преград для саморазвития и самосовершенствования. Что всегда можно найти время для своего здоровья. И я уверена в своих силах!</p>
					<p class="autor">Светлана Четверухина </p>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment3.png" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Результаты превзошли все мои ожидания. Впервые мне удалось сделать то, чего никогда раньше не получалось».</p>
					<p>Самое лучшее - результаты превзошли все мои ожидания. Моей основной целью было увеличение энергии, подвижности, хорошего самочувствия. А получила еще массу всего интересного и неожиданного. Например, постройнела на 5 кг. :-)</p>
					<p>Все получилось, Лиза. И в моей жизни действительно произошел АПГРЕЙД.</p>
					<p>Впервые мне удалось сделать то, чего никогда раньше не получалось. Я помню дни, когда безуспешно плутала в дебрях информации, пробовала то, другое и бросала, даже не успев толком начать.</p>
					<p>Участие в программе открыло новый взгляд на многие вещи и рассеяло «туман». До сих пор не верится, если честно. :-) Новая Я — ну что ж, поздравляю с преображением! Ура!</p>
					<p class="autor">Ольга Вотинова</p>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment15.png" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Я увидел свои возможности».</p>
					<p>Я увидел свои возможности. Получил много мудрости. Это тренировка на высоком уровне! 
					Главное отличие Елизаветы - это фокус ее внимания на всем, что она делает, глубокое погружение в
					каждого участника и в каждый вопрос. Все ситуации она пропускает через себя и потом дает \
					практические рекомендации. Я желаю ей создать свой Мировой Бренд!!! И демонстрировать дальше
					 мировой уровень! Реализовать самые амбициозные цели! И пусть каждый участник Вашей команды
					 сумеет найти себя и раскрыться по полной - пусть у Вас будут только Мастера своего дела!!!</p>
					<p class="autor"> Виктор Сериков</p>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment4.png" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Вы пробудили в каждом из нас великого творца своей судьбы!»</p>
					<p>Елизавета! Своей программой Вы создали не просто шедевр! Вы сумели воплотить свою задумку в жизнь!</P>
					<p>Все мы в какой-то момент почувствовали себя великолепным оркестром! Каждый из нас мог почувствовать себя и первой скрипкой, и пронзительной виолончелью! Иногда кому-то приходилось брать на себя партию духовых, а кто-то вел мелодию трогательной флейты.</p>
					<p>Каждому из нас была предоставлена возможность сыграть свою удивительную мелодию в прекрасной симфонии, под руководством удивительного дирижера!</p>
					<p>Вы - великий мастер, и мы все, все участники,  благодарим Вас и благоговеем перед Вашим уникальным талантом!</p>
					<p>Вы позволили нам почувствовать многие неизведанные тональности и сыграть мелодию своей души! </p>
					<P>Вы пробудили в каждом из нас великого творца своей судьбы! И теперь мы все уже будем сами играть на струнах своей души, перебирая «вариации на тему», и создавая новые, потрясающие по вкусу и наполнению, композиции, наполняя Вселенную прекрасными вибрациями своих тончайших и удивительных инструментов, почувствовать и настроить которые помогли нам Вы своей замечательной  программой!</p>
					<p>Я вас каждый день с благодарностью вспоминаю, просто Ваш курс РЕАЛЬНО перевернул мою жизнь, появились силы и энергия идти вперед!</p>
					<p class="autor">Виктория Винникова </p>
				</div>
			</div>
			<div style="clear:both"></div>
			
			<div class="more_otz" id="but_mr">
				<a href="javascript:doPopup('#first')"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/more-otz.png"></a>
			</div>
			
			<div id="first">
				<div style="clear:both"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment6.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Только  потому что вложена душа, есть желание идти вслед и вместе с ВАМИ!»</p>
						<p>Учитель приходит тогда, когда готов ученик!</p><p>
						Елизавета и ее команда оказались для меня тем учителем, которого я готова была встретить! СПАСИБО!!! За то, что вы серьезно занимаетесь таким интересным делом и делаете это ОТ ВСЕЙ ДУШИ. И я думаю, только потому что вложена душа, есть желание идти вслед и вместе с ВАМИ!</p><p>
						Спасибо еще раз!</P>
						<p class="autor">Ольга Берюбаева </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment8.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Нашла работу, о которой даже и во сне не снилось».</p>
						<p>Дорогая и любимая миллионами, в том числе и мною, Елизавета и Ваша необычайная команда! Спасибо Вам за поддержку в трудный момент моей жизни, когда я окончательно приняла решение уйти с прежней работы, где перстала развиваться и НАШЛА РАБОТУ, О КОТОРОЙ ДАЖЕ и ВО СНЕ НЕ СНИЛОСЬ. Я мечтала выйти из замкнутого круга и увидела в каком направлении можно двигаться. Ваша программа рассчитана на любую аудиторию, тем и уникальна. Спасибо, что Вы есть на белом свете и мне спокойней и уверенней с каждым днем вместе с Вами.</p>
						<p class="autor">Валентина Николаева  </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment9.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Грамотнейшая систематизация».</p>
						<p>Елизавета, спасибо Вам за Вашу работу; за огромный труд, — во-первых, в поисках и обработке нужной информации, и во-вторых, — в ее грамотной систематизации, нет, грамотнейшей систематизации, и в этом Вы, безусловно, мастер. Благодаря своему таланту Вы превращаете самую обычную информацию в ценнейший материал, в реальный инструмент для изменения личности, который, лично мне, оказал и оказывает неоценимую помощь. Я искренне желаю Вам успеха в создании множества таких шедевральных инструментов :)</p>
						<p class="autor">Айзада Жунисбекова </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment10.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Материальные затраты окупились многократно».</p>
						<p>Доброго здоровья всем! Сегодня я буду благодарить Елизавету и ее команду. За замечательный тренинг, за море полезной информации.</P><p>
						Курс построен просто изумительно, с таким тонким знанием психологии человека, что просто удивляешься. Каждый новый пост ожидался с нетерпением и воспринимался как ценный подарок. И, конечно, материальные затраты окупились многократно!
						</p>
						<p class="autor">Olga Slavickiene </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment11.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Ваш курс завёл  механизм: запустил не только намерение, он запустил конкретные действия! От теории – к практике – к Успеху!»</p>
						<p>У меня появились силы, энергия, желание бороться за свою мечту, за своё предназначение, призвание!</p><p>
						Ваш курс завёл  механизм: запустил не только намерение, он запустил конкретные действия! От теории – к практике – к Успеху!
						</p>
						<p>
						Только обладая хорошим  здоровьем, только через обладание энергией, через самоорганизацию вы сможете осуществить свои  мечты, качественно, с удовольствием заниматься любимым делом, 
						</p><p>
						радоваться и наслаждаться жизнью многие годы! Участие в программах Елизаветы закладывает этот фундамент для вашего счастья, легкости и радости Жизни!
					</p><p>Проверено!</p>
						<p class="autor">Ирина Козеева </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment12.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Позитив! Радость! Мудрость! Доступность! Елизавета - великолепный, вдохновляющий тренер».</p>
						<p>Сознание буквально перевернулось с ног на голову( в хорошем смысле) :) Появилась уверенность в с своих силах, постоянное ощущение, что все еще впереди!!!))) Колоссальный подъем энергии, радость жизни, огромное желание творить свою прекрасную жизнь!!)</p><p>
						Понравилось все.  Прекрасная подача материала. Материал не перегружен, дает все самое важное. Практические упражнения дают возможность осмыслить информацию, пропустить через себя.</p>
						<p>Очень хорошая программа! Позитив! Радость! </p>
					<p>Мудрость! Доступность!  Елизавета - великолепный, вдохновляющий тренер. Спасибо огромное!</p>
						<p class="autor">Наталья Чернобай </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment17.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Я убедился на личном опыте, что в жизни можно добиться всего, чего пожелаешь».</p>
						<p>Самое ценное, на мой взгляд, - это то, что в результате обучения стало больше энергии, появилось в жизни
						 больше позитива, я стал спокойнее, увереннее в себе, в своих силах. Теперь я знаю, что в жизни можно
						  добиться всего, чего пожелаешь. Я это знал и раньше, но только теоретически. Сейчас я убедился в этом
						   на личном опыте и опыте других участников программы.
						 </p>
						<p class="autor">Иван Петренко </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment14.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Большой прилив энергии».</p>
						<p>Я получила большой прилив энергии для того, на что раньше не хватало времени, много новой полезной информации, радость, хорошее настроение, больше спокойствия, осознанность, улучшенный time-management.
						</p>
						<p class="autor">Диана Супрун </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment19.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Открылся источник атомной энергии».</p>
						<p>
						Елизавета, ты как будто проводник всего самого прогрессивного в теме бизнеса. У меня с тобой настолько
						кругозор расширился и моя личная скорость, что даже страшно становится, как будто у тебя внутри какой-то
						супер двигатель. Главное, что я сам стал таким, меня в последние дни прет просто невозможно, открылся
						источник атомной энергии, о котором я думал, причем настолько сильный. Я спал сегодня два часа и с таким
						мощным прорывом все в бизнесе улучшал и руководил, как никогда, причем все быстрее и быстрее,
						эффективность растет на глазах, и главное не устал, твой вебинар прослушал, а энергии еще хоть отбавляй.
						Уже не знаю, чего ожидать, только все в голове уложится, как Лиза что-то да откроет, и мозг снова
						бомбардируется новыми парадигмами и идеями :)
						</p>
						<p class="autor">Сергей Иванов</p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment16.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Я начала делать все с наслаждением».</p>
						<p>Самое лучшее, на мой взгляд, - это выработка устойчивой привычки, неважно какой. Просто необходимость делать что-то каждый день. Раньше меня хватало дней на 5, максимум на неделю. Сейчас я уверена, что, составив для себя новую декларацию, я с большей долей вероятности доведу ее до конца. </p>
						<p>Еще один положительный момент - это то, что я действительно по совету Елизаветы начала делать все с наслаждением - это самый главный критерий закрепления полезной привычки. 
						</p>
						<p>Ну и кроме того, именно сейчас я почувствовала вкус к жизни, стараюсь каждый день испытывать положительные эмоции. Я получила очень большую порцию вдохновения и от тренера, и от участников. 
						</p>
						<p class="autor"> Елена Авдеева</p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment18.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«За последнее время я научилась слышать себя, чувствовать каждую клеточку своего тела».</p>
						<p>К счастью, я опять на ногах! </p>
						<p>Откровенно говоря, не без Вашего участия и поддержки.</p>
						<p>Каждый момент в жизни, при котором силы и даже надежды покидали меня, спасала работа Вашей команды.</p>
						<p>Ну а Елизавете, конечно же, отдельное, самое БОЛЬШОЕ СПАСИБО. </p>
						<p>С помощью ее открытой энергии с самой первой минуты, просматривая ее видео или пролистывая сообщения, меня охватывало вдохновение и возвращалась улыбка на лице. 
						</p>
						<p>За последнее время я научилась слышать себя, чувствовать каждую клеточку своего тела. </p>
						<p>Научилась не только бежать и преодолевать препятствия во что бы то ни стало, а точно знать, - стоит ли биться о закрытую дверь сейчас или же стоит переждать. </p>
						<p>Я очень рада, что Вы есть. </p>
						<p>Я счастлива, что с Божьей помощью на моем пути и пути многих людей есть такая команда - мощный двигатель, учитель, поддержка! </p>
						<p class="autor">Анна Феоктистова </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment22.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Основа курса - принципы целостности личности, честности и любви».</p>
						<p>
						Оформлена глобальная цель, которая раньше была чем-то эфимерным. Отличие этого курса от остальных в том,
						что он основан на принципах целостности личности, честности и любви, которые я искренне разделяю. 
						Я пошел на этот курс потому, что мне близки ценности, пропогандируемые Елизаветой. Поэтому я был уверен,
						что это станет выгодной инвестицией в себя. Если Вы действительно разделяете эти ценности, то, безусловно,
						получите что-то полезное для себя, и эти знания не встретят никаких барьеров - как это случилось со мной.
						</p>
						<p class="autor">Никита Чуклинов</p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment20.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Всегда даете больше, чем я ожидаю».</p>
						<p>Для меня впечатлением WOW!!!!!! сопровождается обучение у Вас. Вы всегда даете больше, чем я ожидаю и даже могу представить. Именно поэтому Ваши советы имеют эффект, а мои действия приносят мне успех и продвижение в самых разных сферах жизни.</p>
						<p class="autor">Ольга Маркова </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment13.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Бездонный колодец чистой воды».</p>
						<p>
						Уважаемая Елизавета! Я подключился к Бизнесс-Классу в феврале. Посмотрел 1-й урок и решил выполнить
						все задания (первый раз в своей жизни). Я открыл для себя просто бездонный колодец чистой воды. Я понял,
						что много вещей в бизнесе делал неправильно и растрачивал свое время. Так же понял, что я неорганизованный
						лентяй. Именно так. Поэтому этот курс я решил впервые в свой жизни пройти "от и до" и изменить себя. То,
						что вы даете на уроках, дорогого стоит. Спасибо вам огромное.
						</p>
						<p class="autor">Антон Смирнов </p>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/banner_line.png"></div>
				<div class="comment">
					<div>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/comment21.png" >
					</div>
					<div class="comment_text">
						<p class="comment_title">«Без вас я бы топталась на месте в смысле развития, бизнеса, здоровья, отношений, да и в смысле эмоций и личной эффективности. Я бы никогда не достигла того, где я сейчас».</p>
						<p>Я очень довольна курсом. Каждый урок - это большой взрыв во вселенной моего личностного развития. </p>
						<p>С нетерпением жду каждый урок, каждый пост, так как моя жизнь очень круто меняется и иногда я сама наблюдаю за собой как за незнакомым человеком :)</p>
						<p>Мое развитие пошло намного быстрее! Спасибо вам за сферическую систему развития, все, что вы делаете, - необыкновенно. Информация об энергии, здоровом образе жизни перевернула все в моей жизни. Я встаю в 5, выпиваю стакан воды с лимонным соком, занимаюсь йогой вместе с Надей Андреевой, выхожу на пробежку или езжу на велосипеде (и это в любую погоду), после душа занимаюсь Рейки, сбрасываю негативную энергию, планирую свой день, планирую задачи в семейном бизнесе на сегодня, готовлю завтрак для семьи и в 8.30 я - как мама-картинка из американских фильмов. Энергичная, быстрая, в отличном настроении, все успеваю, красивая, ухоженная, мама, домохозяйка, бизнес-леди и заботливая жена одновременно :). Ваши советы по отношениям помогли спасти мой брак, наладить отношения с мамой, с которой мы никогда не ладили. Наш бизнес стремительно развивается, и, заодно, я каждый день приближаюсь к своему собственному бизнесу. Я отказалась от кофе, перестала переедать и ушла усталость в течении дня. Ранний подъем дал мне время, которого у меня не было. Я в один миг отрезала воспоминания о прошлом, обиды, мнения других людей, лень, сплетни, потерю времени, нечестные поступки и стараюсь в каждой сфере своей жизни быть на высоте. </p>
						<p>Дорогая Елизавета! Долгое время слежу за вашими статьями, семинарами, видео. Все, что вы делаете - законспектировано и перечитано  много раз. Не решалась на обратную связь, так как всегда думала, что вам не до меня, что вы и так очень заняты. Просто сегодня проснулась и поняла, что без вас я бы топталась на месте в смысле развития, бизнеса, здоровья, отношений, да и в смысле эмоций и личной эффективности (я уже всю свою жизнь делю на ваши сферы:)). Я бы никогда не достигла того, где я сейчас. А ваша программа дала мне твердую уверенность, что все, о чем я мечтаю - реально, а разница между мной и успешной женщиной из моей мечты - знания и внедрение их в жизнь. </p>
						<p>Спасибо Вам еще раз и надеюсь, что ваша деятельность так же изменит жизнь участников, как изменилась моя.</p>
						<p class="autor">Ольга Йылмаз </p>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="price_table">
				<div class="price_table_header">
					<div class="variants">3 варианта участия</div>
					<div class="th-row">
						<div class="th">Ежемесячный</div>  
						<div class="th" style="color:#ff9900;">Премиум</div>
						<div class="th">VIP</div>
					</div>
				</div>
				<div class="col1">
					<div class="row1">
					<span class="price">$150</span><span class="period" >/месяц</span>
					</div>

				<div class="price_descr">1 модуль + <br />1 вебинар вопрос/ответ</div>
				<div class="minus">-</div>
				<div class="minus1">-</div>
				<div class="minus">-</div>
				</div>
				
				<div class="col2">
					<div class="row1">
						<img class="oldpriceimg" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/oldprice.png">
						<div class="oldprice">$1950/год</div>
						<span class="col2price">$950</span><span class="col2period" >/год</span>
						<div class="eko"><span>Вы экономите </span><span class="sale" >$1000</span></div>
					</div>
				
				<div class="price_descr">13 модулей + <br />12 вебинаров вопрос/ответ</div>
				<div class="price_descr">Приглашения на <br>мастер-классы с гостями</div>
				<div class="price_descr" style="height:auto;">Доступ к <br>6 вебинарам:
					<p>Как побороть лень</p>
					<p>Взрыв Энергии</p>
					<p>Как изменить все в<br />своей жизни к лучшему</p>
					<p>Как найти свое<br />профессиональное<br />призвание</p>
					<p>Как прийти к здоровому<br />образу жизни за 45</br>дней?</p>
					<p>Как реализовать свой<br />потенциал?</p>				
				</div>
				<div class="minus2">-</div>
				</div>

				<div class="col3">
					<div class="row1">
					<img class="oldpriceimg" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/oldprice.png">
					<div class="oldprice">$3985/год</div>
					<span class="price">$1750</span><span class="period" >/год</span>
					<div class="eko"><span>Вы экономите </span><span class="sale" >$2235</span></div>
					</div>
					<div class="price_descr">13 модулей + <br />12 вебинаров вопрос/ответ</div>
					<div class="price_descr">Приглашения на <br>мастер-классы с гостями</div>
					<div class="price_descr" style="height:auto;">Доступ к <br>6 вебинарам:
						<p>Как побороть лень</p>
						<p>Взрыв Энергии</p>
						<p>Как изменить все в<br />своей жизни к лучшему</p>
						<p>Как найти свое<br />профессиональное<br />призвание</p>
						<p>Как прийти к здоровому<br />образу жизни за 45</br>дней?</p>
						<p>Как реализовать свой<br />потенциал?</p>				
					</div>
					<div class="price_descr" style="height:auto;margin-top:47px">Доступ ко всем <br>программам в записи:
						<p>Живи по Полной!</p>
						<p>Феномен Мастерства</p>
						<p>Апгрейд Здоровья</p>
						<p>Апгрейд <br >Эффективности</p>
						<p>Бизнес Класс</p>
						<p>Как устроиться на <br>работу мечты?</p>
					</div>
				</div>
				<div style="clear:both"></div>

				<div class="price_table_footer">
					<div id="rezult" class="downtime"></div>
				</div>
				
				<img class="greyline1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/grey_line.png">
				<img class="greyline2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/grey_line.png">
				<img class="greyline3" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/grey_line.png">
				<img class="greyline4" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/grey_line.png">
			</div><!--price table-->
			<div id="footer">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/about_foto.png">
				<div class="about">
					<div class="about_name">Елизавета Бабанова</div>
					<div class="about_autor">автор программы</div>
					<div class="about_desc">
						<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: 
							Экономика, Финансы, Маркетинг, Менеджмент.</p>
						<p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
						<p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
						<p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
						<p>Член Международной Ассоциации Непрерывного Образования.</p>
					</div>
	            </div>
	            <div class="vk">
						<!-- VK Widget -->
						<iframe width="200" scrolling="no" height="200" frameborder="0" name="fXD32f6b" src="//vk.com/widget_community.php?app=0&amp;width=200px&amp;gid=44023512&amp;mode=0&amp;height=400&amp;url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&amp;13b1da20c61" id="vkwidget1" style="overflow: hidden; height: 400px;"></iframe>
				</div>

			</div><!--footer-->
                     
             <div style="clear:both"></div>      
            
            <div class="footer_info">
				<div class="copy" >ИП Бабанова Елизавета Дмитриевна   ИНН 370259937869   2013 Все права защищены</div>
				<nav class="bottom_menu">
					<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
					<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
					<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
				</nav>
			</div>




		</div><!--content-->

	</div>
<script>
	function countdown(){
		/*
		var today = new Date().getTime();
		if (!localStorage.startdate)
			localStorage.startdate = today;
		
		var start = localStorage.startdate;

		var perDays = 24*60*60*1000;
		var end = Number(start) + 7*Number(perDays);
		var dateX = new Date(end-today);
		
		if (Math.round(dateX/perDays)-1 < 0) {
			var reglink = 'http://elizavetababanova.com/form17/';
			var descr = '<span class=days><br /><br />Акция уже закочилась.<br />Вы можете оплатить участие по полной стоимости.</span>';
		}
		else {
		*/
			var reglink = 'http://elizavetababanova.com/form16/';
		/*
			var descr = '<div class="downtime_title">До конца предложения <br>осталось:</div><span class=days>' + (Math.round(dateX/perDays)-1 + '</span><span class=downtime><span style=font-weight:bold;> дней </span>' + dateX.getUTCHours().toString() + ' часов ' + dateX.getMinutes().toString() + ' минут ' + dateX.getSeconds().toString() + ' секунд</span>');
		}
		*/
		var towrite = '<div class="footer_button"><a href='+reglink+'><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/ch_button.png"></a></div>';
		towrite = towrite + '<div class="footer_button"><a href='+reglink+'><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/ch_button.png"></a></div>';
		towrite = towrite + '<div class="footer_button"><a href='+reglink+'><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/ch_button.png"></a></div>';
		towrite = towrite + '<div style="clear:both"></div>';
		towrite = towrite + '<div>' + '<br /><br /><br />' /*descr*/ + '</div>';
		towrite = towrite + '<div class="garantia">' +
					'<img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/garantia.png" alt="" />' +
					'<div class="gtext">В течение 3х месяцев вы можете осуществить полный возврат вашей инвестиции, если вы почувствуете, что эта программа не подходит вам на данном этапе жизни.</div></div>';
		towrite = towrite + '<div style="clear:both"></div>';
		towrite = towrite + '<div class=reg><a href='+reglink+'><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-selling/images/reg.png"></a></div>';
				
		var result = document.getElementById('rezult');
		result.innerHTML = towrite;
	}
	countdown();
	setInterval(countdown, 1000);
</script>

<script>
	function doPopup(name){
		if ($(name).is(':visible')){
			$(name).hide('slow');
		}
		else {
			$("#first").hide();
			$(name).slideToggle(2000);
		}
	}
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 998435878;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/998435878/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>