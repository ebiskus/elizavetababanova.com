<?php
/*
Template Name: Страница 11mistakes-2
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>7 шагов к работе вашей мечты</title>
<meta property="fb:admins" content="100003273128680"/>  
<title></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
<link href="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/css/style.css" rel="stylesheet">
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/js/jquery.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/js/plugins.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/js/custom.js"></script>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
<meta property="og:title" content="7 шагов к работе вашей мечты"/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="http://elizavetababanova.com/7shagov-work-dream" />
<meta property="fb:admins" content="100003273128680"/> 
<meta property="og:description" content="Запись открытого вебинара Елизаветы Бабановой о том, как найти работу своей мечты." />
<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/11mistakes/11mistakes-2/img/untitled8.jpg" />
<script type="text/javascript">
 VK.init({apiId:  2461907, onlyWidgets: true});
</script>
<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
<link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/11mistakes/11mistakes-1/img/photo.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="main">
	<header class="header">
		<div class="inner">
			<a class="logo" href="http://elizavetababanova.com/" target="_blank">
				<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b1.png" alt=".">
			</a>
			<nav class="menu">
				<a target="_blank" href="http://vk.com/elizaveta.babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b2.png" alt=".">
				</a>
				<a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b3.png" alt=".">
				</a>
				<a target="_blank" href="http://twitter.com/liza_babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b4.png" alt=".">
				</a>
				<a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b5.png" alt=".">
				</a>
			</nav>
		</div>
		<i class="border"></i>
	</header>
	<div class="inner">
		<nav class="menu2">
			<a class="i i1 tworow " href="http://elizavetababanova.com/11-mistakes/">11 грандиозных ошибок, совершаемых<br> при устройстве на работу</a>
			<a class="i i2 active" href="http://elizavetababanova.com/7shagov-work-dream">7 шагов к работе вашей мечты</a>
			<a class="i i3" href="http://elizavetababanova.com/dream-job">Программа курса</a>
			</nav>
		<section class="block">
			<div class="i i2">
				<p class="title"><span class="custom">7</span> шагов к работе вашей мечты</p>
				<section class="body">
				
				<iframe width="880" height="495" src="http://www.youtube.com/embed/h81cLirAYfA" frameborder="0" allowfullscreen></iframe>
			</div>
		</section>
					
		<section class="block2">
			<div class="block3"><!--
				<span class="i">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b7.png" alt=".">
				</span>
				<span class="i">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b8.png" alt=".">
				</span>
				<span class="i">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b9.png" alt=".">
				</span>
				<span class="i">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b10.png" alt=".">
				</span>
				<span class="i">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b11.png" alt=".">
				</span>
				<span class="i">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b12.png" alt=".">
				</span>-->
				<p>&nbsp;</p>
				<p class="text">Понравилось видео? Поделитесь им в социальных сетях и оставьте свой комментарий ниже</p>
				
				<center>         <?php 
    if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )  
        echo ""; 
    else
        require('social_button.php');
?> </center>
			</div>
			<div class="block4">
				<img class="i img" src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b13.png" alt=".">
				<section class="i map">
					<a href="http://elizavetababanova.com/otzivy" target="_blank"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b14.png" alt="."></a>
				</section>
			</div>
			
		</section>
		<i class="border2"></i>
		<section class="block5">
		<a href="http://elizavetababanova.com/dream-work"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/button3.jpg" style="margin:10px 80px;"></a>
		<a href="http://ecommtools.com/buy/elizavetababanova/dreamwork"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/button1.jpg"></a><a href="http://elizavetababanova.com/form2/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/button2.jpg"></a></section>
		<!--<i class="border3"></i>
		<section class="block6">
			<div class="i i1"><a href="http://vkontakte.ru/share.php?url=http://elizavetababanova.com/7shagov-work-dream" target="_blank">Расскажите</a> о видео друзьям ВКонтакте! </div>
			<div class="i i2"><a href="http://twitter.com/intent/tweet?text=Рекомендую посмотреть видео о 7 шагов к работе вашей мечты!:&url=http://elizavetababanova.com/7shagov-work-dream" target="_blank">Твитнуть</a> о 7 шагах к работе вашей мечты!</div>
		</section>
		<i class="border3"></i>
		<section class="block7">
			<img class="img" src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/img/b15.png" alt=".">
			<div class="facebook">
			<div class="fb-like" data-href="http://elizavetababanova.com/7shagov-work-dream" data-send="false" data-width="280" data-show-faces="true"></div></div>
		</section>-->
		<section class="block8">
			<div class="left">
				<div id="vk_comments"></div>
				<script type="text/javascript">
				VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
				</script>
			</div>
			<div class="right">
				<div class="fb-comments" data-href="http://elizavetababanova.com/7shagov-work-dream" data-width="400" data-num-posts="5"></div>
			</div>
		</section>
		<section class="block8">
		<?php comments_template('/comments2.php'); ?>
		</section>
		<footer class="footer">
			<i class="border6"></i>
			<p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
		</footer>
	</div>
</section>

</body>
</html>