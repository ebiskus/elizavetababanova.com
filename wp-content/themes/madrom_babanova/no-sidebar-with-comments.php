<?php
/*
Template Name: No-Sidebar-With-Comments
*/
?> 

<?php
require('./wp-blog-header.php');
require_once('header.php');
require_once('functions.php');
?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
 bcn_display();
}
?>
</div>

<div class="singl">

<div id="wrap" class="container singl" style="width: 820px;">
 
<div id="content" style="width:820px;">	
     
					<?php if (have_posts()) : ?>	
						<?php while (have_posts()) : the_post(); ?>
						<div class="page_post" <?php post_class() ?> id="post-<?php the_ID(); ?>">
							
									<h1 class="title avant_book">
              
<?php 

        the_title();
?> 
  
              </h1>   
          		
							<div class="postdate avant_book">  
              <?php 
    if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )  
        echo ""; 
    else
        
?>
              
              <?php if (current_user_can('edit_post', $post->ID)) { ?> <?php edit_post_link('Edit', '', ''); } ?>
              
              </div>
			
			
			
							<div class="entry">
                                <?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(300,225), array("class" => "alignleft post_thumbnail")); } ?>
								<?php the_content('Читать далее &raquo;'); ?>
								<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							</div> 
							
              
              
                          						 
						
							<?php edit_post_link('Редактировать эту запись','','.'); ?>
							
						</div><!--/post-<?php the_ID(); ?>-->  
				
				
				<?php endwhile; ?>
			
				<?php endif; ?>
				
</div><!-- #content-->
<p>&nbsp;</p>
<center>         <?php 
    if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )  
        echo ""; 
    else
        require('social_button.php');
?> </center>



<div style="float: left;width: 390px;min-height: 100px;">
<div id="vk_comments"></div>
<script type="text/javascript">
VK.Widgets.Comments("vk_comments", {limit: 5, width: "390", attach: "*"});
</script></div>

<div style="float: right;width: 390px;min-height: 100px;">
	<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="390" data-num-posts="5"></div></div>


<div style="clear:both;"></div> 
<p>&nbsp;</p><br />
  <?php comments_template('/comments2.php'); ?>


 </div><!-- #wrapper2 --> 
 
</div>

 <div class="line_shadow"></div>
 
 <div id="wrapper3">

<div id="footer">
<?php require('footer.php'); ?>
</div><!-- #footer --> 

</div><!-- #wrapper3 -->