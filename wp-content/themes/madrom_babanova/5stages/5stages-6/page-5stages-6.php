<?php
/*
Template Name: Страница 5stages-6-programm
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta property="fb:admins" content="100003273128680"/>  
<title>Программа тренинга</title>
<script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
<link href="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/css/style.css" rel="stylesheet">
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/js/jquery.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/js/plugins.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/js/custom.js"></script>
<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/og_pm.png">
<meta property="og:title" content="Программа тренинга"/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="http://elizavetababanova.com/programma-treninga"/>
<meta property="og:description" content="Это программа, вскрывающая чип-код Вашего внутреннего гения.В процессе прохождения программы Вы узнаете, как совершить грандиозный прорыв из &quot;хорошего&quot;, а, возможно, даже &quot;отличного&quot; специалиста своего дела на уровень мастера.">
<script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
<script type="text/javascript">
 VK.init({apiId:  2461907, onlyWidgets: true});
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js">
</script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/js/scrollto.js" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
<link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-6/img/.jpg" />
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="main">
	<header class="header">
		<div class="inner">
			<a class="logo" href="http://elizavetababanova.com/" target="_blank">
				<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/b1.png" alt=".">
			</a>
			<nav class="menu">
				<a target="_blank" href="http://vk.com/elizaveta.babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/b2.png" alt=".">
				</a>
				<a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/b3.png" alt=".">
				</a>
				<a target="_blank" href="http://twitter.com/liza_babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/b4.png" alt=".">
				</a>
				<a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/b5.png" alt=".">
				</a>
			</nav>
		</div>
		<i class="border"></i>
	</header>
	<div class="inner">
		<nav class="menu2">
			<a class="i i1 tworow" href="http://elizavetababanova.com/5stages">5 уровней<br> развития человека</a>
			<a class="i i7" href="http://elizavetababanova.com/research">Исследования</a>
			<a class="i i2 tworow" href="http://elizavetababanova.com/survey">Результаты<br>опроса</a>
			<a class="i i3 tworow" href="http://elizavetababanova.com/6-moih-vectorov">6 моих<br>векторов</a>
			<a class="i i4 tworow" href="http://elizavetababanova.com/phenomen-masterstva">Феномен<br>Мастерства</a>
			<a class="i i5 tworow active" href="#">Программа<br>тренинга</a>
			<a class="i i6" href="#">Скоро!</a>
		</nav>
		<section class="block">
			<div class="i i1">
				<p class="title"><span class="custom">П</span>рограмма тренинга</p>
				<section class="body">

<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/header-pm_cr1.jpg" alt="." usemap="#Navigation">
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/header-pm_cr2.jpg" alt=".">
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/header-pm_cr3.jpg" alt=".">
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/header-pm_cr4.jpg" alt=".">
<map name="Navigation">
  <area shape="rect" coords="0,1090,440,1180" href="#app_1" alt="Информация">
  <area shape="rect" coords="0,1180,440,1270" href="#app_2" alt="Информация">
  <area shape="rect" coords="0,1270,440,1360" href="#app_3" alt="Информация">
  <area shape="rect" coords="0,1360,440,1450" href="#app_4" alt="Информация">
  <area shape="rect" coords="440,1090,880,1180" href="#app_5" alt="Информация">
  <area shape="rect" coords="440,1180,880,1270" href="#app_6" alt="Информация">
  <area shape="rect" coords="440,1270,880,1360" href="#app_7" alt="Информация">
  <area shape="rect" coords="440,1360,880,1450" href="#app_8" alt="Информация">
  </map>
  

  
				</section>
<div class="text2">
		
		<center>
<a href="http://ecommtools.com/buy/elizavetababanova/9"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/opl-but.jpg" style="padding-top: 50px; padding-right:35px;" width="40%"></a>
                
<a href="http://elizavetababanova.com/form/index2.php"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/za-but.jpg" style="padding-top: 50px;" width="40%"></a>
</center>
		<br />

<div class="citata">
    
	<p class="cblue avangardec text_tr fs36 otz_tit">Отзывы</p>

<div class="otz">
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/kysil.png">
<p class="fs24"><a href="http://www.facebook.com/olgaksl?fref=ts">Ольга</a><p>
<p class="fs16">Director at LegalShield, Independent Associate, Чикаго, США</p>
<p>"Высокий и качественный уровень подачи информации, конкретные задачи, шаги, дружеские «пинки», мотивация, поддержка… И в конечном итоге — невероятные результаты. О!, но этот момент был только началом моего прорыва. Прошло время, а я каждый день пользуюсь знаниями, применяю их на практике и наслаждаюсь  результатами. Самое замечательное ещё впереди…. Я  благодарю Вас за всю Вашу отдачу, за то что Вы такая какая есть. За то, что искренне делитесь своим опытом, за качество, за глубокий смысл всего чему обучаете. Вы истинный мастер своего дела и делаете это с любовью! С большим уважением и восхищением!"</p>
</div>
<div class="otz">
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/grigorieva.png">
<p class="fs24">Юлия Григорьева</p>
<p class="fs16">Пермь, Россия</p>
<p>"Благодаря Вам и таким людям как Вы, наступает Золотая эра! Я очень рада нашей встрече и новым возможностям изменить свою жизнь. Любви и процветания Вам!"</p>
</div>
<div class="more_otz" id="but_mr"><a href="javascript:doPopup('#first')"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/more-otz.png"></a></div>

<div id="first">
<div class="otz" style="min-height:200px;" >
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/kalitvenec.png">
<p class="fs24">Игорь Калитвенцев</p>
<p class="fs16">Киев, Украина</p>
<p>"Елизавета, Вы мой ангел и проводник к новым высотам на жизненном пути."</p>
</div>
<div class="otz" style="min-height:180px;" >
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/mokshanina.png">
<p class="fs24">Евгения Мокшанина</p>
<p class="fs16">Верона, Италия</p>
<p>"Огромное спасибо вам Елизавета за неугосимый энтузиазм в знании, которым просветляете сердца людей, ободряете волю, укрепляете Дух. Какое же это всё-таки счастье возрождаться, наслаждаться позитивно настроенным окружением, мыслящим по-новому."</p>
</div>
<div class="otz" style="min-height:200px;" >
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/lapshova.png">
<p class="fs24">Ирина Лапшова</p>
<p class="fs16">Москва, Россия</p>
<p>"Меня покорили достигнутые Вами результаты, а также безграничная преданность делу, которому вы служите, и ваше огромное желание помогать людям!"</p>
</div>

<div class="otz" style="min-height:200px;" >
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/sofia.png">
<p class="fs24">София</p>
<p class="fs16">Бишкек, Кыргызстан</p>
<p>"Елизавета, я Вам очень благодарна за уроки жизненной мудрости! Моя жизнь уже начала меняться в положительную сторону с момента, как я узнала о Вас! Выполняю Ваши рекомендации и чувствую, что с каждым днем становлюсь все счастливее, радостнее, энергичнее... Быть Вашим учеником и последователем - это великая честь для меня! Благодарю Вас! Читая отзывы других, я чувствую силу и желание продолжать этот приятный путь самосовершенствования!"</p>
</div>



<div class="otz" style="min-height:200px;" >
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/kuzmicheva.png">
<p class="fs24">Ирина Кузьмичева</p>
<p class="fs16">Севастополь, Украина</p>
<p>"Елизавета, Ваши статьи, курсы, семинары вдохновляют и заставляют чувствовать себя, как на крыльях! И на этих выросших крыльях вдохновения, радости бытия и благодарности я уже лечу к своей мечте  Спасибо что укрепляете их! Благодаря им (и Вам!) я уже столько сделала из того, о чем раньше даже и думать не смела (ну разве что чуть-чуть). И так это здорово!"</p>
</div>



<div class="otz">
<p class="fs24">Татьяна Enco</p>
<p class="fs16">Дурлешты, Молдова</p>
<p>"Елизавета благодарю Вас за великолепное обучение, за Вашу искренность, за Ваш оптимизм, за веру в нас, Ваших учеников! Я искренне рада что Вы являетесь моим Наставником."</p>
</div>
<div class="otz">
<p class="fs24">Андрей</p>
<p class="fs16">Киев, Украина</p>
<p>"Елизавета, мне очень импонирует Ваш подход: все четко, структурировано и по полочкам. Даете ясность для головы, все в точности совпадает с реальностью."</p>
</div>
<div class="otz">
<p class="fs24">Татьяна Аниканова</p>
<p class="fs16">Киев, Украина</p>
<p>"Спасибо Лиза за уроки! Да уроки отличные, скорее это как концентрат знаний, выведенный пройдя определённый опыт в жизни. И очень важно видеть и знать что такие учителя есть, которые могут поделиться своим опытом - они как путеводные огни и это понимание сильно помогает мне в жизни."</p>
</div>
<div class="otz">
<p class="fs24">Николай</p>
<p class="fs16">Новосибирск, Россия</p>
<p>"Здравствуйте, Елизавета! Благодарю за статью и полезные Уроки! 
Для себя открыл: <br>
1) Учиться у Лучших!<br> 
2) Мыслить созидательно!<br> 
3) Говорить позитивно!<br>
4) В проблеме искать семя успеха!<br>
5) Действовать!"</p>
</div>
<div class="otz">
<p class="fs24">Мари Егорова</p>
<p class="fs16">Кемерово, Россия</p>
<p>"Елизавета, Вы тот человек, на которого хочется равняться. Спасибо, что направляете нас на путь к более яркой, насыщенной, лучшей жизни!
Благодаря постепенному развитию у меня уже появился ряд полезных привычек. Появился какой-то внутренний контроль над собой, что очень помогает не отступать от целей и не отвлекаться от них. Хотя работы еще много, позитивные изменения налицо! Буду рада и дальше развиваться и смотреть ваши уроки, и читать статьи."</p>
</div>
<div class="otz">
<p class="fs24">Ашдод</p>
<p class="fs16">Израиль</p>
<p>"Добрый вечер Елизавета! Родная моя любимая моя наставница как же я Вас 
УВАЖАЮ!!! 
ВЫ открыли Мне глаза на жизнь и заставили поверить в себя в свою 
мечту и в 65 начать все сначала. Жаль что этого всего не было лет 20 
назад."</p>
</div>
<div class="otz">
<p class="fs24">Любовь Кулагина</p>
<p class="fs16">Оренбург, Россия</p>
<p>"Спасибо Елизавете - как тренеру и наставнику, взявшему меня за руку и ведущему по пути к жизни, наполненной смыслом и постоянным ощущением счастья!
Команде звезд Елизаветы - за поддержку и неусыпный контроль за прочностью того канатика, которым связана моя лодочка, прибившаяся к вашему величайшему кораблю! 
Я сорганизовала себя, стала уверенней, поднялась самооценка, самоуважение, повысились стандарты, я стала делать то, что должна была делать, но не делала, я начала делать то, что я знала, что мне это нужно делать, но что я тоже не делала, находя причины и оправдания."</p>
</div>

<div class="otz">
<p class="fs24">Виктор</p>
<p class="fs16">Россия</p>
<p>"Ваши уроки, Елизавета, заряжают по полной! А самое главное, что я много делал и до уроков, но как то всё это было не системно, в разброс!Сейчас, благодаря Вам, всё становится чётко, исчезает суета!"</p>
</div>
<div class="otz">
<p class="fs24">Ирина Козеева</p>
<p>"Всю свою жизнь я искала, накапливала знания, книги, мысли, идеи по воплощению гармоничного развития личности. У меня возникали мысли, что пора выстроить систему, которая помогла бы людям жить точнее, счастливее, реализовывать таланты, на благо свое и мира!
А Вы смогли создать это! Систематизировать опыт успешных, мудрых людей, разложить все по полочкам, найти способ донести и  дать каждому желающему возможность улучшить свою жизнь пошаговыми действиями!"</p>
</div>
<div class="otz">
<p class="fs24">Саша</p>
<p class="fs16">Монреаль, Канада</p>
<p>"Елизавета! Спасибо за возможность понять, что я делал много, но не правильно. Посмотрев Ваши уроки я осознал, что делал и развивался не целостно."</p>
</div>


<div class="otz">
<p class="fs24">Yelena Gabriella Aronov</p>
<p class="fs16">Columbus, Ohio</p>
<p>"Более 20 лет занимаясь личностным развитием и посетив свещенные места в более чем 23 странах мира, общаясь с Мастерами разных эпох, могу с уверенностью сказать, что курс Елизаветы это совокупность знаний и мастерства , прочувствованных на собственном опыте. А это главный ключ к истинному успеху самопознания и раскрытия скрытых резервов нашей личности! Мы не имем права обучать тому, что не применяем по жизни сами! Это очередной подарок судьбы быть в числе слушателей этого курса!"</p>
</div>




</div>

<div class="about" style="min-height:600px;">
<div style="float:left; width:600px;">
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/elizaveta.png">
<h3 class="text_tr ptsans sin fs30">Елизавета Бабанова</h3>
<p>В 15 лет уехала из России, став победителем конкурса "Акт в поддержку свободы" на бесплатное годовое обучение в США.</p>
<p>Окончила 2 ВУЗа в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
<p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
<p>С 18 лет занимается личностным развитием. Изучила и синтезировала самые продвинутые российские и западные методики и технологии достижения успеха.</p>
<p>Основатель и совладелец двух успешных интернет-проектов в области саморазвития и бизнес-обучения.</p>
<p>В процессе профессиональной деятельности и глубокого изучения вопросов саморазвития разработала для себя "Систему сферического развития", обеспечивающую систематизированный подход к достижению высоких результатов.</p>
<p>После внесения своих данных в форму вы также получите в подарок новую, улучшенную версию Таблицы Сферического Развития, самого эффективного инструмента для быстрого и рационального внедрения Системы Сферического Развития в свою жизнь.</p>
</div>
<!-- VK Widget -->
<div style="height: 400px; float:right; width: 200px; background: none repeat scroll 0% 0% transparent; margin-top:90px;" id="vk_groups"></div>
<script type="text/javascript">
VK.Widgets.Group("vk_groups", {mode: 0, width: "200", height: "400"}, 44023512);
</script>
</div>
<center>
<a href="http://ecommtools.com/buy/elizavetababanova/9"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/opl-but.jpg" style="padding-top: 50px; padding-right:35px;" width="40%"></a>
                
<a href="http://elizavetababanova.com/form/index2.php"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/za-but.jpg" style="padding-top: 50px;" width="40%"></a>
</center>


	
		
		
<br />
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/garant.jpg" alt="." style="padding-top:20px;">
		</div>
		<div class="about2" style="margin:10px auto;">
		<p class="avangardec text_tr fs36yellow otz_tit" style="text-align:center;">Есть вопросы?</p><br>
		<p>Остались вопросы по программе <span style="text-weight:bold;">&laquo;Феномен Мастерства&raquo;</span>? Мы будем рады ответить на них.</p>
		<p>Пишите на почту <a href="mailto:elizavetababanova@gmail.com">elizavetababanova@gmail.com</a> или в скайп elizavetamiami</p>
</div>
</section>
</div>

				</section>
			</div>
		</section>
		<div style="position: absolute;top:1700px;left:10px;"><a name="app_1"></div>
		<div style="position: absolute;top:2300px;left:10px;"><a name="app_2"></div>	
		<div style="position: absolute;top:2900px;left:10px;"><a name="app_3"></div>
		<div style="position: absolute;top:3530px;left:10px;"><a name="app_4"></div>
		<div style="position: absolute;top:4200px;left:10px;"><a name="app_5"></div>
		<div style="position: absolute;top:4880px;left:10px;"><a name="app_6"></div>	
		<div style="position: absolute;top:5630px;left:10px;"><a name="app_7"></div>
		<div style="position: absolute;top:6450px;left:10px;"><a name="app_8"></div>

		<footer class="footer">
			<i class="border6"></i>
			<p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
		</footer>
	</div>
</section>
<script>
function doPopup(name){
	if ($(name).is(':visible')){
		$(name).hide('fast');
	}
	else {
		$("#first").hide();
		$(name).slideToggle(800);
	}
}
</script>
</body>
</html>