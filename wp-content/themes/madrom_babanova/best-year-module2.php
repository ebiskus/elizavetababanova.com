<?php
/*
Template Name: Лучший год Модуль 2
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/module.css">

	<title>Иммунитет к препятствиям, комплексам и страхам.</title>

    <meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/share02.jpg">
	<meta property="og:title" content="Иммунитет к препятствиям, комплексам и страхам."/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear-module2"/>
	<meta name="description" content="Годовая программа «Лучший год Вашей жизни». В этом высокоинтенсивном модуле вы проработаете свои ограничивающие убеждения и определите корень своего основного страха, который мешает вам лететь на нужной вам скорости.">

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div>
			<img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script>
	<!--/LiveInternet-->	
	
	
	
	
	
	
	
	
	






	
<script src="/wp-content/themes/madrom_babanova/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/wp-content/themes/madrom_babanova/js/jquery.easing.1.3.js" type="text/javascript"></script> 
<link rel="stylesheet" href="/wp-content/themes/madrom_babanova/js/jquery.fancybox.css" type="text/css" media="screen">
<script type="text/javascript" src="/wp-content/themes/madrom_babanova/js/jquery.fancybox.pack.js"></script> 
<script src="/wp-content/themes/madrom_babanova/js/waypoints.min.js"></script>	

<script type="text/javascript">
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    $(document).ready(function () {
        var referrer = document.referrer;

 		$('.various').fancybox({ height: '443px', width: '550px'});

        $('#holod').waypoint(function () {
            $('#demoplay').show('slow', 'easeInOutBack');
        }, { offset: 200, triggerOnce: true });
    });
    </script>

<style>
#demoplay
{
	position:fixed; 
	right: 0; 
	top: 114px; 
	width: 150px; 
    padding: 10px;
	background-color: #00ccff;
	text-align: center;
	line-height: 1.2em;
	color: white;
	z-index: 10;
	
	border-bottom-left-radius: 10px;
 	-moz-border-radius-bottomleft: 10px;
	-webkit-border-bottom-left-radius: 10px;
	border-top-left-radius: 10px;
 	-moz-border-radius-topleft: 10px;
	-webkit-border-top-left-radius: 10px;
	 -moz-box-shadow: 0 0 10px #3B3B3B; /* Для Firefox */
    -webkit-box-shadow: 0 0 10px #3B3B3B; /* Для Safari и Chrome */
    box-shadow: 0 0 10px #3B3B3B; /* Параметры тени */
}

#demoplay img
{
	margin-bottom: 0px;
}

#demoplay img:hover
{
	width: 100px;
	margin-bottom: -4px;
}
#demoplay a { color: white; }
#demoplay .close
{
	position: absolute;
	right: 5px;
	top: 0;
	color: black;
}
</style>
	
	
		
	
</head>

<body>




    <div id="demoplay" style="display:none;">
    	<a class="close" onclick="$(&#39;#demoplay&#39;).hide();" title="Закрыть окошко">x</a>
    	Фрагмент из модуля «Иммунитет к препятствиям, комплексам и страхам»
        <a class="various fancybox.iframe" href="http://www.youtube.com/embed/W4d5CFV1piQ?rel=0" "/wp-content/themes/madrom_babanova/js/frame1.html"><img src="/wp-content/themes/madrom_babanova/js/play.png">
        просмотреть</a>
    </div>







    <div id="bym-wrapper" align="center">
		<div id="bym-social">
			<div id="bym-logo">
			   <a href="/"><img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/vk_b.gif"></a>
				<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/facebook_b.gif"></a>
				<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tweeter_b.gif"></a>
				<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/youtube_b.gif"></a>
			</div>
		<div style="clear:both" id="holod"></div>

		</div>
		<div id="bym-header" align="center">
			<div class="bym-all_year">
				<div class="bym-mounth"><p class="bym-digit">02</p><p class="bym-name">ФЕВРАЛЬ</p></div>
				<div class="bym-description"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/title2.png">
					<p class="bym-text">
					В этом высокоинтенсивном модуле вы проработаете свои ограничивающие убеждения и определите корень своего основного страха, который мешает вам лететь на нужной вам скорости. В результате прохождения этого модуля вы станете самым осознанно бесстрашным человеком из всех, кого вы знаете.
					</p>
					</p>
				</div>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/02.png">
			</div>
			<div style="clear:both"></div>

		</div>
		<div id="bym-content">
			<div id="bym-mon">
				<div id="bym-mon1-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m2mon1.jpg" ></div>
				<div id="bym-mon1">
				<ul>
					<li>Понимание глубинных источников и причин комплексов и страхов</li>
					<li>Признаки и последствия комплексов и страхов</li>
					<li>Разница между рациональным страхом и иррациональным</li>

					<li>Распространненые провокаторы:
							<ul>
								<li>Люди</li>
								<li>Ощущение потери контроля </li>
								<li>Сомнения</li>
								<li>Страх успеха</li>
								<li>Страх поражения</li>

							</ul>
					</li>
					<li>Определение вашего основного ограничивающего убеждения</li>
				</ul>
				</div>
			  
				<div id="bym-mon2" style="padding-top:200px;">
				<ul>
					<li>Способы преодоления внутренних и внешних ограничений
							<ul>
								<li>Чем заменяются комплексы и страхи?</li>
								<li>Новая модель ощущения и поведения</li>

							</ul>
					</li>
					<li>Один страх, который нам на самом деле нужен
							<ul>
								<li>Способы сделать из него помощника в достижении целей</li>
							</ul>
					</li>
					<li>Закрепление рационально бесстрашного мышления</li>
				</ul>
				</div>
				<div id="bym-mon2-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m2mon2.jpg"></div>
			</div>
		</div>


		<div style="clear:both"></div>




		<div id="bym-reg">

			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>


			<div style="clear:both"></div>

		</div>






		<div style="clear:both"></div>

		<div class="whatsay">
			<div><img id="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
			<div>Что говорят участники второго модуля</div>
			<div><img id="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
		</div>
		<div style="clear:both"></div>


		
		<div id="bym-comment">	




	
			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/natalya-sahnenko.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Очень сильный модуль»</p>
					<p>Очень сильный модуль. Я бы назвала его "Погружение в глубины своей Тени за потенциалом" или сравнила с прыжком в глубины морские за поиском жемчужин. </p>
					<p class="autor">Наталья Сахненко | Москва, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>






	
			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/manzhosova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Очень полезная информация!»</p>
					<p>Очень полезная информация! Мне были полезны все упражнения! Некоторые меня порадовали, я была собой довольна. Некоторые заставили задуматься. СПАСИБО, что заставляете нас меняться, думать и действовать!</p>
					<p class="autor">Елена Манжосова | Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>




	
			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/natalya-poddubnaya.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Это невероятное озарение: приходит понимание, принятие, внутреннее спокойствие»</p>
					<p>Это невероятное озарение: приходит понимание, принятие, внутреннее спокойствие, доверие к жизни, понимание того, куда идти и желание пробовать преодолевать все препятствия и страхи! 
Вы несете в себе невероятный потеницал, который слышится в каждом слове!</p>
					<p class="autor">Наталья Поддубная | Донецк, Украина</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>




	
			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/elena-makarova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Благодарю за крайне полезные занятия!»</p>
					<p>Благодарю за крайне полезные занятия! Чувствую, как жизнь меняется прямо на глазах!</p>
					<p class="autor">Елена Макарова | Санкт-Петербург, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>



	

			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/natalya-terehova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Спасибо за свет, свет знаний!»</p>
					<p>Спасибо за свет, свет знаний! Страх превращается в любовь!</p>
					<p class="autor">Наталья Терехова | Смоленск, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>



	


			<div class="comment">
				<div class="no_image"></div>
				<div class="comment_text">
					<p>Столько полезных рекомендаций! Если внедрить хотя бы половину, страхи разбегутся сами!</p>
					<p class="autor">Лариса Александрова | Одесса, Украина</p>
				</div>
			</div>
		</div>
		
		<div style="clear:both"></div>








		<div id="bym-reg-bot">

			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>


			<div style="clear:both"></div>

		</div>






		<div id="bym-footer">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/aboutfoto.png">
			<div class="bym-about">
			   <div class="bym-about_name">Елизавета Бабанова</div>
			   <div class="bym-about_autor">автор семинара</div>
			   <div class="bym-about_desc">
					<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
				   <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
				   <p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
				   <p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
				  <p>Член Международной Ассоциации Непрерывного Образования.</p>
			   </div>
			</div>
			<div class="bym-vk">
			<iframe id="vkwidget1" height="350" frameborder="0" width="200" scrolling="no" style="overflow: hidden; height: 350px;" src="//vk.com/widget_community.php?app=0&width=200px&gid=44023512&mode=0&height=350&url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&13b1da20c61" name="fXD32f6b"></iframe>
			</div>
		</div>


		<div class="bym-footer_info">
			<div class="bym-copy">ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены</div>
			<nav class="bym-bottom_menu">
			<a href="/privacy-policy.html">Политика конфиденциальности</a> | <a href="/terms.html">Правила пользования</a> | <a href="/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div class="bym-site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></div>
		</div>


	</div>
</body>