<?php
require('./wp-blog-header.php');
require_once('header.php');
require_once('functions.php');
?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
 bcn_display();
}
?>
</div>

<div class="singl">

<div id="wrap" class="container singl">

<div id="content" class="eleven columns">	
     
					<?php if (have_posts()) : ?>	
						<?php while (have_posts()) : the_post(); ?>
						<div class="page_post" <?php post_class() ?> id="post-<?php the_ID(); ?>">
							<h1 class="title avant_book">
              
<?php 

        the_title();
?> 
  
              </h1>   
						
          		
							<div class="postdate avant_book">  
              <?php 
    if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )  
        echo ""; 
    else
        
?>
              
              <?php if (current_user_can('edit_post', $post->ID)) { ?> <?php edit_post_link('Edit', '', ''); } ?>
              
              </div>
			
			
			
							<div class="entry">
                                <?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(300,225), array("class" => "alignleft post_thumbnail")); } ?>
								<?php the_content('Читать далее &raquo;'); ?>
								<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							</div> 
							<div class="postmeta">
              <?php the_category(', ') ?>
              <!-- 
              <?php if(get_the_tags()) { ?><?php  the_tags('Метки: ', ', '); } ?>
              -->
              </div>
              
              
                           <?php 
    if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )  
        echo ""; 
    else
        require('social_button.php');
?> 
              
						    						 
						
						
						<!--
						<div class="navigation clearfix">
								<div class="alignleft"><?php previous_post_link('&laquo; %link') ?></div>
								<div class="alignright"><?php next_post_link('%link &raquo;') ?></div>
						</div>
					 
              
	          
							<?php edit_post_link('Редактировать эту запись','','.'); ?>
							--> 	
						</div><!--/post-<?php the_ID(); ?>-->  
				<?php comments_template(); ?>
				
				<?php endwhile; ?>
			
				<?php endif; ?>
				
</div><!-- #content-->

   
<?php 
 require('sidebar.php');
?>

 </div><!-- #wrapper2 --> 
</div>


<div id="footer">
<?php require('footer.php'); ?>
</div><!-- #footer --> 

</div><!-- #wrapper3 -->