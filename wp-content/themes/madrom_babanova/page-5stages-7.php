<?php
/*
Template Name: Страница 5stages/5stages-7-gosti
*/
?>
<!doctype html>
<html xmlns:og="http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>
    <meta name="title" content="Феноменальные гости программы Феномен Мастерства" />
    <title>Гости программы</title>
    <meta name="description" content="Свои мастер классы дадут феноменальные гости программы: Ирина Хакамада, Дмитрий Шаменков и Артем Агабеков!">
    <link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-7/img/share2.jpg" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/css/style.css" rel="stylesheet">
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/js/custom.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-7/img/share2.jpg">
    <meta property="og:title" content="Феноменальные гости программы &quot;Феномен Мастерства&quot;"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://elizavetababanova.com/gosti-fm"/>
    <meta property="og:description" content="Свои мастер классы дадут феноменальные гости программы: Ирина Хакамада, Дмитрий Шаменков и Артем Агабеков!">
    <script type="text/javascript">
        VK.init({apiId:  2461907, onlyWidgets: true});
    </script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
    <link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-7/img/share.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <section class="main">
        <header class="header">
            <div class="inner">
                <a class="logo" href="http://elizavetababanova.com/" target="_blank">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/b1.png" alt=".">
                </a>
                <nav class="menu">
                    <a target="_blank" href="http://vk.com/elizaveta.babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/b2.png" alt=".">
                    </a>
                    <a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/b3.png" alt=".">
                    </a>
                    <a target="_blank" href="http://twitter.com/liza_babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/b4.png" alt=".">
                    </a>
                    <a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/b5.png" alt=".">
                    </a>
                </nav>
            </div>
            <i class="border"></i>
        </header>
        <div class="inner">
            <nav class="menu2">
                <a class="i i1 threerow" href="http://elizavetababanova.com/5stages">5 уровней<br>развития<br>человека</a>
                <a class="i i7" href="http://elizavetababanova.com/research">Исследования</a>
                <a class="i i2 tworow" href="http://elizavetababanova.com/survey">Результаты<br>опроса</a>
                <a class="i i3 tworow" href="http://elizavetababanova.com/6-moih-vectorov">6 моих<br>векторов</a>
                <a class="i i4 tworow" href="http://elizavetababanova.com/phenomen-masterstva">Феномен<br>Мастерства</a>
                <a class="i i8 threerow" href="http://elizavetababanova.com/kak-realizovat-potentsial">Как<br>реализовать<br>потенциал</a>
                <a class="i i5 tworow" href="http://elizavetababanova.com/programma-treninga">Программа<br>тренинга</a>
                <a class="i i6 tworow active" href="">Гости<br>программы</a>
            </nav>
            <section class="block">
                <div class="i i1">
                    <p class="title"><span class="custom">Г</span>ости программы</p>
                    <section class="body">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/header-31.jpg" alt="." style="padding-top:20px;padding-bottom:10px;">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/header-32.jpg" alt=".">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/header-33.jpg" alt="." style="padding-top:10px;padding-bottom:10px;">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-7/img/header-34.jpg" alt="." style="padding-bottom:10px;">
                    <center>
                        <a href="http://elizavetababanova.com/10urokov/irina-hakamada.html" style="margin-left:20px;font-weight:bold;color:#376D9C;font-size:17px;">10 уроков от Ирины Хакамада</a>
                        <a href="http://elizavetababanova.com/10urokov/artemagabekov.html" style="margin-left:37px;font-weight:bold;color:#376D9C;font-size:17px;">10 уроков от Артема Агабекова</a>
                    </center>
                    <center>
                        <a href="http://ecommtools.com/buy/elizavetababanova/9">
                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/opl-but.jpg" style="margin-top: 50px; margin-right:35px;" width="40%">
                        </a>
                        <a href="http://elizavetababanova.com/form/index2.php">
                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6/img/za-but.jpg" style="margin-top: 50px;" width="40%">
                        </a>
                    </center>
                </div>
            </section>
            <footer class="footer">
                <i class="border6"></i>
                <p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
                <nav class="menu3">
                    <a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a>
                    |
                    <a href="http://elizavetababanova.com/terms.html">Правила пользования</a>
                    |
                    <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
                </nav>
            </footer>
        </div>
    </section>
</body>
</html>