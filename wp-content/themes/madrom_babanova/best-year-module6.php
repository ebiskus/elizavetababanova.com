<?php
/*
Template Name: Лучший год Модуль 6
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/module.css">

	<title>Как раскрыть Талант?</title>

	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/share06.jpg"> 
	<meta property="og:title" content="Как раскрыть Талант?"/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear-module6"/>
	<meta name="description" content="Годовая программа «Лучший год Вашей жизни». Чтобы ваш полет и ваша жизнь была полноценной, вы должны иметь два развитых полушария мозга.">

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div>
			<img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script>
	<!--/LiveInternet-->	
</head>

<body>
    <div id="bym-wrapper" align="center">
		<div id="bym-social">
			<div id="bym-logo">
			   <a href="/"><img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/vk_b.gif"></a>
				<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/facebook_b.gif"></a>
				<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tweeter_b.gif"></a>
				<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/youtube_b.gif"></a>
			</div>
		<div style="clear:both"></div>

		</div>
		<div id="bym-header" align="center">
			<div class="bym-all_year">
				<div class="bym-mounth"><p class="bym-digit">06</p><p class="bym-name">ИЮНЬ</p></div>
				<div class="bym-description"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/title6.png">
					<p class="bym-text">
					Чтобы ваш полет и ваша жизнь была полноценной, вы должны иметь два развитых полушария мозга. Тогда вы можете и генерировать творческие идеи, и реализовывать их с помощью логических действий. В этом модуле вы получите фантастическую методику по развитию творческого гения.
					</p>
					</p>
				</div>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/06.png">
			</div>
			<div style="clear:both"></div>

		</div>
		<div id="bym-content">
			<div id="bym-mon">
				<div id="bym-mon1-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m6mon1.jpg" ></div>
				<div id="bym-mon1">
				<ul>
					<li>ДНК Таланта
							<ul>
								<li>Что врожденное?</li>
								<li>Что развивается и за счет чего?</li>
							</ul>
					</li>
					<li>5 компонентов творческого интеллекта</li>
					<li>3 основных инструмента творческого интеллекта </li>
					<li>Шаги для подключения к своему творческому потенциалу 
							<ul>
								<li>Практические упражнения для самостоятельной работы</li>
							</ul>
					</li>
					<li>Что разрушает творческий интеллект и как себя обезопасить?</li>
				</ul>
				</div>
			  
				<div id="bym-mon2" style="padding-top:200px;" >
				<ul>
					
					<li>Как создать условия для максимальной творческой реализации?
							<ul>
								<li>Где брать свежие идеи?</li>
								<li>Практика “освобождения ума” </li>
								<li>Развитие “творческой дисциплины”</li>
							</ul>
					</li>

					<li>Единственный ключ к Большому Таланту</li>
					<li>30 микросоветов для макрорезультата в области раскрытия творческих способностей</li>

				</ul>
				</div>
				<div id="bym-mon2-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m6mon2.jpg"></div>
			</div>
		</div>
		<div style="clear:both"></div>
		<div id="bym-reg">
			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>
			<div style="clear:both"></div>

		</div>

		<div class="whatsay">
			<div><img id="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
			<div>Что говорят участники шестого модуля</div>
			<div style="padding-left:9px"><img id="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
		</div>
		<div style="clear:both"></div>


		
		<div id="bym-comment">	


		
		
					<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/ingrid-drujinina.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Я во многом преуспела!"</p>
					<p>Я очень рада перечитать свою Декларацию и увидеть, что я преуспела! Получилось выполнить 70%. Я все еще в процессе, это не конечная цель, а то, что нужно поддерживать постоянно и ни в коем случае больше не терять. Это прописывание, "заземление" приносит мне восхитительные плоды! Я еще даже не осознаю масштабов, как меняется мой способ мышления, становится более четким, структурированным и "удобным в использовании". Это просто потрясающе! И то, чего мне так хотелось.</p>
					<p class="autor">Ингрид Дружинина | Хабаровск, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>




	
			<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/evtihevich.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Спасибо за вдохновение!"</p>
					<p>Огромное спасибо, Елизавета и команда, за вдохновение! :) Сразу хочется действовать! И чувствуешь огромный прилив творческой энергии!</p>
					<p class="autor">Екатерина Евтихевич | Минск, Белоруссия</p>
				</div>
			</div>


		<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>


			<div style="clear:both"></div>
			
			<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/ermakova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Вдохновляющий и глубокий модуль"</p>
					<p>Модуль, как обычно, был очень вдохновляющим, глубоким и информативным. Обязательно его переслушаю и буду постепенно внедрять те знания, которыми Вы поделились.</p>
					<p class="autor">Екатерина Ермакова | Прага, Чехия</p>
				</div>
			</div>
			<div style="clear:both"></div>
<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/2014/05/Elena-Stepanova2.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Мощный вебинар!"</p>
					<p>Благодарю за такой мощный вебинар! Буду развивать в себе креатив!</p>
					<p class="autor">Елена Степанова | Москва, Россия</p>
				</div>
			</div>
			<div style="clear:both"></div>
<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="margin-top:20px;" class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/2014/07/unnamed-1.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Блестящий, невероятно вдохновляющий!"</p>
					<p>"Хочу сказать огромное спасибо за 6й модуль - блестящий, невероятно вдохновляющий, заряжающий, познавательный, расширяющий рамки мышления!!! Лиза в очередной раз покорила меня своим мастерством, мудростью, настойчивостью ментора (за это отдельная благодарность:)! и заражающим состоянием умиротворения и inner peace в душе - то, к чему я очень сильно стремлюсь!
To-do list по шагам этого модуля я для себя уже составила и начала прорабатывать."</p>
					<p class="autor">Марина Щербина | Киев, Украина</p>
				</div>
			</div>

<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

		

			<div style="clear:both"></div>
		
		<div id="bym-footer">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/aboutfoto.png">
			<div class="bym-about">
			   <div class="bym-about_name">Елизавета Бабанова</div>
			   <div class="bym-about_autor">автор семинара</div>
			   <div class="bym-about_desc">
					<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
				   <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
				   <p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
				   <p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
				  <p>Член Международной Ассоциации Непрерывного Образования.</p>
			   </div>
			</div>
			<div class="bym-vk">
			<iframe id="vkwidget1" height="350" frameborder="0" width="200" scrolling="no" style="overflow: hidden; height: 350px;" src="//vk.com/widget_community.php?app=0&width=200px&gid=44023512&mode=0&height=350&url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&13b1da20c61" name="fXD32f6b"></iframe>
			</div>
		</div>


		<div class="bym-footer_info">
			<div class="bym-copy">ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены</div>
			<nav class="bym-bottom_menu">
			<a href="/privacy-policy.html">Политика конфиденциальности</a> | <a href="/terms.html">Правила пользования</a> | <a href="/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div class="bym-site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></div>
		</div>


	</div>
</body>