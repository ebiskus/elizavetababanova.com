<?php
/*
Template Name: Страница 5stages-6-programm-35
*/
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>
    <title>Программа тренинга</title>
    <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/css/style.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/custom.js"></script>
    <meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/og_pm.png">
    <meta property="og:title" content="Программа тренинга"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://elizavetababanova.com/programma-treninga"/>
    <meta property="og:description" content="Это программа, вскрывающая чип-код Вашего внутреннего гения.В процессе прохождения программы Вы узнаете, как совершить грандиозный прорыв из &quot;хорошего&quot;, а, возможно, даже &quot;отличного&quot; специалиста своего дела на уровень мастера.">
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">VK.init({apiId:  2461907, onlyWidgets: true});</script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/scrollto.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
    <link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-6-35/img/.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <section class="main">
	    <header class="header">
		    <div class="inner">
			    <a class="logo" href="http://elizavetababanova.com/" target="_blank">
				    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/b1.png" alt=".">
			    </a>
			    <nav class="menu">
                    <a target="_blank" href="http://vk.com/elizaveta.babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/b2.png" alt=".">
                    </a>
                    <a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/b3.png" alt=".">
                    </a>
                    <a target="_blank" href="http://twitter.com/liza_babanova">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/b4.png" alt=".">
                    </a>
                    <a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/b5.png" alt=".">
                    </a>
		        </nav>
		    </div>
		    <i class="border"></i>
        </header>
	    <div class="inner">
		    <section class="block">
			    <div class="i i1">
				    <p class="title"><span class="custom">П</span>рограмма тренинга</p>
				    <section class="body">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/header-pm_cr1.jpg" alt="." usemap="#Navigation">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/header-pm_cr2.jpg" alt=".">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/header-pm_cr3.jpg" alt=".">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/header-pm-cr4.jpg" alt=".">
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/header-pm-cr5.png" alt=".">
                        <map name="Navigation">
                            <area shape="rect" coords="0,1530,440,1620" href="#app_1" alt="Информация">
                            <area shape="rect" coords="0,1620,440,1710" href="#app_2" alt="Информация">
                            <area shape="rect" coords="0,1710,440,1800" href="#app_3" alt="Информация">
                            <area shape="rect" coords="0,1800,440,1890" href="#app_4" alt="Информация">
                            <area shape="rect" coords="440,1530,880,1620" href="#app_5" alt="Информация">
                            <area shape="rect" coords="440,1620,880,1710" href="#app_6" alt="Информация">
                            <area shape="rect" coords="440,1710,880,1800" href="#app_7" alt="Информация">
                            <area shape="rect" coords="440,1800,880,1890" href="#app_8" alt="Информация">
                        </map>
				    </section>
                    <div class="text2">
                        <center>
                            <table cellpadding="20">
                                <tr>
                                    <td>
                                        <a target="_blank" href="http://elizavetababanova.ecommtools.com/buy/23">
                                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/opl-but.jpg" width="300">
                                        </a>
                                    </td>
                                    <td>
                                        <a target="_blank" href="http://elizavetababanova.com/form11/">
                                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/za-but.jpg" width="300">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </center>
                        <br />

                    <div class="citata">
                        <center><p class="cblue avangardec text_tr fs36 otz_tit">Отзывы выпускников</p></center>
                        <div class="otz_video">
                            <table cellpadding="10">
                                <tr>
                                    <td width="400"><iframe width="400" height="225" src="http://www.youtube.com/embed/yZF1ldYnqTI" frameborder="0" allowfullscreen></iframe></td>
                                    <td width="400"><iframe width="400" height="225" src="http://www.youtube.com/embed/9oZ4UMWMA3o" frameborder="0" allowfullscreen></iframe></td>
                                </tr>
                                <tr>
                                    <td width="400"><iframe width="400" height="225" src="http://www.youtube.com/embed/GgQuVWnylW0" frameborder="0" allowfullscreen></iframe></td>
                                    <td width="400"><iframe width="400" height="225" src="http://www.youtube.com/embed/rlk-PR4BoXM" frameborder="0" allowfullscreen></iframe></td>
                                </tr>
                                <tr>
                                    <td width="400"><iframe width="400" height="225" src="http://www.youtube.com/embed/gMwB6xrpbck" frameborder="0" allowfullscreen></iframe></td>
                                    <td width="400"><iframe width="400" height="225" src="http://www.youtube.com/embed/1yLYFeqB2HM" frameborder="0" allowfullscreen></iframe></td>
                                </tr>
                            </table>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/nataliya-terehova.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24">Наталья Терехова<p>
                                <p class="fs16">Смоленск, Россия</p>
                                <p>"Благодаря работе на тренинге я еще больше стала ЦЕНИТЬ: время, знания, силы, людей, - все то, что я выбираю в жизни. Я стала получать ещё больше удовольствия от работы, благодаря глубокому погружению. Я сильнее испытываю чувство благодарности по отношению к тем людям, которые отдают мне часть своего времени, сил, внимания, я стала бережнее относиться ко всем эти дарам. И в том случае, когда я принимаю их, и тогда, когда отдаю сама.</p>
                                <p>Самыми ценными оказались для меня две идеи, которые я сразу же внедрила в свою жизнь и продолжаю с ними жить вот уже несколько дней. Они помогают мне двигаться, сохраняя равновесие. В левой руке я держу слова  Ирины Хакамада: "Моя энергия ценна, и просто так я ее никому не отдам!", в правой - вопрос Елизаветы Бабановой: "Как то, что я сейчас делаю, приближает меня к моей цели?". Это дает мне еще большее пробуждение в настоящем моменте, еще более глубокое переживание ответственности за него, за свои действия, мысли, слова.</p>
                                <p>Упражнения воодушевляли, приносили более глубокое осознание того, кем я являюсь сейчас и как это отличается от того, кем я хочу быть. Те ответы, которые давали другие члены группы, позволяли открыть для себя новый взгляд на ситуацию, делая богаче и объемнее восприятие мира. Я пока не встречала еще такой силы воли в намерении отдавать самое лучшее людям, как я чувствовала это от Елизаветы Бабановой, такой упорядоченности и богатства очень конкретных и ценных знаний. Все самое лучшее - это выбор Елизаветы, и я чувствую, что только лучшее пришло ко мне от нее и от ее Команды за эти дни проекта "Феномен Мастерства". Мастерство направлялось ко мне через ПРОВОДНИКОВ, и ВСЕ они были МАСТЕРАМИ. Я получила значительно больше, чем ожидала. После прохождения курса я настолько наполнена, что не хочется пока ничего брать извне. Но есть страстное желание всё, что я узнала, пропустить теперь через мои действия и сделать жизнью."</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/victor-serikov.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24"><a href="http://vk.com/id164886458" target="_blank">Виктор Сериков</a></p>
                                <p class="fs16">Могилев, Белоруссия</p>
                                <p>"Я увидел свои возможности. Получил много мудрости. Это тренировка на высоком уровне!</p>
                                <p>Главное отличие Елизаветы - это фокус ее внимания на всем, что она делает, глубокое погружение в каждого участника и в каждый вопрос. Все ситуации она пропускает через себя и потом дает практические рекомендации. Я желаю ей создать свой Мировой Бренд!!! И демонстрировать дальше мировой уровень! Реализовать самые амбициозные цели! И пусть каждый участник Вашей команды сумеет найти себя и раскрыться по полной - пусть у Вас будут только Мастера своего дела!!!"</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/svetlana-strelnikova.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24">Светлана Стрельникова</p>
                                <p class="fs16">Christchurch, New Zealand</p>
                                <p>"Программа «Феномен Мастерства» удивительным образом объединяет теорию и практику. Елизавета смогла разработать практическую методику по раскрытию кода гения на основе научных исследований и достижений. Кроме того, упражнения и техники вебинаров позволяют сразу проделать глубокую работу. Елизавета вносит необходимую и своевременную корректировку сразу по ходу вебинара.</p>
                                <p>Именно в рамках этой программы я впервые в жизни сформулировала свою глобальную жизненную цель, основанную на моих ценностях. Эта цель оказалась несколько неожиданной для меня самой. Но даже по прошествии нескольких месяцев она вдохновляет меня, и я сделала то, о чем мечтала и думала многие годы. Я читаю свою цель, как любовное письмо, и черпаю энергию и фокус для ежедневной работы.</p>
                                <p>«Феномен Мастерства» научил меня стараться делать все по максимуму, даже в мелочах. Это особенная и удивительная программа! Спасибо Елизавете и всей команде за столь глубокий и серьезный проект."</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/ekaterina-borisova.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24"><a href="http://vk.com/id4159075" target="_blank">Екатерина Борисова</a></p>
                                <p class="fs16">Ростов-на-Дону, Россия</p>
                                <p>"Изменения только начинаются, но их перспективы меня очень вдохновляют! Волшебным семенам, которые мы вместе с Елизаветой и Вашей командой ""посеяли"" на этом курсе, нужно время, чтобы прорасти! ))) Вы удивительно чутко чувствуете, что нужно аудитории, и даете ответы на важные вопросы! Курс можно сравнить с полетом в Космос - очень много впечатляющих открытий, подъем на невероятную высоту с космической скоростью!</p>
                                <p>Благодаря упражнениям узнаешь о себе много нового: глубоко погружаясь в себя и честно отвечая на вопросы, начинаешь видеть себя со стороны, и открываются те сферы, над которыми нужно поработать! Мне нравятся идеи, концепции курса, подход к подаче материала, очень комфортная атмосфера на занятиях. :)</p>
                                <p>Сама программа - явное подтверждение системы мастерства, о которой говорится в курсе! Я очень довольна, что прошла этот курс! Все встало на свои места, сомнения и метания ушли, время прорыва пришло!!! Спасибо за знакомство с потрясающими людьми, с мастерами, драйверами и вдохновителями.)))"</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/yuliya-grigorieva.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24">Юлия Григорьева</p>
                                <p class="fs16">Россия</p>
                                <p>"Перед началом программы у меня были только "подозрения" о том, чего я хочу. После программы у меня определились четкие цели, появились конкретные планы и проекты, которые принесут в ближайшем будущем хорошую прибыль. Я знаю, что мне нужно делать. Огромное желание, мотивация, страсть. Каждый день жизни имеет ценность, нельзя откладывать мечты на потом, "долгий ящик" сжечь нужно. ))))  Я живу Сегодня, Здесь и Сейчас. Я покорю свой Эверест, обязательно.</p>
                                <p>Елизавета - тренер от Бога. Это ее призвание. 99% информации, которую она дает, - в "десятку". Просто квинтэссенция знаний в освещаемой области. Потрясающая работа с группой: наставляет, вдохновляет, корректирует. Она искренне заинтересована в том, чтобы мы усвоили знания, которые она нам дает. Совершенно особая атмосфера любви и тепла на тренинге.  И конечно, мне близки духовные ценности Елизаветы, ее вектора развития и отношение к жизни и бизнесу - все это мне абсолютно резонирует. Если сказать коротко: в лице Елизаветы - смена эпох. Это чудо, сотворенное ее руками, ее сердцем, ее душой.</p>
                                <p>Я в полном восторге, точнее сказать, пребываю в эйфории! В этом тренинге совместилось всё: знания, инструменты, супер мастер-классы от Гостей тренинга, все самые светлые чувства, забота и поддержка. Я вытянула счастливый билет! )) Мастер-классы обогатили мой опыт и знания. Это очень здорово, когда человек, достигший определенных результатов, делится своим опытом."</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/valentina-rozhkova.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24"><a href="https://www.facebook.com/vasuvalya" target="_blank">Валентина Рожкова</a></p>
                                <p class="fs16">Медногорский, Карачаево-Черкесия, Россия</p>
                                <p>"До прохождения программы не было такой уверенности в том, что  изменения могут быть столь радикальными. После программы я в этом абсолютно уверена. Полученную пользу трудно переоценить. Меняется отношение к себе, к работе, к отношениям, к времени, к жизни.</p>
                                <p>Отличие этой программы для меня  -  в душевности, простоте, глубоких и разносторонних знаниях. Елизавета знает все о теме, которой проводит тренинг. Она - мастер своего дела. У неё высокая планка. Она очень требовательна к себе. Заинтересована в высоких результатах у слушателей. Результат превзошёл самые смелые мои ожидания.</p>
                                <p>Я вдвое старше Елизаветы, но я могу обращаться к ней только на "Вы", уровень её самореализации настолько высок, что это вызывает почтительное уважение. Команда тоже на высоте. Слаженная, высококвалифицированная, доброжелательная, готовая с радостью прийти на помощь. Могу пожелать лишь дальнейших успехов. Мечтаю, чтобы как можно больше людей с огромной пользой для себя посетило этот тренинг. Изменимся мы... Мир изменится к лучшему!"</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/rita-skriptsova.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24"><a href="http://vk.com/id3161030" target="_blank">Рита Скрипцова</a></p>
                                <p class="fs16">Санкт-Петербург, Россия</p>
                                <p>"Я ещё больше утвердилась в своих ценностях. Я научилась понимать, чего я хочу, научилась видеть будущее, не бояться мечтать и знать, что все по силам. Главное - не останавливаться. Елизавета не сравнивает одних людей с другими. Она ищет зерно в каждом и искренне помогает человеку увидеть свои слабые места и сильные стороны. Она НЕ говорит что мы, тут собравшиеся, те 10% от всего населения Земли, лучшая часть человечества. Она не сравнивает людей, а помогает человеку сравнить себя с тем, кем он может стать, если захочет. Этот курс - бесценный материал, который можно использовать по прошествии времени."</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/nikita-chucklinov-1.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24">Никита Чуклинов</p>
                                <p class="fs16">Санкт-Петербург, Россия</p>
                                <p>"Оформлена глобальная цель, которая раньше была чем-то эфимерным. Отличие этого курса от остальных в том, что он основан на принципах целостности личности, честности и любви, которые я искренне разделяю. Я пошел на этот курс потому, что мне близки ценности, пропогандируемые Елизаветой. Поэтому я был уверен, что это станет выгодной инвестицией в себя. Если Вы действительно разделяете эти ценности, то безусловно получите что-то полезное для себя, и эти знания не встретят никаких барьеров - как это случилось со мной."</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/oxana-demiyanenko.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24">Оксана Демьяненко</p>
                                <p class="fs16">Новосибирск, Россия</p>
                                <p>"Все встало на свои места, стало понятно, что и как делать, как ставить цели. Появился огромный прирост энергии, которую я использую для глубокого погружения и рождения новых идей. "Феномен Мастерства" - чрезвычайно глубокий курс, реальный фундамент для прорыва во всех сферах жизни. Елизавета - потрясающе умная, глубокая и интеллигентная. Четко ведет участников к цели, не дает отвлекаться на постороннее, дает самые ценные выжимки информации. Инвестиция уже окупилась за счет улучшения самочувствия, прилива энергии и появления новых знакомых, которых я хотела бы видеть в близком и среднем круге.</p>
                                <p>Елизавета и Команда! Я вам очень благодарна за колоссальную работу!!! Это невероятно, что вы так виртуозно и с любовью справляетесь со своими задачами. Есть пример для подражания. Желаю вам Успехов в достижении своих целей, желаю также как сейчас помогать людям. Спасибо! Спасибо! Спасибо!!!"</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="left">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/otzivi_fm/nadezhda-deroz-1.jpg">
                            </div>
                            <div class="content">
                                <p class="fs24">Надежда Дероз</p>
                                <p class="fs16">Луганск, Украина</p>
                                <p>"Быть мастером - это большая честь, но это еще и большая удача в жизни, и я безмерно счастлива и благодарна своей судьбе за то, что мне выпала такая потрясающая возможность пройти эту программу по развитию собственного мастерства! Очень ценю это! Для меня процесс развития - это жизненная потребность и необходимость, даже не мыслю себя без познания. Но если раньше тот процесс был отчасти "стихийным" просто потому, что хотелось что-то познавать, то теперь у меня есть четкий вектор, и это дает мне дополнительную энергию!</p>
                                <p>Теперь все, что я уже планировала ранее относительно своей профессии, приобрело для меня особый смысл и ценность! Сейчас я понимаю, что мне не нужен мировой уровень или слава, я просто хочу и буду делать свое любимое дело с душой и всегда на 100%!</p>
                                <p>Самая главная польза от упражнений - это бескомпромиссная честность с собой, возможность заглянуть настолько глубоко в себя, чтобы увидеть там самую суть и найти все ресурсы для движения. Каждое такое погружение и открытие дало мне больше веры в себя, в свои силы, больше принятия себя. Для меня самым главным отличием стала аутентичность стиля Елизаветы, ее верность себе, своим идеям и принципам, высокие стандарты в совокупности с огромной отдачей и верой в других людей. Я получила мощнейший концентрат практически применимых знаний, для меня это настоящий клад, который я буду приумножать и развивать!</p>
                                <p>Елизавета, Вы всем нам уже стали очень близкой, я благодарю Вас и Вашу команду за первоклассно проведенный тренинг! Все было, как я уже написала выше, на высшем уровне Мастерства! Огромное спасибо!  Желаю Вам вдохновить еще больше людей ищущих, стремящихся к развитию во всех сферах жизни! Удачи и огромного счастья!"</p>
                            </div>
                        </div>
                        <div class="otz">
                            <div class="content" style="margin-left:159px">
                                <p class="fs24">Ирина Бобровских</p>
                                <p class="fs16">Ярославль, Россия</p>
                                <p>"Укрепились отношения в семье - я нашла оптимальный баланс между семьёй и работой. Увеличилось количество клиентов - заработала в 2 раза больше, чем в предыдущем месяце! Невероятно! Помогли упражнения: от того, что прописываешь самостоятельно, а затем начинаешь применять в работе и жизни, получаешь результат (при качественном выполнении), и это дает массу позитива и вдохновения. Очень профессиональный и глубокий тренинг, охватывающий все сферы жизни. Побольше бы таких тренингов!"</p>
                            </div>
                        </div>
                        <div class="about" style="min-height:600px;padding-bottom:0px;">
                            <div style="float:left; width:600px;">
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/elizaveta.png">
                                <h3 class="text_tr ptsans sin fs30">Елизавета Бабанова</h3>
                                <p>В 15 лет уехала из России, став победителем конкурса "Акт в поддержку свободы" на бесплатное годовое обучение в США.</p>
                                <p>Окончила 2 ВУЗа в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
                                <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
                                <p>С 18 лет занимается личностным развитием. Изучила и синтезировала самые продвинутые российские и западные методики и технологии достижения успеха.</p>
                                <p>Основатель и совладелец двух успешных интернет-проектов в области саморазвития и бизнес-обучения.</p>
                                <p>В процессе профессиональной деятельности и глубокого изучения вопросов саморазвития разработала для себя "Систему сферического развития", обеспечивающую систематизированный подход к достижению высоких результатов.</p>
                                <p>После внесения своих данных в форму вы также получите в подарок новую, улучшенную версию Таблицы Сферического Развития, самого эффективного инструмента для быстрого и рационального внедрения Системы Сферического Развития в свою жизнь.</p>
                            </div>

                            <!-- VK Widget -->
                            <div style="height: 400px; float:right; width: 200px; background: none repeat scroll 0% 0% transparent; margin-top:90px;" id="vk_groups"></div>
                            <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "200", height: "400"}, 44023512);
                            </script>
                        </div>
                        <center>
			                <table cellpadding="20">
			                    <tr>
			                        <td>
			                            <a target="_blank" href="http://elizavetababanova.ecommtools.com/buy/23">
			                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/opl-but.jpg" width="300">
			                            </a>
			                        </td>
                                    <td>
                                        <a target="_blank" href="http://elizavetababanova.com/form11/">
                                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/za-but.jpg" width="300">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </center>
                        <br />
                        <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/5stages/5stages-6-35/img/garant.jpg" alt="." style="margin-top:-20px;">
                        <center><hr style="margin-top:15px;color:gray;" width="80%"></center>
		            </div>
		            <div class="about2" style="margin:10px auto;">
		                <p class="avangardec text_tr fs36yellow question" style="text-align:center;">Есть вопросы?</p>
		                <center>
		                    <p>Остались вопросы по программе <span style="font-weight:bold;">&laquo;Феномен Мастерства&raquo;</span>? Мы будем рады ответить на них.</p>
		                    <p>Пишите на почту <a href="mailto:elizavetababanova@gmail.com">elizavetababanova@gmail.com</a> или в скайп elizavetamiami</p>
                        <center>
                    </div>
                </div>
            </section>
            <div style="position: absolute;top:2047px;left:10px;"><a name="app_1"></div>
            <div style="position: absolute;top:2677px;left:10px;"><a name="app_2"></div>
            <div style="position: absolute;top:3327px;left:10px;"><a name="app_3"></div>
            <div style="position: absolute;top:3881px;left:10px;"><a name="app_4"></div>
            <div style="position: absolute;top:4551px;left:10px;"><a name="app_5"></div>
            <div style="position: absolute;top:5227px;left:10px;"><a name="app_6"></div>
            <div style="position: absolute;top:5954px;left:10px;"><a name="app_7"></div>
            <div style="position: absolute;top:6757px;left:10px;"><a name="app_8"></div>

            <footer class="footer">
                <i class="border6"></i>
                <p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
                <nav class="menu3">
                    <a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
                    <a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
                    <a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
                </nav>
            </footer>
	    </div>
    </section>
    <script>
        function doPopup(name){
            if ($(name).is(':visible')){
                $(name).hide('fast');
            }
            else {
                $("#first").hide();
                $(name).slideToggle(800);
            }
        }
    </script>
</body>
</html>