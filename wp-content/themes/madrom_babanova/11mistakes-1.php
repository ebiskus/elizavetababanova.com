﻿<?php
/*
Template Name: Страница 11mistakes
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>11 грандиозных ошибок, совершаемых при устройстве на работу</title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
<link href="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/css/style.css" rel="stylesheet">
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/js/jquery.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/js/plugins.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/js/custom.js"></script>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
<meta property="og:title" content="11 грандиозных ошибок, совершаемых при устройстве на работу"/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="http://elizavetababanova.com/11-mistakes/" />
<meta property="fb:admins" content="100003273128680"/> 
<meta property="og:description" content="Проверьте, какие из этих ошибок совершаете вы. Скорее всего, вы найдете причину своих неудач в поиске работы и сможете стать намного более конкурентноспособным на рынке труда." />
<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/11mistakes/11mistakes-1/img/b1.jpg" />
<script type="text/javascript">
 VK.init({apiId:  2461907, onlyWidgets: true});
</script>
<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
<link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/11mistakes/11mistakes-1/img/photo.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="main">
	<header class="header">
		<div class="inner">
			<a class="logo" href="http://elizavetababanova.com/" target="_blank">
				<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/img/b1.png" alt=".">
			</a>
			<nav class="menu">
				<a target="_blank" href="http://vk.com/elizaveta.babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/img/b2.png" alt=".">
				</a>
				<a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/img/b3.png" alt=".">
				</a>
				<a target="_blank" href="http://twitter.com/liza_babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/img/b4.png" alt=".">
				</a>
				<a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/img/b5.png" alt=".">
				</a>
			</nav>
		</div>
		<i class="border"></i>
	</header>
	<div class="inner">
		<nav class="menu2">
			<a class="i i1 tworow active" href="http://elizavetababanova.com/11-mistakes/">11 грандиозных ошибок, совершаемых<br> при устройстве на работу</a>
			<a class="i i2" href="http://elizavetababanova.com/7shagov-work-dream">7 шагов к работе вашей мечты</a>
			<a class="i i3" href="http://elizavetababanova.com/dream-job">Программа курса</a>
			</nav>
		<section class="block">
			<div class="i i1">
				<p class="title"><span class="custom">11</span> грандиозных ошибок, совершаемых при устройстве на работу</p>
				
			</div>
		</section>
<div class="text" style="margin-top:30px;">
<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-1/img/b1.jpg" style="float:left;margin:0px 20px 20px 0px;width:250px;"><p>На самом популярном <a target="_blank" href="http://hh.ru/">ресурсе</a> по поиску работы в России на одну вакансию приходится 36 резюме. И это только те люди , которые ищут работу самым неэффективным способом. </p>

<p>В своем поиске, ищете ли вы работу впервые или хотите сменить существующую, вы конкурируете с людьми, которые не допускают ошибок, представленных в этом отчете. В итоге именно они получают самые выгодные предложения и стремительный карьерный рост.  </p>

<p>Проверьте, какие из этих ошибок совершаете вы. Скорее всего, вы найдете причину своих неудач в поиске работы и сможете стать намного более конкурентноспособным на рынке труда.</p>


<p>Вы можете скачать этот материал в формате PDF <a  target="_blank" href="http://elizavetababanova.com/files/11_oshibok_pri_ustroystve_na_rabotu.pdf">по этой ссылке</a> или ознакомиться с ним ниже на этой странице.</p>

<p>&nbsp;</p>
</div>




<div>

<table style="margin-top:10px;margin-bottom:15px;cellspacing:5px;cellpadding:10px;">
	<tr><td colspan="2"><p class="tableheader">ОШИБКИ ВЫБОРА ИНДУСТРИИ</p></td></tr>
		<tr><td width="300" class="row1"><p>Ошибка #1:<br>
Вы не понимаете, какую работу вы на самом деле хотите выполнять</td>
			<td><ul>
			<li>Вы не проделали глубокого внутреннего исследования</li>
			<li>Не понимаете, из чего будут состоять ваши 40 часов жизни каждую неделю</li>
			<li>Вы не изучили ваш рынок и не сделали анализ перспективы карьерного роста в выбранной профессии</li>
			<li>Место привлекло вас только благодаря зарплате, бренду компании или названию должности</li>
			</ul></td>
		</tr>
		<tr><td width="300" class="row1"><p>Ошибка #2:<br>
Вы плохо понимаете индустрию</td>
			<td><ul>
			<li>Вы не знаете главных проблем индустрии на сегодняшний момент</li>
			<li>Вы не изучили своего будущего потенциального клиента</li>
			<li>Вы не оценили потенциал развития вашей карьеры именно в этой индустрии</li>
			<li>Вы не просчитали перспективы развития самой индустрии</li>
			</ul></td>
		</tr>
		<tr><td colspan="2"><p class="tableheader">Ошибки при поиске вакансии</p></td></tr>
		<tr><td width="300" class="row1"><p>Ошибка #3:<br>
Вы не относитесь к поиску работы как к работе.</td>
			<td><ul>
			<li>Вы инвестируете в поиск работы лишь 10-15 минут в день, отправив несколько резюме в HR-фирмы или разместив их на сайтах поиска работы в онлайн.</li>
			</ul></td>
		</tr>
		<tr><td width="300" class="row1"><p>Ошибка #4:<br>
Вы подаете заявку на одну или две позиции.</td>
			<td><ul>
			<li>Вы недооцениваете конкуренцию на рынке труда и предполагаете, что, подав заявку на пару вакансий, вы сможете благоприятно трудоустроиться.</li>
			</ul></td>
		</tr><tr><td width="300" class="row1"><p>Ошибка #5:<br>
Ваши компетенции не соответствуют желаемой работе.</td>
			<td><ul>
			<li>Если из 10 людей, которые подают на эту работу, будет хотя бы один, чьи компетенции подпадают под нее идеально, именно он и получит работу. Будете ли вы таким человеком?</li>
			</ul></td>
		</tr>
		<tr><td width="300" class="row1"><p>Ошибка #6:<br>
Вы не заточили свое резюме под работу (или еще хуже, обманули в каких-либо деталях)</td>
			<td><ul>
			<li>Ваше резюме содержит шаблонные фразы, скопированные с других резюме.</li>
			<li>Оно не содержит пояснений, почему вы претендуете на конкретную вакансию.</li>
			</ul></td>
		</tr>
		

				
		<tr><td width="300" class="row1"><p>Ошибка #7:<br>
Вы не выполнили всех условий подачи заявки</td>
			<td><ul>
			<li>Вы не изучили глубоко все условия и мгновенно предстали перед работодателем “халтурщиком”</li>
			</ul></td>
		</tr>
		
		<tr><td width="300" class="row1"><p>Ошибка #8:<br>
Вы плохо подготовились к интервью</td>
			<td><ul>
			<li>Вы “плаваете” в ключевых терминах индустрии и ниши, в которой работает ваша компания</li>
			<li>Вы не знаете, что конкретно производит и каким образом распространяет свои продукты или услуги компания</li>
			<li>Вы не изучили, в чем их преимущество над другими фирмами</li>
			<li>Вы не поняли, в чем преимущество конкурентов, и в чем они проигрывают</li>
			<li>Вы не узнали требования к кандидату</li>
			<li>Вы не понимаете задачи, которые вас берут решать в фирме.</li>
			<li>Вы не проявили достаточно энтузиазма к работе на заключительной стадии интервью и не спросили о вашем следующем шаге </li>
			</ul></td>
		</tr>
		<tr><td colspan="2"><p class="tableheader">Ошибки во время прохождения интервью</p></td></tr>
		<tr><td width="300" class="row1"><p>Ошибка #9:<br>
Вы были неадекватно одеты</td>
			<td><ul>
			<li>“Неадекватно” – означает, что ваш образ не соответствует образу профессионала, занимающую вакансию, на которую вы претендуете.</li>
			<li>Если ваша одежда неаккуратна, если вы помяты, или если вы женщина и ваша юбка слишком коротка, именно это, а также еще множество подобных “мелочей” может стать причиной отказа в работе.</li>
			</ul></td>
		</tr>
		
		<tr><td width="300" class="row1"><p>Ошибка #10:<br>
Вы нарушили одну из “норм” прохождения интервью:</td>
			<td><ul>
			<li>Опоздали</li>
			<li>Были либо не уверены в себе, либо сверх уверены</li>
			<li>Говорили слишком мало, либо слишком много</li>
			<li>Не задали ни одного вопроса касательно вакансии</li>
			<li>Инициировали вопрос о компенсации</li>
			<li>Вы плохо попрощались</li>
			</ul></td>
		</tr>
		




		<tr><td width="300" class="row1"><p>Ошибка #11:<br>
Вы не знаете, сколько вы стоите.</td>
			<td><ul>
			<li>Если вы не проделали предварительную работу и не выяснили, сколько стоят специалисты вашего уровня, это покажет, что вы несерьезно относитесь к поиску работы, а значит, и к самой работе.</li>
			</ul></td>
		</tr>
</table>
</div>

<div class="text">
<p>Наивно думать, что наличие диплома, пусть даже и очень престижного университета, является гарантией устройства на интересную высокооплачиваемую работу. </p>

<p>Существует множество подводных камней, каждый из которых может стать препятствием на пути к работе вашей мечты.</p>

<p>Если вы позволите своему работодателю сомневаться, взять или не взять вас на работу, скорее всего, он вас не возьмет (если это, конечно, серьезный и уважающий себя работодатель). Вы должны произвести на него такое впечатление, чтобы он, глядя на вас, сказал: “WOW! Это тот человек, которого я так давно искал”.</p>

<p>Существует определенная стратегия, следуя которой, вы с большой вероятностью получите то место , которое подходит именно вам, и в котором вы сможете реализовать все ваши таланты и способности, имея при этом хорошее финансовое вознаграждение и великолепные условие труда.</p>

<p>17 апреля я провела бесплатный онлайн-семинар “7 шагов к работе вашей мечты”. На семинаре я рассказала о том, как избежать этих 11 главных ошибок и еще массу побочных, которые совершают люди при устройстве на работу. А главное, я дала пошаговый план, который позволит вам получить работу вашей мечты.</p>

<p>Вы можете, как и большинство людей, получать свой опыт и набивать себе шишки, проходя десятки интервью в поисках идеального места работы. А можете воспользоваться опытом человека, который точно знает, что нужно делать, чтобы работа мечты стала реальностью.</p>

<p>Каждый день мы делаем выбор: становиться лучшим, быть посредственным середнячком, или вообще, болтаться в хвосте рынка труда (когда вы не нужны никому).</p>

<p>Если кто-то из ваших непосредственных конкурентов посмотрит этот вебинар, а вы его пропустите, как вы думаете, кто из вас будет впереди?</p>

<p>До встречи!</p>
<p>P.S. А какая у вас самая большая проблема в поиске работы? Ответьте на этот вопрос в комментариях к этому посту.</p>


</div>

<div class="text2">
		
		
		<br /><center>         <?php 
    if( is_page('kontakty') || is_page('about') || is_page('konsultacii') || is_page('archives') )  
        echo ""; 
    else
        require('social_button.php');
?> </center>
		</div>
<i class="border3" style="width: 800px;margin: 0 auto;"></i>
		<section class="block8">
			<div class="left">
				<div id="vk_comments"></div>
				<script type="text/javascript">
				VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
				</script>
			</div>
			<div class="right">
				<div class="fb-comments" data-href="http://elizavetababanova.com/11-mistakes" data-width="400" data-num-posts="5"></div>
			</div>
		</section>
		<section class="block8">
		<?php comments_template('/comments2.php'); ?>
		</section>
		<footer class="footer">
			<i class="border6"></i>
			<p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
		</footer>
	</div>
</section>

</body>
</html>