<?php
/*
 Template Name: Страница трансляции вебинара Изобилие
 */
/*?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<link rel="stylesheet" type="text/css" href="style2.css">
	<meta property="fb:admins" content="100003273128680"/>
		<meta property="vk:app_id" content="3398000" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
		<link href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-webinar/style/style2.css" rel="stylesheet">
	<title>Бесплатный онлайн-тренинг. 7 шагов к изобилию</title>
	
	 	<meta property="og:title" content="Бесплатный онлайн-тренинг 7 шагов к изобилию"/>
		<meta name="description" content="9 сентября, в 19:00 по Москве. От психологии процветания к конкретной практике достижения финансовых целей.">
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/7izobilie-training"/>

   <meta property="og:image" content="http://elizavetababanova.com/7izobilie/img/ss.jpg">
  
	
	
	<script src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/js/jquery-1.7.1.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/js/jquery.fancybox.css" type="text/css" media="screen">
<script type="text/javascript" src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/js/jquery.fancybox.pack.js"></script> 
<script src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/js/waypoints.min.js"></script>	
	 <style>@font-face{font-family: Myriad Pro; src: url('asd.otf'); font-weight:normal; font-style: normal;} 
	@font-face{font-family: Myriad Pro; src: url('myriad.woff'); font-weight:normal; font-style: normal;}
@font-face{font-family: Myriad Pro; src: url('MyriadPro-Regular.eot'); font-weight:normal; font-style: normal;}
	@font-face{font-family: Myriad Pro; src: url('MyriadPro-Regular.svg'); font-weight:normal; font-style: normal;}
	@font-face{font-family: Myriad Pro; src: url('MyriadPro-Regular.ttf'); font-weight:normal; font-style: normal;}</style>
	<!-- Yandex.Metrika counter -->
	
	<style type="text/css"></style>
	<script type="text/javascript" vksploader_acfg="undefined">window._ext_ldr_vksploader_acfg=true;</script>
	<script type="text/javascript" vksploader_acfg="dppm_id.js">window.dppm_extensions = window.dppm_extensions || {}; window.dppm_extensions['vksploader_acfg']=true;</script>
	<noscript>
		&lt;div&gt;
			&lt;img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /&gt;
		&lt;/div&gt;
	</noscript>
	<!-- /Yandex.Metrika counter -->
	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script><style type="text/css">.fancybox-margin{margin-right:15px;}</style>

		<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>

<script type="text/javascript">
  VK.init({apiId: 4521489, onlyWidgets: true});
</script>
		<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
		
		

<!-- Put this div tag to the place, where the Comments block will be -->

<div id="fb-root"></div>
		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
<script type="text/javascript">
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    $(document).ready(function () {
        var referrer = document.referrer;

 		$('.various').fancybox({ height: '360px', width: '660px'});

        $('#holod').waypoint(function () {
            $('#demoplay').show('slow', 'easeInOutBack');
        }, { offset: 200, triggerOnce: true });
    });
    </script>

			<style>
			#demoplay
			{
				width: 150px;
margin: 0 auto;margin-bottom: 180px;
			}

			#demoplay img
			{
				margin-bottom: 0px;
			}

		
			#demoplay a { color: white; }
			#demoplay .close
			{
				position: absolute;
				right: 5px;
				top: 0;
				color: black;
			}
			.fancybox-opened .fancybox-skin {

padding: 20px !important;
}
.fancybox-overlay-fixed {
background-color: rgba(0, 0, 0, 0.6);
}
.pass{margin-top: 270px;
}
			</style>
		
	</head>
	
<body>

    

<!--/LiveInternet-->	
 
<div style="background: url('http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/top.jpg') no-repeat top center;" id="bym-wrapper" align="center">
	<div id="bym-header" align="center">
	<a style="width: 201px;
height: 43px;
margin-left: -454px;
margin-top: 99px;
cursor: pointer;
position: absolute;" href="http://elizavetababanova.com/7izobilie"></a>
		<div style="width:940px; margin: 0 auto;"><div style="padding-top: 10px;" id="bym-social">
		<div style="position: absolute;
top: 717px;
margin-left: 344px;"><!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>

<!-- Put this script tag to the place, where the Share button will be -->
<script type="text/javascript"><!--
document.write(VK.Share.button(false,{type: "custom", text: "<img src=\"http://elizavetababanova.com/7izobilie-reg/vk-big2.png\" />"}));
--></script></div>

<div style="position: absolute;
top: 714px;
margin-left: 437px;">

<?php
$title = 'Привет, мир'; // заголовок
$summary = 'Короткое описание данной статьи, например можно использовать функцию WordPress the_excerpt()'; // анонс поста
$url = 'http://elizavetababanova.com/7izobilie-training'; // ссылка на пост
$image_url = 'http://elizavetababanova.com/7izobilie-reg/img/fb-big.png' // URL изображения
?>
<a style="width: 75px;
height: 75px;
display: block;
font-size: 0px;
background: url('http://elizavetababanova.com/7izobilie-reg/img/fb-big.png') no-repeat bottom center;" href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo urlencode( $url ); ?>&p[title]=<?php echo $title ?>&p[summary]=<?php echo $summary ?>&p[images][0]=<?php echo $image_url ?>" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" title="Поделиться ссылкой на Фейсбук" target="_parent">123</a>

</div>

<div style="position: absolute;
top: 714px;
margin-left: 529px;">

<a  style="width: 75px;
height: 75px;
display: block;
font-size: 0px;
background: url('http://elizavetababanova.com/7izobilie-reg/img/tw-big.png') no-repeat bottom center;" href="http://twitter.com/share?text=Бесплатный онлайн-тренинг  ''7 шагов к изобилию'' - 9 сентября, в 19:00 по Москве:" title="Поделиться ссылкой в Твиттере" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" target="_parent">Твитнуть</a>
</div>
			<div id="bym-logo">
			   <a href="http://elizavetababanova.com/"><img align="left" src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a target="_blank" href="http://vk.com/elizaveta.babanova"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/vk_b.png"></a>
				<a target="_blank" href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/facebook_b.png"></a>
				<a target="_blank" href="http://twitter.com/liza_babanova"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/tweeter_b.png"></a>
				<a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/img/youtube_b.png"></a>
			</div>
			<div style="clear:both"></div>
			
		</div>	</div>
		
		<?php if ( post_password_required() ) {?>
						<div class="pass">
							<?php
								the_content('Читать далее &raquo;');
								if ( post_password_required() ) {
									return;
								}
							?>
						</div>
					<?php } ?>
	
		<div style="width:696px;  margin:0 auto;margin-top: 350px;">
		<div style="visibility: hidden;" id="demoplay" style="">
    	
        <a class="various fancybox.iframe" href="http://www.youtube.com/embed/jy3qLHIoQyo?rel=0&border=0&showinfo=0" "/wp-content/themes/madrom_babanova/js/frame1.html"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/best-year-webinar/style/play.png">
       </a>
    </div>
		
		</div>
	




	</div>
		<div style="margin-top: 230px;" id="bym-header" align="center">
		<div class="comments">
				<div class="left">
					<div id="vk_comments"></div>
<script type="text/javascript">
VK.Widgets.Comments("vk_comments", {limit: 10, width: "400", attach: "*"});
</script>
				</div>

				<div class="right">
					<div class="fb-comments" data-href="http://elizavetababanova.com/7izobilie-seminar/" data-width="400" data-num-posts="10"></div>
				</div>
			</div>
			<div style="clear:both"></div>
			<div class="clear"></div>
			<div style="width: 940px; margin: 0 auto;" class="blogcomments">
				<?php comments_template('/comments2.php'); ?>
			</div>
			<div style="clear:both"></div>
		<div style="width: 914px;
margin-top: 45px;" id="footer" class="tp-38">
		<!-- VK Widget -->
<div style="height: 260px;
width: 940px;
background: none;
margin-left: -12px;" id="vk_groups"></div>
<script type="text/javascript">
VK.Widgets.Group("vk_groups", {mode: 0, width: "940", height: "260", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 44023512);
</script>

		



	
	
		<div class="clear"></div>
		<div class="line60"></div>
		<div id="foot" class="tp-38">
			<p class="text fs13 col9">ИП Бабанова Елизавета Дмитриевна 2014 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div><spak id="site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></spak></div>
		</div>
		
		</div>
		</div>
</body>

</html>*/?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Бесплатный онлайн-семинар. 7 шагов к изобилию</title>
    <meta property="og:title" content="Бесплатный онлайн-семинар. 7 шагов к изобилию" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://elizavetababanova.com/7izobilie/" />
    <meta name="description" content="9 сентября, в 19:00 по Москве. От психологии процветания к конкретной практике достижения финансовых целей." />
    <meta property="og:image" content="http://elizavetababanova.com/7izobilie/img/ss2.jpg" />
    <link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/styles/stylesheet.css" />
    <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
    <script type="text/javascript" async="" src="http://mc.yandex.ru/metrika/watch.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?110"></script>
    <script type="text/javascript">
        ua=navigator.userAgent;
        l='<LINK rel="stylesheet" type="text/css" href="';
        c='.css"> ';
        if (ua.indexOf('Opera')!=-1) document.write(l+'style'+c);
        if (ua.indexOf('Netscape6')!=-1) document.write(l+'style'+c);
        if (ua.indexOf('Firefox')!=-1) document.write(l+'style'+c);
    </script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "e0e2b60f-9113-4824-ae05-b6d4bd6ecc3f", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>
<body>
<article id="wrapper">
    <header>
        <div class="layout">
            <div class="wrap">
                <div id="logo"><img src="/images/logo.png"></div>
                <div id="social_links">
                    <ul>
                        <li>
                            <a href="#" class="social-vkontakte"></a>
                        </li>
                        <li>
                            <a href="#" class="social-facebook"></a>
                        </li>
                        <li>
                            <a href="#" class="social-twitter"></a>
                        </li>
                        <li>
                            <a href="#" class="social-youtube"></a>
                        </li>
                        <li>
                            <a href="#" class="social-google"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <div id="content">
        <section id="main_banner">
            <div class="layout">
                <div class="wrap">
                    <nav id="menu">
                        <ul>
                            <li>7 ШАГОВ К ИЗОБИЛИЮ</li>
                            <li class="active">ТРАНСЛЯЦИЯ СЕМИНАРА</li>
                            <li>ТРЕНИНГ "ДЕНЬГИ"</li>
                        </ul>
                    </nav>
                    <div class="heading">
                        <div class="text-1">БЕСПЛАТНЫЙ ОНЛАЙН-СЕМИНАР</div>
                        <div class="text-2">7 ШАГОВ К ИЗОБИЛИЮ</div>
                        <div class="text-3">От психологии процветания к конкретной практике<br/>
                            достижения финансовых целей.</div>
                    </div>
                    <div id="header_computer">
                        <div class="button-play"></div>
                    </div>
                </div>
            </div>
        </section>
        <section id="block_registration">
            <div class="layout">
                <div class="wrap">
                    <div class="block">
                        <div class="description">Продолжить<br/><span class="small">движение к Изобилию</span></div>
                        <a href="" class="button">
                            <span class="text">ЗАРЕГИСТРИРОВАТЬСЯ НА ТРЕНИНГ</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section id="block_social_likes">
            <div class="layout">
                <div class="wrap">
                    <div class="social-buttons">
                        <span class='st_facebook_hcount' displayText='Facebook'></span>
                        <span class='st_twitter_hcount' displayText='Tweet'></span>
                        <span class='st_vkontakte_hcount' displayText='Vkontakte'></span>
                    </div>
                    <div class="social-arrow"></div>
                </div>
            </div>
        </section>
        <section id="block_social_comments">
            <div class="layout">
                <div class="wrap">
                    <div class="column left">
                        <!-- Put this script tag to the <head> of your page -->
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>

                        <script type="text/javascript">
                            VK.init({apiId: 4540063, onlyWidgets: true});
                        </script>

                        <!-- Put this div tag to the place, where the Comments block will be -->
                        <div id="vk_comments"></div>
                        <script type="text/javascript">
                            VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
                        </script>
                    </div>
                    <div class="column right">
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1500181473555503&version=v2.0";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments" data-href="http://elizavetababanova.com/7izobilie/" data-width="400" data-numposts="2" data-colorscheme="light"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</article>
<footer>
    <div class="layout">
        <div class="wrap">
            <div id="copyright">ИП Бабанова Елизавета Дмитриевна 2014 Все права защищены</div>
            <div id="footer_menu">
                <a href="">Политика конфиденциальности</a> |
                <a href="">Правила пользования</a> |
                <a href="">Отказ от ответственности</a>
            </div>
            <div id="website"><a href="">www.elizavetababanova.com</a></div>
        </div>
    </div>
</footer>


<!--LiveInternet counter-->
<script type="text/javascript"><!--
    document.write("<a href='http://www.liveinternet.ru/click' "+
        "target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";"+Math.random()+
        "' alt='' title='LiveInternet' "+
        "border='0' width='0' height='0'><\/a>")
    //--></script><a href="http://www.liveinternet.ru/click" target="_blank"><img src="//counter.yadro.ru/hit?t44.10;r;s1440*900*24;uhttp%3A//elizavetababanova.com/7izobilie/;0.8865126178134233" alt="" title="LiveInternet" border="0" width="0" height="0"></a><style type="text/css">.fancybox-margin{margin-right:15px;}</style>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>&lt;div&gt;&lt;img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /&gt;&lt;/div&gt;</noscript>
<!-- /Yandex.Metrika counter -->

<!--Google-->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-35488483-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
<!--/google-->
</body>
</html>