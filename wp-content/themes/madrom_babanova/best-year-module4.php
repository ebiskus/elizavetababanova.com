<?php
/*
Template Name: Лучший год Модуль 4
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/module.css">

	<title>Эмоции</title>

	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/share04.jpg"> 
	<meta property="og:title" content="Эмоции."/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear-module4"/>
	<meta name="description" content="Годовая программа «Лучший год Вашей жизни». Весна в разгаре. Природа начинает просыпаться, и вместе с ней возникает необходимость в приливе положительной энергии.">
<style type="text/css">#bym-header {
max-width: 1600px;
width: 100%;
background: url('http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/bym-space2.jpg') no-repeat bottom center;
height: 500px;
display: inline-block;
text-align: center;
}</style>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div>
			<img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script>
	<!--/LiveInternet-->	
		
<script src="/wp-content/themes/madrom_babanova/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/wp-content/themes/madrom_babanova/js/jquery.easing.1.3.js" type="text/javascript"></script> 
<link rel="stylesheet" href="/wp-content/themes/madrom_babanova/js/jquery.fancybox.css" type="text/css" media="screen">
<script type="text/javascript" src="/wp-content/themes/madrom_babanova/js/jquery.fancybox.pack.js"></script> 
<script src="/wp-content/themes/madrom_babanova/js/waypoints.min.js"></script>	

<script type="text/javascript">
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    $(document).ready(function () {
        var referrer = document.referrer;

 		$('.various').fancybox({ height: '423px', width: '699px'});

        $('#holod').waypoint(function () {
            $('#demoplay').show('slow', 'easeInOutBack');
        }, { offset: 200, triggerOnce: true });
    });
    </script>

<style>
#demoplay
{
	position:fixed; 
	right: 0; 
	top: 114px; 
	width: 150px; 
    padding: 10px;
	background-color: #00ccff;
	text-align: center;
	line-height: 1.2em;
	color: white;
	z-index: 10;
	
	border-bottom-left-radius: 10px;
 	-moz-border-radius-bottomleft: 10px;
	-webkit-border-bottom-left-radius: 10px;
	border-top-left-radius: 10px;
 	-moz-border-radius-topleft: 10px;
	-webkit-border-top-left-radius: 10px;
	 -moz-box-shadow: 0 0 10px #3B3B3B; /* Для Firefox */
    -webkit-box-shadow: 0 0 10px #3B3B3B; /* Для Safari и Chrome */
    box-shadow: 0 0 10px #3B3B3B; /* Параметры тени */
}

#demoplay img
{
	margin-bottom: 0px;
}

#demoplay img:hover
{
	width: 100px;
	margin-bottom: -4px;
}
#demoplay a { color: white; }
#demoplay .close
{
	position: absolute;
	right: 5px;
	top: 0;
	color: black;
}
</style>
	
</head>

<body>
<div id="demoplay">
    	<a class="close" onclick="$(&#39;#demoplay&#39;).hide();" title="Закрыть окошко">x</a>
    	Фрагмент из модуля «Эмоции. Как с помощью счастья прийти к успеху и процветанию?»
        <a class="various fancybox.iframe" href="http://www.youtube.com/embed/8qXqQbuWVDI?rel=0" "/wp-content/themes/madrom_babanova/js/frame1.html"><img src="/wp-content/themes/madrom_babanova/js/play.png">
        просмотреть</a>
    </div>	
	
    <div id="bym-wrapper" align="center">
		<div id="bym-social">
			<div id="bym-logo">
			   <a href="/"><img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/vk_b.gif"></a>
				<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/facebook_b.gif"></a>
				<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tweeter_b.gif"></a>
				<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/youtube_b.gif"></a>
			</div>
		<div style="clear:both"></div>

		</div>
		<div id="bym-header" align="center">
			<div class="bym-all_year">
				<div class="bym-mounth"><p style="color: #ffcc66;" class="bym-digit">04</p><p style="color: #ffcc66; class="bym-name">АПРЕЛЬ</p></div>
				<div class="bym-description"><img style="visibility:hidden;" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/title4.png">
					<p style="color:white;padding-top: 44px;" class="bym-text">
					Весна в разгаре. Природа начинает просыпаться, и вместе с ней возникает необходимость в приливе положительной энергии. В этом модуле вы освоите искусство поддержания мотивации и хорошего расположения духа при любых обстоятельствах. 
					</p>
					</p>
				</div>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/04.png">
			</div>
			<div style="clear:both"></div>

		</div>
		<div id="bym-content-m9">
			<div id="bym-mon">
				<div id="bym-mon1-img" style="padding-top:86px;"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m4mon1.jpg" ></div>
				<div id="bym-mon1">
				<ul>
					<li>Под влиянием чего изменяется ваш мозг?  </li>
					<li>Как закрепить положительные изменения навсегда?</li>
					<li>Какая часть мозга развита у счастливых людей больше, чем у несчастных? Как ее развить вам?</li>
					<li>Гедонистическая адаптация – можно ли ее избежать и как?</li>
					<li>Линия Лосады – модель для улучшения как бизнеса, так и отношений.</li>
<li>Как использовать ограничение своего мозга себе во благо? </li>
					<li>Депрессивный ген – если он есть у вас, то как его отключить? </li>
<li>Сила воли в установлении позитивных привычек не помогает. А что помогает?</li>
					<li>Феномен «наученной беспомощности». Выявите у себя наличие этой установки и узнайте, как ее преодолеть.</li>
					<li>Как тратить деньги, чтобы это приносило счастье?</li>
					<li>Что еще более важно, чем вера в свои способности?</li>
					<li>Эффект Пигмалиона – что это и как вы с его помощью можете улучшить поведение других людей?</li>
					<li>Кризис как каталист роста – что умеют люди, которые используют кризис себе во благо, и как нам овладеть этой способностью?</li>

				</ul>
				</div>
				
				<div id="bym-mon2-m9-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m4mon2.png"></div>
				<div id="bym-mon2-m9" >
				<a style="color: #cc33ff;
font-size: 48px;
line-height: 18px;text-decoration: none;" href="http://www.elizavetababanova.com/emotions-upgrade/">Апгрейд Эмоций  </a>
				<p>
					дополнительная рекомендуемая <br>21-дневная программа для повышения
					эмоционального фона и закрепления материала
				</p>
				</div>

			</div>
		</div>
		<div style="clear:both"></div>
		<div id="bym-reg">
			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>
			<div style="clear:both"></div>

		</div>
		<div style="clear:both"></div>

		<div class="whatsay">
			<div><img id="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
			<div>Что говорят участники четвертого модуля</div>
			<div style="padding-left:9px"><img id="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
		</div>
		<div style="clear:both"></div>


		
		<div id="bym-comment">	


		
		
					<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/2014/05/Elena-Stepanova2.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Модуль очень насыщенный!»</p>
					<p>Модуль очень насыщенный! Получила огромный заряд счастья, вдохновения, энергии и позитива. Спасибо, что делитесь результатами передовых исследований, открытиями, достижениями, прекрасными жизненными историями! Чувствуется Ваша глубокая работа над материалом.</p>
					<p class="autor">Елена Степанова | Москва, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>




	
			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/irina-holod.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Модуль потрясающий!»</p>
					<p>Модуль потрясающий! Он стал возможностью, с одной стороны, оценить еще раз то, что у меня уже есть, не забывать об этом в повседневной рутине, с другой стороны – стимулом найти и раскрыть потенциал, увеличить уровень своего счастья в тех сферах жизни, где я не использую его полностью. И еще раз убедилась в верности выражения «Хочешь быть счастливым – будь им!»</p>
					<p class="autor">Ирина Холод | Ялта, Украина</p>
				</div>
			</div>


		

			<div style="clear:both"></div>




			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>
			
			<div class="comment">

				<div>
					<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/olga-votinova.png">
				</div>
				<div class="comment_text">
					<p class="comment_title">«Это самая потрясающая инвестиция!»</p>
					<p>Так классно, что есть Елизавета и ее Команда! Я только что закончила этот модуль, получила массу позитива, а самое главное - очередную порцию знаний, с которой уже начала работать. «Лучший год» — это самая потрясающая инвестиция, которую я когда-либо делала, потому что она полностью изменила мою жизнь и задала совершенно новый вектор движения. ОГЦ сработала, дух захватывает. Меня даже немножко выбило из графика, я попала в водоворот событий, интересностей и вкусностей. Елизавета, огромное спасибо за те безграничные просторы, которые благодаря Вам начали передо мной открываться, за новое видение, за другое измерение!</p>
					<p class="autor">Ольга Вотинова | Пермь, Россия</p>
				</div>
			</div>


		

			<div style="clear:both"></div>

		<div id="bym-footer">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/aboutfoto.png">
			<div class="bym-about">
			   <div class="bym-about_name">Елизавета Бабанова</div>
			   <div class="bym-about_autor">автор семинара</div>
			   <div class="bym-about_desc">
					<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
				   <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
				   <p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
				   <p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
				  <p>Член Международной Ассоциации Непрерывного Образования.</p>
			   </div>
			</div>
			<div class="bym-vk">
			<iframe id="vkwidget1" height="350" frameborder="0" width="200" scrolling="no" style="overflow: hidden; height: 350px;" src="//vk.com/widget_community.php?app=0&width=200px&gid=44023512&mode=0&height=350&url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&13b1da20c61" name="fXD32f6b"></iframe>
			</div>
		</div>


		<div class="bym-footer_info">
			<div class="bym-copy">ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены</div>
			<nav class="bym-bottom_menu">
			<a href="/privacy-policy.html">Политика конфиденциальности</a> | <a href="/terms.html">Правила пользования</a> | <a href="/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div class="bym-site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></div>
		</div>


	</div>
</body>