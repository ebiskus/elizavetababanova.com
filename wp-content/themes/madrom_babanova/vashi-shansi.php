<?php
/*
Template Name: Страница Ваши шансы
*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta property="fb:admins" content="100003273128680"/>

    <title>Первый год в цифрах</title>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/vashi-shansi/style.css" rel="stylesheet">

    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/jquery.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/plugins.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/custom.js"></script>

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
    <script type="text/javascript">VK.init({apiId:  2461907, onlyWidgets: true});</script>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>

    <meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/vashi-shansi/images/pic-share-1.png">
    <meta property="og:title" content="Каковы шансы?"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://elizavetababanova.com/vashi-shansi"/>
    <meta name="description" content="Хотите доказательство того, что ваша жизнь является чудом? Вот оно!">

<meta property="og:description" content="Хотите доказательство того, что ваша жизнь является чудом? Вот оно!" />


    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

    <!--Google-->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35488483-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!--/google-->

</head>
<body>

    <div id="fb-root"></div>
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    <div id="fb-root"></div>

    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

	<?php
		require('./wp-blog-header.php');
		require_once('header.php');
		require_once('functions.php');
	?>
	
    <section class="main">
        <div class="inner">
			<a title="<?php bloginfo('name') ?>" href="<?php bloginfo('siteurl') ?>/">
				<div id="logo_top"></div>
			</a>
            <div class="image">
<map id="shansi" name="shansi">
    <area coords="120,5190,840,5231" href="http://blogs.law.harvard.edu/abinazir/2011/06/15/what-are-chances-you-would-be-born/" target="_blank" alt="Оригинал">
</map>

                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/vashi-shansi/images/sketch_960.png" usemap="#shansi">
            </div>

<div style="padding: 30px 60px 0px 60px; font-size: 15px; font-weight: bold; text-align: justify; line-height: 180%;">Если эта информация заставила вас подумать о своей жизни с другой стороны, а тем более, если она вдохновила вас на создание более интересной и насыщенной жизни, поделитесь ей в своих социальных сетях. Каждый человек должен знать, что он - Чудо. <img src=http://elizavetababanova.com/wp-includes/images/smilies/icon_smile.gif /wp-content/themes/madrom_babanova/image/smile1.png valign=center style="vertical-align: middle;"></div>

			<div id="div-social">
				<?php
					require('social_button.php');
				?> 
			</div>
        </div>
		
	    <section class="block8">
			<div class="left">
				<div id="vk_comments"></div>
				<script type="text/javascript">
					VK.Widgets.Comments("vk_comments", {limit: 5, width: "400", attach: "*"});
				</script>
			</div>
			<div class="right">
				<div class="fb-comments" data-href="http://elizavetababanova.com/vashi-shansi/" data-width="400" data-num-posts="5"></div>
			</div>
		</section>
		<div class="clr"></div>
		<section class="block8">
			<?php comments_template('/comments2.php'); ?>
		</section>
		<footer class="footer">
			<i class="border6"></i>
			<p class="text1">ИП Бабанова Елизавета Дмитриевна &nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2014 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
		</footer>		
    </section>

<div class="lint">
        <!--LiveInternet counter--><script type="text/javascript"><!--
        document.write("<a href='http://www.liveinternet.ru/click' "+
        "target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";"+Math.random()+
        "' alt='' title='LiveInternet' "+
        "border='0' width='0' height='0'><\/a>")
        //--></script><!--/LiveInternet-->
    </div>


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter18188044 = new Ya.Metrika({id:18188044,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>

    <noscript>
        <div>
            <img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

</body>
</html>