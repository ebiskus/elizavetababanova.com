<?php
/*
Template Name: Лучший год Модуль 1
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/module.css">

	<title>Воздушный старт. Система запуска.</title>

	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/share01.jpg"> 
	<meta property="og:title" content="Воздушный старт. Система запуска."/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear-module1"/>
	<meta name="description" content="Годовая программа «Лучший год Вашей жизни». 1 модуль начнется с четкого определения, что вы хотите достичь в этом году.">

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div>
			<img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script>
	<!--/LiveInternet-->	
	
	
	
	
	
	
	
<script src="/wp-content/themes/madrom_babanova/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/wp-content/themes/madrom_babanova/js/jquery.easing.1.3.js" type="text/javascript"></script> 
<link rel="stylesheet" href="/wp-content/themes/madrom_babanova/js/jquery.fancybox.css" type="text/css" media="screen">
<script type="text/javascript" src="/wp-content/themes/madrom_babanova/js/jquery.fancybox.pack.js"></script> 
<script src="/wp-content/themes/madrom_babanova/js/waypoints.min.js"></script>	

<script type="text/javascript">
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    $(document).ready(function () {
        var referrer = document.referrer;

 		$('.various').fancybox({ height: '443px', width: '550px'});

        $('#holod').waypoint(function () {
            $('#demoplay').show('slow', 'easeInOutBack');
        }, { offset: 200, triggerOnce: true });
    });
    </script>

<style>
#demoplay
{
	position:fixed; 
	right: 0; 
	top: 114px; 
	width: 150px; 
    padding: 10px;
	background-color: #00ccff;
	text-align: center;
	line-height: 1.2em;
	color: white;
	z-index: 10;
	
	border-bottom-left-radius: 10px;
 	-moz-border-radius-bottomleft: 10px;
	-webkit-border-bottom-left-radius: 10px;
	border-top-left-radius: 10px;
 	-moz-border-radius-topleft: 10px;
	-webkit-border-top-left-radius: 10px;
	 -moz-box-shadow: 0 0 10px #3B3B3B; /* Для Firefox */
    -webkit-box-shadow: 0 0 10px #3B3B3B; /* Для Safari и Chrome */
    box-shadow: 0 0 10px #3B3B3B; /* Параметры тени */
}

#demoplay img
{
	margin-bottom: 0px;
}

#demoplay img:hover
{
	width: 100px;
	margin-bottom: -4px;
}
#demoplay a { color: white; }
#demoplay .close
{
	position: absolute;
	right: 5px;
	top: 0;
	color: black;
}
</style>
	
	
	
	
	
	
	
</head>

<body>




    <div id="demoplay" style="display:none;">
    	<a class="close" onclick="$(&#39;#demoplay&#39;).hide();" title="Закрыть окошко">x</a>
    	Фрагмент из модуля «Воздушный Старт. Система Запуска»
        <a class="various fancybox.iframe" href="http://www.youtube.com/embed/eyNnUA6INCc?rel=0" "/wp-content/themes/madrom_babanova/js/frame1.html"><img src="/wp-content/themes/madrom_babanova/js/play.png">
        просмотреть</a>
    </div>
	
	
	

    <div id="bym-wrapper" align="center">
		<div id="bym-social">
			<div id="bym-logo">
			   <a href="/"><img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/vk_b.gif"></a>
				<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/facebook_b.gif"></a>
				<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tweeter_b.gif"></a>
				<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/youtube_b.gif"></a>
			</div>
		<div style="clear:both" id="holod"></div>

		</div>
		<div id="bym-header" align="center">
			<div class="bym-all_year">
				<div class="bym-mounth"><p class="bym-digit">01</p><p class="bym-name">ЯНВАРЬ</p></div>
				<div class="bym-description"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/title1.png">
					<p class="bym-text">
					1 модуль начнется с четкого определения, что вы хотите достичь в этом году. Вы получите отлаженную систему "запуска полета” плюс необходимые "ускорители". В космической индустрии появился новый способ запуска корабля в космос - не с Земли, а с воздуха, что позволяет сократить ресурсы и усилия на самом ответственном начальном этапе пути. Мы будем действовать аналогично. В этом модуле вы получите инструменты, которые не заберут 80% ваших усилий и ресурсов во время запуска вашей ракеты в космос. Вместо этого вы овладеете самыми передовыми технологиями massive action и внедрите их в свою жизнь.
					</p>
				</div>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/01.png">
			</div>
			<div style="clear:both"></div>

		</div>
		<div id="bym-content">
			<div id="bym-mon">
				<div id="bym-mon1-img" style="padding-top:40px;"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m1mon1.png"></div>
				<div id="bym-mon1">
				Пройдя этот модуль вы:
				<ul>
					<li>Определите вашу главную профессиональную и личную цель</li>
					<li>Исходя из нее, вы освоите принцип выстраивания конгруентных планов на:
							<ul>
								<li>Год</li>
								<li>Месяц</li>
								<li>Неделю</li>
								<li>День</li>
							</ul>
					</li>
					<li>Определите, что для вас является аналогом “4х минутной мили”</li>
					<li>Разработаете стратегию по преодолению своего основного барьера или “стеклянного потолка”</li>
					<li>Узнаете, что такое “метод 365” и как использовать его для стремительного карьерного роста</li>
				</ul>
				</div>
			  
				<div id="bym-mon2">
				А также:
				<ul>
					<li>Освоите навыки суперпродуктивности, которые помогут вам делать больше за меньшее количество времени</li>
					<li>Разовьете и усилите свои профессиональные компетенции </li>
				</ul>
				</div>
				<div id="bym-mon2-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m1mon2.jpg"></div>
<!--
				<div style="clear:both"></div>
				<div class="price-top">$150</div>
-->				
			</div>
		</div>
		<div style="clear:both"></div>
		<div id="bym-reg">
			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>
		</div>

		<div style="clear:both"></div>
		<div class="whatsay">
			<div><img id="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
			<div>Что говорят участники первого модуля</div>
			<div><img id="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
		</div>
		<div style="clear:both"></div>
		
		<div id="bym-comment">		
			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/olga-markova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Определена стратегия преодоления «стеклянного потолка», пришло понимание, что прорыв происходит ПРЯМО СЕЙЧАС»</p>
					<p>Запуск состоялся! Ощущение ПОЛЕТА!<br />
					Основная Грандиозная Цель определена.<br />
					Составлен План действий.<br />
					Определена стратегия преодоления «стеклянного потолка», пришло понимание, что прорыв происходит ПРЯМО СЕЙЧАС!<br />
					Намечены шаги.</p>
					<p class="autor">Ольга Маркова | Москва, Россия</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>




			<div class="comment">
				<div class="no_image">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/natalia-sahnenko.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Елизавета - Вы волшебница-скульптор!»</p>
					<p>Было так здорово! Всем спасибо за совместное путешествие! Елизавета - Вы волшебница-скульптор! Вы не лепите из нас, как из безликой массы что-то свое, а как будто берете природный материал и, учитывая его красоту и уникальность, убираете что-то лишнее, грубоватое, негармоничное, и получается в итоге произведение искусства!!! Целый поток Благодарности изливается из моей Души.</p>
					<p class="autor">Наталья Сахненко | Москва, Россия</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>



			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/natalia-terehova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Это была отличная работа! Ясность, структурированность, окутанные чувственностью и душевностью»</p>
					<p>Это была отличная работа! Ясность, структурированность, окутанные чувственностью и душевностью! Спасибо за такой подарок, он «говорит» о том, что, если я буду Ему соответствовать, то Мой Год НЕИЗБЕЖНО будет ЛУЧШИМ!</p>
					<p>Любому полету предшествует подготовка, мне очень помог стартовать предновогодний семинар! Он пришел в наилучшее время и обратил меня к СЕБЕ именно тогда, когда предпраздничная суета выворачивает наизнанку. Я поняла истинную суть поста перед Великими Праздниками: обращение ВНУТРЬ, ВОПРОС, ОЖИДАНИЕ, и тогда — РОЖДЕНИЕ. Как часто мы сами убиваем это, отдаваясь на растерзание внешнему. И тогда остается только ВНЕШНИЙ праздник (мыльный пузырь, фантик), который быстро обнаруживает свою пустоту.</p>
					<p class="autor">Наталья Терехова</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>

			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/irina-podborskaya.jpg">
				</div>
				<div class="comment_text">
					<p class="comment_title">«В карьере, благодаря Вам, я сделала огромный прорыв»</p>
					<p>Елизавета, я этот год была тесно с Вами, спасибо огромное за вдохновение. В карьере, благодаря Вам, я сделала огромный прорыв, я на вершине с точки зрения амбиций, но не могла понять чего мне не хватает, чтобы быть счастливой женщиной. А сейчас случился прорыв, я даже плачу, но это слезы счастья, я поняла, что мне нужен «зайка», я хочу ребеночка, я к этому готова как финансово, так и профессионально. Мне было страшно много лет, что уйду в декрет и все, ради чего я работала, будет зря. А теперь понимаю, что РЕБЕНОК это моя великая цель. Елизавета, если бы не Вы, я бы, наверное, не созрела.</p>
					<p class="autor">Ирина Подборская</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>

			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/olga-votinova.png" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Уже первый модуль этой программы впечатлил меня своим размахом и насыщенностью»</p>
					<p>Лиза, хочу поблагодарить вас за старт.</p>
					<p>Меня покорило ваше умение интенсивно работать и выкладываться по полной.</p>
					<p>Меня покорило то, как вы работаете с группой, насколько грамотно выстраиваете общение с ней, насколько профессионально разбираетесь в теме и как подаете информацию, насколько легко оперируете сложными понятиями. О многих из них я даже и не слышала.</p>
					<p>Когда слушала трансляцию, в очередной раз убедилась, что пришла  учиться к человеку, который действительно знает, что говорит и говорит по делу.</p>
					<p>Хочу также сказать, что я была на Апгрейде Здоровья, была на Апгрейде Эффективности, но «Лучший год Вашей Жизни» - действительно великолепный курс. Уже первый модуль этой программы впечатлил меня своим размахом и насыщенностью. И уже первое занятие по программе дало мне понять насколько все серьезно и насколько мелко я плавала, и насколько интересно там, куда я попала. Подобное ощущение у меня возникало, когда я впервые увидела море (до того были только речки и пруды), когда почувствовала исходящую от него мощь и как у меня захватило дух. УХ.</p>
					<p>Здорово!<br />
					Спасибо еще раз.<br />
					Апгрейды, как оказалось, стали подготовительным этапом. Они меня закалили и подготовили к восхождению.</p>
					<p class="autor">Ольга Вотинова | Пермь, Россия</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>

			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/ekaterina-strahova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«План понятен, вектор развития поставлен»</p>
					<p>Спасибо за первый урок! Всё чётко и ясно. План понятен, вектор развития поставлен.</p>
					<p class="autor">Екатерина Страхова | Швеция</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>

			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/natalia-poddubnaya.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Вы и Ваша команда, как всегда, даете больше, чем ожидают»</p>
					<p>Вы и Ваша команда как всегда даете больше, чем ожидают!</p>
					<p>Сегодня я многое узнала и прочистила свой мозг, а также обозначила внутренний эмоциональный подъем. Верю, что получится на целый год удержать мотивацию и наконец-то пробить свой стеклянный потолок с таким-то наставником, как Вы.</p>
					<p class="autor">Наталья Поддубная | Донецк, Украина</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>

			<div class="comment">
				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/svetlana-chetveruhina.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Я одна из тех сомневающихся, которая хотела, но раздумывала. А сейчас, после прохождения первого модуля, я очень рада, что приняла верное решение и стала участником программы "Лучшей год Моей жизни"»</p>
					<p>Я одна из тех сомневающихся, которая хотела, но раздумывала. А сейчас, после прохождения первого модуля, я очень рада, что приняла верное решение и стала участником программы «Лучшей год Моей жизни». Я прекрасно «вижу», как вместе с Вами и командой единомышленников я пробью свой «стеклянный потолок», совершу ПРОРЫВ в своей сфере. С радостью буду достигать своих грандиозных  целей. Буду развиваться и развивать, наполняться и отдавать, вдохновляться и вдохновлять.</p>
					<p class="autor">Светлана Четверухина | Новосибирск, Россия</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>

			<div class="comment">
				<div class="no_image">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/elizaveta-kalinkina.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Ваша потрясающая программа «Лучший год Ваше жизни» уже начинает работать, хотя на дворе только январь»</p>
					<p>Елизавета! Ваша потрясающая программа «Лучший год Вашей жизни» уже начинает работать, хотя на дворе только январь! Спасибо Вам за невероятное вдохновение и мощнейшую энергетику, которая передается даже по скайпу! Вы делаете потрясающее волшебство - своими методами, подкрепленными научными обоснованиями и проверенными на себе, вы помогаете раскопать в себе тайное желание - ту реализацию, которой обычно очень хочешь, но очень боишься. Вы создали точно выверенную систему, которая дает какие-то мистические результаты - с каждым словом погружаюсь в знания и в себя, а потом где-то щелкает и все встает на свои места, само по полочкам раскладывается. Остается брать и делать.</p>
					<p>Ваши слова «Мне не все равно» сразу убедили меня, что это именно то, что мне нужно в этом году чтобы взлететь на новые вершины.</p>
					<p>Благодарю Вас за то, что Вы и ваша команда делаете с такой любовью и на таком высочайшем уровне!</p>
					<p class="autor">Елизавета Калинкина | Тихвин, Россия</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>

			<div class="comment">
				<div class="no_image">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tatiana-blache.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Получила огромный заряд энергии и, что самое главное, - это желание преуспеть в деле своей жизни»</p>
					<p>Приняла участие в первом модуле программы «Воздушный старт. Система запуска», было так здорово. Получила массу положительных эмоций, огромный заряд энергии и, что самое главное, - желание преуспеть в деле своей жизни.</p>
					<p class="autor">Tatiana Blache | Гап, Франция</p>
				</div>
			</div>
			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="clear:both"></div>


			<div class="comment">
				<div class="no_image"></div>
				<div class="comment_text">
					<p>Спасибо, Лиза, за сегодняшний урок! Особеннно за глубину проникновения, за содержание и нарушение регламента (вместо 1.5 часов - почти 4 часа!). Спасибо.</p>
					<p class="autor">Марат</p>
				</div>
			</div>
		</div>
		
		<div style="clear:both"></div>
		<div id="bym-reg-bot">
			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>
			<div style="clear:both"></div>
		</div>

		<div id="bym-footer">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/aboutfoto.png">
			<div class="bym-about">
			   <div class="bym-about_name">Елизавета Бабанова</div>
			   <div class="bym-about_autor">автор семинара</div>
			   <div class="bym-about_desc">
					<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
				   <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
				   <p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
				   <p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
				  <p>Член Международной Ассоциации Непрерывного Образования.</p>
			   </div>
			</div>
			<div class="bym-vk">
				<iframe id="vkwidget1" height="350" frameborder="0" width="200" scrolling="no" style="overflow: hidden; height: 350px;" src="//vk.com/widget_community.php?app=0&width=200px&gid=44023512&mode=0&height=350&url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&13b1da20c61" name="fXD32f6b"></iframe>
			</div>
		</div>


		<div class="bym-footer_info">
			<div class="bym-copy">ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены</div>
			<nav class="bym-bottom_menu">
			<a href="/privacy-policy.html">Политика конфиденциальности</a> | <a href="/terms.html">Правила пользования</a> | <a href="/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div class="bym-site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></div>
		</div>


	</div>
</body>