<?php
/*
Template Name: Страница 11mistakes-3
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Как устроиться на работу своей мечты?</title>
<meta property="fb:admins" content="100003273128680"/>  
<title></title>
<script>
function doPopup(name){
	if ($(name).is(':visible')){
		$(name).hide('fast');
	}
	else {
		$("#first").hide();
		$(name).slideToggle(800);
	}
}
</script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
<link href="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/css/style.css" rel="stylesheet">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/js/jquery.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/js/plugins.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/js/custom.js"></script>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?80"></script>
<meta property="og:title" content="Как устроиться на работу своей мечты?"/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="http://elizavetababanova.com/dream-job" />
<meta property="fb:admins" content="100003273128680"/> 
<meta property="og:description" content="Онлайн-курс, который приведет вас из пункта А - места, где вы сейчас находитесь, в пункт В - туда, где вас ждет работа вашей мечты." />
<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/11mistakes/11mistakes-3/img/image1_fb.jpg" />
<script type="text/javascript">
 VK.init({apiId:  2461907, onlyWidgets: true});
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js">
</script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-2/js/scrollto.js" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="utf-8"></script>
<link rel="image_src" href="http://elizavetababanova.com/wp-content/themes/madrom_babanova/11mistakes/11mistakes-1/img/photo.jpg" />
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250572091654130";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="main">
	<header class="header">
		<div class="inner">
			<a class="logo" href="http://elizavetababanova.com/" target="_blank">
				<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/b1.png" alt=".">
			</a>
			<nav class="menu">
				<a target="_blank" href="http://vk.com/elizaveta.babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/b2.png" alt=".">
				</a>
				<a target="_blank" href="https://www.facebook.com/pages/%D0%95%D0%BB%D0%B8%D0%B7%D0%B0%D0%B2%D0%B5%D1%82%D0%B0-%D0%91%D0%B0%D0%B1%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0/418192508271995">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/b3.png" alt=".">
				</a>
				<a target="_blank" href="http://twitter.com/liza_babanova">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/b4.png" alt=".">
				</a>
				<a target="_blank" href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide">
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/b5.png" alt=".">
				</a>
			</nav>
		</div>
		<i class="border"></i>
	</header>
	<div class="inner">
		<nav class="menu2">
			<a class="i i1 tworow " href="http://elizavetababanova.com/11-mistakes/">11 грандиозных ошибок, совершаемых<br> при устройстве на работу</a>
			<a class="i i2" href="http://elizavetababanova.com/7shagov-work-dream">7 шагов к работе вашей мечты</a>
			<a class="i i3 active" href="#">Программа курса</a>
			</nav>
		<section class="block">
			<div class="i i1">
				<p class="title"><span class="custom">П</span>рограмма курса</p>
				
			</div>
		</section>
		<section class="block2">
			<a href="#button_1"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/zayavka.jpg" style="position: absolute;left: 650px;top: 450px;"></a>

		<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/image1.jpg">
		<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/image4.jpg">
			<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/image2.jpg" style="margin-left: -40px;">
			
<section class="block5" id="button"><a name="button" target="_blank" href="http://ecommtools.com/buy/elizavetababanova/dreamwork"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/button1.jpg"></a><a target="_blank" href="http://elizavetababanova.com/form2/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/button2.jpg"></a></section>
			
<div style="position: absolute;top:5800px;left:10px;"><a name="button_1"></a></div>






<div class="about" style="min-height:560px;">
<!--<p class="cblue avangardec text_tr fs36 otz_tit" style="padding-left: 20px;padding-bottom: 20px;">Об авторе отчета</p>-->
<div style="float:left; width:840px;">
<!-- VK Widget -->
<div style="height: 400px; float:right; width: 200px; background: none repeat scroll 0% 0% transparent; margin-top:40px;margin-left:20px;" id="vk_groups"><iframe style="overflow: hidden; height: 400px;" id="vkwidget1" src="//vk.com/widget_community.php?app=0&amp;width=200px&amp;gid=44023512&amp;mode=0&amp;height=400&amp;url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&amp;13b1da20c61" name="fXD32f6b" frameborder="0" height="200" scrolling="no" width="200"></iframe></div>

<img src="http://elizavetababanova.com/11mistakes/img/elizaveta.png">
<h3 class="text_tr ptsans sin fs30" style="line-height:1.1;">Елизавета Бабанова</h3>
<p>На свою первую работу я устроилась, когда мне было 14 лет. Я работала на городском телеканале, создавая радио- и телевизионные передачи для молодежи.</p>

<p>В 15 лет я выиграла конкурс на годовое обучение в США. В условия этого конкурса входило прохождение группового и индивидуального интервью.</p>

<p>Во время учебы я работала на разных работах: преподавала математику в начальной школе, а также работала в библиотеке и в компьютерной лаборатории. Затем я устроилась в качестве оплачиваемого практиканта в инвестиционную фирму. После окончания вуза я прошла 3 интервью в 3 хедж фонда (хедж фонд - самый элитный тип инвестиционной компании, минимальная инвестиция в который составляет $1,000,000). Все 3 компании сделали мне предложение, и в итоге я сама делала между ними выбор.</p>

<p>Мне всегда удавалось устроиться на желаемую работу. У меня был 100% рейтинг успеха. И я никогда не задумывалась, почему это было так, просто предполагая, что у меня есть какое-то отличительное свойство, которое всегда обеспечивало мне получение желаемой должности.</p>

<p>Но только когда я открыла свой бизнес на русскоязычном пространстве, я увидела огромное количество ошибок, которые мои потенциальные сотрудники совершали при попытке попасть в мою команду.</p>
<p>Спустя 3 года постоянного поиска звезд, я обнаружила, что причина моего успешного устройства на работу заключается в глубоком интуитивном понимании, что, как, и почему нужно делать при трудоустройстве. И именно это давало мне достижение 100% результата.</p>
<p>Но со временем мое интуитивное понимание переросло в систему. Теперь, будучи уже по ту сторону приема на работу, я могу точно сказать, что вам нужно делать, чтобы, во-первых, найти работу вашей мечты, и, во-вторых, как на нее устроиться.</p>
<p>Отчет «11 грандиозных ошибок, совершаемых при устройстве на работу», который родился в результате построения моей системы, поможет вам обнаружить их в своем поиске работы. </p>
<p>Исправив эти ошибки, вы сможете смоделировать мой опыт. А значит, и устроиться на любое место, которое вы выберете для себя в качестве желаемого трудоустройства.</p>
</div>

<i class="clear_auto"></i>
</div>

<p class="avangardec text_tr fs36 otz_tit" style="color:#1aa6e8;">Отзывы</p>
<div class="otz" style="min-height:100px;">
<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/otzivi/3.jpg">
<p class="fs24">Наталья Третьякова</p>
<p class="fs16">Новороссийск, Россия</p>
<p>Елизавета! Спасибо! У вас потрясающий талант коротко излагать основные идеи, ведущие к глобальному успеху. Благодарю за ваше вдохновляющее влияние и высокие стандарты в обучении.</p>
<i class="clear_auto"></i></div>

<div class="otz">
<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/otzivi/13.jpg" style="margin-bottom: 50px;">
<p class="fs24">Алексей Греков</p>
<p class="fs16">Санкт-Петербург, Россия</p>
<p>Это мой первый опыт в области тренингов по личностному росту и саморазвитию. Мне понравилось, курс разжег интерес во мне к этой теме. Елизавета, конечно, пленяет своей энергией и внутренней силой. Чувствуется большая, очень системная работа, проделанная ею. Еще я чувствовал ее любовь и желание помочь. Спасибо!</p>
<i class="clear_auto"></i></div>

<div class="more_otz" id="but_mr"><a href="javascript:doPopup('#first')"><img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/5stages/5stages-6/img/more-otz.png"></a></div>

<div id="first" style="display: none;">

<div class="otz" style="min-height:100px;margin-top:20px;">
<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/otzivi/Irina_lapshova.jpg">
<p class="fs24">Ирина Лапшова</p>
<p class="fs16">Москва, Россия</p>
<p>Меня покорили достигнутые Вами результаты, а также безграничная преданность делу, которому вы служите!</p>
<i class="clear_auto"></i></div>

<div class="otz" style="min-height:100px;">
<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/otzivi/alex-lichik.jpg" style="margin-bottom: 50px;">
<p class="fs24">Александр Личик</p>
<p class="fs16">Волковыск, Беларусь</p>
<p>Уважаемая Елизавета, искренне радуюсь и благодарю вас за то, что делитесь своими конструктивными мыслями и идеями. В ваших постах есть душа, когда читаешь, такое ощущение, что в голове звучит ваш голос, наверное так должно и происходить с работами мастеров. Спасибо за то, что даете людям возможность расти и развиваться, учиться и постигать.</p>
<i class="clear_auto"></i></div>

<div class="otz" style="min-height:100px;">
<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/otzivi/kasyk.jpg" style="margin-bottom: 50px;">
<p class="fs24">Ирина Касюк</p>
<p class="fs16">Находка, Россия</p>
<p>Спасибо, Елизавета! Спасибо за позитив!! Огромная энергия от любой Вашей информации, статей, видео! Зашла на Ваш сайт и поняла, что это то, что я искала!!! Я резко изменила свое представление о бизнесе, теперь я знаю, что Я сама все СМОГУ!!! Мне нужна, как воздух, Ваша поддержка, Ваши подсказки, статьи, видео!!! Я уже очень многое применила из того, что Вы дали мне и надеюсь еще много нового узнаю! У меня огромное желание учиться дальше!</p>
<i class="clear_auto"></i></div>

<div class="otz" style="min-height:100px;">
<img src="http://elizavetababanova.com/3mistakes/img/nik-bark.jpg" style="margin-bottom: 100px;">
<p class="fs24">Николай  Барковский</p>
<p class="fs16">Новосибирск, Россия</p>
<p>Искренне БлагоДарю! Информация, что вы даете очень ценная, заряженная Энергией, высочайшего качества, Живая!!! Для себя открыл:
<br>
1) Учиться у Лучших!<br>
2) Мыслить созидательно!<br>
3) Говорить позитивно!<br>
4) В проблеме искать семя успеха!<br>
5) Действовать!</p>
<i class="clear_auto"></i></div>

<div class="otz" style="min-height:100px;">
<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/otzivi/4.jpg">
<p class="fs24">Екатерина Шагина</p>
<p>Спасибо, Елизавета за то, что дарите людям новую жизнь!!!</p>
<i class="clear_auto"></i></div>







<div class="otz">
<img src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/image/otzivi/9.jpg" style="margin-bottom: 50px;">
<p class="fs24">Игорь Калитвенцев</p>
<p class="fs16">Киев, Украина</p>
<p>Елизавета, благодарю за большое сердце и стремление менять жизнь других к лучшему, возвращать либо укреплять веру в нас, и за бесценный опыт, дающий нам прекрасный инструментарий для личностных трансформаций!</p>
<i class="clear_auto"></i></div>



<div class="otz" style="min-height:100px;">
<p class="fs24">Ольга, Куликовка</p>
<p class="fs16">Ульяновск, Россия</p>
<p>Я каждый день говорю спасибо, что я встретила вас, Елизавета. Вы не представляете, какое влияние вы оказываете на жизнь других людей. </p>
<i class="clear_auto"></i></div>





<div class="otz" style="min-height:100px;">
<p class="fs24">Любовь Кулагина</p>
<p class="fs16">Оренбург, Россия</p>
<p>Всем здравствуйте! Всем огромное спасибо! Елизавете - как тренеру и наставнику, взявшему меня за руку и ведущему по пути к жизни, наполненной смыслом и постоянным ощущением счастья! Команде звезд Елизаветы - за поддержку и неусыпный контроль за прочностью того канатика, которым связана моя лодочка, прибившаяся к вашему величайшему кораблю! </p>
<i class="clear_auto"></i></div>

<div class="otz" style="min-height:100px;">
<p class="fs24">Людмила Кучанская</p>
<p class="fs16">Днепропетровск, Украина</p>
<p>Очень Благодарна за эту полезную информацию. Вроде все знала, но по крупицам. Елизавета, Вы собрали все воедино, разложили по полочкам, приподнесли очень понятным доступным языком. Вы отдаете от души, от сердца, руководствуясь своим опытом - это очень важно. Для меня это как глоток чистого воздуха. Еще раз огромнейшее спасибо.</p>
<i class="clear_auto"></i></div>



<div class="otz" style="min-height:100px;">
<p class="fs24">Татьяна Исакова</p>
<p>Спасибо Елизавета! В ваших материалах содержится дух успеха человека, получившего реальные результаты в жизни. Поэтому ваши статьи вдохновляют. Я уже, как мне кажется, нахожусь в круге своего успеха, потому что шла к этому долго и настойчиво. Пока результат еще мал, но я чувствую его ценность. Ваши статьи как будто приносят аромат цветов с высокогорных лугов, аромат мечты. И каждый раз от этого мысли светлеют. Спасибо вам за ценную систему!</p>
<i class="clear_auto"></i></div>


<div class="otz" style="min-height:100px;">
<p class="fs24">Родион Христов</p>
<p>Елизавета, человеку который действительно решил измениться в лучшую сторону и начать жить счастливой жизнью сегодня, здесь и сейчас — лучших Ваших реальных рекомендайций о механизме достижения счастливой жизни не найти. Спасибо. Сегодня я принял решение жить по Вашим рекомендациям и начать жить по новому!</p>
<i class="clear_auto"></i></div>

<div class="otz" style="min-height:100px;">
<p class="fs24">Анастасия Шкребец</p>
<p class="fs16">Волгодонск, Россия</p>
<p>Елизавета, какая Вы умница! Большущее спасибо за технику улучшения жизни! Я участвовала в тренинге, читаю Ваши статьи, переслушиваю уроки, веду ТСР, с 6 декабря (как запланировала) начала massive action, запустила мой бизнес. Готовлюсь к следующему прорыву. Финансы стали выравниваться, вес стал снижаться, гораздо больше успеваю, хотя включила в свой график и 15 минут английского, и бассейн, и тренинги по ораторству, и 10 минут уборки дома, и медитацию 10 — 40 минут, и даже зарядку! Откуда время взялось?!! Откуда энергии столько, что вечером себя просто укладываю спать?! И диву даюсь! Как я раньше всего этого не понимала, не осознавала, как я раньше жила вообще?! Полная голова большого количества знаний, а вот такой стройной и всепоглощающей системы развития себя, своей жизни я еще не встречала! Мне кажется Вам нужно ее запатентовать! И подумать об ученой степени. Я серьезно. Успехов, здоровья, сил и любви Вам — все то, что с Вашей помощью просто лавиной врывается в мою жизнь.</p>
<i class="clear_auto"></i></div>






		</section>
		<br><p>&nbsp:</p><br>
		<section class="block5"><a href="http://ecommtools.com/buy/elizavetababanova/dreamwork"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/button1.jpg"></a><a href="http://elizavetababanova.com/form2/"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/button2.jpg"></a></section>
		<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/11mistakes/11mistakes-3/img/image3.jpg">
			
		<footer class="footer">
			<i class="border6"></i>
			<p class="text">ИП Бабанова Елизавета Дмитриевна&nbsp;&nbsp;&nbsp;ИНН 370259937869&nbsp;&nbsp;&nbsp;2013 Все права защищены</p>
			<nav class="menu3">
				<a href="http://elizavetababanova.com/privacy-policy.html">Политика конфиденциальности</a> |
				<a href="http://elizavetababanova.com/terms.html">Правила пользования</a> |
				<a href="http://elizavetababanova.com/disclaimer.html">Отказ от ответственности</a>
			</nav>
		</footer>
	</div>
</section>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 985392624;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/985392624/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>