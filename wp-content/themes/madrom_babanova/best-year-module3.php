<?php
/*
Template Name: Лучший год Модуль 3
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/module.css">

	<title>Вечный Двигатель</title>

	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/share03.jpg"> 
	<meta property="og:title" content="Вечный Двигатель."/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear-module3"/>
	<meta name="description" content="Годовая программа «Лучший год Вашей жизни». Успешные люди отличаются тем, что они умеют правильно распределять свои усилия и ресурсы, чтобы не выгорать.">

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div>
			<img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script>
	<!--/LiveInternet-->	
	
	
	
	
	
	
	
	
	
	

	
<script src="/wp-content/themes/madrom_babanova/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/wp-content/themes/madrom_babanova/js/jquery.easing.1.3.js" type="text/javascript"></script> 
<link rel="stylesheet" href="/wp-content/themes/madrom_babanova/js/jquery.fancybox.css" type="text/css" media="screen">
<script type="text/javascript" src="/wp-content/themes/madrom_babanova/js/jquery.fancybox.pack.js"></script> 
<script src="/wp-content/themes/madrom_babanova/js/waypoints.min.js"></script>	

<script type="text/javascript">
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    $(document).ready(function () {
        var referrer = document.referrer;

 		$('.various').fancybox({ height: '443px', width: '550px'});

        $('#holod').waypoint(function () {
            $('#demoplay').show('slow', 'easeInOutBack');
        }, { offset: 200, triggerOnce: true });
    });
    </script>

<style>
#demoplay
{
	position:fixed; 
	right: 0; 
	top: 114px; 
	width: 150px; 
    padding: 10px;
	background-color: #00ccff;
	text-align: center;
	line-height: 1.2em;
	color: white;
	z-index: 10;
	
	border-bottom-left-radius: 10px;
 	-moz-border-radius-bottomleft: 10px;
	-webkit-border-bottom-left-radius: 10px;
	border-top-left-radius: 10px;
 	-moz-border-radius-topleft: 10px;
	-webkit-border-top-left-radius: 10px;
	 -moz-box-shadow: 0 0 10px #3B3B3B; /* Для Firefox */
    -webkit-box-shadow: 0 0 10px #3B3B3B; /* Для Safari и Chrome */
    box-shadow: 0 0 10px #3B3B3B; /* Параметры тени */
}

#demoplay img
{
	margin-bottom: 0px;
}

#demoplay img:hover
{
	width: 100px;
	margin-bottom: -4px;
}
#demoplay a { color: white; }
#demoplay .close
{
	position: absolute;
	right: 5px;
	top: 0;
	color: black;
}
</style>
	
	
	
	
	
	
	
</head>

<body>




    <div id="demoplay" style="display:none;">
    	<a class="close" onclick="$(&#39;#demoplay&#39;).hide();" title="Закрыть окошко">x</a>
    	Фрагмент из модуля «Вечный двигатель»
        <a class="various fancybox.iframe" href="http://www.youtube.com/embed/7igFeszGa6o?rel=0" "/wp-content/themes/madrom_babanova/js/frame1.html"><img src="/wp-content/themes/madrom_babanova/js/play.png">
        просмотреть</a>
    </div>	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    <div id="bym-wrapper" align="center">
		<div id="bym-social">
			<div id="bym-logo">
			   <a href="/"><img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/vk_b.gif"></a>
				<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/facebook_b.gif"></a>
				<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tweeter_b.gif"></a>
				<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/youtube_b.gif"></a>
			</div>
		<div style="clear:both" id="holod"></div>

		</div>
		<div id="bym-header" align="center">
			<div class="bym-all_year">
				<div class="bym-mounth"><p class="bym-digit">03</p><p class="bym-name">МАРТ</p></div>
				<div class="bym-description"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/title3.png">
					<p class="bym-text">
					Успешные люди отличаются тем, что они умеют правильно распределять свои усилия и ресурсы, чтобы не выгорать. В этом модуле вы получите формулу оптимального поддержания себя в тонусе и узнаете, как заточить ее под свои индивидуальные потребности. Так как весна - идеальное время для обновления энергии, вы научитесь использовать природные механизмы для максимального наполнения и сумеете наладить работу своего "вечного двигателя". 
					</p>
					</p>
				</div>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/03.png">
			</div>
			<div style="clear:both"></div>

		</div>
		<div id="bym-content">
			<div id="bym-mon">
				<div id="bym-mon1-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m3mon1.jpg" ></div>
				<div id="bym-mon1">
				<ul>
					<li>Распорядок дня для оптимального самочувствия</li>
					<li>Как проснуться «успешным»?
							<ul>
								<li>Утренняя практика</li>
								<li>8 стратегий для идеального начала дня </li>
								<li>2 пути оптимизации первых нескольких часов после пробуждения для запаса энергии на весь день</li>
							</ul>
					</li>
					<li>20 рекомендаций для энергетического обновления в течение дня</li>
					<li>Что делать вечером, чтобы хорошо высыпаться?
							<ul>
								<li>Вечерняя практика</li>
							</ul>
					</li>
					<li>Главные принципы здорового питания для оптимального здоровья
							<ul>
								<li>Проверенное сотнями лет применения руководство по сопоставимости продуктов, следуя которому вы избавитесь от проблем с пищеварением и обнулением энергии во второй половие дня</li>
							</ul>
					</li>
				</ul>
				</div>
			  
				<div id="bym-mon2" style="padding-top:200px;">
				<ul>
					<li>Главные принципы здорового движения для оптимального здоровья
							<ul>
								<li>Как подобрать для себя идеальный тип тренировки, чтобы она не изматывала, а давала вам сил </li>
							</ul>
					</li>

					<li>Что является основным для вашего энергетического состояния (подсказка: это не связано с телом, едой и спортом)</li>
					<li>5 альтернатив кофе и другим энергетическим допингам</li>
				</ul>
				</div>
				<div id="bym-mon2-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m3mon2.jpg"></div>
			</div>
		</div>
		
		
		
		
		

		
		

		<div style="clear:both"></div>




		<div id="bym-reg">

			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>


			<div style="clear:both"></div>

		</div>






		<div style="clear:both"></div>

		<div class="whatsay">
			<div><img id="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
			<div>Что говорят участники третьего модуля</div>
			<div><img id="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
		</div>
		<div style="clear:both"></div>


		
		<div id="bym-comment">	


		
		
					<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/elena-sabirova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Информация очень актуальна и важна»</p>
					<p>Информация модуля очень актуальна и важна! Я работаю врачом в сфере  антивозрастной медицины, изучаю и  нетрадиционные подходы к  оздоровлению, сталкиваюсь с невежеством и псевдокомпетентностью людей в вопросах здорового образа жизни. Поэтому, Ваш целостный и доступный обзор способов улучшить здоровье, меня очень порадовал! Спасибо!</p>
					<p class="autor">Елена Сабирова | Иваново, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>




	
			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/irina-holod.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Хочется внедрить недостающие пункты в свою жизнь»</p>
					<p>Модуль "Вечный двигатель" вдохновил, структурировал ту информацию, которая уже была в голове или когда-то встречалась. Хочется внедрить поскорее недостающие пункты в свою жизнь! Новой для меня оказалась информация о крестоцветных овощах, о том, что воду лучше употреблять теплой и о том, что еда гораздо сильнее влияет на уровень стресса в организме, чем окружающая среда. Еще раз спасибо!</p>
					<p class="autor">Ирина Холод | Ялта, Украина</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>




	
			<div class="comment">

				<div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/ingrid-drujinina.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">«Это невероятное озарение: приходит понимание, принятие, внутреннее спокойствие»</p>
					<p>Некоторые методы модулей 1 и 2 стали просто открытием для меня: сознательный оптимизм, правило Парето и другие вещи, в которых я так нуждалась! ""Апгрейд Эффективности"" — это просто находка. Плодотворная работа именно в начале дня — снова открытие, и так много других потрясающе классных работающих результативных вещей! У меня есть ребенок, но я успеваю сделать то, что мне нужно для развития. Отношения с мужем стали налаживаться после некоторых очень важных предложений второго модуля.
Я убедилась, что для внедрения информации, нужно около месяца, и это объяснило мне причины многих неудач. Я в восторге. :)</p>
					<p class="autor">Ингрид Дружинина | Хабаровск, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>








		<div id="bym-reg-bot">

			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>


			<div style="clear:both"></div>

		</div>

		
		
		
		
		
		
		
		

		<div id="bym-footer">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/aboutfoto.png">
			<div class="bym-about">
			   <div class="bym-about_name">Елизавета Бабанова</div>
			   <div class="bym-about_autor">автор семинара</div>
			   <div class="bym-about_desc">
					<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
				   <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
				   <p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
				   <p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
				  <p>Член Международной Ассоциации Непрерывного Образования.</p>
			   </div>
			</div>
			<div class="bym-vk">
			<iframe id="vkwidget1" height="350" frameborder="0" width="200" scrolling="no" style="overflow: hidden; height: 350px;" src="//vk.com/widget_community.php?app=0&width=200px&gid=44023512&mode=0&height=350&url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&13b1da20c61" name="fXD32f6b"></iframe>
			</div>
		</div>


		<div class="bym-footer_info">
			<div class="bym-copy">ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены</div>
			<nav class="bym-bottom_menu">
			<a href="/privacy-policy.html">Политика конфиденциальности</a> | <a href="/terms.html">Правила пользования</a> | <a href="/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div class="bym-site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></div>
		</div>


	</div>
</body>