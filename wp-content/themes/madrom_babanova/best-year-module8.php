<?php
/*
Template Name: Лучший год Модуль 8
*/
?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/module.css">

	<title>Ваше Созвездие</title>

	<meta property="og:image" content="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/share08.jpg"> 
	<meta property="og:title" content="Ваше Созвездие."/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="http://elizavetababanova.com/bestyear-module8"/>
	<meta name="description" content="Годовая программа «Лучший год Вашей жизни». Август - время собирать плоды. В этом месяце мы сфокусируемся на улучшении отношений и формировании продвинутого социального круга.">

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter18188044 = new Ya.Metrika({
						id : 18188044,
						webvisor : true,
						clickmap : true,
						trackLinks : true,
						accurateTrackBounce : true
					});
				} catch(e) {
				}
			});

			var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function() {
				n.parentNode.insertBefore(s, n);
			};
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div>
			<img src="//mc.yandex.ru/watch/18188044" style="position:absolute; left:-9999px;" alt="" />
		</div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<!--Google-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35488483-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!--/google-->

	<!--LiveInternet counter-->
	<script type="text/javascript"><!--
		document.write("<a href='http://www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.10;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='0' height='0'><\/a>")
	//--></script>
	<!--/LiveInternet-->	
</head>

<body>
    <div id="bym-wrapper" align="center">
		<div id="bym-social">
			<div id="bym-logo">
			   <a href="/"><img align="left" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/logo.png"></a>
			</div>
			<div id="bym-buttons">
				<a href="http://vk.com/elizaveta.babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/vk_b.gif"></a>
				<a href="https://www.facebook.com/pages/%d0%95%d0%bb%d0%b8%d0%b7%d0%b0%d0%b2%d0%b5%d1%82%d0%b0-%d0%91%d0%b0%d0%b1%d0%b0%d0%bd%d0%be%d0%b2%d0%b0/418192508271995"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/facebook_b.gif"></a>
				<a href="http://twitter.com/liza_babanova"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/tweeter_b.gif"></a>
				<a href="http://www.youtube.com/channel/UCRjycmpoI5UFwvThJ4ujYeA?feature=guide"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/youtube_b.gif"></a>
			</div>
		<div style="clear:both"></div>

		</div>
		<div id="bym-header" align="center">
			<div class="bym-all_year">
				<div class="bym-mounth"><p class="bym-digit">07</p><p class="bym-name">ИЮЛЬ</p></div>
				<div class="bym-description"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/title8.png">
					<p class="bym-text">
					В этом месяце мы сфокусируемся на улучшении отношений и формировании продвинутого социального круга. Вы узнаете, как окружить себя достойными людьми, способными составить вам компанию в вашем полете. Пройдя этот модуль, вы сможете 1) укрепить отношения с близкими людьми и 2) сделать апгрейд своего окружения, создав свое собственное "созвездие".
</p>
					</p>
				</div>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/08.png">
			</div>
			<div style="clear:both"></div>

		</div>
		<div id="bym-content">
			<div id="bym-mon">
				<div id="bym-mon1-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m8mon1.jpg" ></div>
				<div id="bym-mon1">
				<ul>
					<li>Определяем, кого вы хотите иметь в своем Созвездии</li>
					<li>4 компонента вашего Созвездия
							<ul>
								<li>Профессиональный круг</li>
								<li>Друзья и знакомые</li>
								<li>Онлайн-контакты </li>
								<li>Контакты через клубы по интересам </li>
							</ul>
					</li>
					<li>Как заинтересовать людей в том, чтобы они стали частью вашего Созвездия?</li>
					<li>Networking для экстравертов
							<ul>
								<li>Стратегии для людей, от природы умеющих завязывать и налаживать связи</li>
							</ul>
					</li>
					<li>Networking для интровертов
							<ul>
								<li>Стратегии для людей, которые от природы стеснительны</li>
							</ul>
					</li>
				</ul>
				</div>
			  
				<div id="bym-mon2" style="padding-top:200px;">
				<ul>
					<li>Фундаментальные правила Networking для всех</li>
					
					<li>Разрушение стереотипов в сфере расширения связей
							<ul>
								<li>3 секретных тактики построения профессиональных отношений </li>
							</ul>
					</li>

					<li>Как взаимодействовать с Созвездием и использовать свою Network, чтобы о вас не складывалось ощущение «юзера»?</li>
					<li>Как самому стать человеком, которого другие люди мечтают иметь в своем Созвездии?</li>
				</ul>
				</div>
				<div id="bym-mon2-img"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/m8mon2.jpg"></div>
			</div>
		</div>
		<div style="clear:both"></div>
		<div id="bym-reg">
			<div class="pic"><a href="http://elizavetababanova.com/form18/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regmodule-1.png"></a></div>
			<div class="pic"><a href="http://elizavetababanova.com/form16/"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/regprog.png"></a></div>
			<div style="clear:both"></div>

		</div>

		<div class="whatsay">
			<div><img id="quo1" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_left.png"></div>
			<div>Что говорят участники седьмого модуля</div>
			<div style="padding-left:9px"><img id="quo2" src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/best-year-selling/images/quotes_right.png"></div>
		</div>
		<div style="clear:both"></div>


		
		<div id="bym-comment">	


		
		
					<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/kolomina.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Очень полезный модуль"</p>
					<p>Очень полезный модуль, ведь без личностных контактов, общения, социума человек не сможет существовать! И сегодня Елизавета показала, как это важно, дала ценные рекомендации участникам о том, как налаживать контакты, взаимодействовать с людьми! </p>
					<p class="autor">Юлия Коломина | Москва, Россия</p>
				</div>
			</div>


			<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

			<div style="clear:both"></div>




	
			<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/2014/06/IMG_20140531_011013.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Интересно и познавательно!"</p>
					<p>Спасибо за вебинар! Все, как всегда, интересно, познавательно и ново!</p>
					<p class="autor">АЙЗАТ КАЛЧАЕВА | БИШКЕК, КЫРГЫЗСТАН</p>
				</div>
			</div>


		<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>


			<div style="clear:both"></div>
			
			
			<div class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/uploads/ishutkina.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Изумительный модуль!"</p>
					<p>"Это изумительный модуль о налаживании контактов! Благодарю за практические примеры писем, за открытость и собственные примеры из жизни! Информация познавательна и поучительна."</p>
					<p class="autor">Анна Ишуткина | Белово, Россия</p>
				</div>
			</div>
			<div style="clear:both"></div>
<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>
			<div style="margin-top:20px;" class="comment">

				<div>
					<img style="width: 130px;
border-radius: 86px; padding-left:0px;
margin-left: 130px;" src="http://elizavetababanova.com/wp-content/themes/madrom_babanova/bestyear/modules/images/manzhosova.jpg" >
				</div>
				<div class="comment_text">
					<p class="comment_title">"Благодарю за вебинар!"</p>
					<p>"Благодарю за вебинар! Внутри  огромное желание двигаться вперед, много позитивной энергии! Отличный настрой для внедрения декларации в жизнь! Постараюсь пронести его  и не "расплескать" по дороге!"</p>
					<p class="autor">Елена Манжосова | Россия
				</div>
			</div>

<div class="line"><img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/banner_line.png"></div>

		

			<div style="clear:both"></div>
		
		<div id="bym-footer">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/bestyear/modules/images/aboutfoto.png">
			<div class="bym-about">
			   <div class="bym-about_name">Елизавета Бабанова</div>
			   <div class="bym-about_autor">автор семинара</div>
			   <div class="bym-about_desc">
					<p>В 15 лет уехала в Америку, окончила 2 вуза в России и США по четырём специальностям: Экономика, Финансы, Маркетинг, Менеджмент.</p>
				   <p>Сделала успешную карьеру в самой конкурентной и высокооплачиваемой области инвестиционной индустрии США.</p>
				   <p>Основатель и совладелец двух успешных бизнесов в области саморазвития и бизнес-обучения.</p>
				   <p>С 18 лет занимается личностным развитием. Разработала Систему Сферического Развития, обеспечивающую систематизированный подход к достижению высоких результатов.</p>
				  <p>Член Международной Ассоциации Непрерывного Образования.</p>
			   </div>
			</div>
			<div class="bym-vk">
			<iframe id="vkwidget1" height="350" frameborder="0" width="200" scrolling="no" style="overflow: hidden; height: 350px;" src="//vk.com/widget_community.php?app=0&width=200px&gid=44023512&mode=0&height=350&url=http%3A%2F%2Felizavetababanova.com%2Fzhivi-po-polnoj%2F&13b1da20c61" name="fXD32f6b"></iframe>
			</div>
		</div>


		<div class="bym-footer_info">
			<div class="bym-copy">ИП Бабанова Елизавета Дмитриевна 2013 Все права защищены</div>
			<nav class="bym-bottom_menu">
			<a href="/privacy-policy.html">Политика конфиденциальности</a> | <a href="/terms.html">Правила пользования</a> | <a href="/disclaimer.html">Отказ от ответственности</a>
			</nav>
			<div class="bym-site"><a href="http://www.elizavetababanova.com">www.elizavetababanova.com</a></div>
		</div>


	</div>
</body>